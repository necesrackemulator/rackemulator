﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NECES.CanDriver
{
    public delegate void MessageReceivedEventHandler(object sender, MessageReceivedEventArgs args);
    public class MessageReceivedEventArgs : EventArgs
    {
        public CanMessage Message { get; }
        public MessageReceivedEventArgs(CanMessage message)
        {
            Message = message;
        }
    }
}
