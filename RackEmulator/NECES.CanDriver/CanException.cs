﻿using System;
using System.Runtime.Serialization;

namespace NECES.CanDriver
{
    [Serializable]
    internal class CanException : Exception
    {
        public CanException()
        {
        }

        public CanException(string message) : base(message)
        {
        }

        public CanException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected CanException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}