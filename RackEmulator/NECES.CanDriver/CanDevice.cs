﻿// (C) NECES All Rights Reserved.
// CanDevice.cs, authored by: Andrew Schade
// Created: 06-20-2016; Last Updated: 06-29-2016 @ 3:46 PM
using System;
using System.Threading.Tasks;

namespace NECES.CanDriver
{
    public abstract class CanDevice
    {
        public event MessageReceivedEventHandler MessageReceived;
        public enum BaudRate
        {
            Baud0Kb = 0,
            Baud125Kb = 125,
            Baud250Kb = 250,
            Baud500Kb = 500,
            Baud1000Kb = 1000
        }

        public enum CanState
        {
            Off,
            Initialized,
            Open,
            Blocked
        }

        protected static object StaticLockDevice = new object();
        private int _canChannel;

        protected object LockDevice = new object();
        protected int PortHandle;

        public string Name { get; protected set; }

        protected virtual BaudRate Baud { get; set; } = BaudRate.Baud250Kb;

        public int CanChannel
        {
            get { return _canChannel; }
            set
            {
                if (value == 0 || value == 1)
                {
                    _canChannel = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException(nameof(value),
                        "CanChannel must be exactly the value of a CAN channel address. Valid values are 0 (CAN1) and 1 (CAN2)");
                }
            }
        }

        protected int PhysicalChannels { get; set; } = 2;

        protected CanState State { get; set; } = CanState.Off;
        public abstract void CloseCan();
        public abstract bool InitializeCan();

        public abstract void OpenCan();
        public abstract CanMessage ReadCanMessage(int msgId = -1);
        public abstract void SetReceiveCallback(Func<CanMessage, Task> messageReceivedCallback);

        public abstract void ShutdownCan();

        public event CanWarnEventHandler WarnEvent;

        public abstract void WriteCanMessage(CanMessage message);

        protected virtual void OnWarnEvent(CanWarnEventArgs e)
        {
            WarnEvent?.Invoke(this, e);
        }

        ~CanDevice()
        {
            ShutdownCan();
        }

        protected virtual void OnMessageReceived(MessageReceivedEventArgs args)
        {
            MessageReceived?.Invoke(this, args);
        }
    }
}