﻿namespace NECES.CanDriver
{
    public class CanMessage
    {
        public CanMessage(int msgId, byte[] bytes)
        {
            MsgId = msgId;
            Bytes = bytes;
        }

        public CanMessage Copy()
        {
            return new CanMessage(MsgId, Bytes);
        }

        public int MsgId { get; set; }
        public byte[] Bytes { get; set; }
    }
}