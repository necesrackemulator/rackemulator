﻿namespace NECES.CanDriver
{
    public class CanReturnArgs
    {
        private int canChannel;

        public CanReturnArgs(int canChannel)
        {
            this.canChannel = canChannel;
        }

        public CanReturnArgs()
        {
        }
    }
}