﻿// (C) NECES All Rights Reserved.
// CanDriver.cs, authored by: Andrew Schade
// Created: 06-20-2016; Last Updated: 07-08-2016 @ 10:54 AM
using System.Threading;
using System.Threading.Tasks;

namespace NECES.CanDriver
{
    /// <summary>
    ///     Provides access to the CAN Drivers. Allows for usage of either CANalyzer or Kvaser CAN devices,
    ///     the ability to pen and close the drivers, as well as listen for messages.
    /// </summary>
    public class CanDriver
    {
        /// <summary>
        ///     Contains settings to specify which CAN device is in use.
        /// </summary>
        public enum DeviceType
        {
            Kvaser,
            Canalyzer,
            Null
        }

        public Task MessageSender;
        private DeviceType _deviceType;

        /// <summary>
        ///     Provides the ability to cancel the listener thread and stop listening on the CAN bus.
        /// </summary>
        public CancellationTokenSource CancellationToken { get; set; }

        /// <summary>
        ///     This is a reference to the active CAN Device driver. This property allows for the sending and receiving of
        ///     messages, as well as initializing and
        ///     closing the CAN channels.
        /// </summary>
        public CanDevice CanDevice { get; private set; }

        /// <summary>
        ///     This is the polling delay--it sets the latency where the listener will wait so many milliseconds before starting
        ///     again.
        /// </summary>
        public int Delay { get; set; } = 20; // milliseconds

        /// <summary>
        ///     This allows the user to specify the Device type using the <see cref="DeviceType" /> Enum.
        /// </summary>
        public DeviceType Device
        {
            get { return _deviceType; }
            set
            {
                _deviceType = value;
                switch (Device)
                {
                    case DeviceType.Canalyzer:
                        CanDevice = new CanAlyzerCanDevice();
//                        CanDevice.InitializeCan();
                        break;
                    case DeviceType.Kvaser:
                        CanDevice = new KvaserCanDevice();
//                        CanDevice.InitializeCan();
                        break;
                    default:
                        CanDevice = new NullCanDevice();
//                        CanDevice.InitializeCan();
                        break;
                }
            }
        }

        /// <summary>
        ///     Stops listening for CAN Messages and closes the CAN channel.
        /// </summary>
        public void CanClose()
        {
            StopListen();
            Listener.Wait();
            CanDevice.CloseCan();
        }

        private Task Listener { get; set; }
        public bool ListeningActive { get; private set; }

        /// <summary>
        ///     Opens the CAN channel on the Device and starts listening for CAN Messages.
        /// </summary>
        public void CanOpen()
        {
            CanDevice.OpenCan();
            ListeningActive = true;
        }

        /// <summary>
        ///     Stops listening for CAN Messages.
        /// </summary>
        public void StopListen()
        {
            ListeningActive = false;
        }
    }
}