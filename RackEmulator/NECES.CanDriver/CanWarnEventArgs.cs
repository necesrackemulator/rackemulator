﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NECES.CanDriver
{
    public delegate void CanWarnEventHandler(object sender, CanWarnEventArgs e);
    public class CanWarnEventArgs : EventArgs
    {
        public string Message { get; }

        public CanWarnEventArgs(string message)
        {
            this.Message = message;
        }

        public CanWarnEventArgs(string message, WarnType warnType)
        {
            Message = message;
            Type = warnType;
        }

        public WarnType Type { get; set; }

        public enum WarnType
        {
            Debug, Warn, Error
        }
    }
}
