﻿// (C) NECES All Rights Reserved.
// CanAlyzerCanDevice.cs, authored by: Andrew Schade
// Created: 06-20-2016; Last Updated: 07-28-2016 @ 2:03 PM


using System;
using System.Threading.Tasks;
using NECES.Utils;
using SQA_utils;
using vxlapi_NET;

namespace NECES.CanDriver
{
    /// <summary>
    ///     TODO: Implement IDisposable
    ///     Provides access to the Drivers for the CANAlyzer CAN Device Drivers.
    ///     Currently using Vector XL API for .NET version 9
    /// </summary>
    public class CanAlyzerCanDevice : CanDevice
    {
        private readonly XLDefine.XL_BusTypes _busType = XLDefine.XL_BusTypes.XL_BUS_TYPE_CAN;
        private readonly XLDriver _canCaseDriver = new XLDriver();
        private ulong _accessMask;

        private bool _callBackSet;
        private XLClass.xl_driver_config _driverConfig = new XLClass.xl_driver_config();

        private int _eventHandle;
        private uint _hwChannel;
        private uint _hwIndex;
        private XLDefine.XL_HardwareType _hwType = XLDefine.XL_HardwareType.XL_HWTYPE_VIRTUAL;

        private int _transmit;

        public override void CloseCan()
        {
            var status = _canCaseDriver.XL_DeactivateChannel(PortHandle, _accessMask);
            if (status != XLDefine.XL_Status.XL_SUCCESS)
            {
                OnWarnEvent(new CanWarnEventArgs($"Canalyzer deactivate channel failed: canStatus = {status}",CanWarnEventArgs.WarnType.Error));
            }
            status = _canCaseDriver.XL_ClosePort(PortHandle);
            if (status != XLDefine.XL_Status.XL_SUCCESS)
            {
                OnWarnEvent(new CanWarnEventArgs($"Canalyzer close port failed: canStatus = {status}",CanWarnEventArgs.WarnType.Error));
            }
        }

        public override bool InitializeCan()
        {
            // Load CAN Driver
            OnWarnEvent(new CanWarnEventArgs("Loading Canalyzer Driver", CanWarnEventArgs.WarnType.Debug));
            var xlStatus = _canCaseDriver.XL_OpenDriver();
            if (xlStatus != XLDefine.XL_Status.XL_SUCCESS)
            {
                OnWarnEvent(new CanWarnEventArgs($"Canalyzer Open Driver Failed: canStatus = {xlStatus}",CanWarnEventArgs.WarnType.Error)); // On Fail
                return false;
            }
            xlStatus = _canCaseDriver.XL_GetDriverConfig(ref _driverConfig);
            if (xlStatus != XLDefine.XL_Status.XL_SUCCESS)
            {
                OnWarnEvent(new CanWarnEventArgs($"Canalyzer Driver Config Failed: canStatus = {xlStatus}",CanWarnEventArgs.WarnType.Error));
                return false;
            }
            OnWarnEvent(new CanWarnEventArgs(
                $"DLL Version: {_canCaseDriver.VersionToString(_driverConfig.dllVersion)}",
                CanWarnEventArgs.WarnType.Debug));
            var channelCount = (int) _driverConfig.channelCount;
            OnWarnEvent(new CanWarnEventArgs($"Number of CAN channels = {channelCount}", CanWarnEventArgs.WarnType.Debug));
            if (channelCount == 2)
            {
                PhysicalChannels = 0;
                OnWarnEvent(new CanWarnEventArgs("No physical CAN channels detected.", CanWarnEventArgs.WarnType.Error));
                return false;
            }
            if (channelCount == 4)
            {
                PhysicalChannels = 2;
            }

            // Display Found CAN Channels
            for (var i = 0; i < channelCount; i++)
            {
                OnWarnEvent(
                    new CanWarnEventArgs(
                        $"\t[{i}] {_driverConfig.channel[i].name}\n\t- Channel Mask: {_driverConfig.channel[i].channelMask}",
                        CanWarnEventArgs.WarnType.Debug));
            }
            return true;
        }

        public override void OpenCan()
        {
            const int queueReceiveSize = 1024;
            var canalyzerBaudRate = (int) Baud*1000;
            var status = _canCaseDriver.XL_GetApplConfig(AppUtils.programName, (uint) CanChannel + 1, ref _hwType,
                ref _hwIndex, ref _hwChannel, _busType);
            if (status == XLDefine.XL_Status.XL_SUCCESS)
            {
            }
            else
            {
                // Application config not found in VCANCONF
                status = _canCaseDriver.XL_SetApplConfig(AppUtils.programName, (uint) CanChannel + 1, _hwType, _hwIndex,
                    (uint) CanChannel, _busType);
                if (status != XLDefine.XL_Status.XL_SUCCESS)
                {
                    OnWarnEvent(new CanWarnEventArgs($"Canalyzer Configuration failed. canStatus = {status}",CanWarnEventArgs.WarnType.Error));
                    return;
                }
            }
            try
            {
                _accessMask = (ulong) GetAccessMask();
            }
            catch (ArgumentOutOfRangeException e)
            {
                OnWarnEvent(new CanWarnEventArgs(e.Message,CanWarnEventArgs.WarnType.Error));
                return;
            }
            var permissionMask = _accessMask;
            status = _canCaseDriver.XL_OpenPort(ref PortHandle, AppUtils.programName, _accessMask, ref permissionMask,
                queueReceiveSize, XLDefine.XL_InterfaceVersion.XL_INTERFACE_VERSION, _busType);
            if (status != XLDefine.XL_Status.XL_SUCCESS)
            {
                OnWarnEvent(new CanWarnEventArgs($"Canalyzer Configuration failed. canStatus = {status}",CanWarnEventArgs.WarnType.Error));
                return;
            }
            _canCaseDriver.XL_CanSetChannelBitrate(PortHandle, _accessMask, (uint) canalyzerBaudRate);
            _canCaseDriver.XL_ActivateChannel(PortHandle, _accessMask, _busType,
                XLDefine.XL_AC_Flags.XL_ACTIVATE_RESET_CLOCK);
            _canCaseDriver.XL_SetNotification(PortHandle, ref _eventHandle, 1);
//            ResetRange(_accessMask);
        }

        public override CanMessage ReadCanMessage(int msgId = -1)
        {
//            lock (StaticLockDevice)
//            {
            const int maxRetryAttempts = 5;
            const int retryDelay = 1; // Milliseconds.

            var retryCount = 0;
            var received = new XLClass.xl_event();
            var queueSize = 0;

            for (; retryCount < maxRetryAttempts; retryCount++)
            {
                _canCaseDriver.XL_GetReceiveQueueLevel(PortHandle, ref queueSize);
                if (queueSize > 0)
                {
                    var status = _canCaseDriver.XL_Receive(PortHandle, ref received);
                    if (status == XLDefine.XL_Status.XL_SUCCESS)
                    {
                        if (received.tag == XLDefine.XL_EventTags.XL_RECEIVE_MSG)
                        {
                            var canMsg = received.tagData.can_Msg;
                            if (MsgFlagsOK(canMsg.flags))
                            {
                                if ((msgId < 0) || canMsg.id == msgId)
                                {
//                                        Console.WriteLine($"MessageReceived: ID: 0x{canMsg.id:X}");
                                    return new CanMessage((int) canMsg.id, canMsg.data);
//                                    _canCaseDriver.can
                                }
                            }
                            OnWarnEvent(new CanWarnEventArgs($"Canalyzer Message Flags indicate error: {canMsg.flags}",
                                CanWarnEventArgs.WarnType.Error));
                        }
                    }
//                        else if (status != XLDefine.XL_Status.XL_ERR_QUEUE_IS_EMPTY)
//                        {
//                            OnWarnEvent(new CanWarnEventArgs($"Canalzyer XL_Receive method returned error: {status}", CanWarnEventArgs.WarnType.Error));
//                        }
                }
                Task.Delay(retryDelay);
            }
//            }
            return null;
//            throw new TimeoutException("No message received");
        }

        public override void SetReceiveCallback(Func<CanMessage, Task> messageReceivedCallback)
        {
            _callBackSet = false;
            Task.Delay(100);
            _callBackSet = true;

            Task.Run(() =>
            {
                var id = 0;
                while (_callBackSet)
                {
                    var message = ReadCanMessage();
                    if (message != null)
                    {
                        messageReceivedCallback.Invoke(message);
                    }
                    id ++;
                    Task.Delay(10);
                    if (id == 34) id = 0;
                }
            });
        }


        public override void ShutdownCan()
        {
            var status = _canCaseDriver.XL_CloseDriver();
            if (status != XLDefine.XL_Status.XL_SUCCESS)
            {
                OnWarnEvent(new CanWarnEventArgs($"Canalyzer driver failed to close: canStatus = {status}",CanWarnEventArgs.WarnType.Error));
            }
        }

        public override void WriteCanMessage(CanMessage message)
        {
//            lock (StaticLockDevice)
//            {
            var msg = new XLClass.xl_event();

            msg.tagData.can_Msg.id = (uint) message.MsgId;
            msg.tagData.can_Msg.dlc = (ushort) message.Bytes.Length;
            msg.tagData.can_Msg.data = message.Bytes.Clone() as byte[];

            msg.tag = XLDefine.XL_EventTags.XL_TRANSMIT_MSG;

            _transmit++;
            if (_transmit > 10)
            {
                _canCaseDriver.XL_CanFlushTransmitQueue(PortHandle, (ulong) GetAccessMask());
                _transmit = 0;
            }

            var status = _canCaseDriver.XL_FlushReceiveQueue(PortHandle);


            if (status == XLDefine.XL_Status.XL_SUCCESS)
            {
                status = _canCaseDriver.XL_CanTransmit(PortHandle, _accessMask, msg);
                if (status != XLDefine.XL_Status.XL_SUCCESS)
                {
                    _canCaseDriver.XL_CanFlushTransmitQueue(PortHandle, (ulong) GetAccessMask());
                    WriteCanMessage(message);
                }
            }
            else
            {
                throw new CanException(
                    $"Canalyzer cannot send message, failed to flush transmit queue. canStatus = {status}");
            }
            _canCaseDriver.XL_FlushReceiveQueue(PortHandle);
//            }
        }

        /// <summary>
        ///     Adds a range of allowed message IDs. Currently not used.
        /// </summary>
        /// <param name="idRanges">Ranges of ids to allow.</param>
        private void AddRange(params IntegerRange[] idRanges)
        {
            XLDefine.XL_Status status;
            for (var i = 0; i < idRanges.Length; i++)
            {
                if (idRanges[i].Begin < 0 || idRanges[i].End < idRanges[i].Begin)
                {
                    throw new ArgumentOutOfRangeException(nameof(idRanges),
                        "Ranges must be valid ranges of positive integers");
                }
                status = _canCaseDriver.XL_CanAddAcceptanceRange(PortHandle, _accessMask, (uint) idRanges[i].Begin,
                    (uint) idRanges[i].End);
                if (status != XLDefine.XL_Status.XL_SUCCESS)
                {
                    OnWarnEvent(new CanWarnEventArgs($"Adding range failed: canStatus = {status}",CanWarnEventArgs.WarnType.Error));
                }
            }

            if ( // TODO: Explain Necessitity of this.
                (status =
                    _canCaseDriver.XL_CanResetAcceptance(PortHandle, _accessMask,
                        XLDefine.XL_AcceptanceFilter.XL_CAN_STD)) !=
                XLDefine.XL_Status.XL_SUCCESS)
            {
                OnWarnEvent(new CanWarnEventArgs($"Reset range failed: canStatus = {status}",CanWarnEventArgs.WarnType.Error));
            }
        }

        /// <summary>
        ///     Provides the access mask based on the active CAN channel.
        /// </summary>
        /// <returns>The value of the access mask to pass into the XL driver.</returns>
        private int GetAccessMask()
        {
            switch (CanChannel)
            {
                case 0:
                    return 0x5;
                case 1:
                    return 0xA;
                default:
                    throw new ArgumentOutOfRangeException(nameof(CanChannel),
                        "Can Channel value must either be 0 or 1 to indicate valid channel address.");
            }
        }

        private bool MsgFlagsOK(XLDefine.XL_MessageFlags flags)
        {
            return
                !(((flags & XLDefine.XL_MessageFlags.XL_CAN_MSG_FLAG_ERROR_FRAME) ==
                   XLDefine.XL_MessageFlags.XL_CAN_MSG_FLAG_ERROR_FRAME) ||
                  ((flags & XLDefine.XL_MessageFlags.XL_CAN_MSG_FLAG_REMOTE_FRAME) ==
                   XLDefine.XL_MessageFlags.XL_CAN_MSG_FLAG_REMOTE_FRAME) ||
                  ((flags & XLDefine.XL_MessageFlags.XL_CAN_MSG_FLAG_OVERRUN) ==
                   XLDefine.XL_MessageFlags.XL_CAN_MSG_FLAG_OVERRUN) ||
                  ((flags & XLDefine.XL_MessageFlags.XL_CAN_MSG_FLAG_NERR) ==
                   XLDefine.XL_MessageFlags.XL_CAN_MSG_FLAG_NERR));
        }

        /// <summary>
        ///     Resets the allowed range. Currently not used.
        /// </summary>
        /// <param name="permissionMask"></param>
        private void ResetRange(ulong permissionMask)
        {
            var status = _canCaseDriver.XL_CanResetAcceptance(PortHandle, permissionMask,
                XLDefine.XL_AcceptanceFilter.XL_CAN_STD);
            if (status == XLDefine.XL_Status.XL_SUCCESS)
            {
                status = _canCaseDriver.XL_CanSetChannelAcceptance(PortHandle, permissionMask, 0xfff, 0xfff,
                    XLDefine.XL_AcceptanceFilter.XL_CAN_STD);
                if (status != XLDefine.XL_Status.XL_SUCCESS)
                    OnWarnEvent(new CanWarnEventArgs($"Canalyzer set channel acceptance failed: canStatus = {status}",CanWarnEventArgs.WarnType.Error));
            }
            else OnWarnEvent(new CanWarnEventArgs($"Canalyzer reset acceptance failed: canStatus = {status}",CanWarnEventArgs.WarnType.Error));
        }
    }
}