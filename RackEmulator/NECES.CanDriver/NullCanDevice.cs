﻿// (C) NECES All Rights Reserved.
// NullCanDevice.cs, authored by: Andrew Schade
// Created: 06-20-2016; Last Updated: 06-29-2016 @ 3:45 PM
using System;
using System.Threading.Tasks;

namespace NECES.CanDriver
{
    public class NullCanDevice : CanDevice
    {
        public CanMessage TxMessage { get; private set; }

        public override void CloseCan()
        {
        }

        public override bool InitializeCan()
        {
            return true;
        }

        public override void OpenCan()
        {
            // Nothing to do here.
        }

        public override CanMessage ReadCanMessage( int msgId = -1)
        {
            throw new NotImplementedException();
        }

        public override void SetReceiveCallback(Func<CanMessage, Task> messageReceivedCallback)
        {
            throw new NotImplementedException();
        }

        public override void ShutdownCan()
        {
            CloseCan();
        }

        public override void WriteCanMessage(CanMessage message)
        {
            lock (LockDevice)
            {
                TxMessage = message.Copy();
            }
        }
    }
}