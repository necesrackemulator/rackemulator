﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using canlibCLSNET;

namespace NECES.CanDriver
{
    /// <summary>
    /// TODO: Implement IDisposable
    /// </summary>
    internal class KvaserCanDevice : CanDevice
    {
        private BaudRate _baudRate = BaudRate.Baud250Kb;
        private int _kvaserBaud = -3;

        public KvaserCanDevice()
        {
            int numChannels;

            Canlib.canInitializeLibrary();
            var canStatus = Canlib.canGetNumberOfChannels(out numChannels);

            if (canStatus == Canlib.canStatus.canOK)
            {
                // Driver loaded successfully
                GetChannelStatus(numChannels);
            }
            else
            {
                throw new Exception("Kvaser CAN DeviceType failed to load.");
            }
        }

        protected override BaudRate Baud
        {
            get { return _baudRate; }
            set
            {

                switch (_baudRate)
                {

                    case BaudRate.Baud0Kb:
                        _baudRate = value;
                        _kvaserBaud = 0;
                        break;
                    case BaudRate.Baud125Kb:
                        _baudRate = value;
                        _kvaserBaud = Canlib.BAUD_125K;
                        break;
                    case BaudRate.Baud250Kb:
                        _baudRate = value;
                        _kvaserBaud = Canlib.BAUD_250K;
                        break;
                    case BaudRate.Baud500Kb:
                        _baudRate = value;
                        _kvaserBaud = Canlib.BAUD_500K;
                        break;
                    case BaudRate.Baud1000Kb:
                        _baudRate = value;
                        _kvaserBaud = Canlib.BAUD_1M;
                        break;
                    default:
                        Baud = BaudRate.Baud500Kb;
                        OnWarnEvent(new CanWarnEventArgs($"Unknown baud rate; using 250kb",
                            CanWarnEventArgs.WarnType.Error));
                        //throw new ArgumentOutOfRangeException(nameof(value), value, "Baudrate must be a legal baud rate.");
                        return;
                }

                if (State == CanState.Open)
                {

                    Canlib.canSetBusParams(PortHandle, _kvaserBaud, 0, 0, 0, 0, 0);
                }
            }
        }

        public override void CloseCan()
        {
            Canlib.canFlushReceiveQueue(PortHandle);
            Canlib.canFlushTransmitQueue(PortHandle);
            Canlib.canClose(PortHandle);
        }

        public override bool InitializeCan()
        {
            int numChannels;

            Canlib.canInitializeLibrary();
            var canStatus = Canlib.canGetNumberOfChannels(out numChannels);

            if (canStatus == Canlib.canStatus.canOK)
            {
                // Driver loaded successfully
                return GetChannelStatus(numChannels);
            }
            else
            {
                OnWarnEvent(new CanWarnEventArgs("Kvaser CAN DeviceType failed to load.",CanWarnEventArgs.WarnType.Error));
                return false;
            }
        }

        public override void OpenCan()
        {
            PortHandle = Canlib.canOpenChannel(CanChannel, 0);
            var status = Canlib.canSetBusParams(PortHandle, _kvaserBaud, 0, 0, 0, 0, 0);
            if (status == Canlib.canStatus.canOK)
            {
                status = Canlib.canBusOn(PortHandle);
                if (status != Canlib.canStatus.canOK)
                {
                    OnWarnEvent(new CanWarnEventArgs($"Failure during Kvaser canBusOn method: canStatus = {status}",CanWarnEventArgs.WarnType.Error));
                }
            }
            else
            {
                OnWarnEvent(new CanWarnEventArgs($"Failure during Kvaser canSetBusParams method: canStatus = {status}",CanWarnEventArgs.WarnType.Error));
            }
        }

        private bool _isReadGeneral = true;

        private Canlib.canStatus ReadGeneralOrSpecific(int porthandle, ref int msgId, byte[] bytes, out int rxDlc, out int rxFlag, out long rxTime)
        {
            return _isReadGeneral ? Canlib.canRead(porthandle, out msgId, bytes, out rxDlc, out rxFlag, out rxTime) : Canlib.canReadSpecific(porthandle, msgId, bytes, out rxDlc, out rxFlag, out rxTime);
        }

        private Canlib.canStatus SyncGeneralOrSpecific(int portHandle, int msgId, int timeout)
        {
            return _isReadGeneral ? Canlib.canReadSync(portHandle, timeout) : Canlib.canReadSyncSpecific(portHandle, msgId, timeout);
        }

        private IntPtr _contextPtr = IntPtr.Zero;
        public override void SetReceiveCallback(Func<CanMessage,Task> messageReceivedCallback)
        {
            Canlib.kvCallbackDelegate kvaserCallbackDelegate = (handle, context, eventArg) =>
            {
                int id;
                var bytes = new byte[8];
                int rxDlc, rxFlag;
                long rxTime;
                if (Canlib.canRead(PortHandle, out id, bytes, out rxDlc, out rxFlag, out rxTime)==Canlib.canStatus.canOK)
                messageReceivedCallback(new CanMessage(id, bytes));

            };
            Canlib.kvSetNotifyCallback(PortHandle, kvaserCallbackDelegate, _contextPtr, Canlib.canNOTIFY_RX);
        }
        public override CanMessage ReadCanMessage(int messageId = -1)
        {
            //lock (StaticLockDevice)
            {
                const int maxRetryAttempts = 50; // Read attempts to try before giving up.
                const int readSyncTimeout = 10; // Read wait timeout.

                _isReadGeneral = messageId < 0;

                int retryCount;
                var bytes = new byte[] {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
                for (retryCount = 0; retryCount < maxRetryAttempts; retryCount++)
                {
                    var canStatus = SyncGeneralOrSpecific(PortHandle, messageId, readSyncTimeout);
                    if (canStatus == Canlib.canStatus.canOK)
                    {
                        int rxDlc;
                        int rxFlag;
                        long rxTime;
                        //int msgId;
                        canStatus = ReadGeneralOrSpecific(PortHandle, ref messageId, bytes, out rxDlc, out rxFlag, out rxTime);
                        if (canStatus == Canlib.canStatus.canOK)
                        {
                            if ((rxFlag & Canlib.canMSG_MASK) == Canlib.canMSG_STD)
                            {
                               return new CanMessage(messageId, bytes);
                            }
//                            OnWarnEvent(new CanWarnEventArgs($"Kvaser canRead Flag = {rxFlag:X8}"));
                        }
                        else if (canStatus == Canlib.canStatus.canERR_NOMSG)
                        {
                            OnWarnEvent(new CanWarnEventArgs($"Kvaser canRead No Message; canStatus = {canStatus}"));
                        }
                        else if (canStatus == Canlib.canStatus.canERR_TIMEOUT)
                        {
//                            OnWarnEvent(new CanWarnEventArgs($"Kvaser canRead Timeout; canStatus = {canStatus}"));
                        }
                        else
                        {
                            OnWarnEvent(new CanWarnEventArgs($"Kvaser canRead General; canStatus = {canStatus}"));
                        }
                    }
                    else if (canStatus == Canlib.canStatus.canERR_TIMEOUT)
                    {
//                        OnWarnEvent(new CanWarnEventArgs($"Kvaser canReadSync Timeout; canStatus = {canStatus}", CanWarnEventArgs.WarnType.Debug));
                    }
                    else
                    {
                        OnWarnEvent(new CanWarnEventArgs($"Kvaser canReadSync General; canStatus = {canStatus}",CanWarnEventArgs.WarnType.Error));
                    }
                }
                return null;
//                throw new TimeoutException($"No message received after {maxRetryAttempts} attempts with {readSyncTimeout} ms timeouts.");

            }
        }

        public override void ShutdownCan()
        {
            CloseCan();
            Canlib.canUnloadLibrary();
        }

        public override void WriteCanMessage(CanMessage message)
        {
            //lock (StaticLockDevice)
            {
                const int dlc = 8;
//                var canStatus = Canlib.canFlushTransmitQueue(PortHandle);
//
//                if (canStatus != Canlib.canStatus.canOK)
//                {
//                    OnWarnEvent(new CanWarnEventArgs($"Cannot flush canTransmitQueue: canStatus {canStatus}",CanWarnEventArgs.WarnType.Error));
//                }
                var canStatus = Canlib.canFlushReceiveQueue(PortHandle);
                if (canStatus != Canlib.canStatus.canOK)
                {
                    OnWarnEvent(new CanWarnEventArgs($"Cannot flush canReceiveQueue: canStatus {canStatus}",CanWarnEventArgs.WarnType.Error));
                    return;
                }
                canStatus = Canlib.canWrite(PortHandle, message.MsgId, message.Bytes, dlc, Canlib.canMSG_STD);
                if (canStatus == Canlib.canStatus.canOK || canStatus == Canlib.canStatus.canERR_HARDWARE)
                {
                    return;
                }
                OnWarnEvent(new CanWarnEventArgs($"Kvaser write Error: canStatus = {canStatus}",CanWarnEventArgs.WarnType.Error));
            }
        }


        private bool GetChannelStatus(int numChannels)
        {
            for (var i = 0; i < numChannels; i++)
            {
                object boardType;
                var status = Canlib.canGetChannelData(i, Canlib.canCHANNELDATA_CARD_TYPE, out boardType);
                if (status == Canlib.canStatus.canOK)
                {
                    var iBoard = (uint) boardType;
                    switch (iBoard)
                    {
                        case Canlib.canHWTYPE_NONE:
                            OnWarnEvent(new CanWarnEventArgs("Board Type None", CanWarnEventArgs.WarnType.Error));
                            return false;
                        case Canlib.canHWTYPE_VIRTUAL:
                            break;
                        case Canlib.canHWTYPE_USBCAN_PRO:
                        case Canlib.canHWTYPE_USBCAN_II:
                        case Canlib.canHWTYPE_LEAF:
                            PhysicalChannels++;
                            break;

                        default:
                            OnWarnEvent(new CanWarnEventArgs($"Unrecognized Board type {(int) boardType:X8}",CanWarnEventArgs.WarnType.Error));
                            return false;
                    }
                }
                else
                {
                    OnWarnEvent(new CanWarnEventArgs($"Error in reading Kvaser Channel Data: canStatus = {status}",CanWarnEventArgs.WarnType.Error));
                    return false;
                }
            }
            return true;
        }
    }
}