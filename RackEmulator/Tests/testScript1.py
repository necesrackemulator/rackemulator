# file: testScript1.py

import subprocess as sp
import os

class ModuleEmulator(object):
    wrappedPath = 'module-emulator-0.0.1/Debug/'
    exe = 'NECES.ModuleEmulator.exe'
    optimus = 'optimus'
    gullwing = 'gullwing'
    canalyzer = 'canalyzer'
    kvaser = 'kvaser'

    class

    noVal = 'noVal'
    errval = 'errVal'


    def __init__(self):
        self.process = sp.Popen([os.path.join(wrappedPath, exe)], cwd = wrappedPath, stdin = sp.PIPE, stdout = sp.PIPE)

    def setSystem(self, system):
        if (system == optimus):
            self.process.stdin.write('system optimus\n')
        elif (system == gullwing):
            self.process.stdin.write('system gullwing\n')
            
    

    def setCandev(self, canDevice):
        if(canDev == canalyzer):
            self.process.stdin.write('candev canalyzer')
        elif (canDev == kvaser):
            self.process.stdin.write('candev kvaser')
    
    def openChannel(self, channel):
        if(channel == 0 or channel == 1):
            self.process.stdin.write('can channel '+channel+' open')
    
    def setVolt(self, modBegin, modEnd, cellBegin, cellEnd, volts):
        self.volts[modBegin:modEnd,cellBegin:cellEnd]