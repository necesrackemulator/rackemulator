# Requirements for CAN Library

+------------------+----------------------------------------------------------+
| I need to have   | Null Device: For no device connected/emulation           |
| support for      +----------------------------------------------------------+
| these devices    | Kvaser Device: for Kvaser Devices                        |
|                  +----------------------------------------------------------+
|                  | CANAlyzer Device: for CANAlyzer Devices                  |
|                  +----------------------------------------------------------+
|                  | Unitiliazed Device: for uninitialized state. Prevents    |
|                  | CAN messages from being sent/received.                   |
+------------------+----------------------------------------------------------+