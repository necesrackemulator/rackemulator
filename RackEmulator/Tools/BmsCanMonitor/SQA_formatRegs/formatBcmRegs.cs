﻿//*************************************************************************
//
// Copyright © 2014 NEC Energy Solutions, Inc. All rights reserved.
//
// NEC Energy Solutions Proprietary Information.
// This document contains proprietary and confidential information and is
// tendered for information and evaluation purposes only; no copy or other
// use or dissemination by a third party can be made of this information
// without the express written permission of NEC Energy Solutions.
//
//*************************************************************************

using System;
using SQA_format;
using SQA_utils;

namespace SQA_formatRegs
{ 
    public class formatBcmRegisters
    {

        /************************************************************************************************************/
        /*                                                                                                          */
        /*  Function: Format the BCM Contactor Status Register.                                                     */
        /*            Format the output based upon bits set to one or zero.                                         */
        /*                                                                                                          */
        /*  Caveats:  Bits set to one are displayed first.                                                          */
        /*                                                                                                          */
        /************************************************************************************************************/
        public void formatContactor(UInt32 contactorBits, UInt32 contactorMask, dispCntl displayControl)
        {
            string[] contactorRegDef = {
                                   "No Failure",
                                   "DC Bus Shorted - Bus Bar Test",
                                   "DC Bus Shorted - Contactor Test",
                                   "DC Bus Voltage and Battery do not match",
                                   "Positive Contactor Welded",
                                   "Negative Contactor Welded",
                                   "Positive Contactor Failed to Close",
                                   "Negative Contactor Failed to Close",
                                   "Not a Real FTC.  Contactor Test was interrupted",
                                   "Cannot Issue Close While Test is in Progress",
                                   "Cannot Issue Close While Open Safely",
                                   "Dual command to close",
                                   "Low Power Mode",
                                   "Invalid Contactor State"
                                  };
           
            formatUtils.formatRegister(contactorBits, contactorMask, displayControl, "Contactor Register", contactorRegDef);
        }

        /************************************************************************************************************/
        /*                                                                                                          */
        /*  Function: Format the Manufacturing Status Register.                                                     */
        /*            Format the output based upon bits set to one or zero.                                         */
        /*                                                                                                          */
        /*  Caveats:  Bits set to one are displayed first.                                                          */
        /*                                                                                                          */
        /************************************************************************************************************/
        public void formatMfgStatus (UInt32 mfgBits, UInt32 mfgMask, dispCntl displayControl)
        {
            string[] mfgRegDef = {
                                  "DIP Switch 0",
                                  "DIP Switch 1",
                                  "DIP Switch 2",
                                  "DIP Switch 3",
                                  "DIP Switch 4",
                                  "HVIL Input",
                                  "Wake Input",
                                  "Contactor Power",
                                  "Module Power",
                                  "CSM Online"
                                  };

            formatUtils.formatRegister(mfgBits, mfgMask, displayControl, "Mfg. Status Register", mfgRegDef);
        }

        public virtual void formatError(UInt32 errorBits, UInt32 errorMask, dispCntl displayControl)
        {
            Utils.outputVirtualMemberError(typeof(formatBcmRegisters).Name, "formatError");
        }

        public virtual void formatStatus(UInt32 statusBits, UInt32 statusMask, dispCntl displayControl)
        {
            Utils.outputVirtualMemberError(typeof(formatBcmRegisters).Name, "formatStatus");
        }

        public virtual void formatWarning (UInt32 warningBits, UInt32 warningMask, dispCntl displayControl)
        {
            Utils.outputVirtualMemberError(typeof(formatBcmRegisters).Name, "formatWarning");
        }

        public virtual void formatControl (UInt32 controlBits, UInt32 controlMask, dispCntl displayControl)
        {
            Utils.outputVirtualMemberError (typeof(formatBcmRegisters).Name, "formatControl");
        }

        public virtual void formatModuleStatus(UInt16 statusBits, UInt16 statusMask, int moduleNumber, dispCntl displayControl)
        {
            Utils.outputVirtualMemberError(typeof(formatBcmRegisters).Name, "formatModuleStatus");
        }

        public virtual string formatEventDateTimeString (UInt32 timeStamp)
        {
            Utils.outputVirtualMemberError(typeof(formatBcmRegisters).Name, "formatEventDateTime");
            return null;
        }

    }  // End of class formatBcmRegisters

    public class roddenberryFormatBcmRegisters : formatBcmRegisters
    {
        /************************************************************************************************************/
        /*                                                                                                          */
        /*  Function: Format the BCM Status Register.                                                               */
        /*            Format the output based upon bits set to one or zero.                                         */
        /*                                                                                                          */
        /*  Caveats:  Bits set to one are displayed first.                                                          */
        /*                                                                                                          */
        /************************************************************************************************************/

        public override void formatStatus(UInt32 statusBits, UInt32 statusMask, dispCntl displayControl)
        {
            string[] statusRegDef = {
                                  "Heartbeat",
                                  "Wake",
                                  "Ready",
                                  "Close Request Asserted",
                                  "Close in Process",
                                  "Open in Process",
                                  "High-Side Contactor Closed",
                                  "Low-Side Contactor Closed",
                                  "Equalization in Process",
                                  "Equalizer Contactor Closed",
                                  "Close Sequence Failed",
                                  "Contactor Test Executing",
                                  "Contactor Test Passed",
                                  "Contactor Test Failed",
                                  "Balancing Active",
                                  "Critical EPO Alarm Active",
                                  "Warning LOS Alarm Active",
                                  "Warning Alarm Active",
                                  "High Voltage Interlock Present",
                                  "Contactor Power Active",
                                  "Module Power Active",
                                  "CSM Online",
                                  "EFT Active",
                                  "CBL Active",
                                  "Revision Request in Progress",
                                  "Equalizer Enabled",
                                  "Equalizer Present",
                                  "Pre-charger Present",
                                  "Equalization Hardware Detected",
                                  "Unused 29",
                                  "Unused 30",
                                  "Manufacturing Mode"
                                 };


            formatUtils.formatRegister(statusBits, statusMask, displayControl, "Status Register", statusRegDef);
        }

        /************************************************************************************************************/
        /*                                                                                                          */
        /*  Function: Format the BCM Error Register.                                                                */
        /*            Format the output based upon bits set to one or zero.                                         */
        /*                                                                                                          */
        /*  Caveats:  Bits set to one are displayed first.                                                          */
        /*                                                                                                          */
        /************************************************************************************************************/

        public override void formatError (UInt32 errorBits, UInt32 errorMask, dispCntl displayControl)
        {
            string[] errorRegDef = {
                                 "MBB Communication Error",
                                 "CSM Communication Error",
                                 "High Voltage Cell Error",
                                 "Low Voltage Cell Error",
                                 "High Battery Voltage Error",
                                 "Low Battery Voltage Error",
                                 ">1 MBB(s) Reporting Cell Vmax Error",
                                 "High Module Temperature Error",
                                 "Integrated Current Error",
                                 "Rack Fuse Failure",
                                 "EPO (Rack) Trip",
                                 "Unused 11",
                                 "High Voltage Interlock Error",
                                 "Module Fault Signal Error",
                                 "Contactor Hold Power Failed HI",
                                 "CANopen Heartbeat Failure",
                                 "Rack Recovery Required",
                                 "Unused 17",
                                 "Unused 18",
                                 "Unused 19",
                                 "Unused 20",
                                 "Unused 21",
                                 ">1 MBB(s) Reporting Cell Vmin Error",
                                 "Unused 23",
                                 "Unused 24",
                                 "Charge Over Current Error",
                                 "Unused 26",
                                 "Unused 27",
                                 "Reversed Polarity on DC Bus",
                                 "Contactor Fault Latch Set",
                                 "Contactor Hold Power Failed LO",
                                 "Unused 31"
                                 };


            formatUtils.formatRegister(errorBits, errorMask, displayControl, "Error Register", errorRegDef);
        }

        public override void formatControl(UInt32 controlBits, UInt32 controlMask, dispCntl displayControl)
        {
            string[] controlRegDef = {
                                  "Heartbeat",
                                  "Clear DTC",
                                  "System Enable",
                                  "Close Request",
                                  "Contactor Test",
                                  "Contactor Clear",
                                  "Enable Balance",
                                  "Force Balance",
                                  "Inhibit Equalizer",
                                  "Equalizer Finish",
                                  "Unused 10",
                                  "Unused 11",
                                  "Unused 12",
                                  "Unused 13",
                                  "Unused 14",
                                  "Unused 15",
                                  "Unused 16",
                                  "Unused 17",
                                  "Unused 18",
                                  "Unused 19",
                                  "Unused 20",
                                  "Unused 21",
                                  "Unused 22",
                                  "Unused 23",
                                  "Unused 24",
                                  "Unused 25",
                                  "Unused 26",
                                  "Unused 27",
                                  "Unused 28",
                                  "Unused 29",
                                  "Unused 30",
                                  "Unused 31"
                                 };


            formatUtils.formatRegister(controlBits, controlMask, displayControl, "Control Register", controlRegDef);
        }

        /************************************************************************************************************/
        /*                                                                                                          */
        /*  Function: Format the BCM Warning Register.                                                              */
        /*            Format the output based upon bits set to one or zero.                                         */
        /*                                                                                                          */
        /*  Caveats:  Bits set to one are displayed first.                                                          */
        /*                                                                                                          */
        /************************************************************************************************************/

        public override void formatWarning (UInt32 warningBits, UInt32 warningMask, dispCntl displayControl)
        {
            string[] warningRegDef = {
                                "High Cell Voltage Warning",
                                "Low Cell Voltage Warning",
                                "MBB(s) Reporting Voltage Mismatch",
                                ">1 MBB(s) Reporting Temperature Limit",
                                "High Module Temperature Warning",
                                "Low Module Temperature Warning",
                                "Current Sensors Differ",
                                "Power Supply OOR High",
                                "Contactor Power OOR High",
                                "5V Logic Power OOR High",
                                "CSM is Reporting Internal Problems",
                                "Unused 11",
                                "Unused 12",
                                "Unused 13",
                                "Unused 14",
                                "Unused 15",
                                "Unused 16",
                                "Unused 17",
                                "Unused 18",
                                "Unused 19",
                                "Integrated Current Warning",
                                "Equalizer Failed To Start",
                                "Equalizer drives cells to voltage limit",
                                "Power Supply OOR Low",
                                "Contactor Power OOR Low",
                                "5V Logic Power OOR Low",
                                "Precharger Failed",
                                "Unused 27",
                                "Unused 28",
                                "Unused 29",
                                "Unused 30",
                                "Unused 31"
                                 };


            formatUtils.formatRegister(warningBits, warningMask, displayControl, "Warning Register", warningRegDef);
        }

        /************************************************************************************************************/
        /*                                                                                                          */
        /*  Function: Format the BCM Module Status Register.                                                        */
        /*            Format the output based upon bits set to one or zero.                                         */
        /*                                                                                                          */
        /*  Caveats:  Bits set to one are displayed first.                                                          */
        /*                                                                                                          */
        /************************************************************************************************************/

        public override void formatModuleStatus (UInt16 statusBits, UInt16 statusMask, int moduleNumber, dispCntl displayControl)
        {
            string[] statusRegDef = {
                                     "No Module Com",
                                     "Voltage Sense Failure",
                                     "Temperature Sense Failure",
                                     "Cell Overvoltage Detected",
                                     "Cell Overvoltage Fault",
                                     "Cell Undervoltage Fault",
                                     "Voltage Compare Error",
                                     "Vref Drift Error",
                                     "Non-volatile Memory Check Error",
                                     "ROM Check Error",
                                     "RAM Check Error",
                                     "Cell Error Limits",
                                     "Unused 12",
                                     "Unused 13",
                                     "Unused 14",
                                     "Unused 15"
                                    };


            formatUtils.formatRegister (statusBits, statusMask, displayControl, String.Format("Module {0} Status Register", moduleNumber), statusRegDef);
        }

        public override string formatEventDateTimeString(UInt32 timeStamp)
        {
            return String.Format("Date/Time {0}.", formatUtils.convertCalEventTime(timeStamp));
        }

    }  // End of class roddenberryFormatBcmRegisters

    public class switchboardFormatBcmRegisters : formatBcmRegisters
    {
        
        /************************************************************************************************************/
        /*                                                                                                          */
        /*  Function: Format the BCM Error Register.                                                                */
        /*            Format the output based upon bits set to one or zero.                                         */
        /*                                                                                                          */
        /*  Caveats:  Bits set to one are displayed first.                                                          */
        /*                                                                                                          */
        /************************************************************************************************************/

        public override void formatError(UInt32 errorBits, UInt32 errorMask, dispCntl displayControl)
        {
            string[] errorRegDef = {
                                    "MBB Communication Error",
                                 "CSM Communication Error",
                                 "High Voltage Cell Error",
                                 "Low Voltage Cell Error",
                                 "High Battery Voltage Error",
                                 "Low Battery Voltage Error",
                                 ">1 MBB(s) Reporting Cell Vmax Error",
                                 "High Module Temperature Error",
                                 "Integrated Current Error",
                                 "Rack Fuse Failure",
                                 "EPO (Rack) Trip",
                                 "Unused 11",
                                 "High Voltage Interlock Error",
                                 "Module Fault Signal Error",
                                 "Contactor Hold Power Failed HI",
                                 "CANopen Heartbeat Error",
                                 "Unused 16",
                                 "Unused 17",
                                 "Unused 18",
                                 "Unused 19",
                                 "Unused 20",
                                 "Unused 21",
                                 ">1 MBB(s) Reporting Cell Vmin Error",
                                 "Unused 23",
                                 "Unused 24",
                                 "Unused 25",
                                 "Unused 26",
                                 "Unused 27",
                                 "Unused 28",
                                 "Contactor Fault Latch Set",
                                 "Contactor Hold Power Failed LO",
                                 "Unused 31"
                                 };


            formatUtils.formatRegister(errorBits, errorMask, displayControl, "Error Register", errorRegDef);
        }

        /************************************************************************************************************/
        /*                                                                                                          */
        /*  Function: Format the BCM Status Register.                                                               */
        /*            Format the output based upon bits set to one or zero.                                         */
        /*                                                                                                          */
        /*  Caveats:  Bits set to one are displayed first.                                                          */
        /*                                                                                                          */
        /************************************************************************************************************/

        public override void formatStatus(UInt32 statusBits, UInt32 statusMask, dispCntl displayControl)
        {
            string[] statusRegDef = {
                                  "Heartbeat",
                                  "Wake",
                                  "Ready",
                                  "Close Request Asserted",
                                  "Close in Process",
                                  "Open in Process",
                                  "High-Side Contactor Closed",
                                  "Low-Side Contactor Closed",
                                  "Unused 8",
                                  "Equalizer Contactor Closed",
                                  "Close Sequence Failed",
                                  "Local Off",
                                  "Remote",
                                  "Unused 13",
                                  "Balancing Active",
                                  "Critical EPO Alarm Active",
                                  "Warning LOS Alarm Active",
                                  "Warning Alarm Active",
                                  "High Voltage Interlock Present",
                                  "Contactor Power Active",
                                  "Module Power Active",
                                  "CSM Online",
                                  "Unused 22",
                                  "Unused 23",
                                  "Unused 24",
                                  "SSR1 Closed",
                                  "SSR2 Closed",
                                  "Jump Start Input Detected",
                                  "Jump Start Mode Active",
                                  "Unused 29",
                                  "Unused 30",
                                  "Unused 31"
                                 };


            formatUtils.formatRegister(statusBits, statusMask, displayControl, "Status Register", statusRegDef);
        }

        public override void formatControl(UInt32 controlBits, UInt32 controlMask, dispCntl displayControl)
        {
            string[] controlRegDef = {
                                  "Heartbeat",
                                  "Clear DTC",
                                  "System Enable",
                                  "Close Request",
                                  "Unused 4",
                                  "Unused 5",
                                  "Unused 6",
                                  "Unused 7",
                                  "Unused 8",
                                  "Unused 9",
                                  "Unused 10",
                                  "Unused 11",
                                  "Unused 12",
                                  "Unused 13",
                                  "Unused 14",
                                  "Unused 15",
                                  "Unused 16",
                                  "Unused 17",
                                  "Unused 18",
                                  "Unused 19",
                                  "Unused 20",
                                  "Unused 21",
                                  "Unused 22",
                                  "Unused 23",
                                  "Unused 24",
                                  "Unused 25",
                                  "Unused 26",
                                  "Unused 27",
                                  "Unused 28",
                                  "Unused 29",
                                  "Unused 30",
                                  "Heartbeat loss create EPO" // Only used for datalog display.
                                 };


            formatUtils.formatRegister(controlBits, controlMask, displayControl, "Control Register", controlRegDef);
        }

        /************************************************************************************************************/
        /*                                                                                                          */
        /*  Function: Format the BCM Warning Register.                                                              */
        /*            Format the output based upon bits set to one or zero.                                         */
        /*                                                                                                          */
        /*  Caveats:  Bits set to one are displayed first.                                                          */
        /*                                                                                                          */
        /************************************************************************************************************/

        public override void formatWarning(UInt32 warningBits, UInt32 warningMask, dispCntl displayControl)
        {
            string[] warningRegDef = {
                                "High Cell Voltage Warning",
                                "Low Cell Voltage Warning",
                                "MBB(s) Reporting Voltage Mismatch",
                                ">1 MBB(s) Reporting Temperature Limit",
                                "High Module Temperature Warning",
                                "Low Module Temperature Warning",
                                "Unused 6",
                                "Power Supply OOR High",
                                "Contactor Power OOR High",
                                "5V Logic Power OOR High",
                                "CSM is Reporting Internal Problems",
                                "Unused 11",
                                "Unused 12",
                                "Unused 13",
                                "Unused 14",
                                "Unused 15",
                                "Unused 16",
                                "Unused 17",
                                "Unused 18",
                                "Unused 19",
                                "Integrated Current Warning",
                                "Unused 21",
                                "Unused 22",
                                "Power Supply OOR Low",
                                "Contactor Power OOR Low",
                                "5V Logic Power OOR Low",
                                "Precharger Failed",
                                "Unused 27",
                                "Unused 28",
                                "Unused 29",
                                "Unused 30",
                                "Unused 31"
                                 };


            formatUtils.formatRegister(warningBits, warningMask, displayControl, "Warning Register", warningRegDef);
        }

        /************************************************************************************************************/
        /*                                                                                                          */
        /*  Function: Format the BCM Module Status Register.                                                        */
        /*            Format the output based upon bits set to one or zero.                                         */
        /*                                                                                                          */
        /*  Caveats:  Bits set to one are displayed first.                                                          */
        /*                                                                                                          */
        /************************************************************************************************************/

        public override void formatModuleStatus(UInt16 statusBits, UInt16 statusMask, int moduleNumber, dispCntl displayControl)
        {
            string[] statusRegDef = {
                                     "No Module Com",
                                     "Voltage Sense Failure",
                                     "Temperature Sense Failure",
                                     "Cell Overvoltage Detected",
                                     "Unused 4",
                                     "Unused 5",
                                     "Unused 6",
                                     "Unused 7",
                                     "Unused 8",
                                     "Unused 9",
                                     "Unused 10",
                                     "Unused 11",
                                     "Unused 12",
                                     "Unused 13",
                                     "Unused 14",
                                     "Unused 15"
                                    };


            formatUtils.formatRegister(statusBits, statusMask, displayControl, String.Format("Module {0} Status Register", moduleNumber), statusRegDef);
        }

        public override string formatEventDateTimeString(UInt32 timeStamp)
        {
            TimeSpan t = TimeSpan.FromSeconds(timeStamp);

            string timeString = string.Format("{0:D3}d:{1:D2}h:{2:D2}m:{3:D2}s",
                                    t.Days,
                                    t.Hours,
                                    t.Minutes,
                                    t.Seconds);
            return String.Format("Time {0}.", timeString);
        }

    }  // End of class switchboardFormatBcmRegisters

    public class sunriseFormatBcmRegisters : formatBcmRegisters
    {
        /************************************************************************************************************/
        /*                                                                                                          */
        /*  Function: Format the BCM Error Register.                                                                */
        /*            Format the output based upon bits set to one or zero.                                         */
        /*                                                                                                          */
        /*  Caveats:  Bits set to one are displayed first.                                                          */
        /*                                                                                                          */
        /************************************************************************************************************/

        public override void formatError(UInt32 errorBits, UInt32 errorMask, dispCntl displayControl)
        {
            string[] errorRegDef = {
                                 "MBB Communication Error",
                                 "CSM Communication Error",
                                 "High Voltage Cell Error",
                                 "Low Voltage Cell Error",
                                 "High Battery Voltage Error",
                                 "Low Battery Voltage Error",
                                 ">1 MBB(s) Reporting Cell Vmax Error",
                                 "High Module Temperature Error",
                                 "Integrated Current Error",
                                 "Rack Fuse Failure",
                                 "EPO (Rack) Trip",
                                 "Unused 11",
                                 "High Voltage Interlock Error",
                                 "Module Fault Signal Error",
                                 "Contactor Hold Power Failed HI",
                                 "CANopen Heartbeat Failure",
                                 "Rack Recovery Required",
                                 "Unused 17",
                                 "Unused 18",
                                 "Unused 19",
                                 "Unused 20",
                                 "Unused 21",
                                 ">1 MBB(s) Reporting Cell Vmin Error",
                                 "Unused 23",
                                 "Unused 24",
                                 "Charge Over Current Error",
                                 "Unused 26",
                                 "Unused 27",
                                 "Reversed Polarity on DC Bus",
                                 "Contactor Fault Latch Set",
                                 "Contactor Hold Power Failed LO",
                                 "Unused 31"
                                 };


            formatUtils.formatRegister(errorBits, errorMask, displayControl, "Error Register", errorRegDef);
        }

        /************************************************************************************************************/
        /*                                                                                                          */
        /*  Function: Format the BCM Status Register.                                                               */
        /*            Format the output based upon bits set to one or zero.                                         */
        /*                                                                                                          */
        /*  Caveats:  Bits set to one are displayed first.                                                          */
        /*                                                                                                          */
        /************************************************************************************************************/

        public override void formatStatus(UInt32 statusBits, UInt32 statusMask, dispCntl displayControl)
        {
            string[] statusRegDef = {
                                  "Heartbeat",
                                  "Wake",
                                  "Ready",
                                  "Close Request Asserted",
                                  "Close in Process",
                                  "Open in Process",
                                  "High-Side Contactor Closed",
                                  "Low-Side Contactor Closed",
                                  "Equalization in Process",
                                  "Equalizer Contactor Closed",
                                  "Close Sequence Failed",
                                  "Contactor Test Executing",
                                  "Contactor Test Passed",
                                  "Contactor Test Failed",
                                  "Balancing Active",
                                  "Critical EPO Alarm Active",
                                  "Warning LOS Alarm Active",
                                  "Warning Alarm Active",
                                  "High Voltage Interlock Present",
                                  "Contactor Power Active",
                                  "Module Power Active",
                                  "CSM Online",
                                  "EFT Active",
                                  "CBL Active",
                                  "Revision Request in Progress",
                                  "Equalizer Enabled",
                                  "Equalizer Present",
                                  "Pre-charger Present",
                                  "Equalization Hardware Detected",
                                  "Unused 29",
                                  "Unused 30",
                                  "Manufacturing Mode"
                                 };


            formatUtils.formatRegister(statusBits, statusMask, displayControl, "Status Register", statusRegDef);
        }

        /************************************************************************************************************/
        /*                                                                                                          */
        /*  Function: Format the BCM Control Register.                                                              */
        /*            Format the output based upon bits set to one or zero.                                         */
        /*                                                                                                          */
        /*  Caveats:  Bits set to one are displayed first.                                                          */
        /*                                                                                                          */
        /************************************************************************************************************/

        public override void formatControl(UInt32 controlBits, UInt32 controlMask, dispCntl displayControl)
        {
            string[] controlRegDef = {
                                  "Heartbeat",
                                  "Clear DTC",
                                  "System Enable",
                                  "Close Request",
                                  "Contactor Test",
                                  "Contactor Clear",
                                  "Enable Balance",
                                  "Force Balance",
                                  "Inhibit Equalizer",
                                  "Equalizer Finish",
                                  "Unused 10",
                                  "Unused 11",
                                  "Unused 12",
                                  "Unused 13",
                                  "Unused 14",
                                  "Unused 15",
                                  "Unused 16",
                                  "Unused 17",
                                  "Unused 18",
                                  "Unused 19",
                                  "Unused 20",
                                  "Unused 21",
                                  "Unused 22",
                                  "Unused 23",
                                  "Unused 24",
                                  "Unused 25",
                                  "Unused 26",
                                  "Unused 27",
                                  "Unused 28",
                                  "Unused 29",
                                  "Unused 30",
                                  "Unused 31"
                                 };


            formatUtils.formatRegister(controlBits, controlMask, displayControl, "Control Register", controlRegDef);
        }

        /************************************************************************************************************/
        /*                                                                                                          */
        /*  Function: Format the BCM Warning Register.                                                              */
        /*            Format the output based upon bits set to one or zero.                                         */
        /*                                                                                                          */
        /*  Caveats:  Bits set to one are displayed first.                                                          */
        /*                                                                                                          */
        /************************************************************************************************************/

        public override void formatWarning(UInt32 warningBits, UInt32 warningMask, dispCntl displayControl)
        {
            string[] warningRegDef = {
                                "High Cell Voltage Warning",
                                "Low Cell Voltage Warning",
                                "MBB(s) Reporting Voltage Mismatch",
                                ">1 MBB(s) Reporting Temperature Limit",
                                "High Module Temperature Warning",
                                "Low Module Temperature Warning",
                                "Current Sensors Differ",
                                "Power Supply OOR High",
                                "Contactor Power OOR High",
                                "5V Logic Power OOR High",
                                "CSM is Reporting Internal Problems",
                                "Unused 11",
                                "Unused 12",
                                "Unused 13",
                                "Unused 14",
                                "Unused 15",
                                "Unused 16",
                                "Unused 17",
                                "Unused 18",
                                "Unused 19",
                                "Integrated Current Warning",
                                "Equalizer Failed To Start",
                                "Equalizer drives cells to voltage limit",
                                "Power Supply OOR Low",
                                "Contactor Power OOR Low",
                                "5V Logic Power OOR Low",
                                "Precharger Failed",
                                "Unused 27",
                                "Unused 28",
                                "Unused 29",
                                "Unused 30",
                                "Unused 31"
                                 };


            formatUtils.formatRegister(warningBits, warningMask, displayControl, "Warning Register", warningRegDef);
        }

        /************************************************************************************************************/
        /*                                                                                                          */
        /*  Function: Format the BCM Module Status Register.                                                        */
        /*            Format the output based upon bits set to one or zero.                                         */
        /*                                                                                                          */
        /*  Caveats:  Bits set to one are displayed first.                                                          */
        /*                                                                                                          */
        /************************************************************************************************************/

        public override void formatModuleStatus(UInt16 statusBits, UInt16 statusMask, int moduleNumber, dispCntl displayControl)
        {
            string[] statusRegDef = {
                                     "No Module Com",
                                     "Voltage Sense Failure",
                                     "Temperature Sense Failure",
                                     "Cell Overvoltage Detected",
                                     "Cell Overvoltage Fault",
                                     "Cell Undervoltage Fault",
                                     "Voltage Compare Error",
                                     "Vref Drift Error",
                                     "Non-volatile Memory Check Error",
                                     "ROM Check Error",
                                     "RAM Check Error",
                                     "Cell Error Limits",
                                     "Unused 12",
                                     "Unused 13",
                                     "Unused 14",
                                     "Unused 15"
                                    };


            formatUtils.formatRegister(statusBits, statusMask, displayControl, String.Format("Module {0} Status Register", moduleNumber), statusRegDef);
        }

        public override string formatEventDateTimeString(UInt32 timeStamp)
        {
            return String.Format("Date/Time {0}.", formatUtils.convertCalEventTime(timeStamp));
        }

    }  // End of class sunriseFormatBcmRegisters
    public class optimusFormatBcmRegisters : sunriseFormatBcmRegisters
    {
    }  // End of class optimusFormatBcmRegisters
}




