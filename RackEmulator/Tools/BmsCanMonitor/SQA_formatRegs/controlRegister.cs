﻿//*************************************************************************
//
// Copyright © 2014 NEC Energy Solutions, Inc. All rights reserved.
//
// NEC Energy Solutions Proprietary Information.
// This document contains proprietary and confidential information and is
// tendered for information and evaluation purposes only; no copy or other
// use or dissemination by a third party can be made of this information
// without the express written permission of NEC Energy Solutions.
//
//*************************************************************************

using System;
using System.Linq;
using System.Text;

namespace SQA_formatRegs
{
    public class controlReg
    {

        /************************************************************************************************************/
        /*                                                                                                          */
        /*  Function: Manipulate the BCM Control Register over CANopen.                                             */
        /*            Format the output.                                                                            */
        /*                                                                                                          */
        /*  Caveats:  None.                                                                                         */
        /*                                                                                                          */
        /************************************************************************************************************/
       
        public const UInt16 CONTROL_HEARTBEAT_ON =        0x0001;
        public const UInt16 CONTROL_CLEAR_DTC =           0x0002;
        public const UInt16 CONTROL_SYSTEM_ENABLE =       0x0004;
        public const UInt16 CONTROL_CLOSE_REQUEST =       0x0008;
        public const UInt16 CONTROL_CONTACTOR_TEST =      0x0010;
        public const UInt16 CONTROL_CONTACTOR_CLEAR =     0x0020;
        public const UInt16 CONTROL_ENABLE_BALANCE =      0x0040;
        public const UInt16 CONTROL_FORCE_BALANCE =       0x0080;
        public const UInt16 CONTROL_INHIBIT_EQUALIZER =   0x0100;
        public const UInt16 CONTROL_EQUALIZER_FINISH =    0x0200;

        public const UInt16 CONTROL_ALL_BITS =            0x03FF;

    }

}
 
                           
