﻿//*************************************************************************
//
// Copyright © 2014 NEC Energy Solutions, Inc. All rights reserved.
//
// NEC Energy Solutions Proprietary Information.
// This document contains proprietary and confidential information and is
// tendered for information and evaluation purposes only; no copy or other
// use or dissemination by a third party can be made of this information
// without the express written permission of NEC Energy Solutions.
//
//*************************************************************************

using System;
using System.Linq;
using System.Text;
using System.Globalization;
using SQA_config;
using SQA_format;
using SQA_utils;

namespace SQA_formatRegs
{
    public class formatRoutinesBCM
    {
        /************************************************************************************************************/
        /*                                                                                                          */
        /*  Function: Format the BCM Control Register.                                                              */
        /*            Format the output based upon bits set to one or zero.                                         */
        /*                                                                                                          */
        /*  Caveats:  Bits set to one are displayed first.                                                          */
        /*                                                                                                          */
        /************************************************************************************************************/

        public void formatControlRegister(UInt32 controlBits, UInt32 controlMask, dispCntl displayControl)
        {
            string[] roddenberryControl = {
                                  "Heartbeat", 
                                  "Clear DTC",
                                  "System Enable",
                                  "Close Request",
                                  "Contactor Test",
                                  "Contactor Clear",
                                  "Enable Balance",
                                  "Force Balance",
                                  "Inhibit Equalizer",
                                  "Equalizer Finish",
                                  "Unused 10",
                                  "Unused 11",
                                  "Unused 12",
                                  "Unused 13",
                                  "Unused 14",
                                  "Unused 15",
                                  "Unused 16",
                                  "Unused 17",
                                  "Unused 18",
                                  "Unused 19",
                                  "Unused 20",
                                  "Unused 21",
                                  "Unused 22",
                                  "Unused 23",
                                  "Unused 24",
                                  "Unused 25",
                                  "Unused 26",
                                  "Unused 27",
                                  "Unused 28",
                                  "Unused 29",
                                  "Unused 30",
                                  "Unused 31"
                                 };

            string[] switchboardControl = {
                                  "Heartbeat", 
                                  "Clear DTC",
                                  "System Enable",
                                  "Close Request",
                                  "Unused 4",
                                  "Unused 5",
                                  "Unused 6",
                                  "Unused 7",
                                  "Unused 8",
                                  "Unused 9",
                                  "Unused 10",
                                  "Unused 11",
                                  "Unused 12",
                                  "Unused 13",
                                  "Unused 14",
                                  "Unused 15",
                                  "Unused 16",
                                  "Unused 17",
                                  "Unused 18",
                                  "Unused 19",
                                  "Unused 20",
                                  "Unused 21",
                                  "Unused 22",
                                  "Unused 23",
                                  "Unused 24",
                                  "Unused 25",
                                  "Unused 26",
                                  "Unused 27",
                                  "Unused 28",
                                  "Unused 29",
                                  "Unused 30",
                                  "Heartbeat loss create EPO" // Only used for datalog display.
                                 };


            // Copy the array based upon the type of system. 
            switch (Rack.systemUnderTest)
            {
                case Rack.systemType.GBS_NULL:
                case Rack.systemType.GBS_C:
                case Rack.systemType.GBS_BUZZ:
                case Rack.systemType.GBS_RODDENBERRY:
                case Rack.systemType.GBS_YODA:
                case Rack.systemType.GBS_SUNRISE:
                case Rack.systemType.GBS_SCSM:
                    {
                        formatUtils.formatRegister(controlBits, controlMask, displayControl, "Control Register", roddenberryControl);
                    }
                    break;
                case Rack.systemType.SWITCHBOARD_48V100K:
                    {
                        formatUtils.formatRegister(controlBits, controlMask, displayControl, "Control Register", switchboardControl);
                    }
                    break;

                default:
                    {
                        Rack.unknownSystemType();
                    }
                    break;
            }

        }
        /************************************************************************************************************/
        /*                                                                                                          */
        /*  Function: Format the BCM Status Register.                                                               */
        /*            Format the output based upon bits set to one or zero.                                         */
        /*                                                                                                          */
        /*  Caveats:  Bits set to one are displayed first.                                                          */
        /*                                                                                                          */
        /************************************************************************************************************/

        public void formatStatusRegister(UInt32 statusBits, UInt32 statusMask, dispCntl displayControl)
        {
            string[] roddenberryStatus = {
                                  "Heartbeat", 
                                  "Wake",
                                  "Ready",
                                  "Close Request Asserted",
                                  "Close in Process",
                                  "Open in Process",
                                  "High-Side Contactor Closed",
                                  "Low-Side Contactor Closed",
                                  "Equalization in Process",
                                  "Equalizer Contactor Closed",
                                  "Close Sequence Failed",
                                  "Contactor Test Executing",
                                  "Contactor Test Passed",
                                  "Contactor Test Failed",
                                  "Balancing Active",
                                  "Critical EPO Alarm Active",
                                  "Warning LOS Alarm Active",
                                  "Warning Alarm Active",
                                  "High Voltage Interlock Present",
                                  "Contactor Power Active",
                                  "Module Power Active",
                                  "CSM Online",
                                  "EFT Active",
                                  "CBL Active",
                                  "Revision Request in Progress",
                                  "Equalizer Enabled",
                                  "Equalizer Present",
                                  "Pre-charger Present",
                                  "Equalization Hardware Detected",
                                  "Unused 29",
                                  "Unused 30",
                                  "Manufacturing Mode"
                                 };

            string[] switchboardStatus = {
                                  "Heartbeat", 
                                  "Wake",
                                  "Ready",
                                  "Close Request Asserted",
                                  "Close in Process",
                                  "Open in Process",
                                  "High-Side Contactor Closed",
                                  "Low-Side Contactor Closed",
                                  "Unused 8",
                                  "Equalizer Contactor Closed",
                                  "Close Sequence Failed",
                                  "Local Off",
                                  "Remote",
                                  "Unused 13",
                                  "Balancing Active",
                                  "Critical EPO Alarm Active",
                                  "Warning LOS Alarm Active",
                                  "Warning Alarm Active",
                                  "High Voltage Interlock Present",
                                  "Contactor Power Active",
                                  "Module Power Active",
                                  "CSM Online",
                                  "Unused 22",
                                  "Unused 23",
                                  "Unused 24",
                                  "SSR1 Closed",
                                  "SSR2 Closed",
                                  "Jump Start Input Detected",
                                  "Jump Start Mode Active",
                                  "Unused 29",
                                  "Unused 30",
                                  "Unused 31"
                                 };


            // Format the register based upon the type of system.
            switch (Rack.systemUnderTest)
            {
                case Rack.systemType.GBS_C:
                case Rack.systemType.GBS_BUZZ:
                case Rack.systemType.GBS_RODDENBERRY:
                case Rack.systemType.GBS_YODA:
                case Rack.systemType.GBS_SUNRISE:
                case Rack.systemType.GBS_SCSM:
                    {
                        formatUtils.formatRegister(statusBits, statusMask, displayControl, "Status Register", roddenberryStatus);
                    }
                    break;
                case Rack.systemType.SWITCHBOARD_48V100K:
                    {
                        formatUtils.formatRegister(statusBits, statusMask, displayControl, "Status Register", switchboardStatus);
                    }
                    break;

                default:
                    {
                        Rack.unknownSystemType();
                    }
                    break;
            }

        }
    } // End of class formatRoutinesBCM
}