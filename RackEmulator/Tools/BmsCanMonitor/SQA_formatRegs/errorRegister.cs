﻿//*************************************************************************
//
// Copyright © 2014 NEC Energy Solutions, Inc. All rights reserved.
//
// NEC Energy Solutions Proprietary Information.
// This document contains proprietary and confidential information and is
// tendered for information and evaluation purposes only; no copy or other
// use or dissemination by a third party can be made of this information
// without the express written permission of NEC Energy Solutions.
//
//*************************************************************************

using System;
using System.Linq;
using System.Text;

namespace SQA_formatRegs
{

    public class errorRegister
    {
        public const UInt32 ERROR_MBB_COMMUNICATION         = 0x00000001;
        public const UInt32 ERROR_CSM_COMMUNICATION         = 0x00000002;
        public const UInt32 ERROR_HIGH_CELL_VOLTAGE         = 0x00000004;
        public const UInt32 ERROR_LOW_CELL_VOLTAGE          = 0x00000008;
        public const UInt32 ERROR_HIGH_BATTERY_VOLTAGE      = 0x00000010;
        public const UInt32 ERROR_LOW_BATTERY_VOLTAGE       = 0x00000020;
        public const UInt32 ERROR_MBBS_CELL_VMAX_ERROR      = 0x00000040;
        public const UInt32 ERROR_HIGH_MODULE_TEMP          = 0x00000080;
        public const UInt32 ERROR_INTEGRATED_CURRENT        = 0x00000100;
        public const UInt32 ERROR_FUSE_CURRENT              = 0x00000200;
        public const UInt32 ERROR_CRITICAL_FAULT            = 0x00000400;
        public const UInt32 ERROR_HVI                       = 0x00001000;
        public const UInt32 ERROR_MODULE_FAULT_SIGNAL       = 0x00002000;
        public const UInt32 ERROR_CONTACTOR_HOLD_POWER1     = 0x00004000;
        public const UInt32 ERROR_CANOPEN_HEARTBEAT         = 0x00008000;
        public const UInt32 ERROR_RACK_RECOVERY             = 0x00010000;
        public const UInt32 ERROR_MBBS_CELL_VMIN            = 0x00400000;
        public const UInt32 ERROR_REVERSE_POLARITY_DC_BUS   = 0x10000000;
        public const UInt32 ERROR_CONTACTOR_FAULT_LATCH     = 0x20000000;
        public const UInt32 ERROR_CONTACTOR_HOLD_POWER2     = 0x40000000;
        public const UInt32 ERROR_ALL_BITS                  = 0x7041F7FF;
        public const UInt32 ERROR_NO_ERROR_BITS             = 0x00000000;

    } // End of class errorRegister

    
}
 
                           
