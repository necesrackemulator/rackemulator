﻿//*************************************************************************
//
// Copyright © 2014 NEC Energy Solutions, Inc. All rights reserved.
//
// NEC Energy Solutions Proprietary Information.
// This document contains proprietary and confidential information and is
// tendered for information and evaluation purposes only; no copy or other
// use or dissemination by a third party can be made of this information
// without the express written permission of NEC Energy Solutions.
//
//*************************************************************************

using System;
using System.Linq;
using System.Text;

namespace SQA_formatRegs
{

    public class statusRegister
    {
        /************************************************************************************************************/
        /*                                                                                                          */
        /*  Function: Read the BCM Status Register over CANopen.                                                    */
        /*            Format the output.                                                                            */
        /*                                                                                                          */
        /*  Caveats:  None.                                                                                         */
        /*                                                                                                          */
        /************************************************************************************************************/
        public const UInt32 STATUS_HEARTBEAT =                      0x00000001;
        public const UInt32 STATUS_WAKE_STANDBY =                   0x00000002;
        public const UInt32 STATUS_READY =                          0x00000004;
        public const UInt32 STATUS_CLOSE_REQUEST_ASSERTED =         0x00000008;
        public const UInt32 STATUS_CLOSE_IN_PROCESS =               0x00000010;
        public const UInt32 STATUS_OPEN_IN_PROCESS =                0x00000020;
        public const UInt32 STATUS_HIGH_SIDE_CONTACTOR_CLOSED =     0x00000040;
        public const UInt32 STATUS_LOW_SIDE_CONTACTOR_CLOSED =      0x00000080;
        public const UInt32 STATUS_EQUALIZATION_IN_PROGRESS =       0x00000100;
        public const UInt32 STATUS_EQUALIZATION_CONTACTOR_CLOSED =  0x00000200;
        public const UInt32 STATUS_CLOSE_SEQUENCE_FAILED =          0x00000400;
        public const UInt32 STATUS_CONTACTOR_TEST_EXECUTING =       0x00000800;
        public const UInt32 STATUS_CONTACTOR_TEST_PASSED =          0x00001000;
        public const UInt32 STATUS_CONTACTOR_TEST_FAILED =          0x00002000;
        public const UInt32 STATUS_BALANCING_ACTIVE =               0x00004000;
        public const UInt32 STATUS_CRITICAL_EPO_ALARM =             0x00008000;
        public const UInt32 STATUS_WARNING_LOS_ALARM =              0x00010000;
        public const UInt32 STATUS_WARNING_ALARM =                  0x00020000;
        public const UInt32 STATUS_INTERLOCK_PRESENT =              0x00040000;
        public const UInt32 STATUS_CONTACTOR_POWER_ACTIVE =         0x00080000;
        public const UInt32 STATUS_MODULE_POWER_ACTIVE =            0x00100000;
        public const UInt32 STATUS_CSM_ONLINE =                     0x00200000;
        public const UInt32 STATUS_EFT_ACTIVE =                     0x00400000;
        public const UInt32 STATUS_CBL_VALID =                      0x00800000;
        public const UInt32 STATUS_REVISION_REQUEST =               0x01000000;
        public const UInt32 STATUS_EQUALIZER_ENABLED =              0x02000000;
        public const UInt32 STATUS_EQUALIZER_PRESENT =              0x04000000;
        public const UInt32 STATUS_PRECHARGER_PRESENT =             0x08000000;
        public const UInt32 STATUS_EQUALIZATION_HARDWARE_DETECTED = 0x10000000;
        public const UInt32 STATUS_MFG_MODE_DETECTED =              0x80000000;

        public const UInt32 STATUS_ALL_BITS =                       0x9FFFFFFF;

        public const UInt32 STATUS_ALARM_BITS = STATUS_CRITICAL_EPO_ALARM | STATUS_WARNING_LOS_ALARM | STATUS_WARNING_ALARM;
        public const UInt32 STATUS_RACK_CLOSED_BITS = STATUS_HIGH_SIDE_CONTACTOR_CLOSED | STATUS_LOW_SIDE_CONTACTOR_CLOSED | STATUS_CONTACTOR_POWER_ACTIVE;

        public const int DEFAULT_STATUS_POLL_DELAY = 20;

    }  // End of class statusRegister


}
 
                           
