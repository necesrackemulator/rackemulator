﻿//*************************************************************************
//
// Copyright © 2014 NEC Energy Solutions, Inc. All rights reserved.
//
// NEC Energy Solutions Proprietary Information.
// This document contains proprietary and confidential information and is
// tendered for information and evaluation purposes only; no copy or other
// use or dissemination by a third party can be made of this information
// without the express written permission of NEC Energy Solutions.
//
//*************************************************************************

using System;
using Modbus.Device;
using SQA_config;
using SQA_utils;

namespace ModBusMonitor
{
    public class writeCmds
    {
        public mbReturnCode writeRackEnable (modBusDev modBus, ModbusIpMaster master)
        {
            mbReturnCode returnCode = mbReturnCode.NO_ERROR;

            int rackNumber;
            UInt32 writeValue = Constant.ZERO;

            for (rackNumber = rackConfig.userMinRack; rackNumber <= rackConfig.userMaxRack; rackNumber++)
            {
                writeValue = writeValue | (UInt32) (Constant.ONE << (rackNumber - Constant.ONE));
            }
            
            if (!modBus.writeRegister32 (master, modBusAddr.BITFIELD_RACK_ENBLE_ADDR, writeValue))
            {
                returnCode = mbReturnCode.MB_ERROR;
            }

            return returnCode;
        }

        public mbReturnCode writeTargetVoltage (modBusDev modBus, ModbusIpMaster master, UInt32 voltageValue)
        {
            mbReturnCode returnCode = mbReturnCode.NO_ERROR;

            if (!modBus.writeRegister32(master, modBusAddr.WRITE_TARGET_VOLTAGE_ADDR, voltageValue))
            {
                returnCode = mbReturnCode.MB_ERROR;
            }

            return returnCode;
        }

        public mbReturnCode writeTimeUTC(modBusDev modBus, ModbusIpMaster master)
        {
            mbReturnCode returnCode = mbReturnCode.NO_ERROR;

            UInt32 timeValue;

            timeValue = timeUtils.elapsedTimeUTC();

            if (!modBus.writeRegister32(master, modBusAddr.WRITE_UTC_TIME_ADDR, timeValue))
            {
                returnCode = mbReturnCode.MB_ERROR;
            }

            return returnCode;
        }

        public mbReturnCode toggleZoneControl (modBusDev modBus, ModbusIpMaster master, ushort zoneControlBit)
        {
            mbReturnCode returnCode = mbReturnCode.NO_ERROR;

            // Set the bit to assert the value.
            modBusUtils.enableZoneControlBit (zoneControlBit);
            if ((returnCode = modBusUtils.sendZoneControl (modBus, master)) == mbReturnCode.NO_ERROR)
            {
                // Delay to allow SIB to respond
                timeUtils.delaySeconds(Constant.TWO);

                // Reset the bit to de-assert the value.
                modBusUtils.disableZoneControlBit(zoneControlBit);
                returnCode = modBusUtils.sendZoneControl(modBus, master);
            }

            return returnCode;
        }

       
    } // End of class writeCmds
}
 
                           
