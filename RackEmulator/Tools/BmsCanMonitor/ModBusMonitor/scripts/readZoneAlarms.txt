// File Name: readZoneAlarms.txt
//
// Function:  Reads Registers containing zone alarms data.
//
// Caveats:   Assumes IP Address is entered.
//
modbus
read bms critical -on
read bms warning -on
read zone alarm -on
up
//
// End of readZoneAlarms.txt