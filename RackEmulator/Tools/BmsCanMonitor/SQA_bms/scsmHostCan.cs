﻿//*************************************************************************
//
// Copyright © 2014 NEC Energy Solutions, Inc. All rights reserved.
//
// NEC Energy Solutions Proprietary Information.
// This document contains proprietary and confidential information and is
// tendered for information and evaluation purposes only; no copy or other
// use or dissemination by a third party can be made of this information
// without the express written permission of NEC Energy Solutions.
//
//*************************************************************************

using System;
using System.Threading;
using SQA_can;
using SQA_format;
using SQA_config;
using SQA_utils;

namespace SQA_bms
{
    public class optimusHostCanOperations : hostCanOperations
    {
        
        const UInt16 CANOPEN_INDEX_READ_MFG_DEVICE_NAME     = 0x1008;
        const UInt16 CANOPEN_INDEX_READ_MFG_HW_VERSION      = 0x1009;
        const UInt16 CANOPEN_INDEX_READ_MFG_SW_VERSION      = 0x100A;
        const UInt16 CANOPEN_INDEX_READ_IDENTITY_OBJECT     = 0x1018;
        const UInt16 CANOPEN_INDEX_READ_SCSM_SERIAL_NUMBER  = 0x2000;
        const UInt16 CANOPEN_INDEX_INTERFACE_REVISION       = 0x2001;
        const UInt16 CANOPEN_INDEX_READ_SOC                 = 0x2002;
        const UInt16 CANOPEN_INDEX_READ_RACK_VOLTAGE        = 0x2003;
        const UInt16 CANOPEN_INDEX_READ_MAIN_BUS_VOLTAGE    = 0x2004;
        const UInt16 CANOPEN_INDEX_READ_AUX_BUS_VOLTAGE     = 0x2005;
        const UInt16 CANOPEN_INDEX_READ_RACK_CURRENT        = 0x2006;
        const UInt16 CANOPEN_INDEX_READ_CHARGE_CURRENT_LIMIT = 0x2007;
        const UInt16 CANOPEN_INDEX_READ_DISCHARGE_CURRENT_LIMIT = 0x2008;
        const UInt16 CANOPEN_INDEX_READ_CELL_VOLTAGE        = 0x2009;
        const UInt16 CANOPEN_INDEX_READ_BALANCE             = 0x2010;
        const UInt16 CANOPEN_INDEX_IDENTIFY_MODULE_VOLTAGE  = 0x200A;
        const UInt16 CANOPEN_INDEX_READ_MODULE_TEMP         = 0x200B;
        const UInt16 CANOPEN_INDEX_IDENTIFY_MODULE_TEMP     = 0x200C;
        const UInt16 CANOPEN_INDEX_READ_ERROR_REG           = 0x200D;
        const UInt16 CANOPEN_INDEX_READ_WARNING_REG         = 0x200E;
        const UInt16 CANOPEN_INDEX_READ_STATUS_REG          = 0x200F;
        const UInt16 CANOPEN_INDEX_READ_MOD_PER_STRING      = 0x2011;
        const UInt16 CANOPEN_INDEX_READ_MOD_STATUS_REG      = 0x2013;
        const UInt16 CANOPEN_INDEX_READ_CONTACTOR_REG       = 0x2014;
        const UInt16 CANOPEN_INDEX_READ_BMS_SERIAL_NUMBER   = 0x2000;
        const UInt16 CANOPEN_INDEX_BOOT_REV                 = 0x2020;
        const UInt16 CANOPEN_INDEX_APP_REV                  = 0x2021;
        const UInt16 CANOPEN_INDEX_READ_CRC                 = 0x2024;
        const UInt16 CANOPEN_INDEX_READ_MODULE_CELL_VOLTAGE = 0x2200;
        
        public optimusHostCanOperations(int maxRack) : base(maxRack)
        {
        }

        public override canReturnCode selectMBB(hostCanOperations hostCanOps, int moduleNumber)
        {

            byte[] cmdBuffer = new byte[CanOpen.CANOPEN_MAX_CMD_BYTES];

            string hdrMsg = String.Format("Set MBB {0} Index", moduleNumber);

            return canOpenXfer.writeHostCanRackCmd(rackAddress, hdrMsg, CANOPEN_INDEX_SET_MBB_INDEX, (byte) moduleNumber, cmdBuffer);
        }
        
        public override canReturnCode readMbbCellVoltages(hostCanOperations hostCanOps, string hdrMsg, int moduleNumber, byte[] voltBuffer, int maxCellCount)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            // Select the MBB for which we want Cell voltage data.
            if ((returnCode = selectMBB(hostCanOps, moduleNumber)) == canReturnCode.NO_ERROR)
            {
                returnCode = readCellVoltagesSDO (hostCanOps, moduleNumber, voltBuffer, maxCellCount);
            }
            return returnCode;
        }

        public override canReturnCode readStatusRegister (ref UInt32 valueStatusReg)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            byte[] resultBuffer = new byte[CanOpen.CANOPEN_MAX_DATA_BYTES];

            string hdrMsg = "Read Status Register";

            returnCode = canOpenXfer.readHostCanRackResult(rackAddress, hdrMsg, CANOPEN_INDEX_READ_STATUS_REG, CanOpen.SUBINDEX_NULL, resultBuffer);
            if (returnCode == canReturnCode.NO_ERROR)
            {
                valueStatusReg = CanOpen.extractRxMsgUInt32(resultBuffer);
                statusBits = valueStatusReg;
            }
            return returnCode;
        }

        public override canReturnCode readErrorRegister(ref UInt32 valueErrorReg)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            string hdrMsg = "Read Error Register";
            byte[] resultBuffer = new byte[CanOpen.CANOPEN_MAX_DATA_BYTES];

            returnCode = canOpenXfer.readHostCanRackResult(rackAddress, hdrMsg, CANOPEN_INDEX_READ_ERROR_REG, CanOpen.SUBINDEX_NULL, resultBuffer);
            if (returnCode == canReturnCode.NO_ERROR)
            {
                valueErrorReg = CanOpen.extractRxMsgUInt32(resultBuffer);
                errorBits = valueErrorReg;
            }

            return returnCode;
        }

        public override canReturnCode readWarningRegister(ref UInt32 valueWarningReg)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            byte[] resultBuffer = new byte[CanOpen.CANOPEN_MAX_DATA_BYTES];

            string hdrMsg = "Read Warning Register";

            returnCode = canOpenXfer.readHostCanRackResult(rackAddress, hdrMsg, CANOPEN_INDEX_READ_WARNING_REG, CanOpen.SUBINDEX_NULL, resultBuffer);

            if (returnCode == canReturnCode.NO_ERROR)
            {
                valueWarningReg = CanOpen.extractRxMsgUInt32(resultBuffer);
                warnBits = valueWarningReg;
            }
            return returnCode;
        }

        public override canReturnCode readModuleStatusRegister(int moduleNumber, ref UInt16 statusBits)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            byte[] resultBuffer = new byte[CanOpen.CANOPEN_MAX_DATA_BYTES];

            string hdrMsg = "Read Module Status";

            returnCode = canOpenXfer.readHostCanRackResult(rackAddress, hdrMsg, CANOPEN_INDEX_READ_MOD_STATUS_REG, (byte) moduleNumber, resultBuffer);

            if (returnCode == canReturnCode.NO_ERROR)
            {
                statusBits = CanOpen.extractRxMsgLSW(resultBuffer);
              
                moduleStatusReg.mbbStatusBits[moduleNumber - Constant.ONE] = statusBits;
            }

            return returnCode;
        }

        public override canReturnCode readInterfaceRevision(string hdrMsg, ref UInt16 intRevision)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            byte[] resultBuffer = new byte[CanOpen.CANOPEN_MAX_DATA_BYTES];

            returnCode = canOpenXfer.readHostCanRackResult(rackAddress, hdrMsg, CANOPEN_INDEX_INTERFACE_REVISION, CanOpen.SUBINDEX_NULL, resultBuffer);

            if (returnCode == canReturnCode.NO_ERROR)
            {
                intRevision = CanOpen.extractRxMsgLSW(resultBuffer);
            }
            return returnCode;
        }

        public override canReturnCode readModuleCount(string hdrMsg, ref UInt16 moduleCnt)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            byte[] resultBuffer = new byte[CanOpen.CANOPEN_MAX_DATA_BYTES];

            returnCode = canOpenXfer.readHostCanRackResult(rackAddress, hdrMsg, CANOPEN_INDEX_READ_MOD_PER_STRING, CanOpen.SUBINDEX_NULL, resultBuffer);

            if (returnCode == canReturnCode.NO_ERROR)
            {
                moduleCnt = CanOpen.extractRxMsgLSW(resultBuffer);
            }
            return returnCode;
        }

        public override canReturnCode readBootRev(string hdrMsg, ref UInt16 majorRev, ref UInt16 minorRev)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            const int READ_MINOR_REV = Constant.ONE;
            const int READ_MAJOR_REV = Constant.TWO;

            byte[] resultBuffer = new byte[CanOpen.CANOPEN_MAX_DATA_BYTES];

            returnCode = canOpenXfer.readHostCanRackResult(rackAddress, hdrMsg, CANOPEN_INDEX_BOOT_REV, READ_MINOR_REV, resultBuffer);

            if (returnCode == canReturnCode.NO_ERROR)
            {
                minorRev = CanOpen.extractRxMsgLSW(resultBuffer);

                returnCode = canOpenXfer.readHostCanRackResult(rackAddress, hdrMsg, CANOPEN_INDEX_BOOT_REV, READ_MAJOR_REV, resultBuffer);
                if (returnCode == canReturnCode.NO_ERROR)
                {
                    majorRev = CanOpen.extractRxMsgLSW(resultBuffer);

                }

            }

            return returnCode;
        }

        public override canReturnCode readAppRev(string hdrMsg, ref UInt16 majorRev, ref UInt16 minorRev, ref UInt16 buildRev)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            const int READ_BUILD_REV = Constant.ONE;
            const int READ_MINOR_REV = Constant.TWO;
            const int READ_MAJOR_REV = Constant.THREE;

            byte[] resultBuffer = new byte[CanOpen.CANOPEN_MAX_DATA_BYTES];

            returnCode = canOpenXfer.readHostCanRackResult(rackAddress, hdrMsg, CANOPEN_INDEX_APP_REV, READ_BUILD_REV, resultBuffer);

            if (returnCode == canReturnCode.NO_ERROR)
            {
                buildRev = CanOpen.extractRxMsgLSW (resultBuffer);
                returnCode = canOpenXfer.readHostCanRackResult(rackAddress, hdrMsg, CANOPEN_INDEX_APP_REV, READ_MINOR_REV, resultBuffer);
                if (returnCode == canReturnCode.NO_ERROR)
                {
                    minorRev = CanOpen.extractRxMsgLSW(resultBuffer);
                    returnCode = canOpenXfer.readHostCanRackResult(rackAddress, hdrMsg, CANOPEN_INDEX_APP_REV, READ_MAJOR_REV, resultBuffer);
                    if (returnCode == canReturnCode.NO_ERROR)
                    {
                        majorRev = CanOpen.extractRxMsgLSW(resultBuffer);
                    }
                }
            }

            return returnCode;
        }


        public override canReturnCode readCRC(string hdrMsg, ref UInt32 readCRC)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            byte[] resultBuffer = new byte[CanOpen.CANOPEN_MAX_DATA_BYTES];

            returnCode = canOpenXfer.readHostCanRackResult(rackAddress, hdrMsg, CANOPEN_INDEX_READ_CRC, CanOpen.SUBINDEX_NULL, resultBuffer);

            if (returnCode == canReturnCode.NO_ERROR)
            {
                readCRC = CanOpen.extractRxMsgUInt32(resultBuffer);
            }

            return returnCode;
        }

        /************************************************************************************************************/
        /*                                                                                                          */
        /*  Function: Read the Contactor Status bits under CANopen.                                                 */
        /*                                                                                                          */
        /*  Caveats:  None.                                                                                         */
        /*                                                                                                          */
        /************************************************************************************************************/
        public override canReturnCode readContactorRegister(ref UInt32 actContactRegister)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            byte[] resultBuffer = new byte[CanOpen.CANOPEN_MAX_DATA_BYTES];
            string hdrMsg = "Read Contactor";

            returnCode = canOpenXfer.readHostCanRackResult(rackAddress, hdrMsg, CANOPEN_INDEX_READ_CONTACTOR_REG, CanOpen.SUBINDEX_NULL, resultBuffer);

            if (returnCode == canReturnCode.NO_ERROR)
            {
                actContactRegister = CanOpen.extractRxMsgLSW(resultBuffer);
                contactBits = actContactRegister;
            }
            return returnCode;
        }

        public override canReturnCode readRackCurrent(string hdrMsg, ref UInt16 rackCurrent)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            byte[] resultBuffer = new byte[CanOpen.CANOPEN_MAX_DATA_BYTES];

            returnCode = canOpenXfer.readHostCanRackResult(rackAddress, hdrMsg, CANOPEN_INDEX_READ_RACK_CURRENT, CanOpen.SUBINDEX_NULL, resultBuffer);

            if (returnCode == canReturnCode.NO_ERROR)
            {
                rackCurrent = CanOpen.extractRxMsgLSW(resultBuffer);
            }

            return returnCode;
        }

        public override canReturnCode readRackVoltage(string hdrMsg, ref UInt16 rackVoltage)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            byte[] resultBuffer = new byte[CanOpen.CANOPEN_MAX_DATA_BYTES];

            returnCode = canOpenXfer.readHostCanRackResult(rackAddress, hdrMsg, CANOPEN_INDEX_READ_RACK_VOLTAGE, CanOpen.SUBINDEX_NULL, resultBuffer);

            if (returnCode == canReturnCode.NO_ERROR)
            {
                rackVoltage = CanOpen.extractRxMsgLSW(resultBuffer);
            }

            return returnCode;
        }

        public override canReturnCode readMainBusVoltage(string hdrMsg, ref UInt16 mainBusVoltage)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            byte[] resultBuffer = new byte[CanOpen.CANOPEN_MAX_DATA_BYTES];

            returnCode = canOpenXfer.readHostCanRackResult(rackAddress, hdrMsg, CANOPEN_INDEX_READ_MAIN_BUS_VOLTAGE, CanOpen.SUBINDEX_NULL, resultBuffer);

            if (returnCode == canReturnCode.NO_ERROR)
            {
                mainBusVoltage = CanOpen.extractRxMsgLSW(resultBuffer);
            }
            return returnCode;
        }


        public override canReturnCode readAuxBusVoltage(string hdrMsg, ref UInt16 auxBusVoltage)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            byte[] resultBuffer = new byte[CanOpen.CANOPEN_MAX_DATA_BYTES];

            returnCode = canOpenXfer.readHostCanRackResult(rackAddress, hdrMsg, CANOPEN_INDEX_READ_AUX_BUS_VOLTAGE, CanOpen.SUBINDEX_NULL, resultBuffer);

            if (returnCode == canReturnCode.NO_ERROR)
            {
                auxBusVoltage = CanOpen.extractRxMsgLSW(resultBuffer);
            }
            return returnCode;
        }

        public override canReturnCode readChargeCurrentLimit(string hdrMsg, ref UInt16 chargeCurrent)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            byte[] resultBuffer = new byte[CanOpen.CANOPEN_MAX_DATA_BYTES];

            returnCode = canOpenXfer.readHostCanRackResult(rackAddress, hdrMsg, CANOPEN_INDEX_READ_CHARGE_CURRENT_LIMIT, CanOpen.SUBINDEX_NULL, resultBuffer);

            if (returnCode == canReturnCode.NO_ERROR)
            {
                chargeCurrent = CanOpen.extractRxMsgLSW(resultBuffer);
            }

            return returnCode;
        }

        public override canReturnCode readDischargeCurrentLimit(string hdrMsg, ref UInt16 dischargeCurrent)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            byte[] resultBuffer = new byte[CanOpen.CANOPEN_MAX_DATA_BYTES];

            returnCode = canOpenXfer.readHostCanRackResult(rackAddress, hdrMsg, CANOPEN_INDEX_READ_DISCHARGE_CURRENT_LIMIT, CanOpen.SUBINDEX_NULL, resultBuffer);

            if (returnCode == canReturnCode.NO_ERROR)
            {
                dischargeCurrent = CanOpen.extractRxMsgLSW(resultBuffer);
            }

            return returnCode;
        }

        public override canReturnCode readSOC(string hdrMsg, ref UInt16 minSOC, ref UInt16 maxSOC, ref UInt16 avgSOC)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            byte[] resultBuffer = new byte[CanOpen.CANOPEN_MAX_DATA_BYTES];

            const int READ_MIN_SOC = Constant.ONE;
            const int READ_MAX_SOC = Constant.TWO;
            const int READ_AVG_SOC = Constant.THREE;

            returnCode = canOpenXfer.readHostCanRackResult(rackAddress, hdrMsg, CANOPEN_INDEX_READ_SOC, READ_MIN_SOC, resultBuffer);

            if (returnCode == canReturnCode.NO_ERROR)
            {
                minSOC = CanOpen.extractRxMsgLSW(resultBuffer);
                returnCode = canOpenXfer.readHostCanRackResult(rackAddress, hdrMsg, CANOPEN_INDEX_READ_SOC, READ_MAX_SOC, resultBuffer);
                if (returnCode == canReturnCode.NO_ERROR)
                {
                    maxSOC = CanOpen.extractRxMsgLSW(resultBuffer);
                    returnCode = canOpenXfer.readHostCanRackResult(rackAddress, hdrMsg, CANOPEN_INDEX_READ_SOC, READ_AVG_SOC, resultBuffer);
                    if (returnCode == canReturnCode.NO_ERROR)
                    {
                        avgSOC = CanOpen.extractRxMsgLSW(resultBuffer);
                    }
                }
            }

            return returnCode;

        }

        public override canReturnCode readCellVoltage(string hdrMsg, ref UInt16 minValue, ref UInt16 maxValue, ref UInt16 avgValue)
        {
            return readMinMaxAvg(hdrMsg, CANOPEN_INDEX_READ_CELL_VOLTAGE, ref minValue, ref maxValue, ref avgValue);
        }

        public override canReturnCode readModuleTemp(string hdrMsg, ref UInt16 minValue, ref UInt16 maxValue, ref UInt16 avgValue)
        {
            return readMinMaxAvg (hdrMsg, CANOPEN_INDEX_READ_MODULE_TEMP, ref minValue, ref maxValue, ref avgValue);
        }

   
        public override canReturnCode readBalance(string hdrMsg, ref UInt16 balanceTarget, ref UInt16 balanceCount)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            byte[] resultBuffer = new byte[CanOpen.CANOPEN_MAX_DATA_BYTES];

            returnCode = canOpenXfer.readHostCanRackResult(rackAddress, hdrMsg, CANOPEN_INDEX_READ_BALANCE, CanOpen.SUBINDEX_NULL, resultBuffer);

            if (returnCode == canReturnCode.NO_ERROR)
            {
                balanceTarget = CanOpen.extractRxMsgLSW(resultBuffer);
                balanceCount = CanOpen.extractRxMsgMSW(resultBuffer);
            }
            return returnCode;
        }

        public override canReturnCode readVoltagePerModule(int moduleNumber, ref UInt16 minCellVoltage, ref UInt16 maxCellVoltage, ref UInt16 avgCellVoltage)
        {
            byte[] resultBuffer = new byte[CanOpen.CANOPEN_MAX_DATA_BYTES];

            // Build message with module number in sub-index.
            string hdrMsg = String.Format("Module {0} Cell Voltage", moduleNumber);

            return readMinMaxAvg (hdrMsg, (byte) (CANOPEN_INDEX_READ_MODULE_CELL_VOLTAGE+(moduleNumber - Constant.ONE)), ref minCellVoltage, ref maxCellVoltage, ref avgCellVoltage);
       
        }

        public canReturnCode readMinMaxAvg(string hdrMsg, UInt16 canIndex, ref UInt16 minValue, ref UInt16 maxValue, ref UInt16 avgValue)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            byte[] resultBuffer = new byte[CanOpen.CANOPEN_MAX_DATA_BYTES];

            string minHdrMsg = String.Format ("Min {0}", hdrMsg);
            string maxHdrMsg = String.Format ("Max {0}", hdrMsg);
            string avgHdrMsg = String.Format ("Avg {0}", hdrMsg);

            const int READ_MIN = Constant.ONE;
            const int READ_MAX = Constant.TWO;
            const int READ_AVG = Constant.THREE;

            returnCode = canOpenXfer.readHostCanRackResult(rackAddress, minHdrMsg, canIndex, READ_MIN, resultBuffer);

            if (returnCode == canReturnCode.NO_ERROR)
            {
                minValue = CanOpen.extractRxMsgLSW(resultBuffer);

                returnCode = canOpenXfer.readHostCanRackResult(rackAddress, maxHdrMsg, canIndex, READ_MAX, resultBuffer);
                if (returnCode == canReturnCode.NO_ERROR)
                {
                    maxValue = CanOpen.extractRxMsgLSW(resultBuffer);

                    returnCode = canOpenXfer.readHostCanRackResult(rackAddress, avgHdrMsg, canIndex, READ_AVG, resultBuffer);
                    if (returnCode == canReturnCode.NO_ERROR)
                    {
                        avgValue = CanOpen.extractRxMsgLSW(resultBuffer);
                    }
                }
            }

            return returnCode;
        }

        public override canReturnCode readIdentifyModuleTemp(string hdrMsg, ref byte minTempModule, ref byte minTempSensor, ref byte maxTempModule, ref byte maxTempSensor)
        {
            return identifyModule (hdrMsg, CANOPEN_INDEX_IDENTIFY_MODULE_TEMP, ref minTempModule, ref minTempSensor, ref maxTempModule, ref maxTempSensor);
        }

        public override canReturnCode readIdentifyModuleVoltage(string hdrMsg, ref byte minVoltModule, ref byte minVoltLoc, ref byte maxVoltModule, ref byte maxVoltLoc)
        {
            return identifyModule(hdrMsg, CANOPEN_INDEX_IDENTIFY_MODULE_VOLTAGE, ref minVoltModule, ref minVoltLoc, ref maxVoltModule, ref maxVoltLoc);
        }

        private canReturnCode identifyModule (string hdrMsg, UInt16 canIndex, ref byte minModule, ref byte minLocation, ref byte maxModule, ref byte maxLocation)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            byte[] resultBuffer = new byte[CanOpen.CANOPEN_MAX_DATA_BYTES];

            const int MIN_MODULE = Constant.ONE;
            const int MIN_LOCATION = Constant.TWO;
            const int MAX_MODULE = Constant.THREE;
            const int MAX_LOCATION = Constant.FOUR;

            returnCode = canOpenXfer.readHostCanRackResult(rackAddress, hdrMsg, canIndex, MIN_MODULE, resultBuffer);
            if (returnCode == canReturnCode.NO_ERROR)
            {
                minModule = resultBuffer[CanOpen.RX_MSG_LSW_LOWER];

                returnCode = canOpenXfer.readHostCanRackResult(rackAddress, hdrMsg, canIndex, MIN_LOCATION, resultBuffer);
                if (returnCode == canReturnCode.NO_ERROR)
                {
                    minLocation = resultBuffer[CanOpen.RX_MSG_LSW_LOWER];

                    returnCode = canOpenXfer.readHostCanRackResult(rackAddress, hdrMsg, canIndex, MAX_MODULE, resultBuffer);
                    if (returnCode == canReturnCode.NO_ERROR)
                    {
                        maxModule = resultBuffer[CanOpen.RX_MSG_LSW_LOWER];

                        returnCode = canOpenXfer.readHostCanRackResult(rackAddress, hdrMsg, canIndex, MAX_LOCATION, resultBuffer);

                        if (returnCode == canReturnCode.NO_ERROR)
                        {
                            maxLocation = resultBuffer[CanOpen.RX_MSG_LSW_LOWER];
                        }
                    }
                }
            }

            return returnCode;
        }
        public override canReturnCode readIdentityObject (hostCanOperations hostCanOps, string hdrMsg, ref UInt32 vendorID, ref UInt32 productCode, ref UInt32 revisionNumber, ref UInt32 serialNumber)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            byte[] resultBuffer = new byte[CanOpen.CANOPEN_MAX_DATA_BYTES];

            const int READ_VENDOR_ID = Constant.ONE;
            const int READ_PRODUCT_CODE = Constant.TWO;
            const int READ_REVISION_NUMBER = Constant.THREE;
            const int READ_SERIAL_NUMBER = Constant.FOUR;

            returnCode = canOpenXfer.readHostCanRackResult(rackAddress, hdrMsg, CANOPEN_INDEX_READ_IDENTITY_OBJECT, READ_VENDOR_ID, resultBuffer);
            if (returnCode == canReturnCode.NO_ERROR)
            {
                vendorID = CanOpen.extractRxMsgUInt32 (resultBuffer);
                returnCode = canOpenXfer.readHostCanRackResult(rackAddress, hdrMsg, CANOPEN_INDEX_READ_IDENTITY_OBJECT, READ_PRODUCT_CODE, resultBuffer);
                if (returnCode == canReturnCode.NO_ERROR)
                {
                    productCode = CanOpen.extractRxMsgUInt32(resultBuffer);

                    returnCode = canOpenXfer.readHostCanRackResult(rackAddress, hdrMsg, CANOPEN_INDEX_READ_IDENTITY_OBJECT, READ_REVISION_NUMBER, resultBuffer);
                    if (returnCode == canReturnCode.NO_ERROR)
                    {
                        revisionNumber = CanOpen.extractRxMsgUInt32(resultBuffer);

                        returnCode = canOpenXfer.readHostCanRackResult(rackAddress, hdrMsg, CANOPEN_INDEX_READ_IDENTITY_OBJECT, READ_SERIAL_NUMBER, resultBuffer);

                        if (returnCode == canReturnCode.NO_ERROR)
                        {
                            serialNumber = CanOpen.extractRxMsgUInt32(resultBuffer);
                        }
                    }
                }
            }

            return returnCode;
        }

        /************************************************************************************************************/
        /*                                                                                                          */
        /*  Function: Read the BCM Serial Number over CANopen.                                                      */
        /*                                                                                                          */
        /*  Caveats:  None.                                                                                         */
        /*                                                                                                          */
        /************************************************************************************************************/
        public override canReturnCode readBcmSerialNumber(hostCanOperations hostCanOps, string hdrMsg, byte[] serialBuffer)
        {
            return readAsciiString (hostCanOps, hdrMsg, serialBuffer, CANOPEN_INDEX_READ_SCSM_SERIAL_NUMBER);
        }

        /************************************************************************************************************/
        /*                                                                                                          */
        /*  Function: Read the BCM Serial Number over CANopen.                                                      */
        /*                                                                                                          */
        /*  Caveats:  None.                                                                                         */
        /*                                                                                                          */
        /************************************************************************************************************/
        public override canReturnCode readBmsSerialNumber(hostCanOperations hostCanOps, string hdrMsg, byte[] serialBuffer)
        {
            return readAsciiString (hostCanOps, hdrMsg, serialBuffer, CANOPEN_INDEX_READ_BMS_SERIAL_NUMBER);
        }

        /************************************************************************************************************/
        /*                                                                                                          */
        /*  Function: Read the BCM Serial Number over CANopen.                                                      */
        /*                                                                                                          */
        /*  Caveats:  None.                                                                                         */
        /*                                                                                                          */
        /************************************************************************************************************/
        public override canReturnCode readMfgDeviceName(hostCanOperations hostCanOps, string hdrMsg, byte[] nameBuffer)
        {
            return readAsciiString(hostCanOps, hdrMsg, nameBuffer, CANOPEN_INDEX_READ_MFG_DEVICE_NAME);
        }

        public override canReturnCode readMfgHwRev(hostCanOperations hostCanOps, string hdrMsg, byte[] hwRevBuffer)
        {
            return readAsciiString(hostCanOps, hdrMsg, hwRevBuffer, CANOPEN_INDEX_READ_MFG_HW_VERSION);
        }
        public override canReturnCode readMfgSwRev(hostCanOperations hostCanOps, string hdrMsg, byte[] swRevBuffer)
        {
            return readAsciiString(hostCanOps, hdrMsg, swRevBuffer, CANOPEN_INDEX_READ_MFG_SW_VERSION);
        }

  
    } // End of class optimushostCanOperations
}
 
                           
