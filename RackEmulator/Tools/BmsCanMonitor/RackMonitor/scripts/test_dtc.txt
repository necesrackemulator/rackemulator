// This is a dtc regression test file
time "test_dtc.txt start"        // Place current data/time in log file.
diag

// Test of Error Register

// Test Error Register b0: 
dtc close event history -DTC_MBB_TOO_FEW

// Test Error Register b1:  
dtc close event history -DTC_CSM_MISSING

// Test Error Register b2:  
dtc close event history -DTC_CELL_VMAX_SEVERE

// Test Error Register b3: 
dtc close event history -DTC_CELL_VMIN_SEVERE

// Test Error Register b4: 
dtc close event history -DTC_VBAT_VMAX

// Test Error Register b5: 
dtc close event history -DTC_VBAT_VMIN

// Test Error Register b6: 
dtc close event history -DTC_CELL_VMAX_ERR

// Test Error Register b7: 
dtc close event history -DTC_CELL_TMAX2

// Test Error Register b8:  
dtc close event history -DTC_MAX_I2T

// Test Error Register b9: 
dtc close event history -DTC_FAST_FUSE_CURRENT

// Test Error Register b10: 
dtc close event history -DTC_EPO

// Test Error Register b12: 
dtc close event history -DTC_HVIL

// Test Error Register b13: 
dtc close event history -DTC_CTR_FAULT_INVALID_HEARTBEAT

// Test Error Register b14: 
dtc close event history -DTC_AIN_HPWR_FAIL_HI

// Test Error Register b22:  
dtc close event history -DTC_CELL_VMIN_ERR
 
// Test Error Register b28: 
dtc close event history -DTC_CSM_BUS_CONNECTED

// Test Error Register b29: 
dtc close event history -DTC_CTR_FAULT_LATCH
 
// Test Error Register b30: 
dtc close event history -DTC_AIN_HPWR_FAIL_LO

// Test of Warning Register

// Test Warning Register b0: 
dtc close event history -DTC_CELL_VMAX
 
// Test Warning Register b1: 
dtc close event history -DTC_CELL_VMIN
 
// Test Warning Register b2: 
dtc close event history -DTC_MOD_VOLTAGE_COMPARE

// Test Warning Register b3: 
dtc close event history -DTC_CELL_T_ERR
  
// Test Warning Register b4: 
dtc close event history -DTC_CELL_TMAX1

// Test Warning Register b5: 
dtc close event history -DTC_CELL_TMIN
 
// Test Warning Register b6: 
dtc close event history -DTC_IBAT_MISMATCH
 
// Test Warning Register b7:  
dtc close event history -DTC_LVBAT_HI
  
// Test Warning Register b8: 
dtc close event history -DTC_AIN_HPWR_OOR_HI

// Test Warning Register b9: 
dtc close event history -DTC_AIN_5V_PWR_OOR_HI
 
// Test Warning Register b10: 
dtc close event history -DTC_CSM_DTC_SET
 
// Test Warning Register b20: 
dtc close event history -DTC_WARNING_I2T

// Test Warning Register b21:   
dtc close event history -DTC_EQUALIZE_START_FAIL

// Test Warning Register b22:  
dtc close event history -DTC_EQUALIZE_OOR_VOLT

// Test Warning Register b23: 
dtc close event history -DTC_LVBAT_LO

// Test Warning Register b24: 
dtc close event history -DTC_AIN_HPWR_OOR_LO
 
// Test Warning Register b25: 
dtc close event history -DTC_AIN_5V_PWR_OOR_LO

// Test Warning Register b26:  
dtc close event history -DTC_PRECHRGR_TIMEOUT

up
time "Done test_dtc.txt"    // Place current data/time in log file.
// Testing for test_dtc.txt complete.
