// This is a datalog read regression test file
time  "datalog_read.txt start"      // Place current data/time in log file.
auxcan
read app rev
read rack number
up
bmc log
display histogram
display soc
display critical hdr
display chronicle hdr
display critical 1
display chronicle 1
up
mbb log
display histogram
up
// Place current data/time in log file.
time "datalog_read.txt end"  
// Testing for datalog_read.txt complete.