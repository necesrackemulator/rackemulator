// This file is for extracting hardware and software revision info
time "config.txt start"
info app
info os
config can
config hw
config sw
open -auxcan
auxcan
poll device
read rack number
read app rev
read app crc
read boot rev
interface revision
read modcount
up
open -modcan
modcan
read bcm app rev
read bcm boot rev
read bcm part number
read bcm serial number
read mbb app rev
read mbb boot rev
read mbb part number
read mbb serial number
read mbb heartbeat
read mbb address
up
csm
read csm app rev
read csm boot rev
read csm part number
read csm serial number
read csm heartbeat
up
time "config.txt end"
// That is all