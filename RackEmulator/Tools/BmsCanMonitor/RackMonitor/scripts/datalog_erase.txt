// This is a datalog erase regression test file
time  "datalog_erase.txt start"      // Place current data/time in log file
auxcan
read app rev
read rack number
read modcount
up
bcm log
write datalog

// Read all of the data log
display histogram
display soc
display critical hdr
display chronicle hdr
up
mbb log
display histogram
up
bcm log
erase all

// Read all of the data log
display bcm histogram -on
display soc
display critical hdr
display chronicle hdr
up
mbb log
display histogram -on
up
bcm log
write datalog

// Read all of the data log
display histogram -on
display soc
display critical hdr
display chronicle hdr
up
mbb log
display histogram -on
up

// Check erase of MBB 1
mbb 1
bcm log
erase mbb
display histogram -on
up
mbb 2-22
bcm log
display histogram -diff
up

// Check erase of MBB 2
mbb 2
bcm log
erase mbb
display histogram -on
up
mbb 3-22
bcm log
display histogram -diff
up

// Check erase of MBB 3
mbb 3
bcm log
erase mbb
display histogram -on
up
mbb 4-22
bcm log
display histogram -diff
up

// Check erase of MBB 4
mbb 4
bcm log
erase mbb
display histogram -on
up
mbb 5-22
bcm log
display histogram -diff
up

// Check erase of MBB 5
mbb 5
bcm log
erase mbb
display histogram -on
up
mbb 6-22
bcm log
display histogram -diff
up

// Check erase of MBB 6
mbb 6
bcm log
erase mbb
display histogram -on
up
mbb 7-22
bcm log
display histogram -diff
up

// Check erase of MBB 7
mbb 7
bcm log
erase mbb
display histogram -on
up
mbb 8-22
bcm log
display histogram -diff
up

// Check erase of MBB 8
mbb 8
bcm log
erase mbb
display histogram -on
up
mbb 9-22
bcm log
display histogram -diff
up

// Check erase of MBB 9
mbb 9
bcm log
erase mbb
display histogram -on
up
mbb 10-22
bcm log
display histogram -diff
up

// Check erase of MBB 10
mbb 10
bcm log
erase mbb
display histogram -on
up
mbb 11-22
bcm log
display histogram -diff
up

// Check erase of MBB 11
mbb 11
bcm log
erase mbb
display histogram -on
up
mbb 12-22
bcm log
display histogram -diff
up

// Check erase of MBB 12
mbb 12
bcm log
erase mbb
display histogram -on
up
mbb 13-22
bcm log
display histogram -diff
up

// Check erase of MBB 13
mbb 13
bcm log
erase mbb
display histogram -on
up
mbb 14-22
bcm log
display histogram -diff
up

// Check erase of MBB 14
mbb 14
bcm log
erase mbb
display histogram -on
up
mbb 15-22
bcm log
display histogram -diff
up

// Check erase of MBB 15
mbb 15
bcm log
erase mbb
display histogram -on
up
mbb 16-22
bcm log
display histogram -diff
up

// Check erase of MBB 16
mbb 16
bcm log
erase mbb
display histogram -on
up
mbb 17-22
bcm log
display histogram -diff
up

// Check erase of MBB 17
mbb 17
bcm log
erase mbb
display histogram -on
up
mbb 18-22
bcm log
display histogram -diff
up

// Check erase of MBB 18
mbb 18
bcm log
erase mbb
display histogram -on
up
mbb 19-22
bcm log
display histogram -diff
up

// Check erase of MBB 19
mbb 19
bcm log
erase mbb
display histogram -on
up
mbb 20-22
bcm log
display histogram -diff
up

// Check erase of MBB 20
mbb 20
bcm log
erase mbb
display histogram -on
up
mbb 21-22
bcm log
display histogram -diff
up

// Check erase of MBB 21
mbb 21
bcm log
erase mbb
display histogram -on
up
mbb 22
bcm log
display histogram -diff
up

// Check erase of MBB 22
mbb 22
bcm log
erase mbb
display histogram -on
up

modcan
read mbb serial number
up
time  "done datalog_erase.txt"   // Place current data/time in log file.
// Testing for datalog_erase.txt complete.