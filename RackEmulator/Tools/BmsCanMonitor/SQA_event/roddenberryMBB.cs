﻿//*************************************************************************
//
// Copyright © 2014 NEC Energy Solutions, Inc. All rights reserved.
//
// NEC Energy Solutions Proprietary Information.
// This document contains proprietary and confidential information and is
// tendered for information and evaluation purposes only; no copy or other
// use or dissemination by a third party can be made of this information
// without the express written permission of NEC Energy Solutions.
//
//*************************************************************************

using System;
using System.Linq;
using System.Text;
using SQA_can;
using SQA_formatEvent;
using SQA_utils;

namespace SQA_event
{
 
    public class roddenberryDataLogMBB : dataLogMBB
    {
        roddenberryModuleVoltage moduleVoltageProfile;
        roddenberryModuleTempPCB moduleTempPCB;
        roddenberryModuleTemp1 moduleTemperatureProfile1;
        roddenberryModuleTemp2 moduleTemperatureProfile2;
        roddenberryModuleTemp3 moduleTemperatureProfile3;
        roddenberryModuleTemp4 moduleTemperatureProfile4;
        roddenberryModuleMetaData moduleMetaDataProfile;
        roddenberryModuleData moduleDataProfile;
        roddenberryModuleBalancer moduleBalancer;
        roddenberryModuleCellVoltage moduleCellVoltageProfile;
        public roddenberryDataLogMBB (profileReadInterface readInterface, int maxModule, int maxCell) : base(readInterface, maxModule, maxCell)
        {
            moduleVoltageProfile = new roddenberryModuleVoltage(maxModule);
            moduleTempPCB = new roddenberryModuleTempPCB(maxModule);
            moduleTemperatureProfile1 = new roddenberryModuleTemp1(maxModule);
            moduleTemperatureProfile2 = new roddenberryModuleTemp2(maxModule);
            moduleTemperatureProfile3 = new roddenberryModuleTemp3(maxModule);
            moduleTemperatureProfile4 = new roddenberryModuleTemp4(maxModule);
            moduleMetaDataProfile = new roddenberryModuleMetaData(maxModule);
            moduleDataProfile = new roddenberryModuleData(maxModule);
            moduleBalancer = new roddenberryModuleBalancer(maxModule);
            moduleCellVoltageProfile = new roddenberryModuleCellVoltage(maxModule, maxCell);
        }

        public override canReturnCode displayHistogram(int moduleNumber, dispCntl displayControl)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            canReturnCode savedReturnCode = canReturnCode.NO_ERROR;

            string moduleName;
        
            moduleName = String.Format("MBB {0}", moduleNumber);

            // Display the MBB meta data.
            if ((returnCode = displayModuleMetaData(moduleNumber, displayControl)) != canReturnCode.NO_ERROR)
            {
                // Save the failing return code.
                savedReturnCode = returnCode;
            }

            // Display the module voltage.
            if ((returnCode = displayModuleVoltage(moduleNumber, displayControl)) != canReturnCode.NO_ERROR)
            {
                // Save the failing return code.
                savedReturnCode = returnCode;
            }

            // Display the MBB cell voltage histogram.
            if ((returnCode = displayModuleCellVoltages (moduleNumber, displayControl)) != canReturnCode.NO_ERROR)
            {
                // Save the failing return code.
                savedReturnCode = returnCode;
            }

            // Display the MBB balancer histogram.
            if ((returnCode = displayCellBalancer(moduleNumber, displayControl)) != canReturnCode.NO_ERROR)
            {
                // Save the failing return code.
                savedReturnCode = returnCode;
            }

            // Display the MBB temp PCB histogram.
            if ((returnCode = displayModuleTempPCB(moduleNumber, displayControl)) != canReturnCode.NO_ERROR)
            {
                // Save the failing return code.
                savedReturnCode = returnCode;
            }


            // Display the MBB temp histograms.
            if ((returnCode = displayTemp(moduleNumber, displayControl)) != canReturnCode.NO_ERROR)
            {
                // Save the failing return code.
                savedReturnCode = returnCode;
            }


            // If there was a failure in the saved return code, return the saved return code.
            if ((returnCode == canReturnCode.NO_ERROR) && (savedReturnCode != canReturnCode.NO_ERROR))
            {
                returnCode = savedReturnCode;
            }

            UserIO.outputInfo(String.Format("End of {0} Histogram Display.", moduleName));
            return returnCode;
        }


        //
        // This is the block of routines to handle Profile Module Voltage.
        //
        public override canReturnCode displayModuleVoltage(int moduleNumber, dispCntl displayControl)
        {
            return displayModuleProfileData(moduleNumber, displayControl, readModuleVoltage, moduleVoltageProfile);
        }

        public canReturnCode readModuleVoltage(byte[] readBuffer, int moduleNumber, string profileTitle)
        {
            return myReadInterface.readProfileMBB(moduleNumber, profileTitle, readBuffer, EEPROM_MBB_VOLTAGE_ADDR);
        }

        //
        // This is the block of routines to handle Temp PCB.
        //
        public override canReturnCode displayModuleTempPCB(int moduleNumber, dispCntl displayControl)
        {
            return displayModuleProfileData(moduleNumber, displayControl, readModuleTempPCB, moduleTempPCB);
        }

        public canReturnCode readModuleTempPCB(byte[] tempPcbBuffer, int moduleNumber, string profileTitle)
        {
            return myReadInterface.readProfileMBB(moduleNumber, profileTitle, tempPcbBuffer, EEPROM_MBB_TEMP_PCB_ADDR);
        }

        //
        // This is the block of routines to handle Temperature.
        //

        public override canReturnCode displayTemp(int moduleNumber, dispCntl displayControl)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            returnCode = displayModuleProfileData(moduleNumber, displayControl, readModuleTemp1, moduleTemperatureProfile1);

            if (returnCode == canReturnCode.NO_ERROR)
            {
                returnCode = displayModuleProfileData(moduleNumber, displayControl, readModuleTemp2, moduleTemperatureProfile2);
                if (returnCode == canReturnCode.NO_ERROR)
                {
                    returnCode = displayModuleProfileData(moduleNumber, displayControl, readModuleTemp3, moduleTemperatureProfile3);
                    if (returnCode == canReturnCode.NO_ERROR)
                    {
                        returnCode = displayModuleProfileData(moduleNumber, displayControl, readModuleTemp4, moduleTemperatureProfile4);
                    }
                }
            }

            return returnCode;
        }

        public canReturnCode readModuleTemp1(byte[] tempBuffer, int moduleNumber, string profileTitle)
        {
            return myReadInterface.readProfileMBB(moduleNumber, profileTitle, tempBuffer, EEPROM_MBB_TEMP1_ADDR);
        }

        public canReturnCode readModuleTemp2(byte[] tempBuffer, int moduleNumber, string profileTitle)
        {
            return myReadInterface.readProfileMBB(moduleNumber, profileTitle, tempBuffer, EEPROM_MBB_TEMP2_ADDR);
        }

        public canReturnCode readModuleTemp3(byte[] tempBuffer, int moduleNumber, string profileTitle)
        {
            return myReadInterface.readProfileMBB(moduleNumber, profileTitle, tempBuffer, EEPROM_MBB_TEMP3_ADDR);
        }

        public canReturnCode readModuleTemp4(byte[] tempBuffer, int moduleNumber, string profileTitle)
        {
            return myReadInterface.readProfileMBB(moduleNumber, profileTitle, tempBuffer, EEPROM_MBB_TEMP4_ADDR);
        }

        //
        // This is the block of routines to handle Meta Data.
        //

        public override canReturnCode displayModuleMetaData (int moduleNumber, dispCntl displayControl)
        {
            return displayModuleProfileData(moduleNumber, displayControl, readModuleMetaData, moduleMetaDataProfile);
        }

        public canReturnCode readModuleMetaData(byte[] moduleBuffer, int moduleNumber, string profileTitle)
        {
            return myReadInterface.readProfileMBB(moduleNumber, profileTitle, moduleBuffer, EEPROM_MBB_METADATA_ADDR);
        }

        //
        // This is the block of routines to handle Profile Balancer.
        //

        public override canReturnCode displayCellBalancer(int moduleNumber, dispCntl displayControl)
        {
            return displayModuleProfileData(moduleNumber, displayControl, readModuleBalancer, moduleBalancer);
        }

        public canReturnCode readModuleBalancer(byte[] balanceBuffer, int moduleNumber, string profileTitle)
        {
            return myReadInterface.readProfileMBB(moduleNumber, profileTitle, balanceBuffer, EEPROM_MBB_BALANCER_ADDR);
        }

        //
        // This is the block of routines to handle Cell Voltage.
        //
        public override canReturnCode displayModuleCellVoltage(int moduleNumber, int cellNumber, dispCntl displayControl)
        {
            return displayCellProfileData(moduleNumber, cellNumber, displayControl, readModuleCellVoltage, moduleCellVoltageProfile);
        }

        public canReturnCode readModuleCellVoltage(byte[] cellVoltBuffer, int moduleNumber, int cellNumber, string profileTitle)
        {
            return myReadInterface.readCellProfile(moduleNumber, cellNumber, profileTitle, cellVoltBuffer, EEPROM_MBB_VOLTAGE_CELL1_ADDR);
        }

    }  // End of class roddenberryDataLogMBB

}
