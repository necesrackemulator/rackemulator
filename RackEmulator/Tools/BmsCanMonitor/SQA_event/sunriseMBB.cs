﻿//*************************************************************************
//
// Copyright © 2014 NEC Energy Solutions, Inc. All rights reserved.
//
// NEC Energy Solutions Proprietary Information.
// This document contains proprietary and confidential information and is
// tendered for information and evaluation purposes only; no copy or other
// use or dissemination by a third party can be made of this information
// without the express written permission of NEC Energy Solutions.
//
//*************************************************************************

using System;
using SQA_modcan;
using SQA_can;
using SQA_formatEvent;
using SQA_config;
using SQA_utils;

namespace SQA_event
{

    public class sunriseDataLogMBB : dataLogMBB
    {
        sunriseModuleRev moduleRev;
        sunriseModuleVoltage moduleVoltageProfile;
        sunriseModuleCurrent moduleCurrentProfile;
        sunriseModuleTempIdle moduleTempIdle;
        sunriseModuleTempCharging moduleTempCharging;
        sunriseModuleTempDischarging moduleTempDischarging;
        sunriseModuleWattHourCharging moduleWattHourCharging;
        sunriseModuleWattHourDischarging moduleWattHourDischarging;
        sunriseModuleTotalWattHourCharged moduleTotalWattHourCharged;
        sunriseModuleTotalWattHourDischarged moduleTotalWattHourDischarged;
        sunriseModuleSoc moduleSocProfile;
        sunriseModuleAlarms moduleAlarmsProfile;
        sunriseModuleCellVoltage moduleCellVoltage;
        sunriseBalancerOff balancerOff;
        sunriseBalancerOn balancerOn;

        public sunriseDataLogMBB (profileReadInterface readInterface, int maxModule, int maxCell) : base (readInterface, maxModule, maxCell)
        {
            moduleRev = new sunriseModuleRev(maxModule);
            moduleVoltageProfile = new sunriseModuleVoltage(maxModule);
            moduleCurrentProfile = new sunriseModuleCurrent(maxModule);
            moduleTempIdle = new sunriseModuleTempIdle(maxModule);
            moduleTempCharging = new sunriseModuleTempCharging(maxModule);
            moduleTempDischarging = new sunriseModuleTempDischarging(maxModule);
            moduleWattHourCharging = new sunriseModuleWattHourCharging(maxModule);
            moduleWattHourDischarging = new sunriseModuleWattHourDischarging(maxModule);
            moduleTotalWattHourCharged = new sunriseModuleTotalWattHourCharged(maxModule);
            moduleTotalWattHourDischarged = new sunriseModuleTotalWattHourDischarged(maxModule);
            moduleSocProfile = new sunriseModuleSoc(maxModule);
            moduleAlarmsProfile = new sunriseModuleAlarms(maxModule);
            moduleCellVoltage = new sunriseModuleCellVoltage(maxModule, maxCell);
            balancerOff = new sunriseBalancerOff(maxModule, maxCell);
            balancerOn = new sunriseBalancerOn(maxModule, maxCell);

        }
       
        public override canReturnCode displayHistogram(int moduleNumber, dispCntl displayControl)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            canReturnCode savedReturnCode = canReturnCode.NO_ERROR;

            string moduleName;

            moduleName = String.Format("MBB {0}", moduleNumber);

            // Display the Profile Revision
            if ((returnCode = displayProfileRev(moduleNumber, displayControl)) != canReturnCode.NO_ERROR)
            {
                // Save the failing return code.
                savedReturnCode = returnCode;
            }

            // Display the Profile Alarms
            if ((returnCode = displayProfileAlarms(moduleNumber, displayControl)) != canReturnCode.NO_ERROR)
            {
                // Save the failing return code.
                savedReturnCode = returnCode;
            }

            // Display the Module Voltage.
            if ((returnCode = displayModuleVoltage(moduleNumber, displayControl)) != canReturnCode.NO_ERROR)
            {
                // Save the failing return code.
                savedReturnCode = returnCode;
            }

            // Display the Profile Current.
            if ((returnCode = displayProfileCurrent(moduleNumber, displayControl)) != canReturnCode.NO_ERROR)
            {
                // Save the failing return code.
                savedReturnCode = returnCode;
            }

            // Display the Profile SOC.
            if ((returnCode = displayProfileSoc(moduleNumber, displayControl)) != canReturnCode.NO_ERROR)
            {
                // Save the failing return code.
                savedReturnCode = returnCode;
            }

            // Display the Profile Temp Idle, Charging, and Discharging.
            if ((returnCode = displayTemp(moduleNumber, displayControl)) != canReturnCode.NO_ERROR)
            {
                // Save the failing return code.
                savedReturnCode = returnCode;
            }


            // Display the Profile Watt Hours Charged/Discharged.
            if ((returnCode = displayWattHours(moduleNumber, displayControl)) != canReturnCode.NO_ERROR)
            {
                // Save the failing return code.
                savedReturnCode = returnCode;
            }


            // Display the Profile Balancer.
            if ((returnCode = displayBalancer(moduleNumber, displayControl)) != canReturnCode.NO_ERROR)
            {
                // Save the failing return code.
                savedReturnCode = returnCode;
            }

            // Display the Module Cell Voltage.
            if ((returnCode = displayModuleCellVoltages(moduleNumber, displayControl)) != canReturnCode.NO_ERROR)
            {
                // Save the failing return code.
                savedReturnCode = returnCode;
            }


            UserIO.outputInfo(String.Format("End of {0} Histogram Display.", moduleName));

            // If there was a failure in the saved return code, return the saved return code.
            if ((returnCode == canReturnCode.NO_ERROR) && (savedReturnCode != canReturnCode.NO_ERROR))
            {
                returnCode = savedReturnCode;
            }

            return returnCode;
        }

        //
        // This is the block of routines to handle Profile Rev.
        //

        public override canReturnCode displayProfileRev(int moduleNumber, dispCntl displayControl)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            byte[] revBuffer = new byte[moduleRev.getProfileSize()];
            string profileTitle = String.Format("MBB {0} {1}", moduleNumber, "Profile Rev");

            if ((returnCode = displayModuleProfileData(moduleNumber, displayControl, readProfileMBBRev, moduleRev)) == canReturnCode.NO_ERROR)
            {
                if ((returnCode = readProfileMBBRev(revBuffer, moduleNumber, profileTitle)) == canReturnCode.NO_ERROR)
                {
                    returnCode = checkProfileRev(revBuffer);
                }

            }
            return returnCode;
        }

        public canReturnCode readProfileMBBRev(byte[] readBuffer, int moduleNumber, string profileTitle)
        {
            return myReadInterface.readProfileMBB(moduleNumber, profileTitle, readBuffer, sunriseModuleMBB.DID_READ_PROFILE_REV);
        }

        public canReturnCode checkProfileRev(byte[] revBuffer)
        {
            const UInt16 EXPECTED_PROFILE_REV = Constant.ONE;

            canReturnCode returnCode = canReturnCode.NO_ERROR;

            UInt16 actProfileRevision;

            actProfileRevision = Utils.extractLittleEndian16(revBuffer, Constant.OFFSET_0);

            if (UserIO.forceErrorDisplay || (actProfileRevision != EXPECTED_PROFILE_REV))
            {
                UserIO.outputExpActShort(outDev.ALL, msgType.ERROR, "Incorrect Profile Revision", EXPECTED_PROFILE_REV, actProfileRevision);
            }

            if (actProfileRevision != EXPECTED_PROFILE_REV)
            {
                returnCode = canReturnCode.DATA_ERROR;
            }
            return returnCode;
        }

  
        //
        // This is the block of routines to handle Profile Module Voltage.
        //
        public override canReturnCode displayModuleVoltage(int moduleNumber, dispCntl displayControl)
        {
            return displayModuleProfileData(moduleNumber, displayControl, readModuleVoltage, moduleVoltageProfile);
        }

        public canReturnCode readModuleVoltage(byte[] readBuffer, int moduleNumber, string profileTitle)
        {
            return myReadInterface.readProfileMBB(moduleNumber, profileTitle, readBuffer, sunriseModuleMBB.DID_READ_MODULE_VOLTAGE);
        }


        //
        // This is the block of routines to handle Profile Current.
        //

        public override canReturnCode displayProfileCurrent(int moduleNumber, dispCntl displayControl)
        {
            return displayModuleProfileData(moduleNumber, displayControl, readProfileMBBCurrent, moduleCurrentProfile);
        }

        public canReturnCode readProfileMBBCurrent(byte[] readBuffer, int moduleNumber, string profileTitle)
        {
            return myReadInterface.readProfileMBB(moduleNumber, profileTitle, readBuffer, sunriseModuleMBB.DID_READ_PROFILE_CURRENT);
        }

        //
        // This is the block of routines to handle Profile SOC.
        //

        public override canReturnCode displayProfileSoc(int moduleNumber, dispCntl displayControl)
        {
            return displayModuleProfileData(moduleNumber, displayControl, readProfileMBBSoc, moduleSocProfile);
        }

        public canReturnCode readProfileMBBSoc(byte[] readBuffer, int moduleNumber, string profileTitle)
        {
            return myReadInterface.readProfileMBB(moduleNumber, profileTitle, readBuffer, sunriseModuleMBB.DID_READ_PROFILE_SOC);
        }

        //
        // This is the block of routines to handle Profile Temp Idle, Charging, and Discharging.
        //

        public override canReturnCode displayTemp (int moduleNumber, dispCntl displayControl)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            if ((returnCode = displayModuleProfileData(moduleNumber, displayControl, readTempIdle, moduleTempIdle)) == canReturnCode.NO_ERROR)
            {
                if ((returnCode = displayModuleProfileData(moduleNumber, displayControl, readTempCharging, moduleTempCharging)) == canReturnCode.NO_ERROR)
                {
                    returnCode = displayModuleProfileData(moduleNumber, displayControl, readTempDischarging, moduleTempDischarging);
                }
            }
            return returnCode;
        }

        public canReturnCode readTempCharging(byte[] readBuffer, int moduleNumber, string profileTitle)
        {
            return myReadInterface.readProfileMBB(moduleNumber, profileTitle, readBuffer, sunriseModuleMBB.DID_READ_TEMP_CHARGING);
        }

 
        public canReturnCode readTempDischarging(byte[] readBuffer, int moduleNumber, string profileTitle)
        {
            return myReadInterface.readProfileMBB(moduleNumber, profileTitle, readBuffer, sunriseModuleMBB.DID_READ_TEMP_DISCHARGING);
        }


        public canReturnCode readTempIdle(byte[] readBuffer, int moduleNumber, string profileTitle)
        {
            return myReadInterface.readProfileMBB(moduleNumber, profileTitle, readBuffer, sunriseModuleMBB.DID_READ_TEMP_IDLE);
        }

        //
        // This is the block of routines to handle Profile Watt-Hours Charging.
        //

        public override canReturnCode displayWattHours (int moduleNumber, dispCntl displayControl)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            if ((returnCode = displayModuleProfileData(moduleNumber, displayControl, readWattHoursCharged, moduleWattHourCharging)) == canReturnCode.NO_ERROR)
            {
                if ((returnCode = displayModuleProfileData(moduleNumber, displayControl, readWattHoursDischarged, moduleWattHourDischarging)) == canReturnCode.NO_ERROR)
                {
                    if ((returnCode = displayModuleProfileData(moduleNumber, displayControl, readTotalWattHoursCharged, moduleTotalWattHourCharged)) == canReturnCode.NO_ERROR)
                    {
                        returnCode = displayModuleProfileData(moduleNumber, displayControl, readTotalWattHoursDischarged, moduleTotalWattHourDischarged);
                    }
                }
            }
            
            return returnCode;
        }
    

        public canReturnCode readWattHoursCharged(byte[] readBuffer, int moduleNumber, string profileTitle)
        {
            return myReadInterface.readProfileMBB(moduleNumber, profileTitle, readBuffer, sunriseModuleMBB.DID_READ_WATT_HOURS_CHARGED);
        }

        public canReturnCode readWattHoursDischarged(byte[] readBuffer, int moduleNumber, string profileTitle)
        {
            return myReadInterface.readProfileMBB(moduleNumber, profileTitle, readBuffer, sunriseModuleMBB.DID_READ_WATT_HOURS_DISCHARGED);
        }

        public canReturnCode readTotalWattHoursCharged(byte[] readBuffer, int moduleNumber, string profileTitle)
        {
            return myReadInterface.readProfileMBB(moduleNumber, profileTitle, readBuffer, sunriseModuleMBB.DID_TOTAL_WATT_HOURS_CHARGED);
        }

        public canReturnCode readTotalWattHoursDischarged(byte[] readBuffer, int moduleNumber, string profileTitle)
        {
            return myReadInterface.readProfileMBB(moduleNumber, profileTitle, readBuffer, sunriseModuleMBB.DID_TOTAL_WATT_HOURS_DISCHARGED);
        }

        //
        // This is the block of routines to handle Profile Alarms.
        //

        public override canReturnCode displayProfileAlarms(int moduleNumber, dispCntl displayControl)
        {
            return displayModuleProfileData(moduleNumber, displayControl, readProfileMBBAlarms, moduleAlarmsProfile);
        }
        public canReturnCode readProfileMBBAlarms(byte[] readBuffer, int moduleNumber, string profileTitle)
        {
            return myReadInterface.readProfileMBB(moduleNumber, profileTitle, readBuffer, sunriseModuleMBB.DID_READ_ALARMS);
        }

        public override canReturnCode displayModuleCellVoltage(int moduleNumber, int cellNumber, dispCntl displayControl)
        {
            return displayCellProfileData(moduleNumber, cellNumber, displayControl, readProfileMBBCellVoltage, moduleCellVoltage);
        }

        public canReturnCode readProfileMBBCellVoltage(byte[] readBuffer, int moduleNumber, int cellNumber, string profileTitle)
        {
            return myReadInterface.readCellProfile(moduleNumber, cellNumber, profileTitle, readBuffer, sunriseModuleMBB.DID_READ_CELL_VOLTAGE);
        }

        //
        // This is the block of routines to handle Profile Balancer.
        //

        public override canReturnCode displayBalancer(int moduleNumber, dispCntl displayControl)
        {

            canReturnCode returnCode = canReturnCode.NO_ERROR;
            canReturnCode savedReturnCode = canReturnCode.NO_ERROR;

            int cellNumber;

            // Loop over the entered range of cells.
            for (cellNumber = cellConfig.userMinCell; cellNumber <= cellConfig.userMaxCell; cellNumber++)
            {
                returnCode = displayCellProfileData(moduleNumber, cellNumber, displayControl, readProfileMBBBalancerOn, balancerOn);
                if (returnCode != canReturnCode.NO_ERROR)
                {
                    // Save the failing return code.
                    savedReturnCode = returnCode;
                }
            }

            // Loop over the entered range of cells.
            for (cellNumber = cellConfig.userMinCell; cellNumber <= cellConfig.userMaxCell; cellNumber++)
            {
                returnCode = displayCellProfileData(moduleNumber, cellNumber, displayControl, readProfileMBBBalancerOff, balancerOff);
                if (returnCode != canReturnCode.NO_ERROR)
                {
                    // Save the failing return code.
                    savedReturnCode = returnCode;
                }
            }

            return savedReturnCode;
        }

    
        public canReturnCode readProfileMBBBalancerOn(byte[] readBuffer, int moduleNumber, int cellNumber, string profileTitle)
        {
            return myReadInterface.readProfileMBB(moduleNumber, profileTitle, readBuffer, sunriseModuleMBB.DID_READ_BALANCER_ON);
        }

        public canReturnCode readProfileMBBBalancerOff(byte[] readBuffer, int moduleNumber, int cellNumber, string profileTitle)
        {
            return myReadInterface.readProfileMBB(moduleNumber, profileTitle, readBuffer, sunriseModuleMBB.DID_READ_BALANCER_OFF);
        }

    } // End of class sunriseDataLogMBB

}


