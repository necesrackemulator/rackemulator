﻿//*************************************************************************
//
// Copyright © 2014 NEC Energy Solutions, Inc. All rights reserved.
//
// NEC Energy Solutions Proprietary Information.
// This document contains proprietary and confidential information and is
// tendered for information and evaluation purposes only; no copy or other
// use or dissemination by a third party can be made of this information
// without the express written permission of NEC Energy Solutions.
//
//*************************************************************************

using System;
using SQA_can;
using SQA_formatEvent;
using SQA_formatRegs;
using SQA_format;
using SQA_config;
using SQA_utils;

namespace SQA_event
{
    public class eventLogBCM
    {

        public const int MIN_CHRONICLE_RECORD = 1;
        public const int MIN_CRITICAL_RECORD = 1;

        public const int CHRONICLE_RECORD_SIZE = 64;
        public const int CRITICAL_RECORD_SIZE = 64;

        const int HDR_SIZE = 0x0C;
        public const UInt16 CHRONICLE_HDR_START_ADDR = 0x57A0;
        public const UInt16 CRITICAL_HDR_START_ADDR = 0x6BC0;

        const int CHRONICLE_DATA_START_ADDR = 0x57C0;
        const int CRITICAL_DATA_START_ADDR = 0x6BE0;

        static UInt16 oldestRecordSavedCritical = Constant.ZERO;
        static UInt16 newestRecordSavedCritical = Constant.ZERO;
        
        static UInt16 oldestRecordSavedEvent = Constant.ZERO;
        static UInt16 newestRecordSavedEvent = Constant.ZERO;

        public eventFormatBCM eventBCM;
        public profileReadInterface myReadInterface;
        public eventLogBCM (profileReadInterface readInterface)
        {
            myReadInterface = readInterface;
            formatBcmRegisters formatBcmRegs = new formatBcmRegisters();
            eventBCM = new eventFormatBCM (formatBcmRegs);
        }

        public canReturnCode displayEventHdr ()
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            string hdrMsg = "Event";

            byte[] hdrBuffer = new byte[HDR_SIZE];

            if ((returnCode = readHeaderData(CHRONICLE_HDR_START_ADDR, hdrMsg, hdrBuffer)) == canReturnCode.NO_ERROR)
            {
                formatUtils.outputEnglishHexFormatFunction(hdrMsg, hdrBuffer, formatHdr);
            }
           
            return returnCode;
        }
   
        public canReturnCode displayCriticalHdr ()
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            string hdrMsg = "Critical";

            byte[] hdrBuffer = new byte[HDR_SIZE];

            if ((returnCode = readHeaderData (CRITICAL_HDR_START_ADDR, hdrMsg, hdrBuffer)) == canReturnCode.NO_ERROR)
            {
                formatUtils.outputEnglishHexFormatFunction(hdrMsg, hdrBuffer, formatHdr);
            }
            
            return returnCode;
        }


        public canReturnCode displayEventRange (bool diffOnly, UInt16 minPossibleRecord, UInt16 maxPossibleRecord)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            string hdrMsg = "Event";

            UInt16 oldestRecordLast = oldestRecordSavedEvent;
            UInt16 newestRecordLast = newestRecordSavedEvent;
           
            if ((returnCode = getRecordRange(CHRONICLE_HDR_START_ADDR, hdrMsg, minPossibleRecord, maxPossibleRecord, ref oldestRecordSavedEvent, ref newestRecordSavedEvent)) == canReturnCode.NO_ERROR)
            {
                displayRecordChanges(diffOnly, "Oldest Event", oldestRecordLast, oldestRecordSavedEvent);
                displayRecordChanges(diffOnly, "Newest Event", newestRecordLast, newestRecordSavedEvent);
            }
        
            return returnCode;
        }
        
       
        public canReturnCode displayCriticalRange (bool diffOnly, UInt16 minPossibleRecord, UInt16 maxPossibleRecord)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            string hdrMsg = "Critical";

            UInt16 oldestRecordLast = oldestRecordSavedCritical;
            UInt16 newestRecordLast = newestRecordSavedCritical;

            if ((returnCode = getRecordRange(CRITICAL_HDR_START_ADDR, hdrMsg, minPossibleRecord, maxPossibleRecord, ref oldestRecordSavedCritical, ref newestRecordSavedCritical)) == canReturnCode.NO_ERROR)
            {
                displayRecordChanges(diffOnly, "Oldest Critical", oldestRecordLast, oldestRecordSavedCritical);
                displayRecordChanges(diffOnly, "Newest Critical", newestRecordLast, newestRecordSavedCritical);
            }
           
            return returnCode;
        }

        public canReturnCode getRecordRange(UInt16 hdrAddress, string hdrMsg, UInt16 minPossibleRecord, UInt16 maxPossibleRecord, ref UInt16 oldestRecord, ref UInt16 newestRecord)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            byte[] hdrBuffer = new byte[HDR_SIZE];

            UInt32 numWrites = Constant.ZERO;
            UInt16 newIndex = Constant.ZERO;
            UInt16 lastIndex = Constant.ZERO;

            if ((returnCode = extractHeaderData(hdrAddress, hdrMsg, ref numWrites, ref newIndex, ref lastIndex)) == canReturnCode.NO_ERROR)
            {  
                // Check if the FIFO has not yet filled
                if (numWrites <= maxPossibleRecord)
                {
                    oldestRecord = minPossibleRecord;
                    newestRecord = newIndex;
                }
                else
                {
                    // FIFO is full and possibly has wrapped.
                    oldestRecord = lastIndex;

                    if (newIndex == Constant.ZERO)
                    {
                        newestRecord = maxPossibleRecord;
                    }
                    else
                    {
                        newestRecord = newIndex;
                    }
                }
            }
            return returnCode;
        }

        private void displayRecordChanges(bool diffOnly, string hdrMsg, UInt16 savedRecordNumber, UInt16 curRecordNumber)
        {
            if (diffOnly)
            {
                if (savedRecordNumber == curRecordNumber)
                {
                    UserIO.outputInfo(String.Format("{0} record: Unchanged {1}.", hdrMsg, curRecordNumber));
                }
                else
                {
                    UserIO.outputInfo(String.Format("{0} record: Previous {1}, Current {2}.", hdrMsg, savedRecordNumber, curRecordNumber));
                }
            }
            else
            {
                UserIO.outputInfo(String.Format ("{0} record: {1}.", hdrMsg, curRecordNumber));
            }
        }

        private void formatHdr(string hdrName, byte[] hdrBuffer)
        {
            const int HDR_VERSION_OFFSET = Constant.ZERO;

            UInt16 version;

            version = Utils.extractUInt16(hdrBuffer, HDR_VERSION_OFFSET);

            UserIO.outputInfo(String.Format("{0} Version: {1}.", hdrName, version));
            UserIO.outputInfo(String.Format("New Index: {0}, Last Index: {1}, Number writes: {2}.", 
                                            extractNewIndex(hdrBuffer), extractLastIndex(hdrBuffer), extractNumberWrites(hdrBuffer)));
            
            UserIO.outputNewLine(outDev.ALL, msgType.INFO);
        }
 
        public canReturnCode readCriticalLast(bool eventOnly)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            UInt16 lastRecord = Constant.ZERO;

            if ((returnCode = extractCriticalLastRecordNumber (ref lastRecord)) == canReturnCode.NO_ERROR)
            {
                returnCode = displayCriticalRecord (lastRecord, eventOnly);
            }
            
            return returnCode;
        }

        public canReturnCode readCriticalPrev(bool eventOnly)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            UInt16 previousRecord = Constant.ZERO;

            if ((returnCode = extractCriticalPreviousRecordNumber (ref previousRecord)) == canReturnCode.NO_ERROR)
            {
                returnCode = displayCriticalRecord (previousRecord, eventOnly);
            }

            return returnCode;
        }
        
        public canReturnCode extractCriticalLastRecordNumber (ref UInt16 lastRecord)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            returnCode = extractLastRecordNumber (CRITICAL_HDR_START_ADDR, "Critical", (UInt16)systemConfig.getMaxBcmCritical(), ref lastRecord);
           
            return returnCode;
        }

        public canReturnCode extractEventLastRecordNumber (ref UInt16 lastRecord)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            returnCode = extractLastRecordNumber(CHRONICLE_HDR_START_ADDR, "Event", (UInt16)systemConfig.getMaxBcmEvent(), ref lastRecord);

            return returnCode;
        }

        
        public canReturnCode readEventLast(bool eventOnly)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            UInt16 lastRecord = Constant.ZERO;

            if ((returnCode = extractEventLastRecordNumber (ref lastRecord)) == canReturnCode.NO_ERROR)
            {
                returnCode = displayEventRecord (lastRecord, eventOnly);
            }
            return returnCode;
        }

        
        public canReturnCode readEventPrev(bool eventOnly)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            UInt16 previousRecord = Constant.ZERO;

            if ((returnCode = extractEventPreviousRecordNumber (ref previousRecord)) == canReturnCode.NO_ERROR)
            {
                returnCode = displayEventRecord (previousRecord, eventOnly);
            }
 
            return returnCode;
        }

       
        public canReturnCode displayCriticalRecord (int recordNumber, bool eventOnly)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            string hdrMsg = "Critical";

            if (eventOnly)
            {
                // Display only the event type and time.
                returnCode = displayEntity(hdrMsg, recordNumber, CRITICAL_RECORD_SIZE, CRITICAL_DATA_START_ADDR, eventBCM.formatEvent);
            }
            else
            {
                // Display the complete record.
                returnCode = displayEntity(hdrMsg, recordNumber, CRITICAL_RECORD_SIZE, CRITICAL_DATA_START_ADDR, eventBCM.formatEventRecord);
            }
            
            return returnCode;
        }


        
        public canReturnCode displayEventRecord(int recordNumber, bool eventOnly)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            string hdrMsg = "Event";

            if (eventOnly)
            {
                // Display only the event type and time.
                returnCode = displayEntity(hdrMsg, recordNumber, CHRONICLE_RECORD_SIZE, CHRONICLE_DATA_START_ADDR, eventBCM.formatEvent);
            }
            else
            {
                // Display the complete record.
                returnCode = displayEntity(hdrMsg, recordNumber, CHRONICLE_RECORD_SIZE, CHRONICLE_DATA_START_ADDR, eventBCM.formatEventRecord);
            }
            
            return returnCode;
        }

        private canReturnCode displayEntity(string hdrMsg, int recordNumber, int recordSize, UInt16 startAddr, Action<string, byte[], Action<string, string>> moduleFunctionName)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            UInt16 blockAddr;

            byte[] recordBuffer = new byte[recordSize];
            string recordDesc = String.Format("{0} Record Number {1}", hdrMsg, recordNumber);

            blockAddr = (UInt16)(startAddr + ((recordNumber - Constant.ONE) * recordSize));
          
            if ((returnCode = (canReturnCode) myReadInterface.readProfile(hdrMsg, recordBuffer, blockAddr)) == canReturnCode.NO_ERROR)
            {
                // Determine the output format.
                switch (AppUtils.displayFormat)
                {
                    case AppUtils.DISPLAY_FORMAT_ENGLISH:
                        {
                            moduleFunctionName(recordDesc, recordBuffer, null);
                        }
                        break;
                    case AppUtils.DISPLAY_FORMAT_HEX:
                        {
                            Utils.outputBuffer(recordDesc, recordBuffer, recordSize);
                        }
                        break;
                    case AppUtils.DISPLAY_FORMAT_NONE:
                    default:
                        break;
                }
                UserIO.outputInfo(String.Format("End of BCM {0} record {1}.", hdrMsg, recordNumber));
            }
            else
            {
                UserIO.outputError(String.Format("Failed to read {0}.", recordDesc));
            }

            return returnCode;
        }

      
        public canReturnCode extractLastRecordNumber(UInt16 hdrAddress, string hdrMsg, UInt16 maxPossibleRecords, ref UInt16 lastRecord)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            UInt32 numWrites = Constant.ZERO;
            UInt16 newIndex = Constant.ZERO;
            UInt16 lastIndex = Constant.ZERO;

            if ((returnCode = extractHeaderData(hdrAddress, hdrMsg, ref numWrites, ref newIndex, ref lastIndex)) == canReturnCode.NO_ERROR)
            {
                // Check if the FIFO has not yet filled
                if (numWrites <= maxPossibleRecords)
                {
                    lastRecord = newIndex;
                }
                else
                {
                    // FIFO is full and possibly has wrapped.
                    if (newIndex == Constant.ZERO)
                    {
                        // FIFO is Full, but has not wrapped.
                        lastRecord = maxPossibleRecords;
                    }
                    else
                    {
                        // If newIndex > zero, FIFO has wrapped.
                        lastRecord = newIndex;
                    }
                }
            }

            return returnCode;
        }

        private UInt16 extractPreviousRecordNumber(UInt16 maxPossibleRecords, UInt32 numWrites, UInt16 lastIndex, UInt16 newIndex)
        {
            UInt16 previousRecord = Constant.ZERO;

            // Check if the FIFO has not yet filled
            if (numWrites <= maxPossibleRecords)
            {
                previousRecord = (UInt16) (newIndex - Constant.ONE);
            }
            else
            {
                // FIFO is full and possibly has wrapped.
                if (newIndex == Constant.ZERO)
                {
                    // FIFO is Full, but has not wrapped.
                    previousRecord = (UInt16) (maxPossibleRecords - Constant.ONE);
                }
                else
                {
                    // If newIndex > zero, FIFO has wrapped.
                    if (newIndex == Constant.ONE)
                    {
                        previousRecord = maxPossibleRecords;
                    }
                    else
                    {
                        previousRecord = (UInt16) (newIndex - Constant.ONE);
                    }
                }
            }

            return previousRecord;
        }

        public canReturnCode extractCriticalPreviousRecordNumber (ref UInt16 previousRecord)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            UInt32 numWrites = Constant.ZERO;
            UInt16 newIndex = Constant.ZERO;
            UInt16 lastIndex = Constant.ZERO;

            if ((returnCode = extractHeaderData (CRITICAL_HDR_START_ADDR, "Critical", ref numWrites, ref newIndex, ref lastIndex)) == canReturnCode.NO_ERROR)
            {
                if (numWrites == Constant.ONE)
                {
                    UserIO.outputInfo("Only one record is present");
                    returnCode = canReturnCode.DATA_ERROR;
                }
                else
                {
                    previousRecord = extractPreviousRecordNumber((UInt16)systemConfig.getMaxBcmCritical(), numWrites, lastIndex, newIndex);
                }
            }

            return returnCode;
        }


        public canReturnCode extractEventPreviousRecordNumber(ref UInt16 previousRecord)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            UInt32 numWrites = Constant.ZERO;
            UInt16 newIndex = Constant.ZERO;
            UInt16 lastIndex = Constant.ZERO;

            if ((returnCode = extractHeaderData(CHRONICLE_HDR_START_ADDR, "Event", ref numWrites, ref newIndex, ref lastIndex)) == canReturnCode.NO_ERROR)
            {
                if (numWrites == Constant.ONE)
                {
                    UserIO.outputInfo("Only one record is present");
                    returnCode = canReturnCode.DATA_ERROR;
                }
                else
                {
                    previousRecord = extractPreviousRecordNumber((UInt16)systemConfig.getMaxBcmEvent(), numWrites, lastIndex, newIndex);
                }
            }

            return returnCode;
        }

   
        public canReturnCode extractHeaderData(UInt16 hdrStartAddr, string hdrMsg, ref UInt32 numWrites, ref UInt16 newIndex, ref UInt16 lastIndex)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            byte[] hdrBuffer = new byte[HDR_SIZE];

            if ((returnCode = readHeaderData (hdrStartAddr, hdrMsg, hdrBuffer)) == canReturnCode.NO_ERROR)
            {
                numWrites = extractNumberWrites(hdrBuffer);
                newIndex = extractNewIndex(hdrBuffer);
                lastIndex = extractLastIndex(hdrBuffer);

                if (numWrites == Constant.ZERO)
                {
                    UserIO.outputInfo(String.Format ("No {0} records are present", hdrMsg));
                    returnCode = canReturnCode.DATA_ERROR;
                }
            }
            
            return returnCode;
        }

        public canReturnCode readHeaderData (UInt16 hdrStartAddr, string hdrMsg, byte[] hdrBuffer)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            if ((returnCode = myReadInterface.readProfile(hdrMsg, hdrBuffer, hdrStartAddr)) != canReturnCode.NO_ERROR)
            {
                UserIO.outputError(String.Format("Failed to read {0} header.", hdrMsg));
            }
            return returnCode;
        }

        private UInt32 extractNumberWrites(byte[] hdrBuffer)
        {
            const int HDR_NUMBER_WRITES_OFFSET = Constant.FOUR;

            return Utils.extractUInt32(hdrBuffer, HDR_NUMBER_WRITES_OFFSET);
        }

        private UInt16 extractNewIndex(byte[] hdrBuffer)
        {
            const int HDR_NEW_INDEX_OFFSET = Constant.TWO;

            return Utils.extractUInt16(hdrBuffer, HDR_NEW_INDEX_OFFSET);
        }

        private UInt16 extractLastIndex(byte[] hdrBuffer)
        {
            const int HDR_LAST_INDEX_OFFSET = Constant.EIGHT;

            return Utils.extractUInt16(hdrBuffer, HDR_LAST_INDEX_OFFSET);
        }

        public canReturnCode readCriticalRecords (int minRecordNumber, int maxRecordNumber, bool eventOnly)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            canReturnCode savedReturnCode = canReturnCode.NO_ERROR;

            int recordNumber;

            // Check if the FIFO has not wrapped around.
            if (minRecordNumber <= maxRecordNumber)
            {
                // The FIFO has not wrapped around.  Display from lowest to highest.
                for (recordNumber = minRecordNumber; recordNumber <= maxRecordNumber; recordNumber++)
                {
                    // If there is a failure to access one record, save the return code.
                    if ((returnCode = displayCriticalRecord(recordNumber, eventOnly)) != canReturnCode.NO_ERROR)
                    {
                        // Save the failing return code.
                        savedReturnCode = returnCode;
                    }
                    else
                    {
                        savedReturnCode = returnCode;
                    }
                }
            }
            else
            {
                // The FIFO has wrapped around.  Display upper part of FIFO (older records).
                for (recordNumber = minRecordNumber; recordNumber <= systemConfig.getMaxBcmCritical(); recordNumber++)
                {
                    // If there is a failure to access one record, save the return code.
                    if ((returnCode = displayCriticalRecord(recordNumber, eventOnly)) != canReturnCode.NO_ERROR)
                    {
                        // Save the failing return code.
                        savedReturnCode = returnCode;
                    }
                    else
                    {
                        savedReturnCode = returnCode;
                    }
                }
                // The FIFO has wrapped around.  Display lower part of FIFO (newer records).
                for (recordNumber = MIN_CRITICAL_RECORD; recordNumber <= maxRecordNumber; recordNumber++)
                {
                    // If there is a failure to access one record, save the return code.
                    if ((returnCode = displayCriticalRecord(recordNumber, eventOnly)) != canReturnCode.NO_ERROR)
                    {
                        // Save the failing return code.
                        savedReturnCode = returnCode;
                    }
                    else
                    {
                        savedReturnCode = returnCode;
                    }
                }
            }
            // If there was a failure in the saved return code, return the saved return code.
            if ((returnCode == canReturnCode.NO_ERROR) && (savedReturnCode != canReturnCode.NO_ERROR))
            {
                returnCode = savedReturnCode;
            }
            return returnCode;
        }

        public canReturnCode readEventRecords(int minRecordNumber, int maxRecordNumber, bool eventOnly)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            canReturnCode savedReturnCode = canReturnCode.NO_ERROR;

            int recordNumber;

            // Check if the FIFO has not wrapped around.
            if (minRecordNumber <= maxRecordNumber)
            {
                // The FIFO has not wrapped around.  Display from lowest to highest.
                for (recordNumber = minRecordNumber; recordNumber <= maxRecordNumber; recordNumber++)
                {
                    // If there is a failure to access one record, save the return code.
                    if ((returnCode = displayEventRecord(recordNumber, eventOnly)) != canReturnCode.NO_ERROR)
                    {
                        // Save the failing return code.
                        savedReturnCode = returnCode;
                    }
                }
            }
            else
            {
                // The FIFO has wrapped around.  Display upper part of FIFO (older records).
                for (recordNumber = minRecordNumber; recordNumber <= systemConfig.getMaxBcmEvent(); recordNumber++)
                {
                    // If there is a failure to access one record, save the return code.
                    if ((returnCode = displayEventRecord(recordNumber, eventOnly)) != canReturnCode.NO_ERROR)
                    {
                        // Save the failing return code.
                        savedReturnCode = returnCode;
                    }
                    else
                    {
                        savedReturnCode = returnCode;
                    }
                }

                // The FIFO has wrapped around.  Display lower part of FIFO (newer records).
                for (recordNumber = MIN_CHRONICLE_RECORD; recordNumber <= maxRecordNumber; recordNumber++)
                {
                    // If there is a failure to access one record, save the return code.
                    if ((returnCode = displayEventRecord(recordNumber, eventOnly)) != canReturnCode.NO_ERROR)
                    {
                        // Save the failing return code.
                        savedReturnCode = returnCode;
                    }
                    else
                    {
                        savedReturnCode = returnCode;
                    }
                }
            }

            // If there was a failure in the saved return code, return the saved return code.
            if ((returnCode == canReturnCode.NO_ERROR) && (savedReturnCode != canReturnCode.NO_ERROR))
            {
                returnCode = savedReturnCode;
            }
            return returnCode;
        }
   
    }  // End of class eventLogBCM

    public class nullEventLogBCM : eventLogBCM
    {
        // eventFormatBCM eventBCM;
        public nullEventLogBCM (profileReadInterface readInterface) : base(readInterface)
        {
            formatBcmRegisters formatBcmRegs = new formatBcmRegisters();
            myReadInterface = readInterface;
            eventBCM = new eventFormatBCM(formatBcmRegs);
        }
    } // End of class nullEventLogBCM

    public class roddenberryEventLogBCM : eventLogBCM
    {
        public roddenberryEventLogBCM (profileReadInterface readInterface) : base (readInterface)
        {
            roddenberryFormatBcmRegisters formatBcmRegs = new roddenberryFormatBcmRegisters();
            myReadInterface = readInterface;
            eventBCM = new eventFormatBCM(formatBcmRegs);
        }

    } // End of class roddenberryEventLogBCM


    public class switchboardEventLogBCM : eventLogBCM
    {
        public switchboardEventLogBCM(profileReadInterface readInterface) : base(readInterface)
        {
            switchboardFormatBcmRegisters formatBcmRegs = new switchboardFormatBcmRegisters();
            myReadInterface = readInterface;
            eventBCM = new eventFormatBCM (formatBcmRegs);
        }

    } // End of class switchboardEventLogBCM

    public class sunriseEventLogBCM : eventLogBCM
    {
        public sunriseEventLogBCM(profileReadInterface readInterface) : base(readInterface)
        {
            sunriseFormatBcmRegisters formatBcmRegs = new sunriseFormatBcmRegisters();
            myReadInterface = readInterface;
            eventBCM = new eventFormatBCM (formatBcmRegs);
        }
    } // End of class sunriseEventLogBCM

    public class optimusEventLogBCM : eventLogBCM
    {
        public optimusEventLogBCM(profileReadInterface readInterface) : base(readInterface)
        {
            optimusFormatBcmRegisters formatBcmRegs = new optimusFormatBcmRegisters();
            myReadInterface = readInterface;
            eventBCM = new eventFormatBCM(formatBcmRegs);
        }
    } // End of class optimusEventLogBCM
}
 
                           
