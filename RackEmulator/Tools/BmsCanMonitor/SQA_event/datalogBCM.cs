﻿//*************************************************************************
//
// Copyright © 2014 NEC Energy Solutions, Inc. All rights reserved.
//
// NEC Energy Solutions Proprietary Information.
// This document contains proprietary and confidential information and is
// tendered for information and evaluation purposes only; no copy or other
// use or dissemination by a third party can be made of this information
// without the express written permission of NEC Energy Solutions.
//
//*************************************************************************

using System;
using SQA_can;
using SQA_formatEvent;
using SQA_config;
using SQA_utils;

namespace SQA_event
{
    public class dataLogBCM
    {
        // These constants define offsets in the BCM NVRAM containing BCM data logs.
        public const int EEPROM_FILE_SOC_ADDR = 0x0000;
        public const int EEPROM_FILE_IDENTITY_ADDR = 0x0040;
        public const int EEPROM_FILE_CALIBRATION_ADDR = 0x0060;
        public const int EEPROM_FILE_METADATA_ADDR = 0x0080;
        public const int EEPROM_FILE_HISTORY_ADDR = 0x00A0;
        public const int EEPROM_HISTOGRAM_SOC_ADDR = 0x0180;
        public const int EEPROM_HISTOGRAM_CURRENT_ADDR = 0x01C0;
        public const int EEPROM_HISTOGRAM_POWER_ADDR = 0x0240;
        public const int EEPROM_HISTOGRAM_MISC_ADDR = 0x02E0;
        public const int EEPROM_HISTOGRAM_VOLT_ADDR = 0x0340;

        public profileReadInterface myReadInterface;

        public dataLogBCM(profileReadInterface readInterface)
        {
            myReadInterface = readInterface;
        }

        public canReturnCode displayProfileData(dispCntl displayControl, Func<byte[], string, canReturnCode> readProfileMBBFunction, formatBasicProfile myProfile)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            int bufferSize = myProfile.getProfileSize();

            string profileTitle = String.Format("{0} Profile", myProfile.getProfileMsg());

            // Create array to hold the last data.
            byte[] readBuffer = new byte[bufferSize];
            byte[] lastBuffer = new byte[bufferSize];

            // Extract the last read data so that it can be compared (diff'ed) later.
            myProfile.extractProfileData(lastBuffer);

            if ((returnCode = readProfileMBBFunction(readBuffer, profileTitle)) == canReturnCode.NO_ERROR)
            {
                // Save the read data for a given MBB.
                myProfile.saveProfileData(readBuffer);
                myProfile.formatProfile(profileTitle, readBuffer, lastBuffer, displayControl);
            }
            else
            {
                UserIO.outputError(String.Format("Failure to read {0}.", profileTitle));
            }
            return returnCode;
        }

        public canReturnCode testIdentity (formatBasicProfile profileIdentity)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            UInt16 expMajorRev = Constant.ZERO;
            UInt16 expMinorRev = Constant.ZERO;
            UInt16 expBuildRev = Constant.ZERO;

            UInt16 bcmMajorRev;
            UInt16 bcmMinorRev;
            UInt16 bcmBuildRev;

            int bufferSize = profileIdentity.getProfileSize();
            byte[] identityBuffer = new byte[bufferSize];

          
            if ((returnCode = readFileIdentity(identityBuffer, profileIdentity.getProfileMsg())) == canReturnCode.NO_ERROR)
            {
                bcmMajorRev = Utils.extractUInt16(identityBuffer, Constant.TWO);
                bcmMinorRev = Utils.extractUInt16(identityBuffer, Constant.FOUR);
                bcmBuildRev = Utils.extractUInt16(identityBuffer, Constant.SIX);

                // Obtain the expected BCM revision.
                systemConfig.bcmRev.extractExpBcmRev(ref expMajorRev, ref expMinorRev, ref expBuildRev);

                // Compare read revision against revision that was expected.
                if (UserIO.forceErrorDisplay || (bcmMajorRev != expMajorRev))
                {
                    UserIO.outputExpActShort(outDev.ALL, msgType.ERROR, "Identity BCM Major Rev", expMajorRev, bcmMajorRev);
                }

                if (UserIO.forceErrorDisplay || (bcmMinorRev != expMinorRev))
                {
                    UserIO.outputExpActShort(outDev.ALL, msgType.ERROR, "Identity BCM Minor Rev", expMinorRev, bcmMinorRev);
                }

                if (UserIO.forceErrorDisplay || (bcmBuildRev != expBuildRev))
                {
                    UserIO.outputExpActShort(outDev.ALL, msgType.ERROR, "Identity BCM Build Rev", expBuildRev, bcmBuildRev);
                }

                // If any revision data mis-compates, return an error.
                if ((bcmMajorRev != expMajorRev) || (bcmMinorRev != expMinorRev) || (bcmBuildRev != expBuildRev))
                {
                    returnCode = canReturnCode.DATA_ERROR;
                }

            }

            return returnCode;
        }
        public canReturnCode testRoddenberrySoc(formatBasicProfile socNVM)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            const int SOC_KEY = 0x12345678;
            const int SOC_KEY_OFFSET = 0x4;
            const int SOC_SCALE_OFFSET = 0x8;

            int bufferSize = socNVM.getProfileSize();
            byte[] socBuffer = new byte[bufferSize];

            UInt32 expSocKey;
            UInt32 actSocKey;

            UInt16 expSocScale;
            UInt16 actSocScale;

            if ((returnCode = readSocNVM (socBuffer, socNVM.getProfileMsg())) == canReturnCode.NO_ERROR)
            {
                actSocKey = Utils.extractUInt32 (socBuffer, SOC_KEY_OFFSET);
                expSocKey = SOC_KEY;

                if (UserIO.forceErrorDisplay || (expSocKey != actSocKey))
                {
                    UserIO.outputExpActLong(outDev.ALL, msgType.ERROR, "SOC Key", expSocKey, actSocKey);
                }

                actSocScale = Utils.extractUInt16(socBuffer, SOC_SCALE_OFFSET);
                expSocScale = Constant.HUNDRED;

                if (UserIO.forceErrorDisplay || (expSocScale != actSocScale))
                {
                    UserIO.outputExpActShort(outDev.ALL, msgType.ERROR, "SOC Scale", expSocScale, actSocScale);
                }

                if ((expSocKey != actSocKey) || (expSocScale != actSocScale))
                {
                    returnCode = canReturnCode.DATA_ERROR;
                }

            }

            return returnCode;
        }

        public canReturnCode testSunriseSoc(formatBasicProfile socNVM)
        {
            const int SOC_KEY = 0x12345678;
            const int SOC_KEY_OFFSET = 0x4;

            canReturnCode returnCode = canReturnCode.NO_ERROR;

            int bufferSize = socNVM.getProfileSize();
            byte[] socBuffer = new byte[bufferSize];

            UInt32 expSocKey;
            UInt32 actSocKey;

            if ((returnCode = readSocNVM (socBuffer, socNVM.getProfileMsg())) == canReturnCode.NO_ERROR)
            {
                actSocKey = Utils.extractUInt32(socBuffer, SOC_KEY_OFFSET);
                expSocKey = SOC_KEY;

                if (UserIO.forceErrorDisplay || (expSocKey != actSocKey))
                {
                    UserIO.outputExpActLong(outDev.ALL, msgType.ERROR, "SOC Key", expSocKey, actSocKey);
                }

                if (expSocKey != actSocKey)
                {
                    returnCode = canReturnCode.DATA_ERROR;
                }
            }
       
            return returnCode;
        }
        public canReturnCode readMiscHistogram(byte[] miscBuffer, string profileTitle)
        {
            return myReadInterface.readProfile(profileTitle, miscBuffer, EEPROM_HISTOGRAM_MISC_ADDR);
        }

        public canReturnCode readVoltageHistogram(byte[] voltBuffer, string profileTitle)
        {
            return myReadInterface.readProfile(profileTitle, voltBuffer, EEPROM_HISTOGRAM_VOLT_ADDR);
        }

        public canReturnCode readSocHistogram(byte[] socBuffer, string profileTitle)
        {
            return myReadInterface.readProfile(profileTitle, socBuffer, EEPROM_HISTOGRAM_SOC_ADDR);
        }
        public canReturnCode readCurrentHistogram(byte[] currentBuffer, string profileTitle)
        {
            return myReadInterface.readProfile(profileTitle, currentBuffer, EEPROM_HISTOGRAM_CURRENT_ADDR);
        }
        public canReturnCode readPowerHistogram(byte[] powerBuffer, string profileTitle)
        {
            return myReadInterface.readProfile(profileTitle, powerBuffer, EEPROM_HISTOGRAM_POWER_ADDR);
        }
        public canReturnCode readMetaData(byte[] metaBuffer, string profileTitle)
        {
            return myReadInterface.readProfile(profileTitle, metaBuffer, EEPROM_FILE_METADATA_ADDR);
        }
        public canReturnCode readFileIdentity(byte[] identityBuffer, string profileTitle)
        {
            return myReadInterface.readProfile(profileTitle, identityBuffer, EEPROM_FILE_IDENTITY_ADDR);
        }

        public canReturnCode readFileCalibration(byte[] calibBuffer, string profileTitle)
        {
            return myReadInterface.readProfile(profileTitle, calibBuffer, EEPROM_FILE_CALIBRATION_ADDR);
        }

        public canReturnCode readFileHistory(byte[] historyBuffer, string profileTitle)
        {
            return myReadInterface.readProfile(profileTitle, historyBuffer, EEPROM_FILE_HISTORY_ADDR);
        }
        public canReturnCode readSocNVM(byte[] socBuffer, string profileTitle)
        {
            return myReadInterface.readProfile(profileTitle, socBuffer, EEPROM_FILE_SOC_ADDR);
        }
        public canReturnCode displayBcmHistogram(dispCntl displayControl)
        {

            canReturnCode returnCode = canReturnCode.NO_ERROR;
            canReturnCode savedReturnCode = canReturnCode.NO_ERROR;

            // Display Meta Data 
            if ((returnCode = displayMetaData(displayControl)) != canReturnCode.NO_ERROR)
            {
                savedReturnCode = returnCode;
            }

            // Display history 
            if ((returnCode = displayFileHistory(displayControl)) != canReturnCode.NO_ERROR)
            {
                savedReturnCode = returnCode;
            }

            // Display Identity
            if ((returnCode = displayIdentity(displayControl)) != canReturnCode.NO_ERROR)
            {
                savedReturnCode = returnCode;
            }

            // Display calibration histogram
            if ((returnCode = displayFileCalibration(displayControl)) != canReturnCode.NO_ERROR)
            {
                savedReturnCode = returnCode;
            }

            // Display Miscellaneious profile
            if ((returnCode = displayMiscHistogram(displayControl)) != canReturnCode.NO_ERROR)
            {
                savedReturnCode = returnCode;
            }

            // Display current profile
            if ((returnCode = displayCurrentHistogram(displayControl)) != canReturnCode.NO_ERROR)
            {
                savedReturnCode = returnCode;
            }

            // Display voltage profile
            if ((returnCode = displayVoltageHistogram(displayControl)) != canReturnCode.NO_ERROR)
            {
                savedReturnCode = returnCode;
            }

            // Display power profile
            if ((returnCode = displayPowerHistogram(displayControl)) != canReturnCode.NO_ERROR)
            {
                savedReturnCode = returnCode;
            }

            // Display SOC profile
            if ((returnCode = displaySocHistogram(displayControl)) != canReturnCode.NO_ERROR)
            {
                savedReturnCode = returnCode;
            }

            // If there was a failure in the saved return code, return the saved return code.
            if ((returnCode == canReturnCode.NO_ERROR) && (savedReturnCode != canReturnCode.NO_ERROR))
            {
                returnCode = savedReturnCode;
            }

            // If there was a failure in the saved return code, return the saved return code.
            if ((returnCode == canReturnCode.NO_ERROR) && (savedReturnCode != canReturnCode.NO_ERROR))
            {
                returnCode = savedReturnCode;
            }

            UserIO.outputInfo("End of BCM Histogram Display.");
            return returnCode;
        }

        public virtual canReturnCode displayMiscHistogram(dispCntl displayControl)
        {
            return canReturnCode.NO_ERROR;
        }

        public virtual canReturnCode displayVoltageHistogram(dispCntl displayControl)
        {
            return canReturnCode.NO_ERROR;
        }

        public virtual canReturnCode displaySocHistogram(dispCntl displayControl)
        {
            return canReturnCode.NO_ERROR;
        }

        public virtual canReturnCode displayCurrentHistogram(dispCntl displayControl)
        {
            return canReturnCode.NO_ERROR;
        }

        public virtual canReturnCode displayPowerHistogram(dispCntl displayControl)
        {
            return canReturnCode.NO_ERROR;
        }

        public virtual canReturnCode displayMetaData(dispCntl displayControl)
        {
            return canReturnCode.NO_ERROR;
        }

        public virtual canReturnCode displayIdentity(dispCntl displayControl)
        {
            return canReturnCode.NO_ERROR;
        }

        public virtual canReturnCode displayFileCalibration(dispCntl displayControl)
        {
            return canReturnCode.NO_ERROR;
        }

        public virtual canReturnCode displayFileHistory(dispCntl displayControl)
        {
            return canReturnCode.NO_ERROR;
        }

        public virtual canReturnCode displayFileHistoryAlarms(dispCntl displayControl)
        {
            return canReturnCode.NO_ERROR;
        }

        public virtual canReturnCode displayFileHistoryResets(dispCntl displayControl)
        {
            return canReturnCode.NO_ERROR;
        }

        public virtual canReturnCode displayFileHistoryPower(dispCntl displayControl)
        {
            return canReturnCode.NO_ERROR;
        }

        public virtual canReturnCode displayFileHistoryMBB(dispCntl displayControl)
        {
            return canReturnCode.NO_ERROR;
        }

        public virtual canReturnCode displaySocNvm(dispCntl displayControl)
        {
            return canReturnCode.NO_ERROR;
        }

   
        public virtual canReturnCode testFileIdentity()
        {
            return canReturnCode.NO_ERROR;
        }

        public virtual canReturnCode testSocNvm()
        {
            return canReturnCode.NO_ERROR;
        }

    }  // End of class dataLogBCM

    public class nullDataLogBCM : dataLogBCM
    {
        public nullDataLogBCM (profileReadInterface readInterface) : base(readInterface)
        {
        }
    } // End of class nullDataLogBCM
    public class roddenberryDataLogBCM : dataLogBCM
    {

        roddenberryProfileMisc profileMisc;
        roddenberryProfileVoltage profileVoltage;
        roddenberryProfileSoc profileSoc;
        roddenberryProfileCurrent profileCurrent;
        roddenberryProfilePower profilePower;
        roddenberryProfileMetaData profileMetaData;
        roddenberryProfileIdentity profileIdentity;
        roddenberryProfileCalibration profileCalibration;
        roddenberryProfileHistory profileHistory;
        roddenberryProfileHistoryAlarms profileHistoryAlarms;
        roddenberryProfileHistoryResets profileHistoryResets;
        roddenberryProfileHistoryPower profileHistoryPower;
        roddenberryProfileHistoryMBB profileHistoryMBB;
        roddenberrySocNvm socNvm;

        public roddenberryDataLogBCM(profileReadInterface readInterface) : base(readInterface)
        {
            profileMisc = new roddenberryProfileMisc();
            profileVoltage = new roddenberryProfileVoltage();
            profileSoc = new roddenberryProfileSoc();
            profileCurrent = new roddenberryProfileCurrent();
            profilePower = new roddenberryProfilePower();
            profileMetaData = new roddenberryProfileMetaData();
            profileIdentity = new roddenberryProfileIdentity();
            profileCalibration = new roddenberryProfileCalibration();
            profileHistory = new roddenberryProfileHistory();
            profileHistoryAlarms = new roddenberryProfileHistoryAlarms();
            profileHistoryResets = new roddenberryProfileHistoryResets();
            profileHistoryPower = new roddenberryProfileHistoryPower();
            profileHistoryMBB = new roddenberryProfileHistoryMBB();
            socNvm = new roddenberrySocNvm();
        }
        public override canReturnCode displayVoltageHistogram(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readVoltageHistogram, profileVoltage);
        }
     
        public override canReturnCode displaySocHistogram(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readSocHistogram, profileSoc);
        }
  
        public override canReturnCode displayCurrentHistogram(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readCurrentHistogram, profileCurrent);
        }

        public override canReturnCode displayPowerHistogram(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readPowerHistogram, profilePower);
        }
 
        public override canReturnCode displayMetaData(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readMetaData, profileMetaData);
        }

        public override canReturnCode displayMiscHistogram(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readMiscHistogram, profileMisc);
        }

        public override canReturnCode displayIdentity(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readFileIdentity, profileIdentity);
        }

        public override canReturnCode displayFileCalibration(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readFileCalibration, profileCalibration);
        }


        public override canReturnCode displayFileHistory(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readFileHistory, profileHistory);
        }

        public override canReturnCode displayFileHistoryAlarms(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readFileHistory, profileHistoryAlarms);
        }

        public override canReturnCode displayFileHistoryResets(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readFileHistory, profileHistoryResets);
        }

        public override canReturnCode displayFileHistoryPower(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readFileHistory, profileHistoryPower);
        }

        public override canReturnCode displayFileHistoryMBB(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readFileHistory, profileHistoryMBB);
        }

        public override canReturnCode displaySocNvm(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readSocNVM, socNvm);
        }

        public override canReturnCode testFileIdentity()
        {
            return testIdentity(profileIdentity);
        }
        public override canReturnCode testSocNvm()
        {
            return testRoddenberrySoc (socNvm);
        }

       
    }  // End of class roddenberryDataLogBCM

    public class switchboardDataLogBCM : dataLogBCM
    {
        switchboardProfileMisc profileMisc;
        switchboardProfileVoltage profileVoltage;
        switchboardSocNvm socNvm;

        // The following roddenberry classes work for switchboard.
        roddenberryProfileSoc profileSoc;
        roddenberryProfileCurrent profileCurrent;
        roddenberryProfilePower profilePower;
        roddenberryProfileMetaData profileMetaData;
        roddenberryProfileIdentity profileIdentity;
        roddenberryProfileCalibration profileCalibration;
        roddenberryProfileHistory profileHistory;
        roddenberryProfileHistoryAlarms profileHistoryAlarms;
        roddenberryProfileHistoryResets profileHistoryResets;
        roddenberryProfileHistoryPower profileHistoryPower;
        roddenberryProfileHistoryMBB profileHistoryMBB;

        public switchboardDataLogBCM (profileReadInterface readInterface) : base(readInterface)
        {
            profileMisc = new switchboardProfileMisc();
            profileVoltage = new switchboardProfileVoltage();
            socNvm = new switchboardSocNvm();

            // The following roddenberry classes work for switchboard.
            profileSoc = new roddenberryProfileSoc();
            profileCurrent = new roddenberryProfileCurrent();
            profilePower = new roddenberryProfilePower();
            profileMetaData = new roddenberryProfileMetaData();
            profileIdentity = new roddenberryProfileIdentity();
            profileCalibration = new roddenberryProfileCalibration();
            profileHistory = new roddenberryProfileHistory();
            profileHistoryAlarms = new roddenberryProfileHistoryAlarms();
            profileHistoryResets = new roddenberryProfileHistoryResets();
            profileHistoryPower = new roddenberryProfileHistoryPower();
            profileHistoryMBB = new roddenberryProfileHistoryMBB();
        }

        
        public override canReturnCode displayVoltageHistogram(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readVoltageHistogram, profileVoltage);
        }
        public override canReturnCode displaySocHistogram(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readSocHistogram, profileSoc);
        }

        public override canReturnCode displayCurrentHistogram(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readCurrentHistogram, profileCurrent);
        }

        public override canReturnCode displayPowerHistogram(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readPowerHistogram, profilePower);
        }

        public override canReturnCode displayMetaData(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readMetaData, profileMetaData);
        }

        public override canReturnCode displayMiscHistogram(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readMiscHistogram, profileMisc);
        }

        public override canReturnCode displayIdentity(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readFileIdentity, profileIdentity);
        }

        public override canReturnCode displayFileCalibration(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readFileCalibration, profileCalibration);
        }
        public override canReturnCode displayFileHistory(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readFileHistory, profileHistory);
        }

        public override canReturnCode displayFileHistoryAlarms(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readFileHistory, profileHistoryAlarms);
        }

        public override canReturnCode displayFileHistoryResets(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readFileHistory, profileHistoryResets);
        }

        public override canReturnCode displayFileHistoryPower(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readFileHistory, profileHistoryPower);
        }


        public override canReturnCode displayFileHistoryMBB(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readFileHistory, profileHistoryMBB);
        }
        public override canReturnCode displaySocNvm(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readSocNVM, socNvm);
        }

  
        public override canReturnCode testFileIdentity()
        {
            return testIdentity(profileIdentity);
        }

        public override canReturnCode testSocNvm()
        {
            // Switchboard uses the same SOC format as Roddenberry.
            return testRoddenberrySoc(socNvm);
        }

    }  // End of class switchboardDataLogBCM

    public class sunriseDataLogBCM : dataLogBCM
    {
        // The following roddenberry classes work for sunrise.
        roddenberryProfileMisc profileMisc;
        roddenberryProfileVoltage profileVoltage;
        roddenberryProfileSoc profileSoc;
        roddenberryProfileCurrent profileCurrent;
        roddenberryProfilePower profilePower;
        roddenberryProfileMetaData profileMetaData;
        roddenberryProfileIdentity profileIdentity;
        roddenberryProfileCalibration profileCalibration;
        roddenberryProfileHistory profileHistory;
        roddenberryProfileHistoryAlarms profileHistoryAlarms;
        roddenberryProfileHistoryResets profileHistoryResets;
        roddenberryProfileHistoryPower profileHistoryPower;
        roddenberryProfileHistoryMBB profileHistoryMBB;

        sunriseSocNvm socNvm;
        public sunriseDataLogBCM (profileReadInterface readInterface) : base(readInterface)
        {
            // The following roddenberry classes work for sunrise.
            profileMisc = new roddenberryProfileMisc();
            profileVoltage = new roddenberryProfileVoltage();
            profileSoc = new roddenberryProfileSoc();
            profileCurrent = new roddenberryProfileCurrent();
            profilePower = new roddenberryProfilePower();
            profileMetaData = new roddenberryProfileMetaData();
            profileIdentity = new roddenberryProfileIdentity();
            profileCalibration = new roddenberryProfileCalibration();
            profileHistory = new roddenberryProfileHistory();
            profileHistoryAlarms = new roddenberryProfileHistoryAlarms();
            profileHistoryResets = new roddenberryProfileHistoryResets();
            profileHistoryPower = new roddenberryProfileHistoryPower();
            profileHistoryMBB = new roddenberryProfileHistoryMBB();

            socNvm = new sunriseSocNvm();
        }

        public override canReturnCode displayVoltageHistogram(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readVoltageHistogram, profileVoltage);
        }

        public override canReturnCode displaySocHistogram(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readSocHistogram, profileSoc);
        }

        public override canReturnCode displayCurrentHistogram(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readCurrentHistogram, profileCurrent);
        }
        public override canReturnCode displayPowerHistogram(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readPowerHistogram, profilePower);
        }

        public override canReturnCode displayMetaData(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readMetaData, profileMetaData);
        }

        public override canReturnCode displayMiscHistogram(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readMiscHistogram, profileMisc);
        }

        public override canReturnCode displayIdentity(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readFileIdentity, profileIdentity);
        }

        public override canReturnCode displayFileCalibration(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readFileCalibration, profileCalibration);
        }

        public override canReturnCode displayFileHistory(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readFileHistory, profileHistory);
        }

        public override canReturnCode displayFileHistoryAlarms(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readFileHistory, profileHistoryAlarms);
        }

        public override canReturnCode displayFileHistoryResets(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readFileHistory, profileHistoryResets);
        }

        public override canReturnCode displayFileHistoryPower(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readFileHistory, profileHistoryPower);
        }

        public override canReturnCode displayFileHistoryMBB(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readFileHistory, profileHistoryMBB);
        }
        public override canReturnCode displaySocNvm(dispCntl displayControl)
        {
            return displayProfileData(displayControl, readSocNVM, socNvm);
        }

        public override canReturnCode testFileIdentity()
        {
            return testIdentity(profileIdentity);
        }

        public override canReturnCode testSocNvm()
        {
            return testSunriseSoc(socNvm);
        }

    }  // End of class sunriseDataLogBCM

    public class optimusDataLogBCM : dataLogBCM
    {
        public optimusDataLogBCM(profileReadInterface readInterface) : base(readInterface)
        {
        }
    } // End of class optimusDataLogBCM
}

