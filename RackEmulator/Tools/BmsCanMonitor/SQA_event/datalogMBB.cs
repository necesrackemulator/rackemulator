﻿//*************************************************************************
//
// Copyright © 2014 NEC Energy Solutions, Inc. All rights reserved.
//
// NEC Energy Solutions Proprietary Information.
// This document contains proprietary and confidential information and is
// tendered for information and evaluation purposes only; no copy or other
// use or dissemination by a third party can be made of this information
// without the express written permission of NEC Energy Solutions.
//
//*************************************************************************

using System;
using System.Linq;
using System.Text;
using SQA_can;
using SQA_config;
using SQA_formatEvent;
using SQA_utils;

namespace SQA_event
{
    public class dataLogMBB
    {
        // These constants define offsets in the BCM NVRAM containing MBB data logs.

        public const int EEPROM_MBB_METADATA_ADDR = 0x03E0;
        public const int EEPROM_MBB_VOLTAGE_ADDR = 0x03F8;
        public const int EEPROM_MBB_TEMP1_ADDR = 0x0424;
        public const int EEPROM_MBB_TEMP2_ADDR = 0x0458;
        public const int EEPROM_MBB_TEMP3_ADDR = 0x048C;
        public const int EEPROM_MBB_TEMP4_ADDR = 0x04C0;
        public const int EEPROM_MBB_TEMP_PCB_ADDR = 0x04F4;
        public const int EEPROM_MBB_VOLTAGE_CELL1_ADDR = 0x0528;
        public const int EEPROM_MBB_BALANCER_ADDR = 0x06FC;

        public profileReadInterface myReadInterface;
        public dataLogMBB (profileReadInterface readInterface, int maxModule, int maxCell)
        {
            myReadInterface = readInterface;
        }
        public canReturnCode displayModuleProfileData(int moduleNumber, dispCntl displayControl, Func<byte[], int, string, canReturnCode> readMbbProfileFunction, formatModuleProfile myModuleProfile)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            int bufferSize = myModuleProfile.getProfileSize();
            string profileTitle = String.Format("MBB {0} {1}", moduleNumber, myModuleProfile.getProfileMsg());

            // Create arrays to hold the last data and current MBB data.
            byte[] oneMbb = new byte[bufferSize];
            byte[] oneMbbLast = new byte[bufferSize];

            // Extract the last read data so that it can be compared (diff'ed) later.
            myModuleProfile.extractModuleProfileData(moduleNumber, oneMbbLast);

            if ((returnCode = readMbbProfileFunction(oneMbb, moduleNumber, profileTitle)) == canReturnCode.NO_ERROR)
            {
                // Save the read data for a given MBB.
                myModuleProfile.saveModuleProfileData(moduleNumber, oneMbb);
                myModuleProfile.formatProfile(profileTitle, oneMbb, oneMbbLast, moduleNumber, displayControl);
            }
            else
            {
                UserIO.outputError(String.Format("Failure to read {0}.", profileTitle));
            }
            return returnCode;
        }

        public canReturnCode displayCellProfileData(int moduleNumber, int cellNumber, dispCntl displayControl, Func<byte[], int, int, string, canReturnCode> readMbbProfileFunction, formatModuleCellProfile myModuleProfile)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            int bufferSize = myModuleProfile.getProfileSize();
            string profileTitle = String.Format("MBB {0} Cell {1} {2}", moduleNumber, cellNumber, myModuleProfile.getProfileMsg());

            // Create arrays to hold the last data and current MBB data.
            byte[] oneMbb = new byte[bufferSize];
            byte[] oneMbbLast = new byte[bufferSize];

            // Extract the last read data so that it can be compared (diff'ed) later.
            myModuleProfile.extractCellProfileData(moduleNumber, cellNumber, oneMbbLast);

            if (returnCode == canReturnCode.NO_ERROR)
            {

                if ((returnCode = readMbbProfileFunction(oneMbb, moduleNumber, cellNumber, profileTitle)) == canReturnCode.NO_ERROR)
                {
                    // Save the read data for a given MBB.
                    myModuleProfile.saveCellProfileData(moduleNumber, cellNumber, oneMbb);
                    myModuleProfile.formatProfile(profileTitle, oneMbb, oneMbbLast, moduleNumber, displayControl);
                }
                else
                {
                    UserIO.outputError(String.Format("Failure to read {0}.", profileTitle));
                }
            }
            return returnCode;
        }

        public canReturnCode displayModuleCellVoltages(int moduleNumber, dispCntl displayControl)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            canReturnCode savedReturnCode = canReturnCode.NO_ERROR;

            int cellNumber;

            // Loop over the entered range of cells.
            for (cellNumber = cellConfig.userMinCell; cellNumber <= cellConfig.userMaxCell; cellNumber++)
            {
                if ((returnCode = displayModuleCellVoltage(moduleNumber, cellNumber, displayControl)) != canReturnCode.NO_ERROR)
                {
                    // Save the failing return code.
                    savedReturnCode = returnCode;
                }
            }

            // If there was a failure in the saved return code, return the saved return code.
            if ((returnCode == canReturnCode.NO_ERROR) && (savedReturnCode != canReturnCode.NO_ERROR))
            {
                returnCode = savedReturnCode;
            }
            return returnCode;
        }

        public virtual canReturnCode displayHistogram(int moduleNumber, dispCntl displayControl)
        {
            return canReturnCode.NO_ERROR;
        }

        public virtual canReturnCode displayProfileRev(int moduleNumber, dispCntl displayControl)
        {
            return canReturnCode.NO_ERROR;
        }
        public virtual canReturnCode displayModuleVoltage(int moduleNumber, dispCntl displayControl)
        {
            return canReturnCode.NO_ERROR;
        }

        public virtual canReturnCode displayProfileCurrent(int moduleNumber, dispCntl displayControl)
        {
            return canReturnCode.NO_ERROR;
        }

        public virtual canReturnCode displayProfileSoc(int moduleNumber, dispCntl displayControl)
        {
            return canReturnCode.NO_ERROR;
        }

        public virtual canReturnCode displayTemp(int moduleNumber, dispCntl displayControl)
        {
            return canReturnCode.NO_ERROR;
        }

        public virtual canReturnCode displayBalancer(int moduleNumber, dispCntl displayControl)
        {
            return canReturnCode.NO_ERROR;
        }

        public virtual canReturnCode displayWattHours(int moduleNumber, dispCntl displayControl)
        {
            return canReturnCode.NO_ERROR;
        }

        public virtual canReturnCode displayProfileAlarms (int moduleNumber, dispCntl displayControl)
        {
            return canReturnCode.NO_ERROR;
        }

        public virtual canReturnCode displayModuleTempPCB(int moduleNumber, dispCntl displayControl)
        {
            return canReturnCode.NO_ERROR;
        }

        public virtual canReturnCode displayModuleMetaData(int moduleNumber, dispCntl displayControl)
        {
            return canReturnCode.NO_ERROR;
        }

        public virtual canReturnCode displayModuleData(int moduleNumber, dispCntl displayControl)
        {
            return canReturnCode.NO_ERROR;
        }

        public virtual canReturnCode displayCellBalancer(int moduleNumber, dispCntl displayControl)
        {
            return canReturnCode.NO_ERROR;
        }

        public virtual canReturnCode displayModuleCellVoltage(int moduleNumber, int cellNumber, dispCntl displayControl)
        {
            return canReturnCode.NO_ERROR;
        }

    } // End of class dataLogMBB

    public class nullDataLogMBB : dataLogMBB
    {
        public nullDataLogMBB(profileReadInterface readInterface, int maxModule, int maxCell) : base (readInterface, maxModule, maxCell)
        {
        }
    }  // End of class nullDataLogMBB

    public class optimusDataLogMBB : dataLogMBB
    {
        public optimusDataLogMBB(profileReadInterface readInterface, int maxModule, int maxCell) : base(readInterface, maxModule, maxCell)
        {
        }
    }  // End of class optimusDataLogMBB
}




