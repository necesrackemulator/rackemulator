﻿//*************************************************************************
//
// Copyright © 2014 NEC Energy Solutions, Inc. All rights reserved.
//
// NEC Energy Solutions Proprietary Information.
// This document contains proprietary and confidential information and is
// tendered for information and evaluation purposes only; no copy or other
// use or dissemination by a third party can be made of this information
// without the express written permission of NEC Energy Solutions.
//
//*************************************************************************

using System;
using SQA_bms;
using SQA_modcan;
using SQA_can;
using SQA_utils;

namespace SQA_event
{
    public class profileReadInterface
    {
        public enum typeInterface : int
        {
            BCM_RMBA = Constant.ZERO,
            MBB_RDBI,
            HOST_UDS,
        };

        private hostCanOperations myhostCanOps;
        private moduleBCM myBCM;
        private moduleMBB myMBB;

        private typeInterface interfaceType;
        public profileReadInterface(typeInterface typeInter, hostCanOperations hostCanOps, moduleBCM BCM, moduleMBB MBB)
        {
            interfaceType = typeInter;
            myhostCanOps = hostCanOps;
            myBCM = BCM;
            myMBB = MBB;
        }

        public UInt16 getMbbBlockAddress(UInt16 offset, int moduleNumber)
        {
            const int EEPROM_MODULE_SIZE = 0x360;

            return (UInt16)(offset + ((moduleNumber - Constant.ONE) * EEPROM_MODULE_SIZE));
        }

        public canReturnCode readProfile(string profileName, byte[] buffer, UInt16 serviceConstant)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

         
            byte expByteCount;
            UInt32 blockAddr;

            expByteCount = (byte)buffer.Length;

            string hdrMsg = String.Format("{0} Profile", profileName);

            switch (interfaceType)
            {
                case typeInterface.HOST_UDS:
                    {
                        UserIO.outputWarning ("readProfile HOST_UDS not presently used.");
                    }
                    break;
                case typeInterface.MBB_RDBI:
                    {
                        UserIO.outputWarning("readProfile MBB_RDBI not presently used.");
                    }
                    break;

                case typeInterface.BCM_RMBA:
                    {
                        blockAddr = serviceConstant;
                        returnCode = myBCM.readDataLogBlock(blockAddr, buffer);
                    }
                    break;

                default:
                    {
                        returnCode = unknownInterface (interfaceType);
                    }
                    break;
            }

            return returnCode;
        }

        public canReturnCode readProfileMBB(int moduleNumber, string profileName, byte[] buffer, UInt16 serviceConstant)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            UInt32 blockAddr;

            string hdrMsg = String.Format("MBB {0} {1} Profile", moduleNumber, profileName);

            switch (interfaceType)
            {
                case typeInterface.HOST_UDS:
                    {
                        if ((returnCode = myhostCanOps.writeUDSIndex(moduleNumber, serviceConstant, buffer)) == canReturnCode.NO_ERROR)
                        {
                            if (!Utils.convertArrayLittleToBigEndian(buffer))
                            {
                                returnCode = canReturnCode.DATA_ERROR;
                            }
                        }
                    }
                    break;
                case typeInterface.MBB_RDBI:
                    {
                        if ((returnCode = myMBB.readMbbLogData(moduleNumber, buffer, serviceConstant)) == canReturnCode.NO_ERROR)
                        {
                            if (!Utils.convertArrayLittleToBigEndian(buffer))
                            {
                                returnCode = canReturnCode.DATA_ERROR;
                            }
                        }
                    }
                    break;

                case typeInterface.BCM_RMBA:
                    {
                        blockAddr = getMbbBlockAddress(serviceConstant, moduleNumber);
                        returnCode = myBCM.readDataLogBlock(blockAddr, buffer);
                    }
                    break;

                default:
                    {
                        returnCode = unknownInterface(interfaceType);
                    }
                    break;
            }

            if (returnCode != canReturnCode.NO_ERROR)
            {
                UserIO.outputError(String.Format("Failed to read {0}.", hdrMsg));
            }

            return returnCode;
        }

        public canReturnCode readCellProfile(int moduleNumber, int cellNumber, string profileName, byte[] buffer, UInt16 serviceConstant)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            UInt32 blockAddr;

            string hdrMsg = String.Format("MBB {0} {1} Profile", moduleNumber, profileName);

            switch (interfaceType)
            {
                case typeInterface.HOST_UDS:
                    {
                        returnCode = writeCellIndex(moduleNumber, cellNumber);

                        if (returnCode == canReturnCode.NO_ERROR)
                        {
                            if ((returnCode = myhostCanOps.writeUDSIndex(moduleNumber, serviceConstant, buffer)) == canReturnCode.NO_ERROR)
                            {
                                if (!Utils.convertArrayLittleToBigEndian(buffer))
                                {
                                    returnCode = canReturnCode.DATA_ERROR;
                                }
                            }
                        }
                    }
                    break;
                case typeInterface.MBB_RDBI:
                    {
                        if ((returnCode = myMBB.readMbbLogData(moduleNumber, buffer, serviceConstant)) == canReturnCode.NO_ERROR)
                        {
                            if (!Utils.convertArrayLittleToBigEndian(buffer))
                            {
                                returnCode = canReturnCode.DATA_ERROR;
                            }
                        }
                       
                    }
                    break;

                case typeInterface.BCM_RMBA:
                    {
                        blockAddr = (UInt32)(getMbbBlockAddress(serviceConstant, moduleNumber) + ((cellNumber - Constant.ONE) * buffer.Length));
                        returnCode = myBCM.readDataLogBlock(blockAddr, buffer);
                    }
                    break;

                default:
                    {
                        returnCode = unknownInterface(interfaceType);
                    }
                    break;
            }

            if (returnCode != canReturnCode.NO_ERROR)
            {
                UserIO.outputError(String.Format("Failed to read {0}.", hdrMsg));
            }

            return returnCode;
        }

        
        public canReturnCode transferDataMBB (int moduleNumber, string profileName, byte[] readBuffer, UInt16 serviceDID)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            string hdrMsg = String.Format("MBB {0} {1}", moduleNumber, profileName);

            switch (interfaceType)
            {
                case typeInterface.HOST_UDS:
                    {
                        returnCode = myhostCanOps.writeUDSIndex(moduleNumber, serviceDID, readBuffer);
                    }
                    break;

                case typeInterface.MBB_RDBI:
                    {
                        returnCode = myMBB.readMbbLogData(moduleNumber, readBuffer, serviceDID);
                    }
                    break;

                case typeInterface.BCM_RMBA:
                    {
                        UserIO.outputWarning("transferDataMBB BCM_RMBA not presently used.");
                    }
                    break;

                default:
                    {
                        returnCode = unknownInterface(interfaceType);
                    }
                    break;
            }

            if (returnCode != canReturnCode.NO_ERROR)
            {
                UserIO.outputError(String.Format("Failed to read {0}.", hdrMsg));
            }
            
            return returnCode;
        }
        public canReturnCode writeCellIndex(int moduleNumber, int cellNumber)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            if (interfaceType == typeInterface.HOST_UDS)
            {
                returnCode = myhostCanOps.writeCellGroup(moduleNumber, cellNumber);
            }
            return returnCode;
        }

        public canReturnCode writeCriticalIndex(int moduleNumber, UInt16 recordIndex)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            switch (interfaceType)
            {
                case typeInterface.HOST_UDS:
                    {
                        returnCode = myhostCanOps.writeCriticalIndex(moduleNumber, recordIndex);
                    }
                    break;

                case typeInterface.MBB_RDBI:
                    {
                        returnCode = myMBB.setCriticalRecordIndex (moduleNumber, recordIndex);
                    }
                    break;

                default:
                    {
                        returnCode = unknownInterface(interfaceType);
                    }
                    break;
            }
            return returnCode;
        }

        public canReturnCode writeEventIndex(int moduleNumber, UInt16 recordIndex)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            switch (interfaceType)
            {
                case typeInterface.HOST_UDS:
                    {
                        returnCode = myhostCanOps.writeEventIndex(moduleNumber, recordIndex);
                    }
                    break;

                case typeInterface.MBB_RDBI:
                    {
                        returnCode = myMBB.setEventRecordIndex(moduleNumber, recordIndex);
                    }
                    break;

                default:
                    {
                        returnCode = unknownInterface(interfaceType);
                    }
                    break;
            }
            return returnCode;
        }

        private canReturnCode unknownInterface(typeInterface interfaceType)
        {

            UserIO.outputError(String.Format("Unknown typeInterface: {0}", interfaceType));

            return canReturnCode.CAN_DEV_UNSUPPORTED;
        }

    } // End of class profileReadInterface

}
 
                           
