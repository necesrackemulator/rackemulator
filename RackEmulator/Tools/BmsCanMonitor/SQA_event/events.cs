﻿//*************************************************************************
//
// Copyright © 2014 NEC Energy Solutions, Inc. All rights reserved.
//
// NEC Energy Solutions Proprietary Information.
// This document contains proprietary and confidential information and is
// tendered for information and evaluation purposes only; no copy or other
// use or dissemination by a third party can be made of this information
// without the express written permission of NEC Energy Solutions.
//
//*************************************************************************

using System;
using SQA_modcan;
using SQA_can;
using SQA_config;
using SQA_utils;

namespace SQA_event
{
    public class events
    {
        public eventLogMBB _eventLogMBB;
        public profileReadInterface myReadInterface;

        static UInt32[] recordNumberSavedCritical = new UInt32[moduleConfig.MAX_MOD_ADDRESS];
        static UInt32[] numberWritesSavedCritical = new UInt32[moduleConfig.MAX_MOD_ADDRESS];

        static UInt32[] recordNumberSavedEvent = new UInt32[moduleConfig.MAX_MOD_ADDRESS];
        static UInt32[] numberWritesSavedEvent = new UInt32[moduleConfig.MAX_MOD_ADDRESS];
        // Class constructor
        public events (profileReadInterface readInterface, eventLogMBB myEventLogMBB)
        {
            myReadInterface = readInterface;
            _eventLogMBB = myEventLogMBB;
        }

  
        public void getCriticalRecordRange(ref UInt16 minimumRecord, ref UInt16 maximumRecord)
        {
            const int MIN_CRITICAL_RECORD = 1;

            minimumRecord = MIN_CRITICAL_RECORD;
            maximumRecord = (UInt16)systemConfig.getMaxMbbCritical();
        }

        public void getEventRecordRange(ref UInt16 minimumRecord, ref UInt16 maximumRecord)
        {
            const int MIN_CHRONICLE_RECORD = 1;

            minimumRecord = MIN_CHRONICLE_RECORD;
            maximumRecord = (UInt16)systemConfig.getMaxMbbEvent();
        }

        public canReturnCode extractRecordRange(int moduleNumber, eventLogMBB.eventRecordType recordType, int minPossibleRecord, int maxPossibleRecord, ref int oldestRecord, ref int newestRecord)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            UInt32 numberWrites = Constant.ZERO;
            UInt16 recordNumber = Constant.ZERO;
            byte[] response = new byte[eventLogMBB.HDR_RESPONSE_LENGTH];

            
            if (recordType == eventLogMBB.eventRecordType.CRITICAL)
            {
                returnCode = getCriticalHdr (moduleNumber, response);
            }
            else
            {
                returnCode = getEventHdr (moduleNumber, response);
            }


            if (returnCode == canReturnCode.NO_ERROR)
            {
                _eventLogMBB.extractHdrData (response, ref recordNumber, ref numberWrites);

                newestRecord = recordNumber;
                if (numberWrites <= maxPossibleRecord)
                {
                    oldestRecord = minPossibleRecord;
                }
                else
                {
                    if (recordNumber == maxPossibleRecord)
                    {
                        oldestRecord = minPossibleRecord;
                    }
                    else
                    {
                        oldestRecord = recordNumber + Constant.ONE;
                    }
                }
            }

            return returnCode;
        }


        /***********************************************************************************/
        /*                                                                                 */
        /* Function:  Display a range of Critical records, provided they are within the    */
        /*            range of available records.                                          */
        /*                                                                                 */
        /* Arguments: minUserRecordNumber - minimum record number requested by the user.   */
        /*            maxUserRecordNumber - maximum record number requested by the user.   */
        /*            eventMBB - MBB event formatting object.                              */
        /*            shortOnly - if true, display only the short form of the record.      */
        /*                                                                                 */
        /* Returns:   canReturnCode.NO_ERROR - if there is no parsing error.           */
        /*            canReturnCode.ARG_ERROR - if there is a parsing error or the record  */
        /*            number is not within the range supported by the module.              */
        /*                                                                                 */
        /* Caveats:   None.                                                                */
        /*                                                                                 */
        /***********************************************************************************/

        public canReturnCode displayCriticalRecordsAllMbb(int minUserRecordNumber, int maxUserRecordNumber, bool shortOnly)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            int oldestRecordPresent = Constant.ZERO;
            int newestRecordPresent = Constant.ZERO;

            UInt16 minDefinedRecord = Constant.ZERO;
            UInt16 maxDefinedRecord = Constant.ZERO;

            int moduleNumber;

            // Obtain the range of records defined by the firmware.
            getCriticalRecordRange(ref minDefinedRecord, ref maxDefinedRecord);

            // Loop over all identified modules.
            for (moduleNumber = moduleConfig.userMinModule; moduleNumber <= moduleConfig.userMaxModule; moduleNumber++)
            {
                if ((returnCode = extractRecordRange(moduleNumber, eventLogMBB.eventRecordType.CRITICAL, minDefinedRecord, maxDefinedRecord, ref oldestRecordPresent, ref newestRecordPresent)) == canReturnCode.NO_ERROR)
                {
                    // Check if FIFO has wrapped around.
                    if (oldestRecordPresent <= newestRecordPresent)
                    {
                        // The FIFO has not wrapped around.
                        // Check if the user did not provide a range.
                        if (minUserRecordNumber == Constant.ZERO)
                        {
                            // No range entered by the user, display all records present.
                            returnCode = displayCriticalRecords(moduleNumber, oldestRecordPresent, newestRecordPresent, shortOnly);
                        }
                        else
                        {
                            // Check if the minUserRecordNumber and maxUserRecordNumber are within the defined range.
                            if (checkRecordLimits(moduleNumber, oldestRecordPresent, newestRecordPresent, minUserRecordNumber, maxUserRecordNumber))
                            {
                                // Display the range requested by the user.
                                returnCode = displayCriticalRecords(moduleNumber, minUserRecordNumber, maxUserRecordNumber, shortOnly);
                            }
                            else
                            {
                                returnCode = canReturnCode.ARG_ERROR;
                            }
                        }
                    }
                    else
                    {
                        // The FIFO has wrapped around.
                        // Check if an argument was entered.
                        if (minUserRecordNumber == Constant.ZERO)
                        {
                            // Display all records, starting with first logged record and ending with the last.
                            returnCode = displayCriticalRecords(moduleNumber, oldestRecordPresent, newestRecordPresent, shortOnly);
                        }
                        else
                        {
                            // Check if the minUserRecordNumber and maxUserRecordNumber are within the firmware defined range.
                            if (checkRecordLimits(moduleNumber, minDefinedRecord, maxDefinedRecord, minUserRecordNumber, maxUserRecordNumber))
                            {
                                // Display the range requested by the user.
                                returnCode = displayCriticalRecords(moduleNumber, minUserRecordNumber, maxUserRecordNumber, shortOnly);
                            }
                            else
                            {
                                returnCode = canReturnCode.ARG_ERROR;
                            }
                        }
                    }
                }
            }
            return returnCode;
        }

        /***********************************************************************************/
        /*                                                                                 */
        /* Function:  Display a range of Event records, provided they are within the       */
        /*            range of available records.                                          */
        /*                                                                                 */
        /* Arguments: minUserRecordNumber - minimum record number requested by the user.   */
        /*            maxUserRecordNumber - maximum record number requested by the user.   */
        /*            eventMBB - MBB event formatting object.                              */
        /*            shortOnly - if true, display only the short form of the record.      */
        /*                                                                                 */
        /* Returns:   canReturnCode.NO_ERROR - if there is no parsing error.           */
        /*            canReturnCode.ARG_ERROR - if there is a parsing error or the record  */
        /*            number is not within the range supported by the module.              */
        /*                                                                                 */
        /* Caveats:   None.                                                                */
        /*                                                                                 */
        /***********************************************************************************/
        public canReturnCode displayEventRecordsAllMbb(int minUserRecordNumber, int maxUserRecordNumber, bool shortOnly)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            int oldestRecordPresent = Constant.ZERO;
            int newestRecordPresent = Constant.ZERO;

            UInt16 minDefinedRecord = Constant.ZERO;
            UInt16 maxDefinedRecord = Constant.ZERO;

            int moduleNumber;

            // Obtain the range of records defined by the firmware.
            getEventRecordRange(ref minDefinedRecord, ref maxDefinedRecord);

            // Loop over all identified modules.
            for (moduleNumber = moduleConfig.userMinModule; moduleNumber <= moduleConfig.userMaxModule; moduleNumber++)
            {
                if ((returnCode = extractRecordRange(moduleNumber, eventLogMBB.eventRecordType.CHRONICLE, minDefinedRecord, maxDefinedRecord, ref oldestRecordPresent, ref newestRecordPresent)) == canReturnCode.NO_ERROR)
                {
                    // Check if FIFO has wrapped around.
                    if (oldestRecordPresent <= newestRecordPresent)
                    {
                        // The FIFO has not wrapped around.
                        // Check if the user did not provide a range.
                        if (minUserRecordNumber == Constant.ZERO)
                        {
                            // No range entered by the user, display all records present.
                            returnCode = displayEventRecords(moduleNumber, oldestRecordPresent, newestRecordPresent, shortOnly);
                        }
                        else
                        {
                            // Check if the minUserRecordNumber and maxUserRecordNumber are within the defined range.
                            if (checkRecordLimits(moduleNumber, oldestRecordPresent, newestRecordPresent, minUserRecordNumber, maxUserRecordNumber))
                            {
                                // Display the range requested by the user.
                                returnCode = displayEventRecords(moduleNumber, minUserRecordNumber, maxUserRecordNumber, shortOnly);
                            }
                            else
                            {
                                returnCode = canReturnCode.ARG_ERROR;
                            }
                        }
                    }
                    else
                    {
                        // The FIFO has wrapped around.
                        // Check if an argument was entered.
                        if (minUserRecordNumber == Constant.ZERO)
                        {
                            // Display all records, starting with first logged record and ending with the last.
                            returnCode = displayEventRecords(moduleNumber, oldestRecordPresent, newestRecordPresent, shortOnly);
                        }
                        else
                        {
                            // Check if the minUserRecordNumber and maxUserRecordNumber are within the firmware defined range.
                            if (checkRecordLimits(moduleNumber, minDefinedRecord, maxDefinedRecord, minUserRecordNumber, maxUserRecordNumber))
                            {
                                // Display the range requested by the user.
                                returnCode = displayEventRecords(moduleNumber, minUserRecordNumber, maxUserRecordNumber, shortOnly);
                            }
                            else
                            {
                                returnCode = canReturnCode.ARG_ERROR;
                            }
                        }
                    }
                }
            }
            return returnCode;
        }

        public canReturnCode displayCriticalRecords(int moduleNumber, int minRecordNumber, int maxRecordNumber, bool shortOnly)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            canReturnCode savedReturnCode = canReturnCode.NO_ERROR;

            UInt16 recordNumber;

            UInt16 minDefinedRecord = Constant.ZERO;
            UInt16 maxDefinedRecord = Constant.ZERO;

            getCriticalRecordRange(ref minDefinedRecord, ref maxDefinedRecord);

            // Check if the FIFO has not wrapped around.
            if (minRecordNumber <= maxRecordNumber)
            {
                // The FIFO has not wrapped around.  Display from lowest to highest.
                for (recordNumber = (UInt16)minRecordNumber; recordNumber <= maxRecordNumber; recordNumber++)
                {
                    // If there is a failure to access one record, save the return code.
                    if ((returnCode = displayCriticalRecord(moduleNumber, recordNumber, shortOnly)) != canReturnCode.NO_ERROR)
                    {
                        // Save the failing return code.
                        savedReturnCode = returnCode;
                    }
                    else
                    {
                        savedReturnCode = returnCode;
                    }
                }
            }
            else
            {
                // The FIFO has wrapped around.  Display upper part of FIFO (older records).
                for (recordNumber = (UInt16)minRecordNumber; recordNumber <= maxDefinedRecord; recordNumber++)
                {
                    // If there is a failure to access one record, save the return code.
                    if ((returnCode = displayCriticalRecord(moduleNumber, recordNumber, shortOnly)) != canReturnCode.NO_ERROR)
                    {
                        // Save the failing return code.
                        savedReturnCode = returnCode;
                    }
                    else
                    {
                        savedReturnCode = returnCode;
                    }
                }

                // The FIFO has wrapped around.  Display lower part of FIFO (newer records).
                for (recordNumber = minDefinedRecord; recordNumber <= maxRecordNumber; recordNumber++)
                {
                    // If there is a failure to access one record, save the return code.
                    if ((returnCode = displayCriticalRecord(moduleNumber, recordNumber, shortOnly)) != canReturnCode.NO_ERROR)
                    {
                        // Save the failing return code.
                        savedReturnCode = returnCode;
                    }
                    else
                    {
                        savedReturnCode = returnCode;
                    }
                }
            }
            // If there was a failure in the saved return code, return the saved return code.
            if ((returnCode == canReturnCode.NO_ERROR) && (savedReturnCode != canReturnCode.NO_ERROR))
            {
                returnCode = savedReturnCode;
            }
            return returnCode;
        }

        public canReturnCode displayEventRecords(int moduleNumber, int minRecordNumber, int maxRecordNumber, bool shortOnly)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            canReturnCode savedReturnCode = canReturnCode.NO_ERROR;

            UInt16 recordNumber;

            UInt16 minDefinedRecord = Constant.ZERO;
            UInt16 maxDefinedRecord = Constant.ZERO;

            getEventRecordRange(ref minDefinedRecord, ref maxDefinedRecord);

            // Check if the FIFO has not wrapped around.
            if (minRecordNumber <= maxRecordNumber)
            {
                // The FIFO has not wrapped around.  Display from lowest to highest.
                for (recordNumber = (UInt16)minRecordNumber; recordNumber <= maxRecordNumber; recordNumber++)
                {
                    // If there is a failure to access one record, save the return code.
                    if ((returnCode = displayEventRecord(moduleNumber, recordNumber, shortOnly)) != canReturnCode.NO_ERROR)
                    {
                        // Save the failing return code.
                        savedReturnCode = returnCode;
                    }
                }
            }
            else
            {
                // The FIFO has wrapped around.  Display upper part of FIFO (older records).
                for (recordNumber = (UInt16)minRecordNumber; recordNumber <= maxDefinedRecord; recordNumber++)
                {
                    // If there is a failure to access one record, save the return code.
                    if ((returnCode = displayEventRecord(moduleNumber, recordNumber, shortOnly)) != canReturnCode.NO_ERROR)
                    {
                        // Save the failing return code.
                        savedReturnCode = returnCode;
                    }
                    else
                    {
                        savedReturnCode = returnCode;
                    }
                }

                // The FIFO has wrapped around.  Display lower part of FIFO (newer records).
                for (recordNumber = minDefinedRecord; recordNumber <= maxRecordNumber; recordNumber++)
                {
                    // If there is a failure to access one record, save the return code.
                    if ((returnCode = displayEventRecord(moduleNumber, recordNumber, shortOnly)) != canReturnCode.NO_ERROR)
                    {
                        // Save the failing return code.
                        savedReturnCode = returnCode;
                    }
                    else
                    {
                        savedReturnCode = returnCode;
                    }
                }
            }

            // If there was a failure in the saved return code, return the saved return code.
            if ((returnCode == canReturnCode.NO_ERROR) && (savedReturnCode != canReturnCode.NO_ERROR))
            {
                returnCode = savedReturnCode;
            }
            return returnCode;
        }

        public canReturnCode displayLastEventRecord(int moduleNumber)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            UInt16 lastRecord = Constant.ZERO;

            if ((returnCode = extractLastEventRecordNumber(moduleNumber, ref lastRecord)) == canReturnCode.NO_ERROR)
            {
                if (lastRecord == Constant.ZERO)
                {
                    UserIO.outputInfo("No Event records are present");
                    returnCode = canReturnCode.DATA_ERROR;
                }
                else
                {
                    returnCode = displayEventRecord(moduleNumber, lastRecord, false);
                }
            }
            return returnCode;
        }

        public canReturnCode displayPrevEventRecord(int moduleNumber)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            UInt16 previousRecord = Constant.ZERO;

            UInt16 minimumRecord = Constant.ZERO;
            UInt16 maximumRecord = Constant.ZERO;

            getEventRecordRange(ref minimumRecord, ref maximumRecord);

            if ((returnCode = extractPreviousRecordNumber(moduleNumber, maximumRecord, eventLogMBB.eventRecordType.CHRONICLE, ref previousRecord)) == canReturnCode.NO_ERROR)
            {
                returnCode = displayEventRecord(moduleNumber, previousRecord, false);
            }

            return returnCode;
        }

        public canReturnCode displayLastCriticalRecord(int moduleNumber)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            UInt16 lastRecord = Constant.ZERO;

            if ((returnCode = extractLastCriticalRecordNumber (moduleNumber, ref lastRecord)) == canReturnCode.NO_ERROR)
            {
                if (lastRecord == Constant.ZERO)
                {
                    UserIO.outputInfo("No Critical records are present");
                    returnCode = canReturnCode.DATA_ERROR;
                }
                else
                {
                    returnCode = displayCriticalRecord(moduleNumber, lastRecord, false);
                }
            }
            return returnCode;
        }

        public canReturnCode displayPrevCriticalRecord(int moduleNumber)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            UInt16 previousRecord = Constant.ZERO;
            UInt16 minimumRecord = Constant.ZERO;
            UInt16 maximumRecord = Constant.ZERO;

            getEventRecordRange(ref minimumRecord, ref maximumRecord);

            if ((returnCode = extractPreviousRecordNumber(moduleNumber, maximumRecord, eventLogMBB.eventRecordType.CRITICAL, ref previousRecord)) == canReturnCode.NO_ERROR)
            {
                returnCode = displayCriticalRecord(moduleNumber, previousRecord, false);
            }
            return returnCode;
        }


        public canReturnCode displayEventRecord(int moduleNumber, UInt16 recordNumber, bool shortOnly)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            byte[] recordBuffer = new byte[_eventLogMBB.getEventRecordSize()];

            string recordDesc = String.Format("MBB {0} Event Record {1}", moduleNumber, recordNumber);

            if ((returnCode = getEventRecord(moduleNumber, recordDesc, recordNumber, recordBuffer)) == canReturnCode.NO_ERROR)
            {
                _eventLogMBB.formatRecordMBB(recordDesc, recordBuffer, shortOnly);
            }

            return returnCode;
        }
        public canReturnCode displayCriticalRecord(int moduleNumber, UInt16 recordNumber, bool shortOnly)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            byte[] recordBuffer = new byte[_eventLogMBB.getCriticalRecordSize()];

            string recordDesc = String.Format("MBB {0} Critical Record {1}", moduleNumber, recordNumber);

            if ((returnCode = getCriticalRecord(moduleNumber, recordDesc, recordNumber, recordBuffer)) == canReturnCode.NO_ERROR)
            {
                _eventLogMBB.formatRecordMBB(recordDesc, recordBuffer, shortOnly);
            }

            return returnCode;
        }
        private canReturnCode getCriticalRecord(int moduleNumber, string recordDesc, UInt16 recordNumber, byte[] recordBuffer)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            if ((returnCode = myReadInterface.writeCriticalIndex(moduleNumber, recordNumber)) == canReturnCode.NO_ERROR)
            {
                returnCode = myReadInterface.transferDataMBB(moduleNumber, recordDesc, recordBuffer, sunriseModuleMBB.DID_GET_CRITICAL_RECORD);
            }

            if (returnCode != canReturnCode.NO_ERROR)
            {
                UserIO.outputError(String.Format("Failed to read {0}.", recordDesc));
            }

            return returnCode;
        }

        private canReturnCode getEventRecord(int moduleNumber, string recordDesc, UInt16 recordNumber, byte[] recordBuffer)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            if ((returnCode = myReadInterface.writeEventIndex(moduleNumber, recordNumber)) == canReturnCode.NO_ERROR)
            {
                returnCode = myReadInterface.transferDataMBB(moduleNumber, recordDesc, recordBuffer, sunriseModuleMBB.DID_GET_CHRONICLE_RECORD);
            }

            if (returnCode != canReturnCode.NO_ERROR)
            {
                UserIO.outputError(String.Format("Failed to read {0}.", recordDesc));
            }

            return returnCode;
        }
        

        public canReturnCode displayMbbCriticalHdr(int moduleNumber)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            byte[] hdrBuffer = new byte[eventLogMBB.HDR_RESPONSE_LENGTH];

            string hdrMsg = "Critical Header";

            returnCode = getCriticalHdr(moduleNumber, hdrBuffer);

            if (returnCode == canReturnCode.NO_ERROR)
            {
                _eventLogMBB.displayHdr(moduleNumber, hdrMsg, hdrBuffer);
            }
            return returnCode;
        }

     
        public canReturnCode displayMbbEventHdr(int moduleNumber)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            byte[] hdrBuffer = new byte[eventLogMBB.HDR_RESPONSE_LENGTH];

            string hdrMsg = "Event Header";

            returnCode = getEventHdr(moduleNumber, hdrBuffer);
            if (returnCode == canReturnCode.NO_ERROR)
            {
                _eventLogMBB.displayHdr(moduleNumber, hdrMsg, hdrBuffer);
            }


            return returnCode;
        }

        public canReturnCode displayMBBEventHdrDiff(int moduleNumber)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            byte[] response = new byte[eventLogMBB.HDR_RESPONSE_LENGTH];

            UInt32 numWritesLast;
            UInt32 recordNumberLast;

            UInt32 numWritesCurrent;
            UInt32 recordNumberCurrent;

            string hdrMsg = String.Format("MBB {0} Event", moduleNumber);

            numWritesLast = numberWritesSavedEvent[moduleNumber - Constant.ONE];
            recordNumberLast = recordNumberSavedEvent[moduleNumber - Constant.ONE];

            if ((returnCode = getEventHdr(moduleNumber, response)) == canReturnCode.NO_ERROR)
            {
                numberWritesSavedEvent[moduleNumber - Constant.ONE] = _eventLogMBB.extractNumberWrites(response);
                recordNumberSavedEvent[moduleNumber - Constant.ONE] = _eventLogMBB.extractRecordNumber(response);

                numWritesCurrent = numberWritesSavedEvent[moduleNumber - Constant.ONE];
                if (numWritesLast == numWritesCurrent)
                {
                    UserIO.outputInfo(String.Format("{0} Unchanged Number Writes: {1}.", hdrMsg, numWritesCurrent));
                }
                else
                {
                    UserIO.outputInfo(String.Format("{0} Number Writes: Previous {1}. Current {2}", hdrMsg, numWritesLast, numWritesCurrent));
                }

                recordNumberCurrent = recordNumberSavedEvent[moduleNumber - Constant.ONE];
                if (recordNumberLast == recordNumberCurrent)
                {
                    UserIO.outputInfo(String.Format("{0} Unchanged Record Number: {1}.", hdrMsg, recordNumberCurrent));
                }
                else
                {
                    UserIO.outputInfo(String.Format("{0} Record Number: Previous {1}. Current {2}", hdrMsg, recordNumberLast, recordNumberCurrent));
                }

            }
            return returnCode;
        }

        public canReturnCode displayMBBCriticalHdrDiff(int moduleNumber)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            byte[] response = new byte[eventLogMBB.HDR_RESPONSE_LENGTH];

            UInt32 numWritesLast;
            UInt32 recordNumberLast;

            string hdrMsg = String.Format("MBB {0} Critical", moduleNumber);

            numWritesLast = numberWritesSavedCritical[moduleNumber - Constant.ONE];
            recordNumberLast = recordNumberSavedCritical[moduleNumber - Constant.ONE];

            if ((returnCode = getCriticalHdr(moduleNumber, response)) == canReturnCode.NO_ERROR)
            {
                numberWritesSavedCritical[moduleNumber - Constant.ONE] = _eventLogMBB.extractNumberWrites(response);
                recordNumberSavedCritical[moduleNumber - Constant.ONE] = _eventLogMBB.extractRecordNumber(response);

                if (numWritesLast == numberWritesSavedCritical[moduleNumber - Constant.ONE])
                {
                    UserIO.outputInfo(String.Format("{0} Unchanged Number Writes: {1}.", hdrMsg, numWritesLast));
                }
                else
                {
                    UserIO.outputInfo(String.Format("{0} Number Writes: Previous {1}. Current {2}", hdrMsg, numWritesLast, numberWritesSavedCritical[moduleNumber - Constant.ONE]));
                }

                if (recordNumberLast == recordNumberSavedCritical[moduleNumber - Constant.ONE])
                {
                    UserIO.outputInfo(String.Format("{0} Unchanged Record Number: {1}.", hdrMsg, recordNumberLast));
                }
                else
                {
                    UserIO.outputInfo(String.Format("{0} Record Number: Previous {1}. Current {2}", hdrMsg, recordNumberLast, recordNumberSavedCritical[moduleNumber - Constant.ONE]));
                }
            }

            return returnCode;
        }

        /***********************************************************************************/
        /*                                                                                 */
        /* Function: Read the MBB Critical Header over MODCAN.                             */
        /*                                                                                 */
        /* Arguments: moduleNumber - the number of the module being accessed.              */
        /*            hdrBuffer - the header is returned in this buffer.                   */
        /*                                                                                 */
        /* Returns:   canReturnCode.NO_ERROR - if no error in reading the data.        */
        /*            canReturnCode.CAN_ERROR - if CAN error occurs.                       */
        /*                                                                                 */
        /* Caveats:  None.                                                                 */
        /*                                                                                 */
        /***********************************************************************************/
        public canReturnCode getCriticalHdr(int moduleNumber, byte[] hdrBuffer)
        {

            string hdrMsg = "Critical Header";

            return myReadInterface.transferDataMBB(moduleNumber, hdrMsg, hdrBuffer, sunriseModuleMBB.DID_GET_CRITICAL_HDR);

        }

        /***********************************************************************************/
        /*                                                                                 */
        /* Function: Read the MBB Event Header over MODCAN.                                */
        /*                                                                                 */
        /* Arguments: mbbNumber - the number of the MBB being accessed.                    */
        /*            hdrBuffer - the header is returned in this buffer.                   */
        /*                                                                                 */
        /* Returns:   canReturnCode.NO_ERROR - if no error in reading the data.        */
        /*            canReturnCode.CAN_ERROR - if CAN error occurs.                       */
        /*                                                                                 */
        /* Caveats:  None.                                                                 */
        /*                                                                                 */
        /***********************************************************************************/
        public canReturnCode getEventHdr(int moduleNumber, byte[] hdrBuffer)
        {
            string hdrMsg = "Event Header";

            return myReadInterface.transferDataMBB(moduleNumber, hdrMsg, hdrBuffer, sunriseModuleMBB.DID_GET_NONCRITICAL_HDR);
        }

        
        public canReturnCode extractPreviousRecordNumber(int moduleNumber, UInt16 maximumRecord, eventLogMBB.eventRecordType recordType, ref UInt16 previousRecord)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            string recordMsg;

            UInt32 numberWrites = Constant.ZERO;
            UInt16 recordNumber = Constant.ZERO;
            byte[] response = new byte[eventLogMBB.HDR_RESPONSE_LENGTH];

            if (recordType == eventLogMBB.eventRecordType.CRITICAL)
            {
                returnCode = getCriticalHdr (moduleNumber, response);
                recordMsg = String.Format("MBB {0} Critical", moduleNumber);
            }
            else
            {
                returnCode = getEventHdr (moduleNumber, response);
                recordMsg = String.Format("MBB {0} Event", moduleNumber);
            }

            if (returnCode == canReturnCode.NO_ERROR)
            {
                _eventLogMBB.extractHdrData (response, ref recordNumber, ref numberWrites);
                if (numberWrites == Constant.ZERO)
                {
                    UserIO.outputInfo(String.Format("No {0} records are present", recordMsg));
                    returnCode = canReturnCode.DATA_ERROR;
                }
                else
                {
                    if (numberWrites == Constant.ONE)
                    {
                        UserIO.outputInfo(String.Format("Only one {0} record is present", recordMsg));
                        returnCode = canReturnCode.DATA_ERROR;
                    }
                    else
                    {
                        if (numberWrites > maximumRecord)
                        {
                            if (recordNumber == Constant.ONE)
                            {
                                previousRecord = maximumRecord;
                            }
                            else
                            {
                                previousRecord = (UInt16)(recordNumber - Constant.ONE);
                            }
                        }
                        else
                        {
                            previousRecord = (UInt16)(recordNumber - Constant.ONE);
                        }
                    }
                }
            }
            return returnCode;
        }

        public canReturnCode extractLastEventRecordNumber(int moduleNumber, ref UInt16 lastRecord)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            byte[] response = new byte[eventLogMBB.HDR_RESPONSE_LENGTH];

            UInt32 numberWrites = Constant.ZERO;
            UInt16 recordNumber = Constant.ZERO;

            returnCode = getEventHdr(moduleNumber, response);
            
            if (returnCode == canReturnCode.NO_ERROR)
            {
                _eventLogMBB.extractHdrData(response, ref recordNumber, ref numberWrites);

                if (numberWrites == Constant.ZERO)
                {
                    lastRecord = Constant.ZERO;
                }
                else
                {
                    lastRecord = recordNumber;
                }
            }

            return returnCode;
        }
        public canReturnCode extractLastCriticalRecordNumber(int moduleNumber, ref UInt16 lastRecord)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            UInt32 numberWrites = Constant.ZERO;
            UInt16 recordNumber = Constant.ZERO;

            byte[] response = new byte[eventLogMBB.HDR_RESPONSE_LENGTH];

            returnCode = getCriticalHdr(moduleNumber, response);
            
            if (returnCode == canReturnCode.NO_ERROR)
            {
                _eventLogMBB.extractHdrData(response, ref recordNumber, ref numberWrites);
                if (numberWrites == Constant.ZERO)
                {
                    lastRecord = Constant.ZERO;
                }
                else
                {
                    lastRecord = recordNumber;
                }
            }

            return returnCode;
        }

 
        /***********************************************************************************/
        /*                                                                                 */
        /* Function:  Verify that the user entered range of records falls within the       */
        /*            range of actual records logged.                                      */
        /*                                                                                 */
        /* Arguments: minAvailRecord - the minimum available record.                       */
        /*            maxAvailRecord - the maximum available record.                       */
        /*            minUserRecordNumber - the minimum record requested by the user.      */
        /*            maxUserRecordNumber - the maximum record requested by the user.      */
        /*                                                                                 */
        /* Returns:   true -  if there is no range error.                                  */
        /*            false - if the inner range does not fall with the bottom-top range.  */
        /*                                                                                 */
        /* Caveats:   None.                                                                */
        /*                                                                                 */
        /***********************************************************************************/
        private bool checkRecordLimits(int moduleNumber, int minAvailRecord, int maxAvailRecord, int minUserRecordNumber, int maxUserRecordNumber)
        {
            bool parseSuccess = true;

            string mbbRangeMsg = String.Format("available MBB {0} record range {1} - {2}", moduleNumber, minAvailRecord, maxAvailRecord);

            if ((minUserRecordNumber < minAvailRecord) || (minUserRecordNumber > maxAvailRecord))
            {
                UserIO.outputError(String.Format("User minimum record {0} not within {1}", minUserRecordNumber, mbbRangeMsg));
                parseSuccess = false;
            }

            if ((maxUserRecordNumber < minAvailRecord) || (maxUserRecordNumber > maxAvailRecord))
            {
                UserIO.outputError(String.Format("User maximum record {0} not within {1}", maxUserRecordNumber, mbbRangeMsg));
                parseSuccess = false;
            }

            return parseSuccess;
        }


        /***********************************************************************************/
        /*                                                                                 */
        /* Name:     checkDiffOnly.                                                        */
        /*                                                                                 */
        /* Function: If a token is present, searches for an "-diff"                        */
        /*           command line arguement. If no such token exists,                      */
        /*           diffOnly is set to false and we unget the read token.                 */
        /*                                                                                 */
        /* Arguments: userCmd - the command for which are parsing arguments.               */
        /*            diffOnly - if -diff was entered is returned.                         */
        /*                                                                                 */
        /* Returns:  true - if there were no errors in parsing.                            */
        /*           false - if there were errors in parsing.                              */
        /*                                                                                 */
        /* Caveats:  None.                                                                 */
        /*                                                                                 */
        /***********************************************************************************/

        public bool checkDiffOnly(string userCmd, ref bool diffOnly)
        {
            bool parseSuccess = true;
            string optString = null;
            const string diffOption = "diff";

            diffOnly = false;

            // Check if a value entered.
            if (Token.tokenPresent())
            {
                // Check if the user selected an option.
                if (Token.getOptString(ref optString))
                {
                    switch (optString)
                    {
                        case diffOption:
                            diffOnly = true;
                            break;

                        default:
                            UserIO.outputError(String.Format("Legal {0} command options are {1}.", userCmd, diffOption));
                            parseSuccess = false;
                            break;
                    }
                }
                else
                {
                    Token.ungetToken();
                }
            }
            return parseSuccess;
        }

        public canReturnCode runAgainstMbbEvents(Func<int, canReturnCode> moduleFunctionName)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            canReturnCode savedReturnCode = canReturnCode.NO_ERROR;

            int moduleNumber;

            // Loop over the entered range of modules.
            for (moduleNumber = moduleConfig.userMinModule; moduleNumber <= moduleConfig.userMaxModule; moduleNumber++)
            {
                // If there is a failure to access one module,  save the failing return code.
                if ((returnCode = moduleFunctionName(moduleNumber)) != canReturnCode.NO_ERROR)
                {
                    savedReturnCode = returnCode;
                }
            }

            // If there was a failure in the saved return code, return the saved return code.
            if ((returnCode == canReturnCode.NO_ERROR) && (savedReturnCode != canReturnCode.NO_ERROR))
            {
                returnCode = savedReturnCode;
            }

            return returnCode;
        }

    } // End of class events

    public class sunriseEvents : events
    {
  
        public sunriseEvents (profileReadInterface readInterface, eventLogMBB myEventLogMBB) : base (readInterface, myEventLogMBB)
        {
            myReadInterface = readInterface;
            _eventLogMBB = myEventLogMBB;
        }

  
    } // End of class sunriseEvents

}


