﻿//*************************************************************************
//
// Copyright © 2014 NEC Energy Solutions, Inc. All rights reserved.
//
// NEC Energy Solutions Proprietary Information.
// This document contains proprietary and confidential information and is
// tendered for information and evaluation purposes only; no copy or other
// use or dissemination by a third party can be made of this information
// without the express written permission of NEC Energy Solutions.
//
//*************************************************************************

using System;
using SQA_modcan;
using SQA_can;
using SQA_formatEvent;
using SQA_format;
using SQA_utils;

namespace SQA_event
{
    public class eventLogMBB
    {
        public const int HDR_VERSION_OFFSET = 0;
        public const int HDR_RECORD_NUMBER_OFFSET = 2;
        public const int HDR_NUMBER_WRITES_OFFSET = 4;
        public const int HDR_CRC_OFFSET = 8;

        public const int HDR_RESPONSE_LENGTH = Constant.TEN;

        public enum eventRecordType : int
        {
            CHRONICLE = 0,
            CRITICAL,
        };

        public profileReadInterface myReadInterface;
        public eventLogMBB (profileReadInterface readInterface)
        {
            myReadInterface = readInterface;
        }
       
   
        public void formatRecordMBB(string recordDesc, byte[] recordBuffer, bool shortOnly)
        {
            eventFormatMBB eventMBB = new eventFormatMBB();

             // Determine the output format.
             switch (AppUtils.displayFormat)
             {
                 case AppUtils.DISPLAY_FORMAT_ENGLISH:
                     {
                         eventMBB.formatEventRecord (recordDesc, recordBuffer, shortOnly);
                     }
                     break;
                 case AppUtils.DISPLAY_FORMAT_HEX:
                     {
                         Utils.outputBuffer(recordDesc, recordBuffer, recordBuffer.Length);
                     }
                     break;
                 case AppUtils.DISPLAY_FORMAT_NONE:
                 default:
                     break;
             }
             UserIO.outputInfo(String.Format("End of {0}.", recordDesc));
             UserIO.outputNewLine(outDev.ALL, msgType.INFO);
        }

        public void displayHdr(int moduleNumber, string hdrTypeMsg, byte[] hdr)
        {
            string hdrMsg = String.Format("MBB {0} {1}", moduleNumber, hdrTypeMsg);

            formatUtils.outputEnglishHexFormatFunction(hdrMsg, hdr, formatHdr);
        }

        public canReturnCode extractLastEventRecordNumber(int moduleNumber, ref UInt16 lastRecord)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;
            byte[] response = new byte[eventLogMBB.HDR_RESPONSE_LENGTH];

            UInt32 numberWrites = Constant.ZERO;
            UInt16 recordNumber = Constant.ZERO;

            returnCode = getEventHdr(moduleNumber, response);

            if (returnCode == canReturnCode.NO_ERROR)
            {
                extractHdrData(response, ref recordNumber, ref numberWrites);

                if (numberWrites == Constant.ZERO)
                {
                    lastRecord = Constant.ZERO;
                }
                else
                {
                    lastRecord = recordNumber;
                }
            }

            return returnCode;
        }
        public canReturnCode extractLastCriticalRecordNumber(int moduleNumber, ref UInt16 lastRecord)
        {
            canReturnCode returnCode = canReturnCode.NO_ERROR;

            UInt32 numberWrites = Constant.ZERO;
            UInt16 recordNumber = Constant.ZERO;

            byte[] response = new byte[eventLogMBB.HDR_RESPONSE_LENGTH];

            returnCode = getCriticalHdr(moduleNumber, response);

            if (returnCode == canReturnCode.NO_ERROR)
            {
                extractHdrData(response, ref recordNumber, ref numberWrites);
                if (numberWrites == Constant.ZERO)
                {
                    lastRecord = Constant.ZERO;
                }
                else
                {
                    lastRecord = recordNumber;
                }
            }

            return returnCode;
        }

        /***********************************************************************************/
        /*                                                                                 */
        /* Function: Read the MBB Critical Header over MODCAN.                             */
        /*                                                                                 */
        /* Arguments: moduleNumber - the number of the module being accessed.              */
        /*            hdrBuffer - the header is returned in this buffer.                   */
        /*                                                                                 */
        /* Returns:   canReturnCode.NO_ERROR - if no error in reading the data.            */
        /*            canReturnCode.CAN_ERROR - if CAN error occurs.                       */
        /*                                                                                 */
        /* Caveats:  None.                                                                 */
        /*                                                                                 */
        /***********************************************************************************/
        public canReturnCode getCriticalHdr(int moduleNumber, byte[] hdrBuffer)
        {
            string hdrMsg = "Critical Header";
            return (canReturnCode)myReadInterface.transferDataMBB(moduleNumber, hdrMsg, hdrBuffer, sunriseModuleMBB.DID_GET_CRITICAL_HDR);

        }

        /***********************************************************************************/
        /*                                                                                 */
        /* Function: Read the MBB Event Header over MODCAN.                                */
        /*                                                                                 */
        /* Arguments: mbbNumber - the number of the MBB being accessed.                    */
        /*            hdrBuffer - the header is returned in this buffer.                   */
        /*                                                                                 */
        /* Returns:   canReturnCode.NO_ERROR - if no error in reading the data.            */
        /*            canReturnCode.CAN_ERROR - if CAN error occurs.                       */
        /*                                                                                 */
        /* Caveats:  None.                                                                 */
        /*                                                                                 */
        /***********************************************************************************/
        public canReturnCode getEventHdr(int moduleNumber, byte[] hdrBuffer)
        {
            string hdrMsg = "Event Header";
            return (canReturnCode)myReadInterface.transferDataMBB(moduleNumber, hdrMsg, hdrBuffer, sunriseModuleMBB.DID_GET_CHRONICLE_RECORD);
        }

        private void formatHdr(string hdrName, byte[] hdrBuffer)
        {

            UInt16 version;

            version = Utils.extractLittleEndian16(hdrBuffer, HDR_VERSION_OFFSET);

            UserIO.outputInfo(String.Format("{0} Header Version: {1}.", hdrName, version));
            UserIO.outputInfo(String.Format("Record Number: {0}.", extractRecordNumber(hdrBuffer)));
            UserIO.outputInfo(String.Format("Number writes: {0}.", extractNumberWrites(hdrBuffer)));
            // UserIO.outputInfo(String.Format("CRC: 0x{0:X4}.", Utils.extractLittleEndian16(hdrBuffer, HDR_CRC_OFFSET)));
            UserIO.outputNewLine(outDev.ALL, msgType.INFO);
        }

        public void extractHdrData(byte[] response, ref UInt16 recordNumber, ref UInt32 numberWrites)
        {
            recordNumber = extractRecordNumber(response);
            numberWrites = extractNumberWrites(response);
        }

        public UInt32 extractNumberWrites(byte[] hdrBuffer)
        {
            return Utils.extractLittleEndian32(hdrBuffer, HDR_NUMBER_WRITES_OFFSET);
        }


        public UInt16 extractRecordNumber(byte[] hdrBuffer)
        {
            return Utils.extractLittleEndian16(hdrBuffer, HDR_RECORD_NUMBER_OFFSET);
        }

        public virtual int getCriticalRecordSize()
        {
            return Constant.ZERO;
        }

        public virtual int getEventRecordSize()
        {
            return Constant.ZERO;
        }

    }  // End of class eventLogMBB

    public class nullEventLogMBB : eventLogMBB
    {
        public nullEventLogMBB (profileReadInterface readInterface) : base (readInterface)
        {
            myReadInterface = readInterface;
        }

    } // End of class nullEventLogMBB

  
    public class sunriseEventLogMBB : eventLogMBB
    {
        public sunriseEventLogMBB (profileReadInterface readInterface) : base (readInterface)
        {
            myReadInterface = readInterface;
        }

        public override int getCriticalRecordSize()
        {
            const int CRITICAL_RECORD_SIZE = 36;
            return CRITICAL_RECORD_SIZE;
        }

        public override int getEventRecordSize()
        {
            const int CHRONICLE_RECORD_SIZE = 36;
            return CHRONICLE_RECORD_SIZE;
        }

    } // End of class sunriseEventLogMBB

    public class optimusEventLogMBB : eventLogMBB
    {
        public optimusEventLogMBB(profileReadInterface readInterface) : base(readInterface)
        {
            myReadInterface = readInterface;
        }

        public override int getCriticalRecordSize()
        {
            const int CRITICAL_RECORD_SIZE = 36;
            return CRITICAL_RECORD_SIZE;
        }

        public override int getEventRecordSize()
        {
            const int CHRONICLE_RECORD_SIZE = 36;
            return CHRONICLE_RECORD_SIZE;
        }

    } // End of class optimusEventLogMBB
}
 
                           
