﻿//*************************************************************************
//
// Copyright © 2014 NEC Energy Solutions, Inc. All rights reserved.
//
// NEC Energy Solutions Proprietary Information.
// This document contains proprietary and confidential information and is
// tendered for information and evaluation purposes only; no copy or other
// use or dissemination by a third party can be made of this information
// without the express written permission of NEC Energy Solutions.
//
//*************************************************************************

using System;
using SQA_formatRegs;
using SQA_format;
using SQA_config;
using SQA_utils;

namespace SQA_formatEvent
{
    public class eventFormatBCM
    {
        formatBcmRegisters myFormatBcmRegs;
        public eventFormatBCM (formatBcmRegisters formatBcmRegs)
        {
            myFormatBcmRegs = formatBcmRegs;
        }

        public void formatEventRecord(string recordDesc, byte[] recordBuffer, Action<string, string> output = null)
        {
            const int EVENT_RECORD_CONTROL_OFFSET = 0;
            const int EVENT_RECORD_STATUS_FLAG_OFFSET = 4;
            const int EVENT_RECORD_RACK_VOLTAGE_OFFSET = 18;
            const int EVENT_RECORD_BUS_VOLTAGE_OFFSET = 20;
            const int EVENT_RECORD_RACK_CURRENT_OFFSET = 22;
            const int EVENT_RECORD_CHARGE_CURRENT_OFFSET = 24;
            const int EVENT_RECORD_DISCHARGE_CURRENT_OFFSET = 26;

            const int EVENT_RECORD_MAX_MODULE_VOLTAGE_OFFSET = 28;
            const int EVENT_RECORD_MAX_MODULE_VOLTAGE_LOC_OFFSET = 29;
            const int EVENT_RECORD_MIN_MODULE_VOLTAGE_OFFSET = 30;
            const int EVENT_RECORD_MIN_MODULE_VOLTAGE_LOC_OFFSET = 31;

            const int EVENT_RECORD_MAX_CELL_VOLTAGE_OFFSET = 32;
            const int EVENT_RECORD_MAX_CELL_VOLTAGE_MODULE_OFFSET = 33;
            const int EVENT_RECORD_MAX_CELL_VOLTAGE_IN_MODULE_LOC_OFFSET = 34;
            const int EVENT_RECORD_AVG_CELL_VOLTAGE_OFFSET = 35;
            const int EVENT_RECORD_MIN_CELL_VOLTAGE_OFFSET = 36;
            const int EVENT_RECORD_MIN_CELL_VOLTAGE_MODULE_OFFSET = 37;
            const int EVENT_RECORD_MIN_CELL_VOLTAGE_IN_MODULE_LOC_OFFSET = 38;

            const int EVENT_RECORD_AVG_SOC_OFFSET = 39;
            const int EVENT_RECORD_MIN_SOC_OFFSET = 40;
            const int EVENT_RECORD_MAX_SOC_OFFSET = 41;
            const int EVENT_RECORD_BALANCE_TARGET_OFFSET = 42;
            const int EVENT_RECORD_BALANCE_COUNT_OFFSET = 43;

            const int EVENT_RECORD_AVG_MODULE_TEMP_OFFSET = 44;

            const int EVENT_RECORD_MAX_TEMP_OFFSET = 45;
            const int EVENT_RECORD_MAX_TEMP_MODULE_OFFSET = 46;
            const int EVENT_RECORD_MAX_TEMP_IN_MODULE_LOC_OFFSET = 47;
            const int EVENT_RECORD_MIN_TEMP_OFFSET = 48;
            const int EVENT_RECORD_MIN_TEMP_MODULE_OFFSET = 49;
            const int EVENT_RECORD_MIN_TEMP_IN_MODULE_LOC_OFFSET = 50;

            const int EVENT_RECORD_MAX_CELL_TEMP_OFFSET = 51;
            const int EVENT_RECORD_MAX_CELL_TEMP_MODULE_OFFSET = 52;
            const int EVENT_RECORD_MAX_CELL_TEMP_IN_MODULE_LOC_OFFSET = 53;
            const int EVENT_RECORD_MIN_CELL_TEMP_OFFSET = 54;
            const int EVENT_RECORD_MIN_CELL_TEMP_MODULE_OFFSET = 55;
            const int EVENT_RECORD_MIN_CELL_TEMP_IN_MODULE_LOC_OFFSET = 56;

            const int EVENT_RECORD_MAX_PCB_TEMP_OFFSET = 57;
            const int EVENT_RECORD_MAX_PCB_TEMP_MODULE_OFFSET = 58;
            const int EVENT_RECORD_MIN_PCB_TEMP_OFFSET = 59;
            const int EVENT_RECORD_MIN_PCB_TEMP_MODULE_OFFSET = 60;

            const int EVENT_RECORD_BCM_VOLTAGE_OFFSET = 61;

            UInt32 controlWord;
            UInt32 statusFlag;

            UInt16 rackVoltage;
            UInt16 mainBusVoltage;
            UInt16 rackCurrent;
            UInt16 chargeCurrent;
            UInt16 dischargeCurrent;

            byte maxModuleVoltage;
            byte minModuleVoltage;
            byte maxModuleVoltageLoc;
            byte minModuleVoltageLoc;

            byte maxCellVoltage;
            byte minCellVoltage;
            byte avgCellVoltage;
            byte maxCellVoltageModuleLoc;
            byte minCellVoltageModuleLoc;
            byte maxCellVoltageModule;
            byte minCellVoltageModule;

            byte maxSoc;
            byte minSoc;
            byte avgSoc;

            byte balanceTarget;
            byte balanceCount;

            byte avgModuleTemp;

            byte maxTemp;
            byte minTemp;
            byte maxTempModule;
            byte minTempModule;
            byte maxTempInModuleLoc;
            byte minTempInModuleLoc;

            byte maxCellTemp;
            byte minCellTemp;
            byte maxCellTempModule;
            byte minCellTempModule;
            byte maxCellTempInModuleLoc;
            byte minCellTempInModuleLoc;

            byte maxPcbTemp;
            byte minPcbTemp;
            byte maxPcbTempModule;
            byte minPcbTempModule;
            byte bcmVoltage;

            float voltage;

            controlWord = Utils.extractUInt32(recordBuffer, EVENT_RECORD_CONTROL_OFFSET);
            statusFlag = Utils.extractUInt32(recordBuffer, EVENT_RECORD_STATUS_FLAG_OFFSET);

            rackVoltage = Utils.extractUInt16(recordBuffer, EVENT_RECORD_RACK_VOLTAGE_OFFSET);
            mainBusVoltage = Utils.extractUInt16(recordBuffer, EVENT_RECORD_BUS_VOLTAGE_OFFSET);
            rackCurrent = Utils.extractUInt16(recordBuffer, EVENT_RECORD_RACK_CURRENT_OFFSET);
            chargeCurrent = Utils.extractUInt16(recordBuffer, EVENT_RECORD_CHARGE_CURRENT_OFFSET);
            dischargeCurrent = Utils.extractUInt16(recordBuffer, EVENT_RECORD_DISCHARGE_CURRENT_OFFSET);

            maxModuleVoltage = recordBuffer[EVENT_RECORD_MAX_MODULE_VOLTAGE_OFFSET];
            maxModuleVoltageLoc = (byte)(recordBuffer[EVENT_RECORD_MAX_MODULE_VOLTAGE_LOC_OFFSET]);
            minModuleVoltage = recordBuffer[EVENT_RECORD_MIN_MODULE_VOLTAGE_OFFSET];
            minModuleVoltageLoc = (byte)(recordBuffer[EVENT_RECORD_MIN_MODULE_VOLTAGE_LOC_OFFSET]);

            maxCellVoltage = recordBuffer[EVENT_RECORD_MAX_CELL_VOLTAGE_OFFSET];
            maxCellVoltageModule = (byte)(recordBuffer[EVENT_RECORD_MAX_CELL_VOLTAGE_MODULE_OFFSET]);
            maxCellVoltageModuleLoc = (byte)(recordBuffer[EVENT_RECORD_MAX_CELL_VOLTAGE_IN_MODULE_LOC_OFFSET]);
            avgCellVoltage = recordBuffer[EVENT_RECORD_AVG_CELL_VOLTAGE_OFFSET];
            minCellVoltage = recordBuffer[EVENT_RECORD_MIN_CELL_VOLTAGE_OFFSET];
            minCellVoltageModule = (byte)(recordBuffer[EVENT_RECORD_MIN_CELL_VOLTAGE_MODULE_OFFSET]);
            minCellVoltageModuleLoc = (byte)(recordBuffer[EVENT_RECORD_MIN_CELL_VOLTAGE_IN_MODULE_LOC_OFFSET]);

            avgSoc = recordBuffer[EVENT_RECORD_AVG_SOC_OFFSET];
            minSoc = recordBuffer[EVENT_RECORD_MIN_SOC_OFFSET];
            maxSoc = recordBuffer[EVENT_RECORD_MAX_SOC_OFFSET];

            balanceTarget = recordBuffer[EVENT_RECORD_BALANCE_TARGET_OFFSET];
            balanceCount = recordBuffer[EVENT_RECORD_BALANCE_COUNT_OFFSET];

            avgModuleTemp = recordBuffer[EVENT_RECORD_AVG_MODULE_TEMP_OFFSET];

            maxTemp = recordBuffer[EVENT_RECORD_MAX_TEMP_OFFSET];
            minTemp = recordBuffer[EVENT_RECORD_MIN_TEMP_OFFSET];
            maxTempModule = (byte)(recordBuffer[EVENT_RECORD_MAX_TEMP_MODULE_OFFSET]);
            minTempModule = (byte)(recordBuffer[EVENT_RECORD_MIN_TEMP_MODULE_OFFSET]);
            maxTempInModuleLoc = (byte)(recordBuffer[EVENT_RECORD_MAX_TEMP_IN_MODULE_LOC_OFFSET]);
            minTempInModuleLoc = (byte)(recordBuffer[EVENT_RECORD_MIN_TEMP_IN_MODULE_LOC_OFFSET]);

            maxCellTemp = recordBuffer[EVENT_RECORD_MAX_CELL_TEMP_OFFSET];
            minCellTemp = recordBuffer[EVENT_RECORD_MIN_CELL_TEMP_OFFSET];
            maxCellTempModule = (byte)(recordBuffer[EVENT_RECORD_MAX_CELL_TEMP_MODULE_OFFSET]);
            minCellTempModule = (byte)(recordBuffer[EVENT_RECORD_MIN_CELL_TEMP_MODULE_OFFSET]);
            maxCellTempInModuleLoc = (byte)(recordBuffer[EVENT_RECORD_MAX_CELL_TEMP_IN_MODULE_LOC_OFFSET]);
            minCellTempInModuleLoc = (byte)(recordBuffer[EVENT_RECORD_MIN_CELL_TEMP_IN_MODULE_LOC_OFFSET]);

            maxPcbTemp = recordBuffer[EVENT_RECORD_MAX_PCB_TEMP_OFFSET];
            minPcbTemp = recordBuffer[EVENT_RECORD_MIN_PCB_TEMP_OFFSET];
            maxPcbTempModule = (byte)(recordBuffer[EVENT_RECORD_MAX_PCB_TEMP_MODULE_OFFSET]);
            minPcbTempModule = (byte)(recordBuffer[EVENT_RECORD_MIN_PCB_TEMP_MODULE_OFFSET]);

            bcmVoltage = recordBuffer[EVENT_RECORD_BCM_VOLTAGE_OFFSET];

            // display the time and event type
            formatEvent(recordDesc, recordBuffer, output);

            myFormatBcmRegs.formatControl(controlWord, controlReg.CONTROL_ALL_BITS, dispCntl.DISPLAY_ON);
            if (output != null)
            {
                output("Control Word:", string.Format("0x{0:X8}", controlWord));
            }

            myFormatBcmRegs.formatStatus(statusFlag, statusRegister.STATUS_ALL_BITS, dispCntl.DISPLAY_ON);

            if (output != null)
            {
                output ("Status Flag:", string.Format("0x{0:X8}", statusFlag));
            }

            displayBusVoltage (rackVoltage, "Rack Voltage", output);
            displayBusVoltage (mainBusVoltage, "Main Bus Voltage", output);
            
            formatUtils.formatCurrentMsg(rackCurrent, formatUtils.RACK_CURRENT_OFFSET, "Rack Current");
            formatUtils.formatCurrentMsg(chargeCurrent, formatUtils.RACK_CURRENT_OFFSET, "Charge Current");
            formatUtils.formatCurrentMsg(dischargeCurrent, formatUtils.RACK_CURRENT_OFFSET, "Discharge Current");

            UserIO.outputNewLine(outDev.ALL, msgType.INFO);

            displayModuleVoltage(maxModuleVoltage, maxModuleVoltageLoc, "Maximum Module Voltage", output);
            displayModuleVoltage(minModuleVoltage, minModuleVoltageLoc, "Minimum Module Voltage", output);

            UserIO.outputNewLine(outDev.ALL, msgType.INFO);

            displayCellVoltage(maxCellVoltage, "Maximum Cell Voltage", output);
            displayLocationInModule(maxCellVoltageModule, maxCellVoltageModuleLoc, "Maximum Cell Voltage", output);

            displayCellVoltage(avgCellVoltage, "Average Cell Voltage", output);

            displayCellVoltage(minCellVoltage, "Minimum Cell Voltage", output);
            displayLocationInModule(minCellVoltageModule, minCellVoltageModuleLoc, "Minimum Cell Voltage", output);

            UserIO.outputNewLine(outDev.ALL, msgType.INFO);

            displaySocValue(avgSoc, "Average SOC", output);

            displaySocValue(minSoc, "Minimum SOC", output);

            displaySocValue(maxSoc, "Maximum SOC", output);

            displayCellVoltage(balanceTarget, "Balance Target", output);

            displayByteValue(balanceCount, "Balance Count", output);

            UserIO.outputNewLine(outDev.ALL, msgType.INFO);

            displayTemperatureValue(avgModuleTemp, "Average Module Temp", output);

            displayTemperatureValue(maxTemp, "Maximum Terminal Temperature", output);

            displayLocationInModule(maxTempModule, maxTempInModuleLoc, "Maximum Terminal Temperature", output);

            displayTemperatureValue(minTemp, "Minimum Terminal Temperature", output);

            displayLocationInModule(minTempModule, minTempInModuleLoc, "Minimum Terminal Temperature", output);

            UserIO.outputNewLine(outDev.ALL, msgType.INFO);

            displayTemperatureValue(maxCellTemp, "Maximum Cell Temperature", output);

            displayLocationInModule(maxCellTempModule, maxCellTempInModuleLoc, "Maximum Cell Temperature", output);

            displayTemperatureValue(minCellTemp, "Minimum Cell Temperature", output);

            displayLocationInModule(minCellTempModule, minCellTempInModuleLoc, "Minimum Cell Temperature", output);

            UserIO.outputNewLine(outDev.ALL, msgType.INFO);

            displayTemperatureValue(maxPcbTemp, "Maximum PCB Temperature", output);

            displayModule(maxPcbTempModule, "Maximum PCB Temperature", output);

            displayTemperatureValue(minPcbTemp, "Minimum PCB Temperature", output);

            displayModule(minPcbTempModule, "Minimum PCB Temperature", output);

            UserIO.outputNewLine(outDev.ALL, msgType.INFO);

            if (formatUtils.isValueValid8(bcmVoltage, "BCM Voltage"))
            {
                // Divide by 4 since the data is scaled as .25 of a Volt.
                voltage = ((float)bcmVoltage / Constant.FOUR);
                UserIO.outputInfo(String.Format("BCM Voltage {0}V.", voltage));
                if (output != null)
                {
                    output("BCM Voltage", string.Format("{0}V.", voltage));
                }
            }
            UserIO.outputNewLine(outDev.ALL, msgType.INFO);
        }

        public void formatEvent(string recordDesc, byte[] recordBuffer, Action<string, string> output = null)
        {
            const int EVENT_RECORD_TIME_STAMP_OFFSET = 8;
            const int EVENT_RECORD_EVENT_ID_OFFSET = 12;
            const int EVENT_RECORD_TIME_MS_OFFSET = 16;

            UInt32 eventId;
            UInt32 timeStamp;
            UInt16 timeStampMs;

            timeStamp = formatDataLogUtils.getProfileTime(recordBuffer, 0, EVENT_RECORD_TIME_STAMP_OFFSET);
            timeStampMs = (UInt16)(Utils.extractUInt16(recordBuffer, EVENT_RECORD_TIME_MS_OFFSET));
            eventId = Utils.extractUInt32(recordBuffer, EVENT_RECORD_EVENT_ID_OFFSET);

            UserIO.outputInfo(String.Format("{0}.", recordDesc));

            if (formatUtils.isValueValid32(eventId, "Event ID"))
            {
                UserIO.outputInfo(String.Format("Event ID 0x{0:X8} ({1}).", eventId, DTC.decodeEventId(eventId)));
            }

            UserIO.outputInfo(myFormatBcmRegs.formatEventDateTimeString (timeStamp));
          
            if (formatUtils.isValueValid16(timeStampMs, "Time Stamp (ms)"))
            {
                UserIO.outputInfo(String.Format("Time Stamp: 0x{0:X4} milliseconds.", timeStampMs));
            }

            UserIO.outputNewLine(outDev.ALL, msgType.INFO);
        }



        private void displayBusVoltage (UInt16 busVoltage, string msg, Action<string, string> output)
        {
            float voltage;

            if (formatUtils.isValueValid16(busVoltage, msg))
            {
                // Divide by ten since the data is scaled as tenths of an Volt.
                voltage = ((float)busVoltage / Constant.TEN);
                UserIO.outputInfo(String.Format("{0}: {1:0000.0}V.", msg, voltage));
                if (output != null)
                {
                    output(string.Format("{0}:", msg), string.Format("{0:0000.0}V.", voltage));
                }
            }
        }

        private void displayCellVoltage(byte cellVoltage, string msg, Action<string, string> output)
        {
            const int CELL_VOLTAGE_SCALE = 20;
            float voltage;

            if (formatUtils.isValueValid8(cellVoltage, msg))
            {
                // Scale the voltage data.
                voltage = ((float)(cellVoltage * CELL_VOLTAGE_SCALE) / Constant.MILLIVOLTS_IN_VOLT);
                UserIO.outputInfo(String.Format("{0} {1:0.000} V.", msg, voltage));
                if (output != null)
                {
                    output(msg, string.Format("{0:0.000}.", voltage));
                }
            }
        }

        private void displaySocValue(byte socValue, string msg, Action<string, string> output)
        {
            float soc;

            if (formatUtils.isValueValid8(socValue, msg))
            {
                // Divide by 2 since the data is scaled as .5 of a percent.
                soc = ((float)socValue / Constant.TWO);
                UserIO.outputInfo(String.Format("{0} {1:00.0}%.", msg, soc));

                if (output != null)
                {
                    output (msg, string.Format("{0}%.", soc));
                }
            }
        }

        private void displayTemperatureValue(byte value, string msg, Action<string, string> output)
        {
            const int TEMP_OFFSET = -40;
            float temp;

            if (formatUtils.isValueValid8(value, msg))
            {
                // Divide by 2 since the data is scaled as .5 of a percent.
                temp = ((float)value / Constant.TWO) + TEMP_OFFSET;
                UserIO.outputInfo(String.Format("{0} {1:000.0} C.", msg, temp));
                if (output != null)
                {
                    output(msg, string.Format("{0} C.", temp));
                }
            }
        }

        private void displayModuleVoltage (byte moduleVoltage, byte module, string msg, Action<string, string> output)
        {
            float voltage;

            if (formatUtils.isValueValid8(moduleVoltage, msg))
            {
                // Scale the module voltage reading.
                voltage = (float)(((float)moduleVoltage) * 0.40);
                UserIO.outputInfo(String.Format("{0} {1:00.0}V.", msg, voltage));

                if (output != null)
                {
                    output(msg, string.Format("{0}V.", voltage));
                }

            }

            displayModule (module, msg, output);
        }

        private void displayModule(byte module, string msg, Action<string, string> output)
        {
            int moduleOffset;

            if (formatUtils.isValueValid8(module, String.Format("{0} Module Number", msg)))
            {
                // Since module number is valid, increment to true module number.
                moduleOffset = module + Constant.ONE;

                UserIO.outputInfo(String.Format("{0} Module Number {1}.", msg, moduleOffset));

                if (output != null)
                {
                    output(String.Format("{0} Module Number", msg), string.Format("{0}", moduleOffset));
                }
            }
        }

        private void displayLocationInModule(byte module, byte location, string msg, Action<string, string> output)
        {
            int moduleOffset;
            int trueLocation;
            string moduleLocationMsg;

            if (formatUtils.isValueValid8(module, String.Format("{0} Module Number", msg)))
            {
                // Since module number is valid, increment to true module number.
                moduleOffset = module + Constant.ONE;

                if (output != null)
                {
                    output (String.Format ("{0} in module", msg), String.Format("{0}", moduleOffset));
                }

                if (formatUtils.isValueValid8(location, String.Format("{0} Location in Module Number {1}", msg, moduleOffset)))
                {
                    // Since location number is valid, increment to true location number.
                    trueLocation = location + Constant.ONE;
                    moduleLocationMsg = String.Format("{0} in module {1} at location", msg, moduleOffset);

                    UserIO.outputInfo(String.Format("{0} {1}.", moduleLocationMsg, trueLocation));
                    if (output != null)
                    {
                        output(moduleLocationMsg, String.Format("{0}", trueLocation));
                    }
                }
                else
                {
                    UserIO.outputInfo(String.Format("{0} in module {1}.", msg, moduleOffset));
                }
            }
            else
            {
                if (formatUtils.isValueValid8(location, String.Format("{0} Location in Module", msg)))
                {
                    trueLocation = location + Constant.ONE;
                    moduleLocationMsg = String.Format("{0} in module at location", msg);

                    UserIO.outputInfo(String.Format("{0} {1}.", moduleLocationMsg, trueLocation));
                    if (output != null)
                    {
                        output(moduleLocationMsg, String.Format("{0}", trueLocation));
                    }
                }
            }
        }

        private void displayLocation(byte location, string msg, Action<string, string> output)
        {
            int trueLocation;

            if (formatUtils.isValueValid8(location, msg))
            {
                // Since the location is a valid number, increment it to the true location.
                trueLocation = location + Constant.ONE;

                UserIO.outputInfo (String.Format("{0} {1}.", msg, trueLocation));

                if (output != null)
                {
                    output(msg, string.Format("{0}", trueLocation));
                }
            }
        }

        private void displayByteValue(byte value, string msg, Action<string, string> output)
        {
            if (formatUtils.isValueValid8(value, msg))
            {
                UserIO.outputInfo(String.Format("{0} {1}.", msg, value));
                if (output != null)
                {
                    output(msg, string.Format("{0}", value));
                }
            }
        }

    }  // End of class eventFormatBCM
 
}
 
                           
