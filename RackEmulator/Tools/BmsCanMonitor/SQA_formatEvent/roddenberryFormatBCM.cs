﻿//*************************************************************************
//
// Copyright © 2014 NEC Energy Solutions, Inc. All rights reserved.
//
// NEC Energy Solutions Proprietary Information.
// This document contains proprietary and confidential information and is
// tendered for information and evaluation purposes only; no copy or other
// use or dissemination by a third party can be made of this information
// without the express written permission of NEC Energy Solutions.
//
//*************************************************************************

using System;
using System.Linq;
using System.Text;
using SQA_format;
using SQA_utils;

namespace SQA_formatEvent
{
    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This file contains the classes for formatting the display of          */
    /*           BCM data logs associated with the Roddenberry BCM.                    */
    /*                                                                                 */
    /***********************************************************************************/

  
    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the Miscellaneous Profile.                                            */
    /*                                                                                 */
    /***********************************************************************************/

    public class roddenberryProfileMisc : formatBasicProfile
    {
        public roddenberryProfileMisc()
        {
            savedProfileBuffer = new byte[getProfileSize()];
        }

        public override int getProfileSize()
        {
            const int HISTOGRAM_MISC_SIZE = 0x0054;
            return HISTOGRAM_MISC_SIZE;
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "Miscellaneous";
            return profileMsg;
        }

        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, dispCntl displayControl)
        {

            string[] profileValues = new string[] 
                                         {"BCM On          ",
                                          "CSM On          ",
                                          "Module On       ",
                                          "Connector Close ",
                                          "Low t-charge    ",
                                          "Above I-limit   ",
                                          "BCM stand-by    ",
                                          "Equalize        ",
                                          "EPO             ",
                                          "Precharge       "
                                         };

            outputLogDataFormatArray(profileTitle, getProfileMsg(), readBuffer, lastProfileBuffer, displayControl, profileValues);
        }

    } // End of class roddenberryProfileMisc

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the Voltage Profile.                                                  */
    /*                                                                                 */
    /***********************************************************************************/

    public class roddenberryProfileVoltage : formatBasicProfile
    {
        public roddenberryProfileVoltage()
        {
            savedProfileBuffer = new byte[getProfileSize()];
        }

        public override int getProfileSize()
        {
            const int HISTOGRAM_VOLT_SIZE = 0x0088;
            return HISTOGRAM_VOLT_SIZE;
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "Voltage";
            return profileMsg;
        }

        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, dispCntl displayControl)
        {

            string[] profileValues = new string[] 
                                               {"Error/No Value",
                                                 "      X <= 600",
                                                 "600 < X <= 620",
                                                 "620 < X <= 640",
                                                 "640 < X <= 660",
                                                 "660 < X <= 680",
                                                 "680 < X <= 700",
                                                 "700 < X <= 720",
                                                 "720 < X <= 740",
                                                 "740 < X <= 760",
                                                 "760 < X <= 780",
                                                 "780 < X <= 800",
                                                 "800 < X <= 820",
                                                 "820 < X <= 840",
                                                 "840 < X <= 860",
                                                 "860 < X <= 880",
                                                 "880 < X <= 900",
                                                 "900 < X <= 920",
                                                 "920 < X <= 940",
                                                 "940 < X <= 960",
                                                 "960 < X <= 980",
                                                 "980 < X <= 1000",
                                                 "1000 < X <= 1020",
                                                 "1020 < X <= 1040",
                                                 "1040 < X <= 1060",
                                                 "1060 < X <= 1080",
                                                 "1080 < X <= 1100",
                                                 "1100 < X <= 1120",
                                                 "1120 < X <= 1140",
                                                 "1140 < X <= 1160",
                                                 "1160 < X <= 1180",
                                                 "1180 < X <= 1200",
                                                 "1200 < X        "
                                                };


            outputLogDataFormatArray(profileTitle, getProfileMsg(), readBuffer, lastProfileBuffer, displayControl, profileValues);
        }

    } // End of class roddenberryProfileVoltage

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the SOC Profile.                                                      */
    /*                                                                                 */
    /***********************************************************************************/

    public class roddenberryProfileSoc : formatBasicProfile
    {
        public roddenberryProfileSoc()
        {
            savedProfileBuffer = new byte[getProfileSize()];
        }

        public override int getProfileSize()
        {
            const int HISTOGRAM_SOC_SIZE = 0x0030;
            return HISTOGRAM_SOC_SIZE;
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "SOC";
            return profileMsg;
        }

        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, dispCntl displayControl)
        {

            string[] profileValues = new string[] 
                                                {"Err/No Value",
                                                 "00 < X <= 10",
                                                 "10 < X <= 20",
                                                 "20 < X <= 30",
                                                 "30 < X <= 40",
                                                 "40 < X <= 50",
                                                 "50 < X <= 60",
                                                 "60 < X <= 70",
                                                 "70 < X <= 80",
                                                 "80 < X <= 90",
                                                 "90 < X <= 100",
                                                };


            outputLogDataFormatArray(profileTitle, getProfileMsg(), readBuffer, lastProfileBuffer, displayControl, profileValues);
        }

    } // End of class roddenberryProfileSoc

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the Current Profile.                                                  */
    /*                                                                                 */
    /***********************************************************************************/

    public class roddenberryProfileCurrent : formatBasicProfile
    {
        public roddenberryProfileCurrent()
        {
            savedProfileBuffer = new byte[getProfileSize()];
        }

        public override int getProfileSize()
        {
            const int HISTOGRAM_CURRENT_SIZE = 0x0080;
            return HISTOGRAM_CURRENT_SIZE;
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "Current";
            return profileMsg;
        }

        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, dispCntl displayControl)
        {

            string[] profileValues = new string[] 
                                               {"Error/No Value  ",
                                                "       X <= -350",
                                                "-350 < X <= -325",
                                                "-325 < X <= -300",
                                                "-300 < X <= -275",
                                                "-275 < X <= -250",
                                                "-250 < X <= -225",
                                                "-225 < X <= -200",
                                                "-200 < X <= -175",
                                                "-175 < X <= -150",
                                                "-150 < X <= -125",
                                                "-125 < X <= -100",
                                                "-100 < X <= -75 ",
                                                " -75 < X <= -50 ",
                                                " -50 < X <= -25 ",
                                                " -25 < X <= 0   ",
                                                "   0 < X <= 25  ",
                                                "  25 < X <= 50  ",
                                                "  50 < X <= 75  ",
                                                "  75 < X <= 100 ",
                                                " 100 < X <= 125 ",
                                                " 125 < X <= 150 ",
                                                " 150 < X <= 175 ",
                                                " 175 < X <= 200 ",
                                                " 200 < X <= 225 ",
                                                " 225 < X <= 250 ",
                                                " 250 < X <= 275 ",
                                                " 275 < X <= 300 ",
                                                " 300 < X <= 325 ",
                                                " 325 < X <= 350 ",
                                                " 350 < X        "
                                                };



            outputLogDataFormatArray(profileTitle, getProfileMsg(), readBuffer, lastProfileBuffer, displayControl, profileValues);
        }

    } // End of class roddenberryProfileCurrent

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the Power Profile.                                                    */
    /*                                                                                 */
    /***********************************************************************************/

    public class roddenberryProfilePower : formatBasicProfile
    {
        public roddenberryProfilePower()
        {
            savedProfileBuffer = new byte[getProfileSize()];
        }

        public override int getProfileSize()
        {
            const int HISTOGRAM_POWER_SIZE = 0x0090;
            return HISTOGRAM_POWER_SIZE;
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "Power";
            return profileMsg;
        }

        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, dispCntl displayControl)
        {

            string[] profileValues = new string[] 
                                               {"Error/No Value    ",
                                                "        X <= -6240",
                                                "-6240 < X <= -5850",
                                                "-5850 < X <= -5460",
                                                "-5460 < X <= -5070",
                                                "-5070 < X <= -4680",
                                                "-4680 < X <= -4290",
                                                "-4290 < X <= -3900",
                                                "-3900 < X <= -3510",
                                                "-3510 < X <= -3120",
                                                "-3120 < X <= -2730",
                                                "-2730 < X <= -2340",
                                                "-2340 < X <= -1950",
                                                "-1950 < X <= -1560",
                                                "-1560 < X <= -1170",
                                                "-1170 < X <= -780",
                                                " -780 < X <= -390",
                                                " -390 < X <= 0   ",
                                                "    0 < X <= 390 ",
                                                "  390 < X <= 780 ",
                                                "  780 < X <= 1170",
                                                " 1170 < X <= 1560",
                                                " 1560 < X <= 1950",
                                                " 1950 < X <= 2340",
                                                " 2340 < X <= 2730",
                                                " 2730 < X <= 3120",
                                                " 3120 < X <= 3510",
                                                " 3510 < X <= 3900",
                                                " 3900 < X <= 4290",
                                                " 4290 < X <= 4680",
                                                " 4680 < X <= 5070",
                                                " 5070 < X <= 5460",
                                                " 5460 < X <= 5850",
                                                " 5850 < X <= 6240",
                                                " 6240 < X        ",
                                                };


            outputLogDataFormatArray(profileTitle, getProfileMsg(), readBuffer, lastProfileBuffer, displayControl, profileValues);
        }

    } // End of class roddenberryProfilePower

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the Meta Data Profile.                                                */
    /*                                                                                 */
    /***********************************************************************************/

    public class roddenberryProfileMetaData : formatBasicProfile
    {
        public roddenberryProfileMetaData()
        {
            savedProfileBuffer = new byte[getProfileSize()];
        }

        public override int getProfileSize()
        {
            const int HISTOGRAM_META_DATA_SIZE = 0x0018;  
            return HISTOGRAM_META_DATA_SIZE;
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "Meta Data";
            return profileMsg;
        }

        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, dispCntl displayControl)
        {

            UInt32 timeStamp;

            formatDataLogUtils.outputDataLogVersion(readBuffer, profileTitle);

            timeStamp = formatDataLogUtils.getProfileTime(readBuffer, Constant.ZERO, Constant.FOUR);

            UserIO.outputInfo(String.Format("Meta Data: Time Stamp {0}.", formatUtils.convertCalEventTime(timeStamp)));

            formatDataLogUtils.displayPart(readBuffer, Constant.EIGHT, "Meta Data: BCM");
            UserIO.outputNewLine(outDev.ALL, msgType.INFO);
        }

    } // End of class roddenberryProfileMetaData

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the Identity Profile.                                                 */
    /*                                                                                 */
    /***********************************************************************************/

    public class roddenberryProfileIdentity : formatBasicProfile
    {
        public roddenberryProfileIdentity()
        {
            savedProfileBuffer = new byte[getProfileSize()];
        }

        public override int getProfileSize()
        {
            const int HISTOGRAM_IDENTITY_SIZE = 0x001C;
            return HISTOGRAM_IDENTITY_SIZE;
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "Identity";
            return profileMsg;
        }

        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, dispCntl displayControl)
        {

            UInt16 bcmMajorRev;
            UInt16 bcmMinorRev;
            UInt16 bcmBuildRev;
            UInt32 nvmInitKey;

            formatDataLogUtils.outputDataLogVersion(readBuffer, profileTitle);

            bcmMajorRev = Utils.extractUInt16(readBuffer, Constant.TWO);
            bcmMinorRev = Utils.extractUInt16(readBuffer, Constant.FOUR);
            bcmBuildRev = Utils.extractUInt16(readBuffer, Constant.SIX);

            UserIO.outputInfo(String.Format("Identity: BCM Rev {0}.{1}.{2}", bcmMajorRev, bcmMinorRev, bcmBuildRev));

            nvmInitKey = Utils.extractUInt32(readBuffer, Constant.EIGHT);
            UserIO.outputInfo(String.Format("Identity: Init Key {0:X8}", nvmInitKey));

            formatDataLogUtils.displayPart(readBuffer, Constant.TWELVE, "Identity: CSM");
            UserIO.outputNewLine(outDev.ALL, msgType.INFO);
        }

    } // End of class roddenberryProfileIdentity


    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the Calibration Profile.                                              */
    /*                                                                                 */
    /***********************************************************************************/

    public class roddenberryProfileCalibration : formatBasicProfile
    {
        public roddenberryProfileCalibration()
        {
            savedProfileBuffer = new byte[getProfileSize()];
        }

        public override int getProfileSize()
        {
            const int HISTOGRAM_CALIBRATION_SIZE = 0x000C;
            return HISTOGRAM_CALIBRATION_SIZE;
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "Calibration";
            return profileMsg;
        }

        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, dispCntl displayControl)
        {

            UInt16 sensorGain;
            UInt16 sensorOffset;

            formatDataLogUtils.outputDataLogVersion(readBuffer, profileTitle);

            sensorGain = (UInt16)((readBuffer[2] << 8) | readBuffer[3]);
            sensorOffset = (UInt16)((readBuffer[4] << 8) | readBuffer[5]);

            UserIO.outputInfo(String.Format("Calibration: Sensor Gain: {0}.", sensorGain));
            UserIO.outputInfo(String.Format("Calibration: Sensor Offset: {0}.", sensorOffset));
            UserIO.outputNewLine(outDev.ALL, msgType.INFO);
        }

    } // End of class roddenberryProfileCalibration


    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the History Profile.                                                  */
    /*                                                                                 */
    /***********************************************************************************/

    public class roddenberryProfileHistory : roddenberryProfileHistoryBase
    {
        public override string getProfileMsg()
        {
            const string profileMsg = "History";
            return profileMsg;
        }

        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, dispCntl displayControl)
        {
            formatUtils.outputEnglishHexFormatFunctionLast(profileTitle, readBuffer, lastProfileBuffer, displayControl, formatFileHistory);
        }

        private void formatFileHistory(string hdrMsg, byte[] histBuffer, byte[] histBufferLast, dispCntl displayControl)
        {
            formatDataLogUtils.outputDataLogVersion(histBuffer, hdrMsg);

            formatFileHistoryPower(hdrMsg, histBuffer, histBufferLast, displayControl);

            formatFileHistoryFaults(hdrMsg, histBuffer, histBufferLast, displayControl);

            formatFileHistoryResets(hdrMsg, histBuffer, histBufferLast, displayControl);

            formatFileHistoryMBB(hdrMsg, histBuffer, histBufferLast, displayControl);
            
        }

  
    } // End of class roddenberryProfileHistory

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the History Alarms Profile.                                           */
    /*                                                                                 */
    /***********************************************************************************/

    public class roddenberryProfileHistoryAlarms : roddenberryProfileHistoryBase
    {
        public override string getProfileMsg()
        {
            const string profileMsg = "History Alarms";
            return profileMsg;
        }

        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, dispCntl displayControl)
        {
            formatUtils.outputEnglishHexFormatFunctionLast(profileTitle, readBuffer, lastProfileBuffer, displayControl, formatFileHistoryFaults);
        }

     

    } // End of class roddenberryProfileHistoryAlarms

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the History Power Profile.                                           */
    /*                                                                                 */
    /***********************************************************************************/

    public class roddenberryProfileHistoryPower : roddenberryProfileHistoryBase
    {
        public override string getProfileMsg()
        {
            const string profileMsg = "History Power";
            return profileMsg;
        }

        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, dispCntl displayControl)
        {
            formatUtils.outputEnglishHexFormatFunctionLast(profileTitle, readBuffer, lastProfileBuffer, displayControl, formatFileHistoryPower);
        }

      

    } // End of class roddenberryProfileHistoryPower

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the History Resets Profile.                                           */
    /*                                                                                 */
    /***********************************************************************************/

    public class roddenberryProfileHistoryResets : roddenberryProfileHistoryBase
    {
        public override string getProfileMsg()
        {
            const string profileMsg = "History Resets";
            return profileMsg;
        }

        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, dispCntl displayControl)
        {
            formatUtils.outputEnglishHexFormatFunctionLast(profileTitle, readBuffer, lastProfileBuffer, displayControl, formatFileHistoryResets);
        }

  
    } // End of class roddenberryProfileHistoryResets

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the History MBB Profile.                                              */
    /*                                                                                 */
    /***********************************************************************************/

    public class roddenberryProfileHistoryMBB : roddenberryProfileHistoryBase
    {
        public override string getProfileMsg()
        {
            const string profileMsg = "History MBB";
            return profileMsg;
        }

        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, dispCntl displayControl)
        {
            formatUtils.outputEnglishHexFormatFunctionLast(profileTitle, readBuffer, lastProfileBuffer, displayControl, formatFileHistoryMBB);
        }

  
    } // End of class roddenberryProfileHistoryMBB

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the History Alarms Profile.                                           */
    /*                                                                                 */
    /***********************************************************************************/

    public class roddenberryProfileHistoryBase : formatBasicProfile
    {
        public roddenberryProfileHistoryBase()
        {
            savedProfileBuffer = new byte[getProfileSize()];
        }

        public override int getProfileSize()
        {
            const int HISTOGRAM_HISTORY_SIZE = 0x00D4;
            return HISTOGRAM_HISTORY_SIZE;
        }

        public void formatFileHistoryPower(string hdrMsg, byte[] histBuffer, byte[] histBufferLast, dispCntl displayControl)
        {
            const int MAX_FILE_HISTORY_POWER_MSG = 9;
            const int MAX_FILE_HISTORY_POWER_OFFSET = 4;

            const int MAX_FILE_HISTORY_COLUMB_MSG = 2;
            const int MAX_FILE_HISTORY_COLUMB_OFFSET = 0xA0;

            string[] histPowerMsg = new string[MAX_FILE_HISTORY_POWER_MSG]
                                                {"Absolute peak power (W)",
                                                 "MSB battery discharge columbs Amp*Sec.",
                                                 "LSB battery discharge columbs Amp*Sec.",
                                                 "MSB battery charge columbs Amp*Sec.",
                                                 "LSB battery charge columbs Amp*Sec.",
                                                 "MSB battery discharge energy Watt*Sec.",
                                                 "LSB battery discharge energy Watt*Sec.",
                                                 "MSB battery charge energy Watt*Sec.",
                                                 "LSB battery charge energy Watt*Sec."
                                                };

            string[] histColumbMsg = new string[MAX_FILE_HISTORY_COLUMB_MSG]
                                                {"Charge columbs while cell > 3.6V in Amp*Sec.",
                                                 "Columbs in A*s cell temp <= -20 degC; charge current >= 10A"
                                                };

            formatFields32(hdrMsg, histBuffer, histBufferLast, displayControl, MAX_FILE_HISTORY_POWER_MSG, MAX_FILE_HISTORY_POWER_OFFSET, histPowerMsg);
            formatFields32(hdrMsg, histBuffer, histBufferLast, displayControl, MAX_FILE_HISTORY_COLUMB_MSG, MAX_FILE_HISTORY_COLUMB_OFFSET, histColumbMsg);
            UserIO.outputNewLine(outDev.ALL, msgType.INFO);
        }

        public void formatFileHistoryFaults(string hdrMsg, byte[] histBuffer, byte[] histBufferLast, dispCntl displayControl)
        {
            const int MAX_FILE_HISTORY_FAULT_MSG = 6;
            const int MAX_FILE_HISTORY_FAULT_OFFSET = 0x28;

            const int MAX_FILE_HISTORY_CONTACT_MSG = 2;
            const int MAX_FILE_HISTORY_CONTACT_OFFSET = 0xA8;

            string[] histFaultMsg = new string[MAX_FILE_HISTORY_FAULT_MSG]
                                                {"UV protection EPO's triggered",
                                                 "OV protection EPO's triggered",
                                                 "Over temp protection EPO's triggered",
                                                 "Alarm level 1 triggered",
                                                 "Alarm level 2 triggered",
                                                 "EPO's triggered"
                                                };

            string[] histContactMsg = new string[MAX_FILE_HISTORY_CONTACT_MSG]
                                                {"High side contactor closes",
                                                 "Low side contactor closes"
                                                };

            formatDataLogUtils.outputDataLogVersion(histBuffer, "History");

            formatFields16(hdrMsg, histBuffer, histBufferLast, displayControl, MAX_FILE_HISTORY_FAULT_MSG, MAX_FILE_HISTORY_FAULT_OFFSET, histFaultMsg);
            formatFields16(hdrMsg, histBuffer, histBufferLast, displayControl, MAX_FILE_HISTORY_CONTACT_MSG, MAX_FILE_HISTORY_CONTACT_OFFSET, histContactMsg);
            UserIO.outputNewLine(outDev.ALL, msgType.INFO);

        }

        public void formatFileHistoryResets(string hdrMsg, byte[] histBuffer, byte[] histBufferLast, dispCntl displayControl)
        {
            const int MAX_FILE_HISTORY_RESET_MSG = 8;
            const int MAX_FILE_HISTORY_RESET_OFFSET = 0x60;

            const int MAX_FILE_HISTORY_CNT_MSG = 2;
            const int MAX_FILE_HISTORY_CNT_OFFSET = 0x9C;

            string[] histResetMsg = new string[MAX_FILE_HISTORY_RESET_MSG]
                                                {"BCM power on reset detected",
                                                 "BCM low voltage reset detected",
                                                 "BCM clock related reset",
                                                 "BCM watchdog timer reset",
                                                 "BCM software reset",
                                                 "BCM code or data flash error detected",
                                                 "BCM microcheck error detected",
                                                 "External reset detected"
                                                };

            string[] histCntMsg = new string[MAX_FILE_HISTORY_CNT_MSG]
                                                {"PLC and CSM time differ by > 2 seconds",
                                                 "BCM flashed to different version"
                                                };


            formatFields16(hdrMsg, histBuffer, histBufferLast, displayControl, MAX_FILE_HISTORY_RESET_MSG, MAX_FILE_HISTORY_RESET_OFFSET, histResetMsg);
            formatFields16(hdrMsg, histBuffer, histBufferLast, displayControl, MAX_FILE_HISTORY_CNT_MSG, MAX_FILE_HISTORY_CNT_OFFSET, histCntMsg);
            UserIO.outputNewLine(outDev.ALL, msgType.INFO);

        }


        public void formatFileHistoryMBB(string hdrMsg, byte[] histBuffer, byte[] histBufferLast, dispCntl displayControl)
        {
            const int MAX_FILE_HISTORY_MBB_MSG = 22;
            const int MAX_FILE_HISTORY_MBB_SWAPPED_OFFSET = 0x34;
            const int MAX_FILE_HISTORY_MBB_CLEARED_OFFSET = 0x70;

            int msgIndex;

            UInt16 displaySwapData;
            UInt16 lastSwapData;
            UInt16 displayClearData;
            UInt16 lastClearData;

            for (msgIndex = Constant.ZERO; msgIndex < MAX_FILE_HISTORY_MBB_MSG; msgIndex++)
            {
                displaySwapData = Utils.extractUInt16(histBuffer, (msgIndex * Constant.TWO) + MAX_FILE_HISTORY_MBB_SWAPPED_OFFSET);
                displayClearData = Utils.extractUInt16(histBuffer, (msgIndex * Constant.TWO) + MAX_FILE_HISTORY_MBB_CLEARED_OFFSET);

                if (displayControl == dispCntl.DISPLAY_DIFF)
                {
                    lastSwapData = Utils.extractUInt16(histBufferLast, (msgIndex * Constant.TWO) + MAX_FILE_HISTORY_MBB_SWAPPED_OFFSET);
                    displaySwapData = (UInt16)(displaySwapData - lastSwapData);

                    lastClearData = Utils.extractUInt16(histBufferLast, (msgIndex * Constant.TWO) + MAX_FILE_HISTORY_MBB_CLEARED_OFFSET);
                    displayClearData = (UInt16)(displayClearData - lastClearData);
                }

                if ((displayControlProfileData(displayControl, (UInt32)displaySwapData)) ||
                    (displayControlProfileData(displayControl, (UInt32)displayClearData)))
                {
                    UserIO.outputInfo(String.Format("History: Module {0} Cleared {1}, Swapped {2}.", msgIndex + Constant.ONE, displayClearData, displaySwapData));
                }
            }
            UserIO.outputNewLine(outDev.ALL, msgType.INFO);

        }

      
    } // End of class roddenberryProfileHistoryBase

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the SOC NVM.                                                          */
    /*                                                                                 */
    /***********************************************************************************/

    public class roddenberrySocNvm : formatBasicProfile
    {
        public roddenberrySocNvm()
        {
            savedProfileBuffer = new byte[getProfileSize()];
        }

        public override int getProfileSize()
        {
            const int FILE_SOC_SIZE = 0x002C;
            return FILE_SOC_SIZE;
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "SOC NVM";
            return profileMsg;
        }

        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, dispCntl displayControl)
        {
            const int SOC_POWER_OFF_OFFSET = 0x0;
            const int SOC_KEY_OFFSET = 0x4;
            const int SOC_SCALE_OFFSET = 0x8;
            const int SOC_VALUES_OFFSET = 0xC;
            const int SOC_CHECKSUM_OFFSET = 0x2A;

            int msgIndex;

            UInt32 powerOffTime;
            UInt32 socKey;

            UInt16 socScale;
            UInt16 socChecksum;
            UInt16 socData;
            UInt16 socDataLast;

            float socFloatData;

            const int MAX_NEW_FILE_SOC_MSG = 15;

            string[] socValueMsg = new string[MAX_NEW_FILE_SOC_MSG] 
                                       {"Coulomb counter for maximum cell voltage SOC",
                                        "Coulomb counter for average cell voltage SOC",
                                        "Coulomb counter for minimum cell voltage SOC",
                                        "Upper band of current error for maximum cell voltage",
                                        "Upper band of current error for average cell voltage",
                                        "Upper band of current error for minimum cell voltage",
                                        "Lower band of current error for maximum cell voltage",
                                        "Lower band of current error for average cell voltage",
                                        "Lower band of current error for minimum cell voltage",
                                        "Voltage SOC for maximum cell voltage",
                                        "Voltage SOC for average cell voltage",
                                        "Voltage SOC for minimum cell voltage",
                                        "Hysteresis transition for maximum cell voltage",
                                        "Hysteresis transition for average cell voltage",
                                        "Hysteresis transition for minimum cell voltage"
                                       };

            powerOffTime = formatDataLogUtils.getProfileTime(readBuffer, Constant.ZERO, SOC_POWER_OFF_OFFSET);

            formatDataLogUtils.displayEventTime("SOC", "Power Off Event:", powerOffTime);

            socKey = Utils.extractUInt32(readBuffer, SOC_KEY_OFFSET);

            socScale = Utils.extractUInt16(readBuffer, SOC_SCALE_OFFSET);

            socChecksum = Utils.extractUInt16(readBuffer, SOC_CHECKSUM_OFFSET);

            UserIO.outputInfo(String.Format("Key: {0:X8}.", socKey));

            UserIO.outputInfo(String.Format("SoC Scale {0}.", socScale));

            // UserIO.outputInfo(String.Format("SOC: Checksum {0:X4}", socChecksum));

            for (msgIndex = Constant.OFFSET_0; msgIndex < MAX_NEW_FILE_SOC_MSG; msgIndex++)
            {
                socData = Utils.extractUInt16(readBuffer, (msgIndex * Constant.TWO) + SOC_VALUES_OFFSET);
                if (displayControl == dispCntl.DISPLAY_DIFF)
                {
                    socDataLast = Utils.extractUInt16(lastProfileBuffer, (msgIndex * Constant.TWO) + SOC_VALUES_OFFSET);
                    socData = (UInt16)(socData - socDataLast);
                }
                socFloatData = ((float)socData) / socScale;
                if ((displayControl == dispCntl.DISPLAY_ALL) ||
                    ((displayControl == dispCntl.DISPLAY_DIFF) && (socFloatData > Constant.ZERO)) ||
                    ((displayControl == dispCntl.DISPLAY_ON) && (socFloatData > Constant.ZERO)) ||
                    ((displayControl == dispCntl.DISPLAY_OFF) && (socFloatData == Constant.ZERO)))
                {
                    UserIO.outputInfo(String.Format("{0}: {1} - {2:0.00}.", "SOC", socValueMsg[msgIndex], socFloatData));
                }
            }  
        }

    } // End of class roddenberrySocNvm

}
 
                           
