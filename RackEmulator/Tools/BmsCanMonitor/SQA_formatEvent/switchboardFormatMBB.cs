﻿//*************************************************************************
//
// Copyright © 2014 NEC Energy Solutions, Inc. All rights reserved.
//
// NEC Energy Solutions Proprietary Information.
// This document contains proprietary and confidential information and is
// tendered for information and evaluation purposes only; no copy or other
// use or dissemination by a third party can be made of this information
// without the express written permission of NEC Energy Solutions.
//
//*************************************************************************

using System;
using System.Linq;
using System.Text;
using System.Globalization;
using SQA_utils;

namespace SQA_formatEvent
{
    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This file contains the classes for formatting the display of          */
    /*           MBB data logs associated with the Switchboard MBB.                    */
    /*                                                                                 */
    /***********************************************************************************/

  
    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the module voltage profile.                                           */
    /*                                                                                 */
    /***********************************************************************************/

    public class switchboardModuleVoltage : formatModuleProfile
    {
        public switchboardModuleVoltage(int maxModule)
        {
            savedProfileModuleBuffer = new byte[maxModule, getProfileSize()];
        }

        public override int getProfileSize()
        {
            const int MODULE_VOLTAGE_SIZE = 0x002C;
            return MODULE_VOLTAGE_SIZE;
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "Module Voltage";
            return profileMsg;
        }

        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, int moduleNumber, dispCntl displayControl)
        {
            const int MAX_FIELDS_SIZE_16 = 4;

            int copyIndex;

            byte[] voltBuffer32 = new byte[getProfileSize() + (Constant.TWO * MAX_FIELDS_SIZE_16)];
            byte[] voltBufferLast32 = new byte[getProfileSize() + (Constant.TWO * MAX_FIELDS_SIZE_16)];

            string[] profileValues = new string[] 
                                              {"Error/No Value  ",
                                               "       X <= 8   ",
                                               "   8 < X <= 12  ",
                                               "  12 < X <= 16  ",
                                               "  16 < X <= 20  ",
                                               "  20 < X <= 24  ",
                                               "  24 < X <= 25.6",
                                               "25.6 < X <= 27.2",
                                               "27.2 < X <= 28.8",
                                               "28.8 < X <= 30.4",
                                               "30.4 < X <= 32  ",
                                               "  32 < X        "
                                              };

            // Perform array copy to make everything 32 bits.

            // First fill destination arrays with zeroes.
            for (copyIndex = Constant.ZERO; copyIndex < voltBuffer32.Length; copyIndex++)
            {
                voltBuffer32[copyIndex] = Constant.ZERO;
                voltBufferLast32[copyIndex] = Constant.ZERO;
            }
            // Copy the first 32 bit fields
            for (copyIndex = Constant.ZERO; copyIndex < Constant.FOUR; copyIndex++)
            {
                voltBuffer32[copyIndex] = readBuffer[copyIndex];
                voltBufferLast32[copyIndex] = lastProfileBuffer[copyIndex];
            }

            // Copy the next two 16 bit fields
            voltBuffer32[6] = readBuffer[4];
            voltBuffer32[7] = readBuffer[5];
            voltBuffer32[10] = readBuffer[6];
            voltBuffer32[11] = readBuffer[7];
            voltBufferLast32[6] = lastProfileBuffer[4];
            voltBufferLast32[7] = lastProfileBuffer[5];
            voltBufferLast32[10] = lastProfileBuffer[6];
            voltBufferLast32[11] = lastProfileBuffer[7];

            // Copy the next seven 32 bit fields
            for (copyIndex = Constant.ZERO; copyIndex < Constant.FOUR * Constant.SEVEN; copyIndex++)
            {
                voltBuffer32[copyIndex + 12] = readBuffer[copyIndex + 8];
                voltBufferLast32[copyIndex + 12] = lastProfileBuffer[copyIndex + 8];
            }

            // Copy the last two 16 bit fields
            voltBuffer32[42] = readBuffer[36];
            voltBuffer32[43] = readBuffer[37];
            voltBuffer32[45] = readBuffer[38];
            voltBuffer32[46] = readBuffer[39];

            voltBufferLast32[42] = lastProfileBuffer[36];
            voltBufferLast32[43] = lastProfileBuffer[37];
            voltBufferLast32[45] = lastProfileBuffer[38];
            voltBufferLast32[46] = lastProfileBuffer[39];

            outputLogDataFormatArray(profileTitle, getProfileMsg(), voltBuffer32, voltBufferLast32, displayControl, profileValues);
        }
    } // End of class switchboardModuleVoltage

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the module Balancer profile.                                          */
    /*                                                                                 */
    /***********************************************************************************/

    public class switchboardModuleBalancer : formatModuleProfile
    {
        public switchboardModuleBalancer(int maxModule)
        {
            savedProfileModuleBuffer = new byte[maxModule, getProfileSize()];
        }

        public override int getProfileSize()
        {
            const int MODULE_BALANCER_SIZE = 0x0038;
            // const int MODULE_BALANCER_SIZE = 0x0024;  // Reduced number of cells
            return MODULE_BALANCER_SIZE;
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "Balancer";
            return profileMsg;
        }

        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, int moduleNumber, dispCntl displayControl)
        {

            string[] profileValues = new string[] 
                                               {"Cell 01 Balancer ON",
                                                "Cell 02 Balancer ON",
                                                "Cell 03 Balancer ON",
                                                "Cell 04 Balancer ON",
                                                "Cell 05 Balancer ON",
                                                "Cell 06 Balancer ON",
                                                "Cell 07 Balancer ON",
                                                "Cell 08 Balancer ON"
                                               };

            outputLogDataFormatArray(profileTitle, getProfileMsg(), readBuffer, lastProfileBuffer, displayControl, profileValues);
        }

    } // End of class switchboardModuleBalancer

 

}
 
                           
