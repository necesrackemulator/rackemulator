﻿//*************************************************************************
//
// Copyright © 2014 NEC Energy Solutions, Inc. All rights reserved.
//
// NEC Energy Solutions Proprietary Information.
// This document contains proprietary and confidential information and is
// tendered for information and evaluation purposes only; no copy or other
// use or dissemination by a third party can be made of this information
// without the express written permission of NEC Energy Solutions.
//
//*************************************************************************

using System;
using System.Linq;
using System.Text;
using SQA_format;
using SQA_utils;

namespace SQA_formatEvent
{
    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This file contains the classes for formatting the display of          */
    /*           BCM data logs associated with the Sunrise BCM.                        */
    /*                                                                                 */
    /***********************************************************************************/

 
    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the SOC NVM.                                                          */
    /*                                                                                 */
    /***********************************************************************************/

    public class sunriseSocNvm : formatBasicProfile
    {
        public sunriseSocNvm()
        {
            savedProfileBuffer = new byte[getProfileSize()];
        }

        public override int getProfileSize()
        {
            const int FILE_SOC_SIZE = 0x0024;
            return FILE_SOC_SIZE;
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "SOC NVM";
            return profileMsg;
        }

        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, dispCntl displayControl)
        {
            const int SOC_POWER_OFF_OFFSET = 0x0;
            const int SOC_KEY_OFFSET = 0x4;
            const int SOC_VALUES_OFFSET = 0x8;
            const int SOC_CHECKSUM_OFFSET = 0x22;

            int msgIndex;

            UInt32 powerOffTime;
            UInt32 socKey;

            UInt16 socChecksum;
            float socData;
            float socDataLast;

            float socFloatData;

            const int MAX_NEW_FILE_SOC_MSG = 6;

            string[] socValueMsg = new string[MAX_NEW_FILE_SOC_MSG] 
                                       {"SOC upper value",
                                        "SOC custom value",
                                        "SOC lower value",
                                        "dSOC upper error",
                                        "dSOC custom error",
                                        "dSOC lower error"
                                       };

            powerOffTime = formatDataLogUtils.getProfileTime(readBuffer, Constant.ZERO, SOC_POWER_OFF_OFFSET);

            formatDataLogUtils.displayEventTime("SOC", "Power Off Event:", powerOffTime);

            socKey = Utils.extractUInt32(readBuffer, SOC_KEY_OFFSET);

            socChecksum = Utils.extractUInt16(readBuffer, SOC_CHECKSUM_OFFSET);

            UserIO.outputInfo(String.Format("Key: {0:X8}.", socKey));

            // UserIO.outputInfo(String.Format("SOC: Checksum {0:X4}", socChecksum));

            for (msgIndex = Constant.OFFSET_0; msgIndex < MAX_NEW_FILE_SOC_MSG; msgIndex++)
            {
                socData = formatUtils.byteFloatConversion(readBuffer, (msgIndex * Constant.FOUR) + SOC_VALUES_OFFSET);

                if (displayControl == dispCntl.DISPLAY_DIFF)
                {
                    socDataLast = formatUtils.byteFloatConversion(lastProfileBuffer, (msgIndex * Constant.FOUR) + SOC_VALUES_OFFSET);
                    socData = (socData - socDataLast);
                }
                socFloatData = socData;

                if ((displayControl == dispCntl.DISPLAY_ALL) ||
                    ((displayControl == dispCntl.DISPLAY_DIFF) && (socFloatData > Constant.ZERO)) ||
                    ((displayControl == dispCntl.DISPLAY_ON) && (socFloatData > Constant.ZERO)) ||
                    ((displayControl == dispCntl.DISPLAY_OFF) && (socFloatData == Constant.ZERO)))
                {
                    UserIO.outputInfo(String.Format("{0}: {1} - {2}.", "SOC", socValueMsg[msgIndex], socFloatData));
                }
            }
        }

    } // End of class sunriseSocNvm

}
 
                           
