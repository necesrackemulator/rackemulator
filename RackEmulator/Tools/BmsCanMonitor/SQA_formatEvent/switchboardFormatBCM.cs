﻿//*************************************************************************
//
// Copyright © 2014 NEC Energy Solutions, Inc. All rights reserved.
//
// NEC Energy Solutions Proprietary Information.
// This document contains proprietary and confidential information and is
// tendered for information and evaluation purposes only; no copy or other
// use or dissemination by a third party can be made of this information
// without the express written permission of NEC Energy Solutions.
//
//*************************************************************************

using System;
using System.Linq;
using System.Text;
using SQA_utils;

namespace SQA_formatEvent
{
    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This file contains the classes for formatting the display of          */
    /*           BCM data logs associated with the Switchboard BCM.                    */
    /*                                                                                 */
    /***********************************************************************************/
  
    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the module voltage profile.                                           */
    /*                                                                                 */
    /***********************************************************************************/

    public class switchboardProfileMisc : formatBasicProfile
    {
        public switchboardProfileMisc()
        {
            savedProfileBuffer = new byte[getProfileSize()];
        }

        public override int getProfileSize()
        {
            const int HISTOGRAM_MISC_SIZE = 0x0054;
            return HISTOGRAM_MISC_SIZE;
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "Miscellaneous";
            return profileMsg;
        }

        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, dispCntl displayControl)
        {

            string[] profileValues = new string[] 
                                         {"BCM On          ",
                                          "CSM On          ",
                                          "Module On       ",
                                          "Connector Close ",
                                          "Low t-charge    ",
                                          "Above I-limit   ",
                                          "BCM stand-by    ",
                                          "Equalize        ",
                                          "EPO             ",
                                          "Precharge       "
                                         };

            outputLogDataFormatArray(profileTitle, getProfileMsg(), readBuffer, lastProfileBuffer, displayControl, profileValues);
        }

    } // End of class switchboardProfileMisc

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the Voltage Profile.                                                  */
    /*                                                                                 */
    /***********************************************************************************/

    public class switchboardProfileVoltage : formatBasicProfile
    {
        public switchboardProfileVoltage()
        {
            savedProfileBuffer = new byte[getProfileSize()];
        }

        public override int getProfileSize()
        {
            const int HISTOGRAM_VOLT_SIZE = 0x0088;
            return HISTOGRAM_VOLT_SIZE;
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "Voltage";
            return profileMsg;
        }

        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, dispCntl displayControl)
        {

            string[] profileValues = new string[] 
                                                {"Err/No Value",
                                                 "     X <= 16",
                                                 "16 < X <= 18",
                                                 "18 < X <= 20",
                                                 "20 < X <= 22",
                                                 "22 < X <= 24",
                                                 "24 < X <= 26",
                                                 "26 < X <= 28",
                                                 "28 < X <= 30",
                                                 "30 < X <= 32",
                                                 "32 < X <= 34",
                                                 "34 < X <= 36",
                                                 "36 < X <= 38",
                                                 "38 < X <= 40",
                                                 "40 < X <= 42",
                                                 "42 < X <= 44",
                                                 "44 < X <= 46",
                                                 "46 < X <= 48",
                                                 "48 < X <= 50",
                                                 "50 < X <= 52",
                                                 "52 < X <= 54",
                                                 "54 < X <= 56",
                                                 "56 < X <= 58",
                                                 "58 < X <= 60",
                                                 "60 < X <= 62",
                                                 "62 < X <= 64",
                                                 "64 < X <= 66",
                                                 "66 < X <= 68",
                                                 "68 < X <= 70",
                                                 "70 < X <= 72",
                                                 "72 < X <= 74",
                                                 "74 < X <= 76",
                                                 "76 < X      "
                                                };
 


            outputLogDataFormatArray(profileTitle, getProfileMsg(), readBuffer, lastProfileBuffer, displayControl, profileValues);
        }

    } // End of class switchboardProfileVoltage

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the SOC NVM.                                                          */
    /*                                                                                 */
    /***********************************************************************************/

    public class switchboardSocNvm : formatBasicProfile
    {
        public switchboardSocNvm()
        {
            savedProfileBuffer = new byte[getProfileSize()];
        }

        public override int getProfileSize()
        {
            const int FILE_SOC_SIZE = 0x002C;
            return FILE_SOC_SIZE;
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "SOC NVM";
            return profileMsg;
        }

        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, dispCntl displayControl)
        {
            const int SOC_POWER_OFF_OFFSET = 0x0;
            const int SOC_KEY_OFFSET = 0x4;
            const int SOC_SCALE_OFFSET = 0x8;
            const int SOC_VALUES_OFFSET = 0xC;
            const int SOC_CHECKSUM_OFFSET = 0x2A;

            int msgIndex;

            UInt32 powerOffTime;
            UInt32 socKey;

            UInt16 socScale;
            UInt16 socChecksum;
            UInt16 socData;
            UInt16 socDataLast;

            float socFloatData;

            const int MAX_NEW_FILE_SOC_MSG = 15;

            string[] socValueMsg = new string[MAX_NEW_FILE_SOC_MSG] 
                                       {"Coulomb counter maximum cell voltage",
                                        "Coulomb counter average cell voltage",
                                        "Coulomb counter minimum cell voltage",
                                        "High iSOC Delta maximum cell voltage",
                                        "High iSOC Delta average cell voltage",
                                        "High iSOC Delta minimum cell voltage",
                                        "Low iSOC Delta maximum cell voltage",
                                        "Low iSOC Delta average cell voltage",
                                        "Low iSOC Delta minimum cell voltage",
                                        "Voltage SOC for maximum cell voltage",
                                        "Voltage SOC for average cell voltage",
                                        "Voltage SOC for minimum cell voltage",
                                        "Hysteresis maximum cell voltage",
                                        "Hysteresis average cell voltage",
                                        "Hysteresis minimum cell voltage"
                                       };

            powerOffTime = formatDataLogUtils.getProfileTime(readBuffer, Constant.ZERO, SOC_POWER_OFF_OFFSET);

            formatDataLogUtils.displayEventTime("SOC", "Power Off Event:", powerOffTime);

            socKey = Utils.extractUInt32(readBuffer, SOC_KEY_OFFSET);

            socScale = Utils.extractUInt16(readBuffer, SOC_SCALE_OFFSET);

            socChecksum = Utils.extractUInt16(readBuffer, SOC_CHECKSUM_OFFSET);

            UserIO.outputInfo(String.Format("Key: {0:X8}.", socKey));

            UserIO.outputInfo(String.Format("SOC Scale {0}.", socScale));

            // UserIO.outputInfo(String.Format("SOC: Checksum {0:X4}", socChecksum));

            for (msgIndex = Constant.OFFSET_0; msgIndex < MAX_NEW_FILE_SOC_MSG; msgIndex++)
            {
                socData = Utils.extractUInt16(readBuffer, (msgIndex * Constant.TWO) + SOC_VALUES_OFFSET);
                if (displayControl == dispCntl.DISPLAY_DIFF)
                {
                    socDataLast = Utils.extractUInt16(lastProfileBuffer, (msgIndex * Constant.TWO) + SOC_VALUES_OFFSET);
                    socData = (UInt16)(socData - socDataLast);
                }
                socFloatData = ((float)socData) / socScale;
                if ((displayControl == dispCntl.DISPLAY_ALL) ||
                    ((displayControl == dispCntl.DISPLAY_DIFF) && (socFloatData > Constant.ZERO)) ||
                    ((displayControl == dispCntl.DISPLAY_ON) && (socFloatData > Constant.ZERO)) ||
                    ((displayControl == dispCntl.DISPLAY_OFF) && (socFloatData == Constant.ZERO)))
                {
                    UserIO.outputInfo(String.Format("{0}: {1} - {2:0.00}.", "SOC", socValueMsg[msgIndex], socFloatData));
                }
            }
        }

    } // End of class switchboardSocNvm
}
 
                           
