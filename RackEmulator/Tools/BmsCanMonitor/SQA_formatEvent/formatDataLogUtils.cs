﻿//*************************************************************************
//
// Copyright © 2014 NEC Energy Solutions, Inc. All rights reserved.
//
// NEC Energy Solutions Proprietary Information.
// This document contains proprietary and confidential information and is
// tendered for information and evaluation purposes only; no copy or other
// use or dissemination by a third party can be made of this information
// without the express written permission of NEC Energy Solutions.
//
//*************************************************************************

using System;
using System.Linq;
using System.Text;
using SQA_format;
using SQA_utils;

namespace SQA_formatEvent
{
    public class formatDataLogUtils
    {
        
        /***********************************************************************************/
        /*                                                                                 */
        /* Function:  Display the File Version of a datalog file block.                    */
        /*                                                                                 */
        /* Arguments: buffer - the byte buffer containing the datalog block.               */
        /*            name - the name of the datalog block.                                */
        /*                                                                                 */
        /* Returns:   Void.                                                                */
        /*                                                                                 */
        /* Caveats:  None.                                                                 */
        /*                                                                                 */
        /***********************************************************************************/
        public static void outputDataLogVersion(byte[] buffer, string name)
        {
            UInt16 version;
            version = (UInt16)((buffer[Constant.ZERO] << Constant.SHIFT_8) | buffer[Constant.ONE]);
            UserIO.outputInfo(String.Format("{0}: File Version {1:X}", name, version));
        }

        /***********************************************************************************/
        /*                                                                                 */
        /* Function:  Display a message containing an event message and a formated event   */
        /*            event time based upon calender time.                                 */
        /*                                                                                 */
        /* Arguments: eventMsg - the event message.                                        */
        /*            eventValueMsg - the type of event.                                   */
        /*            secondCnt - the number of seconds elapsed for this event.            */
        /*                                                                                 */
        /* Returns:   Void.                                                                */
        /*                                                                                 */
        /* Caveats:  None.                                                                 */
        /*                                                                                 */
        /***********************************************************************************/
        public static void displayElapsedTime(string eventMsg, string eventValueMsg, UInt32 secondsCnt)
        {
            UserIO.outputInfo(String.Format("{0}: {1}   {2}", eventMsg, eventValueMsg, formatUtils.convertElapsedTime(secondsCnt)));
        }

       
        /***********************************************************************************/
        /*                                                                                 */
        /* Function:  Display a message containing an event message and a formated event   */
        /*            event time based upon calender time.                                 */
        /*                                                                                 */
        /* Arguments: eventMsg - the event message.                                        */
        /*            eventValueMsg - the type of event.                                   */
        /*            secondCnt - the number of seconds elapsed since a time in the past.  */
        /*                                                                                 */
        /* Returns:   Void.                                                                */
        /*                                                                                 */
        /* Caveats:  None.                                                                 */
        /*                                                                                 */
        /***********************************************************************************/
        public static void displayEventTime(string eventMsg, string eventValueMsg, UInt32 secondsCnt)
        {
            UserIO.outputInfo(String.Format("{0}: {1}   {2}", eventMsg, eventValueMsg, convertEventTime(secondsCnt)));
        }

        /***********************************************************************************/
        /*                                                                                 */
        /* Function:  Return a calendar date string.                                       */
        /*                                                                                 */
        /* Arguments: secondCnt - the number of seconds elapsed since a time in the past,  */
        /*                        Jan. 1, 1997.                                            */
        /*                                                                                 */
        /* Returns:   A date in string format.                                             */
        /*                                                                                 */
        /* Caveats:  None.                                                                 */
        /*                                                                                 */
        /***********************************************************************************/
        private static string convertEventTime(UInt32 secondsCnt)
        {
            // Set date to Jan. 1, 1997.
            DateTime date = new DateTime(1997, 1, 1, 0, 0, 0, 0, 0);
            date = date.AddSeconds(secondsCnt);
            return date.ToString();
        }

        public static UInt32 getProfileTime(byte[] buffer, int bufferIndex, int offset)
        {
            UInt32 profileTime;

            profileTime = Utils.extractUInt32(buffer, (bufferIndex * Constant.FOUR) + offset);
            return profileTime;
        }

        /************************************************************************************************************/
        /*                                                                                                          */
        /*  Function: Format and display the Part Number and Serial Number fields read from the data log.           */
        /*            12 bytes contain the 8 byte serial number followed by the 4 byte part number.                 */
        /*                                                                                                          */
        /*  Parameters: buffer - the buffer containing 12-bytes of part number data.                                */
        /*              byteOffset - the offset of the 12 bytes.                                                    */
        /*              msgString - a descriptive message of the device whose part number is being displayed.       */
        /*                                                                                                          */
        /*  Caveats:  None.                                                                                         */
        /*                                                                                                          */
        /************************************************************************************************************/

        public static void displayPart(byte[] buffer, int byteOffset, string msgString)
        {
            const int SERIAL_DATA_SIZE = Constant.EIGHT;

            byte[] serialData = new byte[SERIAL_DATA_SIZE];

            UInt32 partA;

            Array.Copy(buffer, byteOffset, serialData, Constant.ZERO, SERIAL_DATA_SIZE);

            partA = Utils.extractUInt32(buffer, byteOffset + SERIAL_DATA_SIZE);

            UserIO.outputInfo(String.Format("{0} Serial Number: {1}", msgString, formatPart.formatSerialNumber(serialData)));
            UserIO.outputInfo(String.Format("{0} Part Number: {1}", msgString, partA));
        }

         
        
    } // End of class formatDataLogUtils
}
 
                           
