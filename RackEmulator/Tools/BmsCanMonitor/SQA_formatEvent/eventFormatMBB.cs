﻿//*************************************************************************
//
// Copyright © 2014 NEC Energy Solutions, Inc. All rights reserved.
//
// NEC Energy Solutions Proprietary Information.
// This document contains proprietary and confidential information and is
// tendered for information and evaluation purposes only; no copy or other
// use or dissemination by a third party can be made of this information
// without the express written permission of NEC Energy Solutions.
//
//*************************************************************************

using System;
using System.Linq;
using System.Text;
using SQA_format;
using SQA_utils;

namespace SQA_formatEvent
{
    public class eventFormatMBB
    {
        public void formatEventRecord(string recordDesc, byte[] recordBuffer, bool shortOnly)
        {
            const int EVENT_RECORD_ALARMS_OFFSET = 0;
            const int EVENT_RECORD_DIGITAL_INPUT_OFFSET = 4;
            const int EVENT_RECORD_DIGITAL_OUTPUT_OFFSET = 6;
            const int EVENT_RECORD_STATE_OFFSET = 10;
            const int EVENT_RECORD_SOC_OFFSET = 11;
            const int EVENT_RECORD_MAX_CELL_VOLTAGE_OFFSET = 12;
            const int EVENT_RECORD_MIN_CELL_VOLTAGE_OFFSET = 14;
            const int EVENT_RECORD_MODULE_VOLTAGE_OFFSET = 16;
            const int EVENT_RECORD_CELL_TEMP_OFFSET = 20;
            const int EVENT_RECORD_BRD_TEMP_OFFSET = 22;
            const int EVENT_RECORD_CURRENT_OFFSET = 24;

            const int EVENT_RECORD_FAULT_FLAGS1_OFFSET = 32;
            const int EVENT_RECORD_FAULT_FLAGS2_OFFSET = 34;

            UInt32 alarmStatus;
            UInt32 digitalOutput;
            UInt16 digitalInput;
            UInt16 current;
            UInt16 cellTemp;
            UInt16 brdTemp;

            UInt16 maxCellVoltage;
            UInt16 minCellVoltage;
            UInt32 moduleVoltage;

            byte stateMBB;
            byte soc;
            
            UInt16 faultFlags1;
            UInt16 faultFlags2;

            // display the time and record number.
            formatEventTime(recordDesc, recordBuffer);

            alarmStatus = Utils.extractLittleEndian32(recordBuffer, EVENT_RECORD_ALARMS_OFFSET);
            displayAlarms (alarmStatus);
  
            if (!shortOnly)
            {
                digitalInput = Utils.extractLittleEndian16(recordBuffer, EVENT_RECORD_DIGITAL_INPUT_OFFSET);
                digitalOutput = Utils.extractLittleEndian32(recordBuffer, EVENT_RECORD_DIGITAL_OUTPUT_OFFSET);

                displayDigitalInput(digitalInput);
                displayDigitalOutput(digitalOutput);

                UserIO.outputNewLine(outDev.ALL, msgType.INFO);

                maxCellVoltage = Utils.extractLittleEndian16(recordBuffer, EVENT_RECORD_MAX_CELL_VOLTAGE_OFFSET);
                minCellVoltage = Utils.extractLittleEndian16(recordBuffer, EVENT_RECORD_MIN_CELL_VOLTAGE_OFFSET);
                moduleVoltage = Utils.extractLittleEndian32(recordBuffer, EVENT_RECORD_MODULE_VOLTAGE_OFFSET);

                if (formatUtils.isValueValid16(maxCellVoltage, "Max Cell Voltage"))
                {
                    UserIO.outputInfo(String.Format("Max Cell Voltage: {0:0.000}V.", (float)maxCellVoltage / Constant.MILLIVOLTS_IN_VOLT));
                }

                if (formatUtils.isValueValid16(minCellVoltage, "Min Cell Voltage"))
                {
                    UserIO.outputInfo(String.Format("Min Cell Voltage: {0:0.000}V.", (float)minCellVoltage / Constant.MILLIVOLTS_IN_VOLT));
                }

                if (formatUtils.isValueValid32(moduleVoltage, "Module Voltage"))
                {
                    UserIO.outputInfo(String.Format("Module Voltage: {0:0.000}V.", (float)moduleVoltage / Constant.MILLIVOLTS_IN_VOLT));
                }

                UserIO.outputNewLine(outDev.ALL, msgType.INFO);

                stateMBB = recordBuffer[EVENT_RECORD_STATE_OFFSET];
                soc = recordBuffer[EVENT_RECORD_SOC_OFFSET];

                if (formatUtils.isValueValid8(soc, "State MBB"))
                {
                    UserIO.outputInfo(String.Format("State MBB: 0x{0:X2}.", stateMBB));
                }

                if (formatUtils.isValueValid8(soc, "SOC"))
                {
                    UserIO.outputInfo(String.Format("SOC: {0}%.", soc));
                }

                UserIO.outputNewLine(outDev.ALL, msgType.INFO);

                cellTemp = Utils.extractLittleEndian16(recordBuffer, EVENT_RECORD_CELL_TEMP_OFFSET);
                brdTemp = Utils.extractLittleEndian16(recordBuffer, EVENT_RECORD_BRD_TEMP_OFFSET);



                if (isTempValueValid16(cellTemp, "Board Temperature"))
                {
                    UserIO.outputInfo(String.Format("Board Temperature: {0:0.0}C.", (float)brdTemp / Constant.TEN));
                }

                if (isTempValueValid16(cellTemp, "Cell Temperature"))
                {
                    UserIO.outputInfo(String.Format("Cell Temperature: {0:0.0}C.", (float)cellTemp / Constant.TEN));
                }

                current = Utils.extractLittleEndian16(recordBuffer, EVENT_RECORD_CURRENT_OFFSET);
                UserIO.outputInfo(String.Format("Current: {0}A.", (float)current));

                // if (formatUtils.isCurrentValueValid16 (current, "Current"))
                // {
                //    UserIO.outputInfo(String.Format("Current: {0}A.", current));
                // }

                UserIO.outputNewLine(outDev.ALL, msgType.INFO);

                faultFlags1 = Utils.extractLittleEndian16(recordBuffer, EVENT_RECORD_FAULT_FLAGS1_OFFSET);
                faultFlags2 = Utils.extractLittleEndian16(recordBuffer, EVENT_RECORD_FAULT_FLAGS2_OFFSET);
                displayFaults(faultFlags1, faultFlags2);

                // crc = Utils.extractLittleEndian16 (recordBuffer, EVENT_RECORD_CRC_OFFSET);
                // UserIO.outputInfo(String.Format ("CRC: 0x{0:X4}.", crc));
            }
        }

        /***********************************************************************************/
        /*                                                                                 */
        /* Function: Determines if a 16 bit temperature value corresponds to NO_VALUE or   */
        /*           ERR_VALUE. If so, a message is displayed.                             */
        /*                                                                                 */
        /* Arguments: value - the value being tested.                                      */
        /*            msg - a message describing the type of value, i.e., cell temperature.*/
        /*                                                                                 */
        /* Returns:  true - if value is valid, i.e., not NO_VALUE or ERR_VALUE.            */
        /*           false - if value is not valid.                                        */
        /*                                                                                 */
        /* Caveats:  None.                                                                 */
        /*                                                                                 */
        /***********************************************************************************/
        private bool isTempValueValid16(UInt16 value, string msg)
        {
            const UInt16 NO_VALUE = 0x8000;
            const UInt16 ERR_VALUE = 0x8FFF;

            bool valueValid = true;

            if (value == NO_VALUE)
            {
                UserIO.outputInfo(String.Format("{0}: NO VALUE.", msg));
                valueValid = false;
            }
            else if (value == ERR_VALUE)
            {
                UserIO.outputInfo(String.Format("{0}: ERR VALUE.", msg));
                valueValid = false;
            }

            return valueValid;
        }

        private void displayFaults(UInt16 faultFlags1, UInt16 faultFlags2)
        {
            string[] faultMsgs1 = {"CAL_FLASH_FAULT", 
                                  "FACT_FLASH_FAULT",
                                  "USER_FLASH_FAULT",
                                  "PROFILER_FLASH_FAULT",
                                  "CEDL_FLASH_FAULT",
                                  "EDL_FLASH_FAULT",
                                  "Unused 6",
                                  "Unused 7",
                                  "Unused 8",
                                  "Unused 9",
                                  "Unused 10",
                                  "Unused 11",
                                  "Unused 12",
                                  "Unused 13",
                                  "RTC_RESET",
                                  "WDT_TIMEOUT"
                                 };
            string[] faultMsgs2 = {"CELL_TEMP_SENSOR_1_OPEN",
                                   "CELL_TEMP_SENSOR_1_SHORT",
                                   "CELL_TEMP_SENSOR_2_OPEN",
                                   "CELL_TEMP_SENSOR_2_SHORT",
                                   "CELL_TEMP_SENSOR_3_OPEN",
                                   "CELL_TEMP_SENSOR_3_SHORT",
                                   "Unused 6",
                                   "Unused 7",
                                   "BOARD_TEMP_SENSOR_OPEN",
                                   "BOARD_TEMP_SENSOR_SHORT",
                                   "Unused 10",
                                   "Unused 11",
                                   "Unused 12",
                                   "Unused 13",
                                   "Unused 14",
                                   "Unused 15"
                                 };

            formatUtils.displayBitMap(faultFlags1, "Fault Flags1", faultMsgs1);
            formatUtils.displayBitMap(faultFlags2, "Fault Flags2", faultMsgs2);
        }

        /***********************************************************************************/
        /*                                                                                 */
        /* Function: Output a formatted display of the Digital Input Register.  For each   */
        /*           defined bit that is set high, display a message describing the bit.   */
        /*                                                                                 */
        /* Arguments: digitialInput bits to be decoded.                                    */
        /*                                                                                 */
        /* Returns:   Void.                                                                */
        /*                                                                                 */
        /* Caveats:   None.                                                                */
        /*                                                                                 */
        /***********************************************************************************/

        private void displayDigitalInput (UInt16 digitalInput)
        {
            string[] inputValues = {"System Powered", 
                                    "RTC Wake Up",
                                    "Sys Fault IN",
                                    "UVP Primary",
                                    "UVP Secondary",
                                    "OVP Primary",
                                    "OVP Secondary",
                                    "Critical Fault",
                                    "QUAD CNTRL 1",
                                    "QUAD CNTRL 2",
                                    "QUAD CNTRL 3",
                                    "QUAD CNTRL 4",
                                    "Temp Fault"
                                   };

            formatUtils.displayBitMap((UInt32)(digitalInput), "Digital Input", inputValues);
        }

        /************************************************************************************************************/
        /*                                                                                                          */
        /*  Function: Format the Manufacturing Status Register.                                                     */
        /*            Format the output based upon bits set to one or zero.                                         */
        /*                                                                                                          */
        /*  Caveats:  Bits set to one are displayed first.                                                          */
        /*                                                                                                          */
        /************************************************************************************************************/

        private void displayDigitalOutput(UInt32 digitalOutput)
        {
            string[] outputValues = {"BCELL_0",
                                     "BCELL_1",
                                     "BCELL_2",
                                     "BCELL_3",
                                     "BCELL_4",
                                     "BCELL_5",
                                     "BCELL_6",
                                     "BCELL_7",
                                     "BCELL_8",
                                     "BCELL_9",
                                     "BCELL_10",
                                     "BCELL_11",
                                     "BCELL_12",
                                     "BCELL_13",
                                     "BCELL_14",
                                     "BCELL_15",
                                     "Sleep",
                                     "Read SEL",
                                     "Read Addr 0",
                                     "Read Addr 1",
                                     "Read Addr 2",
                                     "Read Addr 3",
                                     "Sample Cell V",
                                     "Read Cell V",
                                     "CS RTC",
                                     "CS Memory",
                                     "P Fault",
                                    };

            formatUtils.displayBitMap(digitalOutput, "Digital Output", outputValues);
        }

        private void displayAlarms (UInt32 alarmStatus)
        {
            string[] alarmMsgs = {"S_CELL_OVP_WARNING",
                                  "S_CELL_OVP_ALARM",
                                  "S_CELL_UVP_WARNING",
                                  "S_CELL_UVP_ALARM",
                                  "Unused 4",
                                  "Unused 5",
                                  "S_CELL_OTP_ALARM",
                                  "S_CELL_OTP_WARNING",
                                  "S_BOARD_OTP_ALARM",
                                  "S_BOARD_OTP_WARNING",
                                  "S_MOD_V_MISMATCH",
                                  "S_MOD_UVP_WARNING",
                                  "S_MOD_UVP_ALARM",
                                  "Unused 13",
                                  "Unused 14",
                                  "Unused 15",
                                  "H_SYS_POWERED",
                                  "H_SYS_FAULT",
                                  "H_OVP_PRI",
                                  "H_OVP_SEC",
                                  "H_UVP_PRI",
                                  "H_UVP_SEC",
                                  "H_CRITICAL_FAULT",
                                  "H_QUAD_CNTRL_1",
                                  "H_QUAD_CNTRL_2",
                                  "H_QUAD_CNTRL_3",
                                  "H_QUAD_CNTRL_4",
                                  "H_TEMP_FAULT"
                                 };

            formatUtils.displayBitMap(alarmStatus, "Alarms", alarmMsgs);
           
        }

        
        private void formatEventTime(string recordDesc, byte[] recordBuffer)
        {
            const int EVENT_RECORD_RTC_SECOND_OFFSET = 26;
            const int EVENT_RECORD_RTC_MINUTE_OFFSET = 27;
            const int EVENT_RECORD_RTC_HOUR_OFFSET = 28;
            const int EVENT_RECORD_RTC_DAY_OFFSET = 29;
            const int EVENT_RECORD_RTC_MONTH_OFFSET = 30;
            const int EVENT_RECORD_RTC_YEAR_OFFSET = 31;

            byte rtcSecond;
            byte rtcMinute;
            byte rtcHour;
            byte rtcDay;
            byte rtcMonth;
            byte rtcYear;

            string rtcString;

            rtcSecond = recordBuffer[EVENT_RECORD_RTC_SECOND_OFFSET];
            rtcMinute = recordBuffer[EVENT_RECORD_RTC_MINUTE_OFFSET];
            rtcHour = recordBuffer[EVENT_RECORD_RTC_HOUR_OFFSET];
            rtcDay = recordBuffer[EVENT_RECORD_RTC_DAY_OFFSET];
            rtcMonth = recordBuffer[EVENT_RECORD_RTC_MONTH_OFFSET];
            rtcYear = recordBuffer[EVENT_RECORD_RTC_YEAR_OFFSET];

            UserIO.outputInfo(String.Format("{0}.", recordDesc));

            rtcString = formatUtils.formatBcdRTC(rtcSecond, rtcMinute, rtcHour, rtcDay, rtcMonth, rtcYear);

            UserIO.outputInfo(String.Format("RTC: {0}", rtcString));

            UserIO.outputNewLine(outDev.ALL, msgType.INFO);
        }

    } // End of class eventFormatMBB

}
 
                           
