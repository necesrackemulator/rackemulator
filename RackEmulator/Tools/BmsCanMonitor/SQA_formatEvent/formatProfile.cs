﻿//*************************************************************************
//
// Copyright © 2014 NEC Energy Solutions, Inc. All rights reserved.
//
// NEC Energy Solutions Proprietary Information.
// This document contains proprietary and confidential information and is
// tendered for information and evaluation purposes only; no copy or other
// use or dissemination by a third party can be made of this information
// without the express written permission of NEC Energy Solutions.
//
//*************************************************************************

using System;
using System.Linq;
using System.Text;
using SQA_utils;

namespace SQA_formatEvent
{ 
   
    public class formatBasicProfile
    {
        public byte[] savedProfileBuffer;

        public virtual void formatProfile(string profileMsg, byte[] readBuffer, byte[] lastProfileBuffer, dispCntl displayControl)
        {
        }

        public virtual string getProfileMsg()
        {
            return null;
        }

        public virtual int getProfileSize()
        {
            return Constant.ZERO;
        }

        /***********************************************************************************/
        /*                                                                                 */
        /* Function:  Generate the data log output for a system that uses Read Data By ID  */
        /*            to obtain the data.                                                  */
        /*                                                                                 */
        /* Arguments: profileTitle - title message associated with this data log profile.  */
        /*            profileMsg - the message appearing on each line of the profile.      */
        /*            readBuffer - the byte buffer of profile data most recently read.     */
        /*            prevBuffer - the byte buffer of profile data read previously.        */
        /*            displayControl - controls display attributes - ON, OFF, DIFF, ALL.   */
        /*            valueMsgs - the string array of messages describing profile values.  */
        /*                                                                                 */
        /* Returns:   Void.                                                                */
        /*                                                                                 */
        /* Caveats:   None.                                                                */
        /*                                                                                 */
        /***********************************************************************************/
        public void outputLogDataFormatArray(string profileTitle, string profileMsg, byte[] readBuffer, byte[] prevBuffer, dispCntl displayControl, string[] valueMsgs)
        {
            // Determine the output format.
            switch (AppUtils.displayFormat)
            {
                case AppUtils.DISPLAY_FORMAT_ENGLISH:
                    {
                        formatProfileArray(profileTitle, profileMsg, readBuffer, prevBuffer, displayControl, valueMsgs);
                    }
                    break;
                case AppUtils.DISPLAY_FORMAT_HEX:
                    {
                        Utils.outputBuffer(profileTitle, readBuffer, readBuffer.Length);
                    }
                    break;
                case AppUtils.DISPLAY_FORMAT_NONE:
                default:
                    break;
            }
        }

        /***********************************************************************************/
        /*                                                                                 */
        /* Function:  Display an array of profile times with descriptive messages for each */
        /*            array value.                                                         */
        /*                                                                                 */
        /* Arguments: profileHdr - the profile header message.                             */
        /*            profileMsg - a message describing the type of profile, i.e., voltage.*/
        /*            readBuffer - a byte buffer containing the read profile data.         */
        /*            lastBuffer - a byte buffer containing the previous read of the       */
        /*                         profile data.                                           */
        /*            displayControl - determines if the user requested to display ALL     */
        /*                             data, the data which is ON or OFF, or the           */
        /*                             difference between current and last read data.      */
        /*            maxFields - the maximum number of profile fields to display.         */
        /*            descriptiveMsg - a string array of descriptive messages.             */
        /*                                                                                 */
        /* Returns:   Void.                                                                */
        /*                                                                                 */
        /* Caveats:  None.                                                                 */
        /*                                                                                 */
        /***********************************************************************************/

        private void formatProfileArray(string profileTitle, string profileMsg, byte[] readBuffer, byte[] lastBuffer, dispCntl displayControl, string[] descriptiveMsg)
        {

            int msgIndex;
            UInt32 profileTime;

            displayProfileTitle(profileTitle, profileMsg.Length + descriptiveMsg[Constant.ZERO].Length);

            for (msgIndex = Constant.ZERO; msgIndex < descriptiveMsg.Length; msgIndex++)
            {
                profileTime = formatDataLogUtils.getProfileTime(readBuffer, msgIndex, Constant.ZERO);

                if (displayControl == dispCntl.DISPLAY_DIFF)
                {
                    profileTime = profileTime - formatDataLogUtils.getProfileTime(lastBuffer, msgIndex, Constant.ZERO);
                }

                // Check if we are to display the data
                if (displayControlProfileData(displayControl, profileTime))
                {
                    formatDataLogUtils.displayElapsedTime(profileMsg, descriptiveMsg[msgIndex], profileTime);
                }
            }
            UserIO.outputNewLine(outDev.ALL, msgType.INFO);
        }

     
        /***********************************************************************************/
        /*                                                                                 */
        /* Function:  Display the header of a profile block. The header is formatted with  */
        /*            a header for days.hh.mm.ss.                                          */
        /*                                                                                 */
        /* Arguments: hdrMsg - the message displayed in the header.                        */
        /*            msgLength - the length of the descriptive messages to follow.        */
        /*                                                                                 */
        /* Returns:   Void.                                                                */
        /*                                                                                 */
        /* Caveats:  None.                                                                 */
        /*                                                                                 */
        /***********************************************************************************/
        private void displayProfileTitle(string hdrMsg, int msgLength)
        {
            string dateStr = "days.hh.mm.ss";
            string padStr = null;
            const int DEFAULT_OFFSET = Constant.FIVE;

            if ((msgLength - hdrMsg.Length + DEFAULT_OFFSET) > Constant.ZERO)
            {
                padStr = " ".PadLeft(msgLength - hdrMsg.Length + DEFAULT_OFFSET);
            }
            else
            {
                padStr = " ";
            }

            UserIO.outputInfo(String.Format("\n{0}{1}{2}", hdrMsg, padStr, dateStr));
        }

        /***********************************************************************************/
        /*                                                                                 */
        /* Function:  Display a message containing an event message and a formated event   */
        /*            event time based upon calender time.                                 */
        /*                                                                                 */
        /* Arguments: eventMsg - the event message.                                        */
        /*            eventValueMsg - the type of event.                                   */
        /*            secondCnt - the number of seconds elapsed for this event.            */
        /*                                                                                 */
        /* Returns:   Void.                                                                */
        /*                                                                                 */
        /* Caveats:  None.                                                                 */
        /*                                                                                 */
        /***********************************************************************************/
        public void displayElapsedTime(string eventMsg, string eventValueMsg, UInt32 secondsCnt)
        {
            UserIO.outputInfo(String.Format("{0}: {1}   {2}", eventMsg, eventValueMsg, convertElapsedTime(secondsCnt)));
        }

        /***********************************************************************************/
        /*                                                                                 */
        /* Function:  Return a calendar date string.                                       */
        /*                                                                                 */
        /* Arguments: secondCnt - the number of seconds to be converted.                   */
        /*                                                                                 */
        /* Returns:   A string containing elaped time in the format of                     */
        /*            "xx days.hh:mm:ss".                                                  */
        /*                                                                                 */
        /* Caveats:  None.                                                                 */
        /*                                                                                 */
        /***********************************************************************************/
        private string convertElapsedTime(UInt32 secondsCnt)
        {
            UInt32 days;
            UInt32 hours;
            UInt32 minutes;
            UInt32 remainingSeconds;

            days = secondsCnt / Constant.SECONDS_IN_DAY;
            remainingSeconds = secondsCnt - (days * Constant.SECONDS_IN_DAY);
            hours = remainingSeconds / Constant.SECONDS_IN_HOUR;
            remainingSeconds = remainingSeconds - (hours * Constant.SECONDS_IN_HOUR);
            minutes = remainingSeconds / Constant.SECONDS_IN_MINUTE;
            remainingSeconds = remainingSeconds - (minutes * Constant.SECONDS_IN_MINUTE);

            return (string.Format("{0:0000}.{1:00}:{2:00}:{3:00}", days, hours, minutes, remainingSeconds));
        }

        /***********************************************************************************/
        /*                                                                                 */
        /* Function:  Determine if a display should be generated based upon the            */
        /*            profile time and displayControl; displayControl indicates if the     */
        /*            user request to display ALL data, On data, OFF data, or only data    */
        /*            which differs.                                                       */
        /*                                                                                 */
        /* Arguments: displayControl - determines if the user requested to display ALL     */
        /*                             data, the data which is ON or OFF, or the           */
        /*                             difference between current and last read data.      */
        /*            profileTime - the profile time value which we consider displaying.   */
        /*                                                                                 */
        /* Returns:   TRUE - if a display is required.                                     */
        /*            FALSE - if no display is required.                                   */
        /*                                                                                 */
        /* Caveats:  None.                                                                 */
        /*                                                                                 */
        /***********************************************************************************/
        public bool displayControlProfileData(dispCntl displayControl, UInt32 profileTime)
        {
            return ((displayControl == dispCntl.DISPLAY_ALL) ||
                    ((displayControl == dispCntl.DISPLAY_DIFF) && (profileTime > Constant.ZERO)) ||
                    ((displayControl == dispCntl.DISPLAY_ON) && (profileTime > Constant.ZERO)) ||
                    ((displayControl == dispCntl.DISPLAY_OFF) && (profileTime == Constant.ZERO)));
        }

        public void saveProfileData(byte[] srcArray)
        {
            int loopIndex;
            int profileSize = srcArray.Length;

            // Perform array copy for the data for a given MBB.
            for (loopIndex = Constant.ZERO; loopIndex < profileSize; loopIndex++)
            {
                // Save the last read data so that it can be used in future command invocations.
                savedProfileBuffer[loopIndex] = srcArray[loopIndex];
            }
        }

        public void extractProfileData(byte[] destArray)
        {
            int loopIndex;
            int profileSize = destArray.Length;

            // Perform array copy for the data for a given MBB.
            for (loopIndex = Constant.ZERO; loopIndex < profileSize; loopIndex++)
            {
                // Extract the last read data so that it can be compared (diff'ed) later.
                destArray[loopIndex] = savedProfileBuffer[loopIndex];
            }
        }

        public void formatFields16(string profileName, byte[] readBuffer, byte[] lastBuffer, dispCntl displayControl, int maxFields, int bufferOffset, string[] descriptiveMsg)
        {
            int msgIndex;
            UInt16 readData16;
            UInt16 lastData16;

            for (msgIndex = Constant.ZERO; msgIndex < maxFields; msgIndex++)
            {
                readData16 = Utils.extractUInt16(readBuffer, (msgIndex * Constant.TWO) + bufferOffset);
                if (displayControl == dispCntl.DISPLAY_DIFF)
                {
                    lastData16 = Utils.extractUInt16(lastBuffer, (msgIndex * Constant.TWO) + bufferOffset);
                    readData16 = (UInt16)(readData16 - lastData16);
                }

                if (displayControlProfileData(displayControl, (UInt32)readData16))
                {
                    UserIO.outputInfo(String.Format("{0}: {1} - {2}.", profileName, descriptiveMsg[msgIndex], readData16));
                }
            }
        }

        public void formatFields32(string profileName, byte[] readBuffer, byte[] lastBuffer, dispCntl displayControl, int maxFields, int bufferOffset, string[] descriptiveMsg)
        {

            int msgIndex;
            UInt32 readData32;
            UInt32 lastData32;

            for (msgIndex = Constant.ZERO; msgIndex < maxFields; msgIndex++)
            {
                readData32 = Utils.extractUInt32(readBuffer, (msgIndex * Constant.FOUR) + bufferOffset);
                if (displayControl == dispCntl.DISPLAY_DIFF)
                {
                    lastData32 = Utils.extractUInt32(lastBuffer, (msgIndex * Constant.FOUR) + bufferOffset);
                    readData32 = readData32 - lastData32;
                }
                if (displayControlProfileData(displayControl, readData32))
                {
                    UserIO.outputInfo(String.Format("{0}: {1} - {2}.", profileName, descriptiveMsg[msgIndex], readData32));
                }
            }
        }

    }  // End of class formatBasicProfile

    public class formatModuleProfile : formatBasicProfile
    {
        public byte[,] savedProfileModuleBuffer;

        public virtual void formatProfile(string profileMsg, byte[] readBuffer, byte[] lastProfileBuffer, int moduleNumber, dispCntl displayControl)
        {
        }

        public void saveModuleProfileData(int moduleNumber, byte[] srcArray)
        {
            int loopIndex;
            int profileSize = srcArray.Length;

            // Perform array copy for the data for a given MBB.
            for (loopIndex = Constant.ZERO; loopIndex < profileSize; loopIndex++)
            {
                // Save the last read data so that it can be used in future command invocations.
                savedProfileModuleBuffer [moduleNumber - Constant.ONE, loopIndex] = srcArray[loopIndex];
            }
        }

        public void extractModuleProfileData(int moduleNumber, byte[] destArray)
        {
            int loopIndex;
            int profileSize = destArray.Length;

            // Perform array copy for the data for a given MBB.
            for (loopIndex = Constant.ZERO; loopIndex < profileSize; loopIndex++)
            {
                // Extract the last read data so that it can be compared (diff'ed) later.
                destArray[loopIndex] = savedProfileModuleBuffer[moduleNumber - Constant.ONE, loopIndex];
            }
        }

    }  // End of class formatModuleProfile

    public class formatModuleCellProfile : formatBasicProfile
    {
        public byte[,,] savedProfileModuleCellBuffer;

        public virtual void formatProfile(string profileMsg, byte[] readBuffer, byte[] lastProfileBuffer, int moduleNumber, dispCntl displayControl)
        {
        }

        public void saveCellProfileData(int moduleNumber, int cellNumber, byte[] srcArray)
        {
            int loopIndex;
            int profileSize = srcArray.Length;

            // Perform array copy for the data for a given MBB.
            for (loopIndex = Constant.ZERO; loopIndex < profileSize; loopIndex++)
            {
                // Save the last read data so that it can be used in future command invocations.
                savedProfileModuleCellBuffer[moduleNumber - Constant.ONE, cellNumber - Constant.ONE, loopIndex] = srcArray[loopIndex];
            }
        }

        public void extractCellProfileData(int moduleNumber, int cellNumber, byte[] destArray)
        {
            int loopIndex;
            int profileSize = destArray.Length;

            // Perform array copy for the data for a given MBB.
            for (loopIndex = Constant.ZERO; loopIndex < profileSize; loopIndex++)
            {
                // Extract the last read data so that it can be compared (diff'ed) later.
                destArray[loopIndex] = savedProfileModuleCellBuffer[moduleNumber - Constant.ONE, cellNumber - Constant.ONE, loopIndex];
            }
        }
    }  // End of class formatModuleCellProfile

}
 

 
                           
