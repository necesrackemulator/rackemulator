﻿//*************************************************************************
//
// Copyright © 2014 NEC Energy Solutions, Inc. All rights reserved.
//
// NEC Energy Solutions Proprietary Information.
// This document contains proprietary and confidential information and is
// tendered for information and evaluation purposes only; no copy or other
// use or dissemination by a third party can be made of this information
// without the express written permission of NEC Energy Solutions.
//
//*************************************************************************

using System;
using System.Linq;
using System.Text;
using System.Globalization;
using SQA_utils;

namespace SQA_formatEvent
{
    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This file contains the classes for formatting the display of          */
    /*           MBB data logs associated with the Sunrise MBB.                        */
    /*                                                                                 */
    /***********************************************************************************/

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the module Temp Charging profile.                                     */
    /*                                                                                 */
    /***********************************************************************************/

    public class sunriseModuleTempCharging : sunriseModuleTemp
    {
        public sunriseModuleTempCharging(int maxModule)
        {
            savedProfileModuleBuffer = new byte[maxModule, getProfileSize()];
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "Cell Temp Charging";
            return profileMsg;
        }

    } // End of class sunriseModuleTempCharging

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the module Temp Charging profile.                                     */
    /*                                                                                 */
    /***********************************************************************************/

    public class sunriseModuleTempDischarging : sunriseModuleTemp
    {

        public sunriseModuleTempDischarging(int maxModule)
        {
            savedProfileModuleBuffer = new byte[maxModule, getProfileSize()];
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "Cell Temp Discharging";
            return profileMsg;
        }

    } // End of class sunriseModuleTempDischarging

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the module Temp Charging profile.                                     */
    /*                                                                                 */
    /***********************************************************************************/

    public class sunriseModuleTempIdle : sunriseModuleTemp
    {
  
        public sunriseModuleTempIdle (int maxModule)
        {
            savedProfileModuleBuffer = new byte[maxModule, getProfileSize()];
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "Cell Temp Idle";
            return profileMsg;
        }

    } // End of class sunriseModuleTempIdle

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the module Watt-Hour Charging profile.                                */
    /*                                                                                 */
    /***********************************************************************************/

    public class sunriseModuleWattHourCharging : sunriseModuleTemp
    {
        public sunriseModuleWattHourCharging (int maxModule)
        {
            savedProfileModuleBuffer = new byte[maxModule, getProfileSize()];
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "Watt-Hour Charging";
            return profileMsg;
        }

    } // End of class sunriseModuleWattHourCharging

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the module Watt-Hour Discharging profile.                             */
    /*                                                                                 */
    /***********************************************************************************/

    public class sunriseModuleWattHourDischarging : sunriseModuleTemp
    {
        public sunriseModuleWattHourDischarging (int maxModule)
        {
            savedProfileModuleBuffer = new byte[maxModule, getProfileSize()];
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "Watt-Hour Discharging";
            return profileMsg;
        }

    } // End of class sunriseModuleWattHourDischarging

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the module Temp profiles.                                             */
    /*                                                                                 */
    /***********************************************************************************/

    public class sunriseModuleTemp : formatModuleProfile
    {
        public override int getProfileSize()
        {
            const int TEMP_PROFILE_SIZE = 72;
            return TEMP_PROFILE_SIZE;
        }

        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, int moduleNumber, dispCntl displayControl)
        {
            string[] profileValues = new string[]
                                               {"       X < -40",
                                                "-40 <= X < -30",
                                                "-30 <= X < -20",
                                                "-20 <= X < -10",
                                                "-10 <= X <   0",
                                                "  0 <= X <  10",
                                                " 10 <= X <  20",
                                                " 20 <= X <  30",
                                                " 30 <= X <  35",
                                                " 35 <= X <  40",
                                                " 40 <= X <  45",
                                                " 45 <= X <  50",
                                                " 50 <= X <  55",
                                                " 55 <= X <  60",
                                                " 60 <= X <  65",
                                                " 65 <= X <  70",
                                                " 70 <= X <  75",
                                                " 75 <= X      "
                                               };

            outputLogDataFormatArray(profileTitle, getProfileMsg(), readBuffer, lastProfileBuffer, displayControl, profileValues);
        }

    } // End of class sunriseModuleTemp

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the module Total Watt-Hours Charged profile.                          */
    /*                                                                                 */
    /***********************************************************************************/

    public class sunriseModuleTotalWattHourCharged : sunriseModuleTotalWattHour
    {

        public sunriseModuleTotalWattHourCharged(int maxModule)
        {
            savedProfileModuleBuffer = new byte[maxModule, getProfileSize()];
        }

        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, int moduleNumber, dispCntl displayControl)
        {
            string[] profileValues = new string[] { "Charged   " };

            outputLogDataFormatArray(profileTitle, getProfileMsg(), readBuffer, lastProfileBuffer, displayControl, profileValues);
        }

    } // End of class sunriseModuleTotalWattHourCharged

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the module Total Watt-Hours Discharged profile.                       */
    /*                                                                                 */
    /***********************************************************************************/

    public class sunriseModuleTotalWattHourDischarged : sunriseModuleTotalWattHour
    {

        public sunriseModuleTotalWattHourDischarged(int maxModule)
        {
            savedProfileModuleBuffer = new byte[maxModule, getProfileSize()];
        }

        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, int moduleNumber, dispCntl displayControl)
        {
            string[] profileValues = new string[] { "Discharged" };

            outputLogDataFormatArray(profileTitle, getProfileMsg(), readBuffer, lastProfileBuffer, displayControl, profileValues);
        }

    } // End of class sunriseModuleTotalWattHourDischarged

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           of the module Total Wat Hour Charged/Discharded profiles.             */
    /*                                                                                 */
    /***********************************************************************************/

    public class sunriseModuleTotalWattHour : formatModuleProfile
    {

        public override int getProfileSize()
        {
            const int PROFILE_TOTAL_WATT_HOURS_SIZE = 4;
            return PROFILE_TOTAL_WATT_HOURS_SIZE;
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "Total Watt-Hours";
            return profileMsg;
        }

    } // End of class sunriseModuleTotalWattHour

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the module voltage profile.                                           */
    /*                                                                                 */
    /***********************************************************************************/

    public class sunriseModuleVoltage : formatModuleProfile
    {
        public sunriseModuleVoltage(int maxModule)
        {
            savedProfileModuleBuffer = new byte[maxModule, getProfileSize()];
        }

        public override int getProfileSize()
        {
            const int MODULE_VOLTAGE_SIZE = 32;
            return MODULE_VOLTAGE_SIZE;
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "Module Voltage";
            return profileMsg;
        }

        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, int moduleNumber, dispCntl displayControl)
        {
            string[] profileValues = new string[] 
                                     {"       X <=  8.0",
                                      " 8.0 < X <= 16.0",
                                      "16.0 < X <= 51.2",
                                      "51.2 < X <= 54.4",
                                      "54.4 < X <= 57.6",
                                      "57.6 < X <= 60.8",
                                      "60.8 < X <= 68.8",
                                      "68.8 < X        "
                                     };


            outputLogDataFormatArray(profileTitle, getProfileMsg(), readBuffer, lastProfileBuffer, displayControl, profileValues);
        }

    } // End of class sunriseModuleVoltage

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the module current profile.                                           */
    /*                                                                                 */
    /***********************************************************************************/

    public class sunriseModuleCurrent : formatModuleProfile
    {

        public sunriseModuleCurrent(int maxModule)
        {
            savedProfileModuleBuffer = new byte[maxModule, getProfileSize()];
        }

        public override int getProfileSize()
        {
                   const int MODULE_CURRENT_SIZE = 92;
            return MODULE_CURRENT_SIZE;
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "Module Current";
            return profileMsg;
        }

        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, int moduleNumber, dispCntl displayControl)
        {
            string[] profileValues = new string[]     
                                       {"       X <= -100",
                                        "-100 < X <= -90 ",
                                        " -90 < X <= -80 ",
                                        " -80 < X <= -70 ",
                                        " -70 < X <= -60 ",
                                        " -60 < X <= -50 ",
                                        " -50 < X <= -40 ",
                                        " -40 < X <= -30 ",
                                        " -30 < X <= -20 ",
                                        " -20 < X <= -10 ",
                                        " -10 < X <  0   ",
                                        "       X =  0   ",
                                        "   0 < X <= 10  ",
                                        "  10 < X <= 20  ",
                                        "  20 < X <= 30  ",
                                        "  40 < X <= 50  ",
                                        "  50 < X <= 60  ",
                                        "  60 < X <= 70  ",
                                        "  70 < X <= 80  ",
                                        "  80 < X <= 90  ",
                                        "  90 < X <= 100 ",
                                        " 100 < X        "
                                       };

            outputLogDataFormatArray(profileTitle, getProfileMsg(), readBuffer, lastProfileBuffer, displayControl, profileValues);
        }

    } // End of class sunriseModuleCurrent

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the module SOC profile.                                               */
    /*                                                                                 */
    /***********************************************************************************/

    public class sunriseModuleSoc : formatModuleProfile
    {
        public sunriseModuleSoc(int maxModule)
        {
            savedProfileModuleBuffer = new byte[maxModule, getProfileSize()];
        }

        public override int getProfileSize()
        {
            const int PROFILE_SOC_SIZE = 40;
            return PROFILE_SOC_SIZE;
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "Module SOC";
            return profileMsg;
        }

        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, int moduleNumber, dispCntl displayControl)
        {
            string[] profileValues = new string[]     
                                               {" 0 <= X < 10",
                                                "10 <= X < 20",
                                                "20 <= X < 30",
                                                "30 <= X < 40",
                                                "40 <= X < 50",
                                                "50 <= X < 60",
                                                "60 <= X < 70",
                                                "70 <= X < 80",
                                                "80 <= X < 90",
                                                "90 <= X <= 100"
                                               };     

            outputLogDataFormatArray(profileTitle, getProfileMsg(), readBuffer, lastProfileBuffer, displayControl, profileValues);
        }

    } // End of class sunriseModuleSoc

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the module Alarm profile.                                             */
    /*                                                                                 */
    /***********************************************************************************/

    
    public class sunriseModuleAlarms : formatModuleProfile
    {
        public sunriseModuleAlarms(int maxModule)
        {
            savedProfileModuleBuffer = new byte[maxModule, getProfileSize()];
        }

        public override int getProfileSize()
        {
            const int ALARMS_PROFILE_SIZE = 68;
            return ALARMS_PROFILE_SIZE;
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "Module Alarms";
            return profileMsg;
        }

        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, int moduleNumber, dispCntl displayControl)
        {
            string[] profileValues = new string[]     
                                               {"CELL OVP        ",
                                                "CELL UVP        ",
                                                "MODULE UVP      ",
                                                "CELL OTP        ",
                                                "BOARD OTP       ",
                                                "H SYS POWERED   ",
                                                "H SYS FAULT     ",
                                                "H OVP PRI       ",
                                                "H OVP SEC       ",
                                                "H UVP PRI       ",
                                                "H UVP SEC       ",
                                                "H CRITICAL FAULT",
                                                "H QUAD CNTRL 1  ",
                                                "H QUAD CNTRL 2  ",
                                                "H QUAD CNTRL 3  ",
                                                "H QUAD CNTRL 4  ",
                                                "H TEMP FAULT    "
                                                };


            outputLogDataFormatArray(profileTitle, getProfileMsg(), readBuffer, lastProfileBuffer, displayControl, profileValues);
        }

    } // End of class sunriseModuleAlarms

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the module Cell Voltage profile.                                      */
    /*                                                                                 */
    /***********************************************************************************/

    public class sunriseModuleCellVoltage : formatModuleCellProfile
    { 

        public sunriseModuleCellVoltage(int maxModule, int maxCell)
        {
            savedProfileModuleCellBuffer = new byte[maxModule, maxCell, getProfileSize()];
        }

        public override int getProfileSize()
        {
            const int PROFILE_MBB_VOLTAGE_CELL_SIZE = 0x0020;
            return PROFILE_MBB_VOLTAGE_CELL_SIZE;
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "Cell Voltage";
            return profileMsg;
        }
         
        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, int moduleNumber, dispCntl displayControl)
        {
            string[] profileValues = new string[]     
                                              {"      X <= 0.5",
                                               "0.5 < X <= 2.0",
                                               "2.0 < X <= 3.2",
                                               "3.2 < X <= 3.4",
                                               "3.4 < X <= 3.6",
                                               "3.6 < X <= 3.8",
                                               "3.8 < X <= 4.3",
                                               "4.3 < X       "
                                              };

            
            outputLogDataFormatArray(profileTitle, getProfileMsg(), readBuffer, lastProfileBuffer, displayControl, profileValues);
        }

    } // End of class sunriseModuleCellVoltage

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the module cell Balancer Off profile.                                 */
    /*                                                                                 */
    /***********************************************************************************/

    public class sunriseBalancerOff : sunriseBalancer
    {
        public sunriseBalancerOff (int maxModule, int maxCell)
        {
            savedProfileModuleCellBuffer = new byte[maxModule, maxCell, getProfileSize()];
        }

        public override void formatProfile(string profileMsg, byte[] readBuffer, byte[] lastProfileBuffer, int moduleNumber, dispCntl displayControl)
        {
            string[] profileValues = new string[] { "    OFF    " };
            
            outputLogDataFormatArray(profileMsg, getProfileMsg(), readBuffer, lastProfileBuffer, displayControl, profileValues);
        }

    } // End of class sunriseBalancerOff

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the module cell Balancer On profile.                                  */
    /*                                                                                 */
    /***********************************************************************************/

    public class sunriseBalancerOn : sunriseBalancer
    {
        public sunriseBalancerOn (int maxModule, int maxCell)
        {
            savedProfileModuleCellBuffer = new byte[maxModule, maxCell, getProfileSize()];
        }

        public override void formatProfile(string profileMsg, byte[] readBuffer, byte[] lastProfileBuffer, int moduleNumber, dispCntl displayControl)
        {
            string[] profileValues = new string[] { "    ON     " };
            outputLogDataFormatArray(profileMsg, getProfileMsg(), readBuffer, lastProfileBuffer, displayControl, profileValues);
        }

    } // End of class sunriseBalancerOn

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the common functions for formatting the display   */
    /*           of the module cell Balancer On/Off profiles.                          */
    /*                                                                                 */
    /***********************************************************************************/
    public class sunriseBalancer : formatModuleCellProfile
    {

       
        public override string getProfileMsg()
        {
            const string profileMsg = "Balancer";
            return profileMsg;
        }

        public override int getProfileSize()
        {
            const int BALANCER_PROFILE_SIZE = 4;
            return BALANCER_PROFILE_SIZE;
        }

    } // End of class sunriseBalancer

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the module Revision profile.                                          */
    /*                                                                                 */
    /***********************************************************************************/

    public class sunriseModuleRev : formatModuleProfile
    {

        public sunriseModuleRev(int maxModule)
        {
            savedProfileModuleBuffer = new byte[maxModule, getProfileSize()];
        }

        public override int getProfileSize()
        {
            const int PROFILE_REV_SIZE = 2;
            return PROFILE_REV_SIZE;
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "Profile Rev";
            return profileMsg;
        }

        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, int moduleNumber, dispCntl displayControl)
        {                
            UInt16 actProfileRevision;

            actProfileRevision = Utils.extractLittleEndian16(readBuffer, Constant.OFFSET_0);

            UserIO.outputInfo(String.Format("{0}: {1}.", getProfileMsg (), actProfileRevision));
            UserIO.outputNewLine(outDev.ALL, msgType.INFO);
        }

    } // End of class sunriseModuleRev

}
 
                           
