﻿//*************************************************************************
//
// Copyright © 2014 NEC Energy Solutions, Inc. All rights reserved.
//
// NEC Energy Solutions Proprietary Information.
// This document contains proprietary and confidential information and is
// tendered for information and evaluation purposes only; no copy or other
// use or dissemination by a third party can be made of this information
// without the express written permission of NEC Energy Solutions.
//
//*************************************************************************

using System;
using System.Linq;
using System.Text;
using System.Globalization;
using SQA_format;
using SQA_utils;

namespace SQA_formatEvent
{
    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This file contains the classes for formatting the display of          */
    /*           MBB data logs associated with the Roddenberry MBB.                    */
    /*                                                                                 */
    /***********************************************************************************/

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the module Temp1 profile.                                             */
    /*                                                                                 */
    /***********************************************************************************/

    public class roddenberryModuleTemp1 : roddenberryModuleTemp
    {
        public roddenberryModuleTemp1 (int maxModule)
        {
            savedProfileModuleBuffer = new byte[maxModule, getProfileSize()];
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "Temp 1";
            return profileMsg;
        }
    } // End of class roddenberryModuleTemp1


    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the module Temp2 profile.                                             */
    /*                                                                                 */
    /***********************************************************************************/

    public class roddenberryModuleTemp2 : roddenberryModuleTemp
    {
        public roddenberryModuleTemp2(int maxModule)
        {
            savedProfileModuleBuffer = new byte[maxModule, getProfileSize()];
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "Temp 2";
            return profileMsg;
        }
    } // End of class roddenberryModuleTemp2


    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the module Temp3 profile.                                             */
    /*                                                                                 */
    /***********************************************************************************/

    public class roddenberryModuleTemp3 : roddenberryModuleTemp
    {
        public roddenberryModuleTemp3(int maxModule)
        {
            savedProfileModuleBuffer = new byte[maxModule, getProfileSize()];
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "Temp 3";
            return profileMsg;
        }
    } // End of class roddenberryModuleTemp3


    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the module Temp4 profile.                                             */
    /*                                                                                 */
    /***********************************************************************************/

    public class roddenberryModuleTemp4 : roddenberryModuleTemp
    {
        public roddenberryModuleTemp4(int maxModule)
        {
            savedProfileModuleBuffer = new byte[maxModule, getProfileSize()];
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "Temp 4";
            return profileMsg;
        }
    } // End of class roddenberryModuleTemp4

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the module Temp profiles.                                             */
    /*                                                                                 */
    /***********************************************************************************/
    public class roddenberryModuleTemp : formatModuleProfile
    {
        public override int getProfileSize()
        {
            const int TEMP_PROFILE_SIZE = 0x0034;
            return TEMP_PROFILE_SIZE;
        }

        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, int moduleNumber, dispCntl displayControl)
        {
            string[] profileValues = new string[]                            
                             {"Error/No Value",
                              "      X <= -20",
                              "-20 < X <= -10",
                              "-10 < X <= 0  ",
                              "  0 < X <= 25 ",
                              " 25 < X <= 35 ",
                              " 35 < X <= 40 ",
                              " 40 < X <= 45 ",
                              " 45 < X <= 50 ",
                              " 50 < X <= 55 ",
                              " 55 < X <= 60 ",
                              " 60 < X       "
                             };

            outputLogDataFormatArray(profileTitle, getProfileMsg(), readBuffer, lastProfileBuffer, displayControl, profileValues);
        }

    } // End of class roddenberryModuleTemp

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the module voltage profile.                                           */
    /*                                                                                 */
    /***********************************************************************************/

    public class roddenberryModuleVoltage : formatModuleProfile
    {
        public roddenberryModuleVoltage(int maxModule)
        {
            savedProfileModuleBuffer = new byte[maxModule, getProfileSize()];
        }

        public override int getProfileSize()
        {
            const int MODULE_VOLTAGE_SIZE = 0x002C;
            return MODULE_VOLTAGE_SIZE;
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "Module Voltage";
            return profileMsg;
        }

        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, int moduleNumber, dispCntl displayControl)
        {
            const int MAX_FIELDS_SIZE_16 = 4;

            int copyIndex;

            byte[] voltBuffer32 = new byte[getProfileSize() + (Constant.TWO * MAX_FIELDS_SIZE_16)];
            byte[] voltBufferLast32 = new byte[getProfileSize() + (Constant.TWO * MAX_FIELDS_SIZE_16)];

            string[] profileValues = new string[] 
                                        {"Error/No Value  ",
                                         "       X <= 13  ",
                                         "  13 < X <= 19.5",
                                         "19.5 < X <= 26  ",
                                         "  26 < X <= 32.5",
                                         "32.5 < X <= 39  ",
                                         "  39 < X <= 41.6",
                                         "41.6 < X <= 44.2",
                                         "44.2 < X <= 46.8",
                                         "46.8 < X <= 49.4",
                                         "49.4 < X <= 52  ",
                                         "  52 < X        "
                                        };

            // Perform array copy to make everything 32 bits.

            // First fill destination arrays with zeroes.
            for (copyIndex = Constant.ZERO; copyIndex < voltBuffer32.Length; copyIndex++)
            {
                voltBuffer32[copyIndex] = Constant.ZERO;
                voltBufferLast32[copyIndex] = Constant.ZERO;
            }
            // Copy the first 32 bit fields
            for (copyIndex = Constant.ZERO; copyIndex < Constant.FOUR; copyIndex++)
            {
                voltBuffer32[copyIndex] = readBuffer[copyIndex];
                voltBufferLast32[copyIndex] = lastProfileBuffer[copyIndex];
            }

            // Copy the next two 16 bit fields
            voltBuffer32[6] = readBuffer[4];
            voltBuffer32[7] = readBuffer[5];
            voltBuffer32[10] = readBuffer[6];
            voltBuffer32[11] = readBuffer[7];
            voltBufferLast32[6] = lastProfileBuffer[4];
            voltBufferLast32[7] = lastProfileBuffer[5];
            voltBufferLast32[10] = lastProfileBuffer[6];
            voltBufferLast32[11] = lastProfileBuffer[7];

            // Copy the next seven 32 bit fields
            for (copyIndex = Constant.ZERO; copyIndex < Constant.FOUR * Constant.SEVEN; copyIndex++)
            {
                voltBuffer32[copyIndex + 12] = readBuffer[copyIndex + 8];
                voltBufferLast32[copyIndex + 12] = lastProfileBuffer[copyIndex + 8];
            }

            // Copy the last two 16 bit fields
            voltBuffer32[42] = readBuffer[36];
            voltBuffer32[43] = readBuffer[37];
            voltBuffer32[45] = readBuffer[38];
            voltBuffer32[46] = readBuffer[39];

            voltBufferLast32[42] = lastProfileBuffer[36];
            voltBufferLast32[43] = lastProfileBuffer[37];
            voltBufferLast32[45] = lastProfileBuffer[38];
            voltBufferLast32[46] = lastProfileBuffer[39];

            outputLogDataFormatArray(profileTitle, getProfileMsg(), voltBuffer32, voltBufferLast32, displayControl, profileValues);
        }

    } // End of class roddenberryModuleVoltage

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the module Balancer profile.                                          */
    /*                                                                                 */
    /***********************************************************************************/

    public class roddenberryModuleBalancer : formatModuleProfile
    {
        public roddenberryModuleBalancer(int maxModule)
        {
            savedProfileModuleBuffer = new byte[maxModule, getProfileSize()];
        }

        public override int getProfileSize()
        {
            const int MODULE_BALANCER_SIZE = 0x0038;
            return MODULE_BALANCER_SIZE;
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "Balancer";
            return profileMsg;
        }

        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, int moduleNumber, dispCntl displayControl)
        {

            string[] profileValues = new string[] 
                                               {"Cell 01 Balancer ON",
                                                "Cell 02 Balancer ON",
                                                "Cell 03 Balancer ON",
                                                "Cell 04 Balancer ON",
                                                "Cell 05 Balancer ON",
                                                "Cell 06 Balancer ON",
                                                "Cell 07 Balancer ON",
                                                "Cell 08 Balancer ON",
                                                "Cell 09 Balancer ON",
                                                "Cell 10 Balancer ON",
                                                "Cell 11 Balancer ON",
                                                "Cell 12 Balancer ON",
                                                "Cell 13 Balancer ON"
                                               };
  
            outputLogDataFormatArray(profileTitle, getProfileMsg(), readBuffer, lastProfileBuffer, displayControl, profileValues);
        }

    } // End of class roddenberryModuleBalancer


    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the module PCB Temperature Profile.                                   */
    /*                                                                                 */
    /***********************************************************************************/

    public class roddenberryModuleTempPCB : formatModuleProfile
    {
        public roddenberryModuleTempPCB(int maxModule)
        {
            savedProfileModuleBuffer = new byte[maxModule, getProfileSize()];
        }

        public override int getProfileSize()
        {
            const int MODULE_TEMP_PCB_SIZE = 0x0034;
            return MODULE_TEMP_PCB_SIZE;
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "PCB Temp";
            return profileMsg;
        }

        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, int moduleNumber, dispCntl displayControl)
        {

            string[] profileValues = new string[] 
                                               {"Error/No Value  ",
                                                "      X <= -20  ",
                                                "-20 < X <= -5   ",
                                                " -5 < X <= 10   ",
                                                " 10 < X <= 25   ",
                                                " 25 < X <= 40   ",
                                                " 40 < X <= 55   ",
                                                " 55 < X <= 70   ",
                                                " 70 < X <= 85   ",
                                                " 85 < X <= 100  ",
                                                "100 < X <= 115  ",
                                                "115 < X         "
                                               };

            outputLogDataFormatArray(profileTitle, getProfileMsg(), readBuffer, lastProfileBuffer, displayControl, profileValues);
        }

    } // End of class roddenberryModuleTempPCB


    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the module Data Profile.                                              */
    /*                                                                                 */
    /***********************************************************************************/
   
    public class roddenberryModuleData : formatModuleProfile
    {
        public roddenberryModuleData(int maxModule)
        {
            savedProfileModuleBuffer = new byte[maxModule, getProfileSize()];
        }

        public override int getProfileSize()
        {
            const int MODULE_DATA_SIZE = 0x0034;
            return MODULE_DATA_SIZE;
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "Module Data";
            return profileMsg;
        }

        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, int moduleNumber, dispCntl displayControl)
        {

            const int DISCHARGE_COLUMB_OFFSET = 4;
            const int CHARGE_COLUMB_OFFSET = 12;
            const int DISCHARGE_ENERGY_OFFSET = 20;
            const int CHARGE_ENERGY_OFFSET = 28;
            const int MODULE_ON_TIME_OFFSET = 36;

            UInt64 dischargeColumbs;
            UInt64 chargeColumbs;
            UInt64 dischargeEnergy;
            UInt64 chargeEnergy;
            UInt32 moduleOnTime;

            UserIO.outputInfo(profileTitle);

            dischargeColumbs = (UInt64)((readBuffer[DISCHARGE_COLUMB_OFFSET] << Constant.SHIFT_32) + readBuffer[DISCHARGE_COLUMB_OFFSET + Constant.OFFSET_4]);
            chargeColumbs = (UInt64)((readBuffer[CHARGE_COLUMB_OFFSET] << Constant.SHIFT_32) + readBuffer[CHARGE_COLUMB_OFFSET + Constant.OFFSET_4]);
            dischargeEnergy = (UInt64)((readBuffer[DISCHARGE_ENERGY_OFFSET] << Constant.SHIFT_32) + readBuffer[DISCHARGE_ENERGY_OFFSET + Constant.OFFSET_4]);
            chargeEnergy = (UInt64)((readBuffer[CHARGE_ENERGY_OFFSET] << Constant.SHIFT_32) + readBuffer[CHARGE_ENERGY_OFFSET + Constant.OFFSET_4]);

            moduleOnTime = formatDataLogUtils.getProfileTime(readBuffer, Constant.ZERO, MODULE_ON_TIME_OFFSET);

            UserIO.outputInfo(String.Format("Discharge Columbs: {0}.", dischargeColumbs));
            UserIO.outputInfo(String.Format("Current Columbs:   {0}.", chargeColumbs));
            UserIO.outputInfo(String.Format("Discharge Energy:  {0}.", dischargeEnergy));
            UserIO.outputInfo(String.Format("Current Energy:    {0}.", chargeEnergy));

            formatDataLogUtils.displayElapsedTime("Module On Time", "", moduleOnTime);
            UserIO.outputNewLine(outDev.ALL, msgType.INFO);

        }
     } // End of class roddenberryModuleData

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the module Data Profile.                                              */
    /*                                                                                 */
    /***********************************************************************************/
   
    public class roddenberryModuleMetaData : formatModuleProfile
    {
        public roddenberryModuleMetaData(int maxModule)
        {
            savedProfileModuleBuffer = new byte[maxModule, getProfileSize()];
        }

        public override int getProfileSize()
        {
            const int MODULE_METADATA_SIZE = 0x0018;
            return MODULE_METADATA_SIZE;
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "Module Meta Data";
            return profileMsg;
        }
        
        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, int moduleNumber, dispCntl displayControl)
        {
            UInt32 timeStamp;

            formatDataLogUtils.outputDataLogVersion(readBuffer, profileTitle);

            timeStamp = formatDataLogUtils.getProfileTime(readBuffer, Constant.ZERO, Constant.FOUR);

            UserIO.outputInfo(String.Format("{0} Time Stamp {1}.", profileTitle, formatUtils.convertCalEventTime(timeStamp)));

            formatDataLogUtils.displayPart(readBuffer, Constant.EIGHT, profileTitle);
            UserIO.outputNewLine(outDev.ALL, msgType.INFO);
        }

    } // End of class roddenberryModuleMetaData

    /***********************************************************************************/
    /*                                                                                 */
    /* Function: This class contains the functions for formatting the display of       */
    /*           the module Cell Voltage profile.                                      */
    /*                                                                                 */
    /***********************************************************************************/

    public class roddenberryModuleCellVoltage : formatModuleCellProfile
    { 

        public roddenberryModuleCellVoltage(int maxModule, int maxCell)
        {
            savedProfileModuleCellBuffer = new byte[maxModule, maxCell, getProfileSize()];
        }

        public override int getProfileSize()
        {
            const int PROFILE_MBB_VOLTAGE_CELL_SIZE = 0x0024;
            return PROFILE_MBB_VOLTAGE_CELL_SIZE;
        }

        public override string getProfileMsg()
        {
            const string profileMsg = "Cell Voltage";
            return profileMsg;
        }
         
        public override void formatProfile(string profileTitle, byte[] readBuffer, byte[] lastProfileBuffer, int moduleNumber, dispCntl displayControl)
        {
            string[] profileValues = new string[]  
                                              {"Error/No Value",
                                               "      X <= 1.0",
                                               "1.0 < X <= 2.0",
                                               "2.0 < X <= 2.4",
                                               "2.4 < X <= 3.7",
                                               "3.7 < X <= 3.8",
                                               "3.8 < X <= 4.0",
                                               "4.0 < X       "
                                              };
            
            outputLogDataFormatArray(profileTitle, getProfileMsg(), readBuffer, lastProfileBuffer, displayControl, profileValues);
        }

    } // End of class roddenberryModuleCellVoltage

}
 
                           
