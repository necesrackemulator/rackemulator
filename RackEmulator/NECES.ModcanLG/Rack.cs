﻿// (C) NECES All Rights Reserved.
// Rack.cs, authored by: Andrew Schade
// Created: 06-20-2016; Last Updated: 07-08-2016 @ 10:17 AM

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NECES.Utils;

namespace NECES.ModcanLG
{
    /// <summary>
    ///     This is the Datatype providing Rack Level information and behaviors. Provides access to module level information
    ///     with instances of the <see cref="Module" /> class.
    /// </summary>
    public class Rack
    {
        private int _numCells = 1;
        private int _numModules;
        private int _numTherms = 1;

        /// <summary>
        ///     Default temperature sensor value to allow for closed contactors: 25.0 degrees Celsius
        /// </summary>
        public double DefaultTemp { get; } = 25.0; // Degrees Celsius

        /// <summary>
        ///     Default voltage value in order to allow for closed contactors: 3.333 Volts
        /// </summary>
        public double DefaultVoltage { get; } = 3.333; // Volts

        /// <summary>
        ///     An indexer to access modules. This is just for convienence. One can still call the Rack.Modules[i] to access the
        ///     modules.
        ///     This indexer doesn't allow you to set the value of a Module, but has only a get accessor.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public Module this[int index] => Modules[index];

        /// <summary>
        ///     This indexer allows you to get a subset of the modules in the rack using the <see cref="IntegerRange" /> Datatype.
        /// </summary>
        /// <param name="range">A range with a beginning and end within the list of modules.</param>
        /// <returns>An array of <see cref="Module" />s specified by the range.</returns>
        public Module[] this[IntegerRange range]
        {
            get
            {
                var mods = new List<Module>();
                for (var i = range.Begin == -1 ? 0 : range.Begin-1; i < (range.Begin == -1 ? NumModules : range.End); i++)
                {
                    mods.Add(this[i]);
                }
                return mods.ToArray();
            }
        }

        /// <summary>
        ///     The Name of the rack under test. Set by the config file or by explicitly setting the system under test.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     The number of cells in each module in the rack. Reinitializes cell voltages when changed, but keeps the temperature
        ///     sensor data.
        /// </summary>
        public int NumCells
        {
            get { return _numCells; }
            set
            {
                if (_numCells == value) return;
                _numCells = value;
                for (var i = 0; i < Modules.Length; i++)
                {
                    Modules[i] = new Module(NumCells, DefaultVoltage, Modules[i].Therms)
                    {
                        Address = i
                    };
                }
            }
        }

        /// <summary>
        ///     The number of modules in the rack. Reinitializes the entire rack when changed: no information will be saved.
        /// </summary>
        public int NumModules
        {
            get { return _numModules; }
            set
            {
                if (_numModules == value) return;
                _numModules = value;
                Modules = new Module[_numModules];
                for (var i = 0; i < Modules.Length; i++)
                {
                    Modules[i] = new Module(NumCells, DefaultVoltage, NumTherms, DefaultTemp)
                    {
                        Address = i
                    };
                }
            }
        }

        /// <summary>
        ///     The number of temperature sensors per module. Reinitializes the temperature settings in the rack, but keeps the
        ///     cell voltages.
        /// </summary>
        public int NumTherms
        {
            get { return _numTherms; }
            set
            {
                if (_numTherms == value) return;

                _numTherms = value;
                for (var i = 0; i < Modules.Length; i++)
                {
                    Modules[i] = new Module(Modules[i].Cells, NumTherms, DefaultTemp) { Address = i};
                }
            }
        }

        /// <summary>
        ///     The list of modules in the rack.
        /// </summary>
        private Module[] Modules { get; set; }

        private bool _subModToggle = false;

    }
}