﻿// (C) NECES All Rights Reserved.
// Module.cs, authored by: Andrew Schade
// Created: 08-01-2016; Last Updated: 08-05-2016 @ 1:49 PM

#region Imports

using System;
using System.Collections.Generic;
using System.Linq;
using NECES.Utils;

#endregion

namespace NECES.ModcanLG
{
    public class Module
    {
        public enum SubModTypes
        {
            Cell,
            Therm
        }


        public Module(int numCells, double defaultVoltage, ModuleDataType[] therms)
        {
            SetCells(numCells, defaultVoltage);
            Therms = therms;
        }

        public Module(ModuleDataType[] cells, int numTherms, double defaultTemp)
        {
            Cells = cells;
            BalanceFlags = new bool[Cells.Length];
            SetTherms(numTherms, defaultTemp);
        }

        public Module(int numCells, double defaultVoltage, int numTherms, double defaultTemp)
        {
            SetCells(numCells, defaultVoltage);
            SetTherms(numTherms, defaultTemp);
        }

        public int Address { get; set; }
        public bool[] BalanceFlags { get; private set; }

        public ModuleDataType[] Cells { get; private set; }

        //ms

        public ModuleDataType[] this[IntegerRange range, SubModTypes type]
        {
            get
            {
                var list = new List<ModuleDataType>();
                for (int i = range.Begin; i < range.End; i++)
                {
                    list.Add(type == SubModTypes.Cell ? Cells[i] : Therms[i]);
                }
                return list.ToArray();
            }
        }

        public bool this[string range]
        {
            get { return this[range, 0]; }
            set
            {
                this[range, 0] = value;
                this[range, 1] = value;
            }
        }

        public bool this[string range, int i]
        {
            get { return SystemFlags[range]; }
            set { this[range] = value; }
        }

        public int Latency { get; set; } = 0;

        public ModuleDataType[] ModVolt
        {
            get
            {
                var values = new ModuleDataType[SubModules];
                for (int i = 0; i < SubModules; i++)
                {
                    values[i] = ModVoltRaw[i] ??
                                Cells.Skip(i*(Cells.Length/SubModules)).Take(Cells.Length/SubModules).Sum();
                }
                return values;
            }
        }

        public double?[] ModVoltRaw { get; } = new double?[2];

        public ModuleDataType Ref2V { get; set; }
        public int StmCount { get; set; }
        public int SubModules { get; set; }

        public Dictionary<string, bool> SystemFlags { get; set; }

        public ModuleDataType[] Therms { get; private set; }

        public bool UnderTest { get; set; }

        private void SetCells(int numCells, double defaultVoltage)
        {
            Cells = new ModuleDataType[numCells];
            BalanceFlags = new bool[numCells];
            for (int i = 0; i < Cells.Length; i++)
            {
                Cells[i] = defaultVoltage;
            }
        }

        private void SetTherms(int numTherms, double defaultTemp)
        {
            Therms = new ModuleDataType[numTherms];
            for (int i = 0; i < Therms.Length; i++)
            {
                Therms[i] = defaultTemp;
            }
        }
    }
}