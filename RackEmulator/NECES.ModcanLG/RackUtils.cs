﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NECES.Utils;

namespace NECES.ModcanLG
{
    public static class RackUtils
    {
        public static void Loop(this Rack rack, IntegerRange modules, Action<Module> action)
        {
            foreach (var module in rack[modules])
            {

                action(module);
            }
        }

        public static void Loop(this Module module, IntegerRange range, bool cellsOrTemps, Action<ModuleDataType> action)
        {
            if (cellsOrTemps)
            {
                // Cells
                for (int i = range.Begin == -1 ? 0:range.Begin-1; i < (range.Begin == -1? module.Cells.Length:range.End);i++)
                {
                    action(module.Cells[i]);
                }
            }
            else
            {
                // Temps
                for (int i = range.Begin == -1 ? 0 : range.Begin - 1; i < (range.Begin == -1 ? module.Therms.Length : range.End); i++)
                {
                    action(module.Therms[i]);
                }
            }
        }
    }
}
