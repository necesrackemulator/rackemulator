﻿using System;
using System.Collections.Generic;
using System.IO;
using SQA_utils;

namespace NECES.Utils
{
    public class Output
    {
        private readonly TextWriter _console = Console.Out;
        private TextWriter _log;
        public bool IsInteractive { get; } = true;
        public ConsolePrompt ConsolePrompt { get; set; }
        public bool ConsoleActive { get; set; }

        public string Logfile
        {
            set
            {
                Logging.userLogDir = Path.GetDirectoryName(Path.GetFullPath(value));
                Logging.userLogFileName = Path.GetFileName(value);
            }
        }

        public Output()
        {
            ConsolePrompt = new ConsolePrompt(this);
            Toggle(Display.Null, Channel.Console, true);
            Toggle(Display.Null, Channel.Log, false);
            Toggle(Display.Warning, Channel.Console, true);
            Toggle(Display.Warning, Channel.Log, false);
            Toggle(Display.Error, Channel.Console, true);
            Toggle(Display.Error, Channel.Log, false);
            Toggle(Display.Debug, Channel.Console, false);
            Toggle(Display.Debug, Channel.Log, false);
            UserIO.debugEnabled = true;
            UserIO.warningEnabled = true;
        }
        public enum Channel
        {
            Console = 1, Log = 2
        }

        public void PutLine(string output, Display flag = Display.Null)
        {
           if (CurrentDisplay(flag, Channel.Console))
           {
               PrintConsole(output, flag);
           }
           if( CurrentDisplay(flag, Channel.Log))
            {
               PrintLog(output,flag);
            }
        }

        private void PrintLog(string output, Display flag)
        {
//            Logging.openLogFileIfNeeded();
            switch (flag)
            {
                case Display.Warning:
                    UserIO.outputLine(outDev.LOG, msgType.WARNING, output);
                    break;
                case Display.Error:
                    UserIO.outputLine(outDev.LOG, msgType.ERROR, output);
                    break;
                case Display.Debug:
                    UserIO.outputLine(outDev.LOG, msgType.ERROR, output);
                    break;
                case Display.Null:
                    UserIO.outputLine(outDev.LOG,msgType.INFO, output);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(flag), flag, null);
            }
//            Logging.closeLogFileIfNeeded();
        }

        private void PrintConsole(string output, Display flag)
        {
            switch (flag)
            {
                case Display.Warning:
                    UserIO.outputLine(outDev.CON, msgType.WARNING, output);
                    break;
                case Display.Error:
                    UserIO.outputLine(outDev.CON, msgType.ERROR, output);
                    break;
                case Display.Debug:
                    UserIO.outputLine(outDev.CON, msgType.DEBUG, output);
                    break;
                case Display.Null:
                    UserIO.outputLine(outDev.CON, msgType.INFO, output);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(flag), flag, null);
            }
        }

        public enum Display
        {
            Warning = 1,
            Error = 2,
            Debug = 4,
            Hex = 8,
            Fatal = 16,
            Null = 0
        }

        private Dictionary<Tuple<Channel, Display>, bool> _displayFlags = new Dictionary<Tuple<Channel, Display>, bool>();

        public void Toggle(Display flag, Channel channel, bool? toggle)
        {
            if (_displayFlags.ContainsKey(new Tuple<Channel, Display>(channel, flag)))
            {
                _displayFlags[new Tuple<Channel, Display>(channel, flag)] = toggle ?? !_displayFlags[new Tuple<Channel, Display>(channel, flag)];
            }
            else
            {
                _displayFlags.Add(new Tuple<Channel, Display>(channel, flag), toggle ?? false);
            }
        }

        public bool CurrentDisplay(Display flag, Channel channel)
        {
            return _displayFlags[new Tuple<Channel, Display>(channel, flag)];
        }

        public void Put(string output, Display flag = Display.Null)
        {
            _console.Write(output);
        }

        public void PutPrompt(string toString)
        {
            UserIO.curPrompt = toString;
            UserIO.outputPrompt(toString);
        }
    }
}