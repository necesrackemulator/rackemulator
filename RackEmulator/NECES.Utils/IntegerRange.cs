﻿using System;
using System.Text;

namespace NECES.Utils
{
    public class IntegerRange
    {
        public int Begin { get; set; }
        public int End { get; set; }

        public IntegerRange()
        {
            Begin = End = 0;
        }

        public IntegerRange(int begin, int end)
        {
            Begin = begin;
            End = end;
        }

        public IntegerRange(int value) : this(value,value)
        {

        }

        public static explicit operator IntegerRange(int i)
        {
            return new IntegerRange(i);
        }

        public static explicit operator int(IntegerRange iR)
        {
            if (Math.Abs(iR.Begin - iR.End) <= 1 )
            {
                return iR.Begin;
            }
            throw new InvalidCastException("Cannot cast to an int when range is not over a single integer.");
        }

        public override string ToString()
        {
            return Begin == End ? ((int) this).ToString() : $"{Begin}-{End}";
        }

        public static IntegerRange Parse(string toParse)
        {
            var builder = new StringBuilder();
            int i = 0;
            toParse = toParse.Trim();
            while (i < toParse.Length && toParse[i] >= '0' && toParse[i] <= '9')
            {
                builder.Append(toParse[i]);
                i++;
            }
            if(builder.Length == 0) throw new ArgumentException(@"An IntegerRange must be of the format #-# or a single integer.", nameof(toParse));
            var begin = int.Parse(builder.ToString());

            while (i< toParse.Length && toParse[i] == ' ') i++;
            if (i>=toParse.Length-1 || toParse[i] != '-')
            {
                return (IntegerRange) begin;
            }
            i++;

            var secondHalf = toParse.Substring(i).Trim();
            builder.Clear();
            i = 0;
            while (i < secondHalf.Length && secondHalf[i] >= '0' && secondHalf[i] <= '9')
            {
                builder.Append(secondHalf[i]);
                i++;
            }
            if (builder.Length == 0) return (IntegerRange) begin;
            var end = int.Parse(builder.ToString());
            return new IntegerRange(begin, end);
        }


    }
}