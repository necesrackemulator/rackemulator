﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NECES.Utils
{
    public class ModuleDataType
    {
        public double? Value { get; set; }
        public ValueFlags Flag { get; set; }
        public enum ValueFlags
        {
            ErrVal, NoVal, VoltVal
        }

        public ModuleDataType()
        {
            Value = null;
            Flag = ValueFlags.NoVal;
        }

        public ModuleDataType(double val)
        {
            Value = val;
            Flag = ValueFlags.VoltVal;
        }

        public ModuleDataType(ValueFlags errVal)
        {
            Flag = errVal;
        }

        public static implicit operator ModuleDataType(double val)
        {
            return new ModuleDataType(val);
        }
    }
}
