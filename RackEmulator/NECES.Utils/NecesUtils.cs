﻿// (C) NECES All Rights Reserved.
// NecesUtils.cs, authored by: Andrew Schade
// Created: 08-04-2016; Last Updated: 08-04-2016 @ 2:40 PM

using System;
using System.Collections.Generic;

namespace NECES.Utils
{
    public static class NecesUtils
    {
        public static byte[] MergeBytes(params byte[][] mergeBytes)
        {
            var bytes = new byte[8];
            foreach (var tmp in mergeBytes)
            {
                for (int i = 0; i < bytes.Length; i++)
                {
                    bytes[i] |= tmp[i];
                }
            }
            return null;
        }

        public static byte[] MergeBytes(this byte[] bytes, params byte[][] mergeBytes)
        {
            foreach (var tmp in mergeBytes)
            {
                for (int i = 0; i < bytes.Length; i++)
                {
                    bytes[i] |= tmp[i];
                }
            }
            return bytes;
        }

        public static ModuleDataType Sum(this IEnumerable<ModuleDataType> values)
        {
            double sum = 0;
            foreach (var voltageValue in values)
            {
                if (voltageValue.Flag == ModuleDataType.ValueFlags.VoltVal)
                {
                    sum += voltageValue.Value ?? 0;
                }
                else { return new ModuleDataType(ModuleDataType.ValueFlags.ErrVal);}
            }
            return sum;
        }

        public static int Shift(this int i, int spots)
        {
            if (spots == 0) return i;
            return spots > 0 ? i << spots : i >> Math.Abs(spots);
        }

        public static int Shift(this byte i, int spots)
        {
            return Shift((int) i, spots);
        }
    }
}