﻿using System;
using System.Runtime.Serialization;
using SQA_utils;

namespace NECES.Utils
{
    [Serializable]
    internal class SqaException : Exception
    {
        public pgmReturnCode ReturnCode { get; }

        public SqaException()
        {
        }

        public SqaException(string message) : base(message)
        {
        }

        public SqaException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public SqaException(string message, pgmReturnCode returnCode) : base(message)
        {
            ReturnCode = returnCode;
        }

        protected SqaException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}