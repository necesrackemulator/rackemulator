﻿using System.Text;

namespace NECES.Utils
{
    public class ConsolePrompt
    {
        public static readonly ConsolePromptType Elapsed = new ConsolePromptType(ConsolePromptType.PromptType.Elapsed);
        public static readonly ConsolePromptType Clock = new ConsolePromptType(ConsolePromptType.PromptType.Clock);
        public static readonly ConsolePromptType Null = new ConsolePromptType(ConsolePromptType.PromptType.Null);
        public void Use(ConsolePromptType type, bool? toggle)
        {
            if (toggle != false)
            {
                Current = type;
            }
        }

        public ConsolePrompt(Output output)
        {
            Output = output;
        }

        public ConsolePromptType Current { get; private set; }
        private Output Output { get; set; }

        public class ConsolePromptType
        {
            public enum PromptType { Elapsed, Clock, Null}
            public ConsolePromptType(PromptType type)
            {
                Type = type;
            }
            public PromptType Type { get; }

            public bool Equals(ConsolePromptType other)
            {
                return Type == other.Type;
            }

            public override int GetHashCode()
            {
                return (int) Type;
            }
        }

        public void WritePrompt()
        {
            var prompt = new StringBuilder();
            prompt.Append("ModuleEmulator");

            // TODO Add clock or elapsed when enabled.

            prompt.Append("> ");
            Output.PutPrompt(prompt.ToString());
        }
    }
}