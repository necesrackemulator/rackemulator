﻿using System;
using System.Runtime.Serialization;

namespace NECES.Utils
{
    [Serializable]
    public class ConsoleInputException : Exception
    {
        public ConsoleInputException()
        {
        }

        public ConsoleInputException(string message) : base(message)
        {
        }

        public ConsoleInputException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ConsoleInputException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}