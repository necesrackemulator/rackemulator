﻿using System.Linq;
using System.Threading.Tasks;
using NECES.CanDriver;
using NECES.CanProtocol;
using NECES.ModcanLG;
using NECES.Utils;

namespace NECES.ModuleEmulator
{
    public class ProgramContext
    {
        public Rack Rack { get; }
        public Output Output { get; }
        public Protocol Protocol { get; }

        public ProgramContext(Rack rack, Output output, Protocol protocol, CanDriver.CanDriver driver)
        {
            Rack = rack;
            Output = output;
            Protocol = protocol;
            Driver = driver;
        }

        public CanDriver.CanDriver Driver { get; }

        public void OperateWorkerThread()
        {
            Driver.CanDevice.SetReceiveCallback(ReceiveMessageCallback);
        }

        public async Task ReceiveMessageCallback(CanMessage message)
        {
            if (Protocol.PollRequest.MsgId == message.MsgId)
            {
                var address = Protocol.PollRequest.AddressEncoder.DecodeFromBytes(message.Bytes, true);
                await Protocol.PollResponse.SendResponse(Rack, address);
            }
            if (Protocol.BalanceRequest.MsgId == message.MsgId)
            {
                var address = Protocol.BalanceRequest.AddressEncoder.DecodeFromBytes(message.Bytes, true);
                await Task.Run(() =>
                {
                    var balancerFlags = new bool[Protocol.BalanceRequest.BalanceFlags.Length];
                    for (int i = 0; i < balancerFlags.Length; i++)
                    {
                        balancerFlags[i] = Protocol.BalanceRequest.BalanceFlags[i].DecodeFromBytes(message.Bytes, true);
                    }
                    var module = Rack[address/Protocol.BalanceRequest.AddressEncoder.SubModules];
                    var subMod = address%Protocol.BalanceRequest.AddressEncoder.SubModules;
                    int flag = 0;
                    for (int i = subMod*(Rack.NumCells/Protocol.BalanceRequest.AddressEncoder.SubModules);
                        i < (subMod + 1)*(Rack.NumCells/Protocol.BalanceRequest.AddressEncoder.SubModules);
                        i++)
                    {
                        module.BalanceFlags[i] = balancerFlags[flag];
                        flag++;
                    }
                });
            }
        }
    }
}