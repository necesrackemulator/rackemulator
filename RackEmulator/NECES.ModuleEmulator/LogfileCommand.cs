﻿using System.IO;

namespace NECES.ModuleEmulator
{
    internal class LogfileCommand : Parser.ProgramTask
    {
        private string _path;

        public LogfileCommand(ProgramContext context, string path) : base(context)
        {
            this._path = path;
        }

        public override void Run()
        {
            Context.Output.Logfile = _path;
        }
    }
}