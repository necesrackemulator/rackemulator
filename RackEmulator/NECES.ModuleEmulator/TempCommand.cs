﻿using System.Threading.Tasks;
using NECES.Utils;

namespace NECES.ModuleEmulator
{
    internal class TempCommand : Parser.ProgramTask
    {
        private readonly IntegerRange _modules;
        private readonly double _temp;

        public TempCommand(ProgramContext context, IntegerRange modules, double temp) : base(context)
        {
            this._modules = modules;
            this._temp = temp;
        }

        public override void Run()
        {
            var task = new Task(() =>
            {
                for (int i = _modules.Begin; i <= _modules.End; i++)
                {
                    for (int j = 0; j < Context.Rack.NumTherms; j++)
                    {
                        Context.Rack[i].Therms[j] = _temp;
                    }
                }
            });

            task.Start();
            task.Wait();
            Context.Output.PutLine($"Module{(_modules.Begin == _modules.End? string.Empty: "s")} Temperature set to {_temp:#0.0##} Degrees Celsius");
        }
    }
}