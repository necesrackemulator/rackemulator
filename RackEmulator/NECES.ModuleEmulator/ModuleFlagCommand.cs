﻿using System.Threading.Tasks;
using NECES.ModcanLG;
using NECES.Utils;

namespace NECES.ModuleEmulator
{
    internal class ModuleFlagCommand : Parser.ProgramTask
    {
        private Commands _flag;
        private IntegerRange _modules;
        private bool _toggle;

        public ModuleFlagCommand(ProgramContext context, IntegerRange modules, Commands flag, bool toggle) : base(context)
        {
            _modules = modules;
            _flag = flag;
            _toggle = toggle;
        }

        public override void Run()
        {
            Context.Rack.Loop(_modules, mod =>
            {
                mod[_flag] = _toggle;
            });
        }
    }
}