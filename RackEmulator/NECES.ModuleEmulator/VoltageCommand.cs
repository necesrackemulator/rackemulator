﻿using System;
using System.Threading.Tasks;
using NECES.Utils;

namespace NECES.ModuleEmulator
{
    internal class VoltageCommand : Parser.ProgramTask
    {
        private readonly IntegerRange _cells;
        private readonly IntegerRange _modules;
        private readonly double _voltage;

        public VoltageCommand(ProgramContext context, IntegerRange modules, IntegerRange cells, double voltage)
            : base(context)
        {
            _modules = modules;
            _cells = cells;
            _voltage = voltage;
        }

        public override void Run()
        {
            var task = new Task(() =>
            {
                for (int i = _modules.Begin-1; i < _modules.End; i++)
                {
                    for (int j = _cells.Begin-1; j < _cells.End; j++)
                    {
                        Context.Rack[i].Cells[j] = _voltage;
                        Context.Output.PutLine($"Module {Context.Rack[i].Address} Cell {j} updated.");
                    }
                }
            });

            task.Start();
            task.Wait();

            Context.Output.PutLine($"Voltage for modules {_modules}, cells {_cells} updated to {_voltage} Volts.");
        }
    }
}