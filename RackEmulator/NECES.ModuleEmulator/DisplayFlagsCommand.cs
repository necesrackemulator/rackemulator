﻿// (C) NECES All Rights Reserved.
// DisplayFlagsCommand.cs, authored by: Andrew Schade
// Created: 08-01-2016; Last Updated: 08-05-2016 @ 1:30 PM

#region Imports

using System;
using System.Collections.Generic;
using System.Text;
using NECES.ModcanLG;
using NECES.Utils;

#endregion

namespace NECES.ModuleEmulator
{
    internal class DisplayFlagsCommand : Parser.ProgramTask
    {
        private readonly IntegerRange _modules;

        public DisplayFlagsCommand(ProgramContext context, IntegerRange modules) : base(context)
        {
            _modules = modules;
        }

        public override void Run()
        {
            var outputList = new List<string>();
            Context.Rack.Loop(_modules, mod =>
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append($"Module {mod.Address}{Environment.NewLine}");
                foreach (var systemFlag in mod.SystemFlags)
                {
                    stringBuilder.Append($"\t{systemFlag.Key.ToUpper()}: {systemFlag.Value}{Environment.NewLine}");
                }
                stringBuilder.Append($"\tRefVolt: {mod.Ref2V}{Environment.NewLine}");
                outputList.Add(stringBuilder.ToString());
            });
            foreach (var str in outputList)
            {
                Context.Output.PutLine(str);
            }
        }
    }
}