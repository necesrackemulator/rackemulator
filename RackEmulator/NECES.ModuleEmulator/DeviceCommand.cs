﻿using System;
using NECES.CanDriver;
using NECES.Utils;

namespace NECES.ModuleEmulator
{
    internal class DeviceCommand : Parser.ProgramTask
    {
        private readonly CanDriver.CanDriver.DeviceType _deviceType;

        public DeviceCommand(ProgramContext context, CanDriver.CanDriver.DeviceType deviceType): base(context)
        {
            this._deviceType = deviceType;
        }

        public override void Run()
        {
            Context.Driver.Device = _deviceType;
            Context.Driver.CanDevice.WarnEvent += WarnEvent;
            if(Context.Driver.CanDevice.InitializeCan())

            Context.Output.PutLine($"Successfully set the device type to: {_deviceType}.{Environment.NewLine}");
            else Context.Output.PutLine($"Failed to initialze CAN driver. Check if CAN device is plugged in.{Environment.NewLine}");
        }
        private  void WarnEvent(object sender, CanWarnEventArgs e)
        {
            switch (e.Type)
            {
                case CanWarnEventArgs.WarnType.Debug:
                    Context.Output.PutLine($"{e.Message}", Output.Display.Debug);
                    break;
                case CanWarnEventArgs.WarnType.Warn:
                    Context.Output.PutLine($"{e.Message}", Output.Display.Warning);
                    break;
                case CanWarnEventArgs.WarnType.Error:
                    Context.Output.PutLine($"{e.Message}", Output.Display.Error);
                    break; // TODO do something about the error
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}