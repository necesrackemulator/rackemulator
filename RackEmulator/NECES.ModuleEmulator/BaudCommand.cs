﻿using System;
using System.Threading.Tasks;
using NECES.CanDriver;

namespace NECES.ModuleEmulator
{
    internal class BaudCommand : Parser.ProgramTask
    {
        private readonly int _baud;

        public BaudCommand(int baud, ProgramContext context) : base(context)
        {
            _baud = baud;
        }

        public override void Run()
        {
            var a = new Task<bool>(() =>
            {
                if (!Enum.IsDefined(typeof(CanDevice.BaudRate), _baud))
                {
                    return false;
                }
                Context.Protocol.BaudRate = (CanDevice.BaudRate) _baud;
                return true;
            });
            a.Start();
            a.Wait();
            Context.Output.PutLine(a.Result
                ? $"CAN Baud Rate successfully set to: {_baud} Kbs"
                : "CAN Baud Rate failed to be set. Make sure your input was a valid baud rate.");
        }
    }
}