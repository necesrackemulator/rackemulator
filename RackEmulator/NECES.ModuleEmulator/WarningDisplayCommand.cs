﻿using NECES.Utils;

namespace NECES.ModuleEmulator
{
    internal class WarningDisplayCommand : Parser.ProgramTask
    {
        private Output.Channel _channel;
        private bool? _toggle;

        public WarningDisplayCommand(ProgramContext context, bool? toggle, Output.Channel channel) : base(context)
        {
            this._toggle = toggle;
            this._channel = channel;
        }

        public override void Run()
        {
            Context.Output.Toggle(Output.Display.Warning, _channel, _toggle);
            Context.Output.PutLine(
                $"Succesfully set Display: {nameof(Output.Display.Warning)} to" +
                $" {Context.Output.CurrentDisplay(Output.Display.Warning, _channel)} on {nameof(_channel)}");
        }
    }
}