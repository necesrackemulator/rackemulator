﻿using NECES.Utils;
using SQA_utils;

namespace NECES.ModuleEmulator
{
    internal class LogToggleCommand : Parser.ProgramTask
    {
        private bool? _toggle;

        public LogToggleCommand(ProgramContext context, bool? toggle) : base(context)
        {
            this._toggle = toggle;
        }

        public override void Run()
        {
            Logging.loggingEnabled = _toggle ?? !Logging.loggingEnabled;
            if (Logging.loggingEnabled)
            {
                Logging.openLogFileIfNeeded();
            }
            else
            {
                Logging.closeLogFileIfNeeded();
            }
            Context.Output.Toggle(Output.Display.Null, Output.Channel.Log, _toggle);
        }
    }
}