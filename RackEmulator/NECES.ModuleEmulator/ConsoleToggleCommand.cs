﻿namespace NECES.ModuleEmulator
{
    internal class ConsoleToggleCommand : Parser.ProgramTask
    {
        private bool _toggleValue;

        public ConsoleToggleCommand(ProgramContext context, bool toggleValue) :base(context)
        {
            this._toggleValue = toggleValue;
        }

        public override void Run()
        {
            Context.Output.ConsoleActive = _toggleValue;
        }
    }
}