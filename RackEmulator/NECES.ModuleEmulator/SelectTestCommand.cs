﻿using System.Net.Configuration;
using NECES.Utils;

namespace NECES.ModuleEmulator
{
    internal class SelectTestCommand : Parser.ProgramTask
    {
        private IntegerRange _cells;
        private IntegerRange _modules;

        public SelectTestCommand(ProgramContext context, IntegerRange modules, IntegerRange cells) : base(context)
        {
            _modules = modules;
            _cells = cells;
        }

        public override void Run()
        {
            for (int i = _modules.Begin; i < _modules.End; i++)
            {
                Context.Rack[i].UnderTest = !Context.Rack[i].UnderTest;
            }
        }
    }
}