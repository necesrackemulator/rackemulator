﻿using NECES.Utils;

namespace NECES.ModuleEmulator
{
    internal class ClockDisplayCommand : Parser.ProgramTask
    {
        private readonly Output.Channel _channel;
        private readonly bool? _toggle;

        public ClockDisplayCommand(ProgramContext context, bool? toggle, Output.Channel channel) : base(context)
        {
            _toggle = toggle;
            _channel = channel;
        }

        public override void Run()
        {
            if (_channel != Output.Channel.Console)
            {
                throw new ConsoleInputException();
            } // TODO: Consider moving to Parser.
            Context.Output.ConsolePrompt.Use(ConsolePrompt.Clock, _toggle);
            Context.Output.PutLine($"Updated Prompt Type: {nameof(ConsolePrompt.Clock)}.");
        }
    }
}