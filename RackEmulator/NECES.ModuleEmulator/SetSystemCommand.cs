﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NECES.CanDriver;
using NECES.ModcanLG;
using NECES.Utils;
using SQA_config;

namespace NECES.ModuleEmulator
{
    internal class SetSystemCommand : Parser.ProgramTask
    {
        private readonly int _systemUnderTest;

        public SetSystemCommand(int systemUnderTest, ProgramContext context) : base(context)
        {
            _systemUnderTest = systemUnderTest;
        }

        public override void Run()
        {
            if (Enum.IsDefined(typeof(systemConfig.systemType), _systemUnderTest))
            {
                systemConfig.buildMbbTypeList();
                int modules = 5;
                int cells = 7;
                int baud = 250;
                string systemName = "Default/Null";

                systemConfig.systemUnderTest = (systemConfig.systemType) _systemUnderTest;
                modules = systemConfig.getMaxModule();
                cells = systemConfig.getMaxCell();
                baud = systemConfig.getModCanBaudRate();
                systemName = systemConfig.getSystemTypeName();

                var setRack = new Task(() =>
                {
                    Context.Rack.NumModules = modules;
                    Context.Rack.NumCells = cells;
                    Context.Rack.Name = systemName;

                    //TODO: Ensure that system has LG flags
                    JObject lgCan = JObject.Parse(File.ReadAllText($"{systemConfig.CONFIG_DIR_NAME}/lgModule.json"));
                    var refVolt = JsonConvert.DeserializeObject<double>(lgCan["Ref2V"].ToString());
                    var stmCount = JsonConvert.DeserializeObject<int>(lgCan["StmCount"].ToString());
                    var systemFlags = JsonConvert.DeserializeObject<Dictionary<string, bool>>(lgCan["SystemFlags"].ToString());
                    var subModules = JsonConvert.DeserializeObject<int>(lgCan["SubModules"].ToString());
                    Context.Rack.Loop((IntegerRange) (-1), module =>
                    {
                        module.SystemFlags = systemFlags;
                        module.Ref2V = refVolt;
                        module.StmCount = stmCount;
                        module.SubModules = subModules;
                    });

                });

                var setBaud = new Task<bool>(() =>
                {
                    if (!Enum.IsDefined(typeof(CanDevice.BaudRate), (int)baud))
                    {
                        return false;
                    }
                    Context.Protocol.BaudRate = (CanDevice.BaudRate) baud;
                    return true;
                });
                setBaud.Start();
                setBaud.Wait();

                if (setBaud.Result)
                {
                    setRack.Start();
                    setRack.Wait();

                    Context.Output.PutLine($"System successfully set to {Context.Rack.Name}");
                }
                else
                {
                    Context.Output.PutLine(
                        "System failed to be set successfuly, please check the config file for errors.", Output.Display.Error);
                }
            }
            else
            {
                Context.Output.PutLine("System failed to be set successfully: System is in config file, but not specified in Code.");
            }
        }
    }
}