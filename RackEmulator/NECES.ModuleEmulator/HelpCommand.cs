﻿namespace NECES.ModuleEmulator
{
    internal class HelpCommand : Parser.ProgramTask
    {
        private string cmd;
        private const string HelpString = @"     Command         Category      Syntax
-------------------- ------------- -----------------------------------------------------------
Help                 Help          `help`
Exit                 Exit          `quit`
Set System           Setup         `system (system name)`
Set CAN Device       Setup/CAN     `can[ ]dev (kvaser|canalyzer|null)`
Set Baud Rate        Setup/CAN     `can baud ###`
Open Channel         Setup/CAN     `can channel # open`
Close channel        Setup/CAN     `can channel # close`
Set Cell Voltage     Set/Mod       `set mod #[-#] cell #[-#] volt (#[.##]|noval|errval)`
Set Temperature      Set/Mod       `set mod #[-#] temp ##[.##]`
Set Module Voltage   Set/Mod       `set mod #[-#][.(a|b)] volt [##.[###]]`
Set Module Latency   Set/Mod       `set mod #[-#] latency ####`
Set Module Flags     Set/Mod       `set mod #[-#] (flag name) (on|off)`
Set Module Ref Volt  Set/Mod       `set mod #[-#] ref #[.###]`
Set General Latency  Set/General   `set latency ####`
Display Mod Volt     Display       `display [mod #[-#] [cell [#[-#]]]] volt`
Display Mod Temp     Display       `display [mod #[-#]] temp`
Display Mod Flags    Display       `display [mod #[-#]] flags`
Logfile Command      Log           `set log (file path)`
Log Enable Command   Log           `set log (on|off)`
Set Display Flags    Log           `set (log|console) display (debug|warning|error) (on|off)`
Script               Automation    `input ""path/to/file""`
";
        public HelpCommand(ProgramContext context) : base(context)
        {
            cmd = string.Empty;
        }

        public HelpCommand(ProgramContext context, string cmd) : this(context)
        {
            this.cmd = cmd;
        }

        public override void Run()
        {
            Context.Output.PutLine(HelpString);
        }
    }
}