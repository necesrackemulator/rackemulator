﻿using NECES.Utils;

namespace NECES.ModuleEmulator
{
    internal class HexDisplayCommand : Parser.ProgramTask
    {
        private Output.Channel _channel;
        private bool? _toggle;

        public HexDisplayCommand(ProgramContext context, bool? toggle, Output.Channel channel) : base(context)
        {
            this._toggle = toggle;
            this._channel = channel;
        }

        public override void Run()
        {
            Context.Output.Toggle(Output.Display.Hex, _channel, _toggle);
            Context.Output.PutLine(
                $"Succesfully set Display: {nameof(Output.Display.Hex)} to" +
                $" {Context.Output.CurrentDisplay(Output.Display.Hex, _channel)} on {nameof(_channel)}");
            // This will output the updated value of the display for Hex on the current channel.
        }
    }
}