﻿using System;
using System.IO;
using NECES.Utils;

namespace NECES.ModuleEmulator
{
    internal class ScriptCommand : Parser.ProgramTask
    {
        private string _path;

        public ScriptCommand(ProgramContext context, string path) : base(context)
        {
            this._path = path;
        }

        public override void Run()
        {
            var parser = new Parser(new StreamReader(File.OpenRead(_path)), Context);
            Parser.ProgramTask task;
            while ((task = parser.ParseNext()) != null)
            {
                try
                {
                    task.Run();
                }
                catch (Exception e)
                {
                    Context.Output.PutLine(e.Message, Output.Display.Error);
                    continue;
                }

                if (task is ExitCommand)
                {
                    break;
                }
            }
        }
    }
}