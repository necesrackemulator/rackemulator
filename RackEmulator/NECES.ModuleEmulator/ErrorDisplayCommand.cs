﻿using NECES.Utils;

namespace NECES.ModuleEmulator
{
    internal class ErrorDisplayCommand : Parser.ProgramTask
    {
        private Output.Channel channel;
        private bool? toggle;

        public ErrorDisplayCommand(ProgramContext context, bool? toggle, Output.Channel channel) : base(context)
        {
            this.toggle = toggle;
            this.channel = channel;
        }

        public override void Run()
        {
            throw new System.NotImplementedException();
        }
    }
}