﻿using NECES.Utils;

namespace NECES.ModuleEmulator
{
    internal class DebugDisplayCommand : Parser.ProgramTask
    {
        private readonly Output.Channel _channel;
        private readonly bool? _toggle;

        public DebugDisplayCommand(ProgramContext context, bool? toggle, Output.Channel channel) : base(context)
        {
            _toggle = toggle;
            _channel = channel;
        }

        public override void Run()
        {
            Context.Output.Toggle(Output.Display.Debug, _channel, _toggle);
            Context.Output.PutLine(
                $"Succesfully set Display: {nameof(Output.Display.Debug)} to" +
                $" {Context.Output.CurrentDisplay(Output.Display.Debug, _channel)} on {_channel}");
            // This will output the updated value of the display for Debug on the current channel.
        }
    }
}