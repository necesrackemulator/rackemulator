﻿namespace NECES.ModuleEmulator
{
    internal class ExitCommand : Parser.ProgramTask
    {
        public ExitCommand(ProgramContext context) : base(context)
        {
        }

        public override void Run()
        {
//            Context.Driver.CanClose();
//            Context.Driver.CanDevice.ShutdownCan();
            // TODO: Pre Exit logic
            System.Environment.Exit(0);
        }
    }
}