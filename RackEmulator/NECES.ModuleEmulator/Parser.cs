﻿using System;
using System.IO;
using NECES.Utils;
using SQA_config;

namespace NECES.ModuleEmulator
{
    /// <summary>
    ///     This class outlines the parsing architecture and provides logic to generate commands
    ///     from CLI/script input.
    ///     TODO: Refactor so that the parser can be more easily extended. (further classes).
    /// </summary>
    public class Parser
    {
        private readonly TextReader _inputReader;

        /// <summary>
        ///     Main constructor for the Parser code.
        /// </summary>
        /// <param name="inputReader">
        ///     A TextReader system for the input. This can either be a StreamReader from a file,
        ///     or the <see cref="Console.In" />.
        /// </param>
        /// <param name="context">The object-model context for the program to operate in. Contains references to the ModcanLG, the CANProtocol,
        /// and the Output wrapper.</param>
        public Parser(TextReader inputReader, ProgramContext context)
        {
            _inputReader = inputReader;
            Context = context;
        }

        /// <summary>
        ///     Parse the next line of input.
        /// </summary>
        /// <returns>Returns a cmd that will later be executed in order to run the code.</returns>
        public ProgramTask ParseNext()
        {
            var line = _inputReader.ReadLine()?.ToLower().Trim();



            try
            {
                if (line.Length == 0)
                    return null;
                var cmds = line.Split(';');
                if (cmds.Length != 1)
                {

                }
                if (line.StartsWith(Commands.System))
                {
                    var subcommand = GetSubCommand(Commands.System, line);
                    return ParseSystemCommand(subcommand);
                }
                if (line.StartsWith(Commands.Set))
                {
                    var subcommand = GetSubCommand(Commands.Set, line);
                    return ParseSetCommand(subcommand);
                }
                if (line.StartsWith(Commands.Test))
                {
                    var subcommand = GetSubCommand(Commands.Test, line);
                    return ParseTestCommand(subcommand);
                }
                if (line.StartsWith(Commands.Get))
                {
                    var subcommand = GetSubCommand(Commands.Get, line);
                    return ParseGetCommand(subcommand);
                }
                if (line.StartsWith(Commands.Can))
                {
                    var subcommand = GetSubCommand(Commands.Can, line);
                    return ParseCanCommand(subcommand);
                }
                if (line.StartsWith(Commands.Wait))
                {
                    var subcommand = GetSubCommand(Commands.Wait, line);
                    return ParseWaitCommand(subcommand);
                }
                if (line.StartsWith(Commands.Help))
                {
                    return ParseHelpCommand(GetSubCommand(Commands.Help, line));
                }
                if (line.StartsWith(Commands.Script))
                {
                    return ParseScriptCommand(GetSubCommand(Commands.Script, line));
                }
                if (line.StartsWith(Commands.Display))
                {
                    return ParseDisplayVarsCommand(GetSubCommand(Commands.Display, line));
                }
                if (line.StartsWith(Commands.Exit))
                {
                    return new ExitCommand(Context);
                }
                throw new ConsoleInputException(@"Unrecognized cmd: type ""help"" to see a list of available commands.");
            }
            catch (NullReferenceException e)
            {
                throw new ConsoleInputException("Console Read Error.");
            }
        }

        private ProgramTask ParseDisplayVarsCommand(string subCommand)
        {
            if (subCommand.StartsWith(Commands.Mod))
            {
                var subSubCommand = GetSubCommand(Commands.Mod, subCommand);
                var modules = IntegerRange.Parse(subSubCommand);
                var subcommand = subSubCommand.Substring(modules.ToString().Length).Trim();
                if (subcommand.StartsWith(Commands.Volt))
                {
                    return new DisplayValuesCommand(Commands.Volt, modules, new IntegerRange(-1), Context);
                }
                if (subcommand.StartsWith(Commands.Temp))
                {
                    return new DisplayValuesCommand(Commands.Temp, modules, new IntegerRange(-1), Context);
                }
                if (subcommand.StartsWith(Commands.Flags))
                {
                    return new DisplayFlagsCommand(Context, modules);
                }
                if (subcommand.StartsWith(Commands.Cell))
                {
                    subSubCommand = GetSubCommand(Commands.Cell, subcommand);
                    var cells = IntegerRange.Parse(subSubCommand);
                    subcommand = subSubCommand.Substring(cells.ToString().Length).Trim();
                    if(subcommand.StartsWith(Commands.Volt))
                    {
                        return new DisplayValuesCommand(Commands.Volt, modules, cells, Context);
                    }
                }
            }
            if (subCommand.StartsWith(Commands.Volt))
            {
                return new DisplayValuesCommand(Commands.Volt, new IntegerRange(-1), new IntegerRange(-1), Context);
            }
            if (subCommand.StartsWith(Commands.Temp))
            {
                return new DisplayValuesCommand(Commands.Temp, new IntegerRange(-1), new IntegerRange(-1), Context);
            }
            if (subCommand.StartsWith(Commands.Flags))
            {
                return new DisplayFlagsCommand(Context, new IntegerRange(-1));
            }
            throw new ConsoleInputException($"Unrecognized command: type \"help\" {Commands.Can} to see a list of available commands.");
        }

        private string GetSubCommand(Commands commands, string command)
        {
            return command.Substring(commands.Name.Length).Trim();
        }

        private ProgramTask ParseSystemCommand(string subcommand)
        {
            systemConfig.buildSystemTypeList();
            var readConfigFile = File.OpenText("config/config.csv");
            int systemUnderTest = 0;
            while (!readConfigFile.EndOfStream)
            {
                var fileLine = readConfigFile.ReadLine();
                var values = fileLine.Split(',');
                if (subcommand.Equals(values[0].ToLower().Trim()))
                {
                    readConfigFile.Close();
                    break;
                }
                systemUnderTest++;
            }
            return new SetSystemCommand(systemUnderTest - 1, Context);
        }

        public ProgramContext Context { get; }

        private ProgramTask ParseBaudCommand(string command)
        {
            return new BaudCommand(int.Parse(command), Context);
        }

        private ProgramTask ParseCanCommand(string cmd)
        {
            if (cmd.StartsWith(Commands.Baud))
            {
                return ParseBaudCommand(GetSubCommand(Commands.Baud, cmd));
            }
            if (cmd.StartsWith(Commands.Channel))
            {
                return ParseChannelCommand(GetSubCommand(Commands.Channel, cmd));
            }
            if (cmd.StartsWith(Commands.Device))
            {
                return ParseDeviceCommand(GetSubCommand(Commands.Device, cmd));
            }
            if (cmd.StartsWith(Commands.Start))
            {
                return new CanStartCommand(Context);
            }
            if (cmd.StartsWith(Commands.Stop))
            {
                return new CanStopCommand(Context);
            }

            throw new ConsoleInputException($"Unrecognized command: type \"help\" to see a list of available commands.");

        }

        private ProgramTask ParseChannelCommand(string cmd)
        {
            var channelNumber = -1;
            var subCmd = cmd;
            if (int.TryParse(cmd[0].ToString(), out channelNumber))
            {
                subCmd = cmd.Substring(1).Trim();

            }
            if (subCmd.StartsWith(Commands.Open))
            {
                return new ChannelCommand(Context, channelNumber-1, true);
            }
            if (subCmd.StartsWith(Commands.Close))
            {
                return new ChannelCommand(Context, channelNumber-1, false);
            }
            throw new ConsoleInputException($"Unrecognized command: please type \"help\" for usage.");
        }

        private ProgramTask ParseConsoleCommand(string cmd)
        {
            if (cmd.StartsWith(Commands.On))
            {
                return new ConsoleToggleCommand(Context, true);
            }
            if (cmd.StartsWith(Commands.Off))
            {
                return new ConsoleToggleCommand(Context, false);
            }
            if (cmd.StartsWith(Commands.Display))
            {
                return ParseDisplayCommand(GetSubCommand(Commands.Display, cmd), Output.Channel.Console);
            }
            throw new ConsoleInputException($"Unrecognized command: please type \"help\" for usage.");
        }

        private ProgramTask ParseDeviceCommand(string cmd)
        {
            if (cmd.StartsWith(Commands.Kvaser))
            {
                return new DeviceCommand(Context, CanDriver.CanDriver.DeviceType.Kvaser);
            }
            if (cmd.StartsWith(Commands.CanAlyzer))
            {
                return new DeviceCommand(Context, CanDriver.CanDriver.DeviceType.Canalyzer);
            }
            if (cmd.StartsWith(Commands.Null))
            {
                return new DeviceCommand(Context, CanDriver.CanDriver.DeviceType.Null);
            }
            throw new ConsoleInputException($"Unrecognized command: please type \"help\" for usage.");
        }

        private ProgramTask ParseGetCommand(string cmd)
        {
            if (cmd.StartsWith(Commands.Rev))
            {
                return ParseGetRevCommand(GetSubCommand(Commands.Rev, cmd));
            }
            throw new ConsoleInputException($"Unrecognized command: please type \"help\" for usage.");
        }

        private ProgramTask ParseGetRevCommand(string cmd)
        {
            if (cmd.StartsWith(Commands.Hardware))
            {
                return new HarwareRevCommand(Context);
            }
            if (cmd.StartsWith(Commands.Software))
            {
                return new SoftwareRevCommand(Context);

            }
            throw new ConsoleInputException($"Unrecongized command: please type \"help\" for usage.");
        }

        private ProgramTask ParseHelpCommand(string cmd)
        {
            return new HelpCommand(Context, cmd);
        }

        private ProgramTask ParseLatencyCommand(string cmd, IntegerRange modules)
        {
            int latency;
            if (!int.TryParse(cmd, out latency))
            {
                throw new ConsoleInputException("");
            }
            return new LatencyCommand(Context, modules,latency);
        }

        private ProgramTask ParseLogCommand(string cmd)
        {
            if (cmd.StartsWith(Commands.Display))
            {
                return ParseDisplayCommand(GetSubCommand(Commands.Display, cmd), Output.Channel.Log);
            }
            if (cmd.StartsWith(Commands.On))
            {
                return new LogToggleCommand(Context, true);
            }
            if (cmd.StartsWith(Commands.Off))
            {
                return new LogToggleCommand(Context, false);
            }
            return new LogfileCommand(Context, cmd);
        }

        private ProgramTask ParseDisplayCommand(string cmd, Output.Channel channel)
        {
            bool? toggle;
            if (cmd.StartsWith(Commands.Clock))
            {
                var subcommand = GetSubCommand(Commands.Clock, cmd);
                toggle = subcommand.Length == 0 ? (bool?)null : subcommand.StartsWith(Commands.On);
                return new ClockDisplayCommand(Context, toggle, channel);
            }
            if (cmd.StartsWith(Commands.Elapsed))
            {
                var subcommand = GetSubCommand(Commands.Elapsed, cmd);
                toggle = subcommand.Length == 0 ? (bool?)null : subcommand.StartsWith(Commands.On);
                return new ElapsedDisplayCommand(Context, toggle, channel);
            }
            if (cmd.StartsWith(Commands.Error))
            {
                var subcommand = GetSubCommand(Commands.Clock, cmd);
                toggle = subcommand.Length == 0 ? (bool?)null : subcommand.StartsWith(Commands.On);
                return new ErrorDisplayCommand(Context, toggle, channel);
            }
            if (cmd.StartsWith(Commands.Warning))
            {
                var subcommand = GetSubCommand(Commands.Warning, cmd);
                toggle = subcommand.Length == 0 ? (bool?)null : subcommand.StartsWith(Commands.On);
                return new WarningDisplayCommand(Context, toggle, channel);
            }
            if (cmd.StartsWith(Commands.Debug))
            {
                var subcommand = GetSubCommand(Commands.Clock, cmd);
                toggle = subcommand.Length == 0 ? (bool?)null : subcommand.StartsWith(Commands.On);
                return new DebugDisplayCommand(Context, toggle, channel);
            }
            if (cmd.StartsWith(Commands.Hex))
            {
                var subcommand = GetSubCommand(Commands.Clock, cmd);
                toggle = subcommand.Length == 0 ? (bool?)null : subcommand.StartsWith(Commands.On);
                return new HexDisplayCommand(Context, toggle, channel);
            }
            throw new ConsoleInputException("Unrecongized cmd");
        }

        private ProgramTask ParseModDataCommand(string cmd)
        {
            //TODO use a dictionary from the Protocol or Rack to find which flags to be setting.
            var modules = IntegerRange.Parse(cmd);
            var subcommand = cmd.Substring(modules.ToString().Length).Trim();
            if (subcommand.StartsWith(Commands.Cell))
            {
                return ParseVoltCommand(GetSubCommand(Commands.Cell, subcommand), modules);
            }
            if (subcommand.StartsWith(Commands.Temp))
            {
                return ParseTempCommand(GetSubCommand(Commands.Temp, subcommand), modules);
            }
            if (subcommand.StartsWith(Commands.Latency))
            {
                return ParseLatencyCommand(GetSubCommand(Commands.Latency, subcommand), modules);
            }
            if (subcommand.StartsWith(Commands.Btdf))
            {
                return ParseFlagCommand(GetSubCommand(Commands.Btdf, subcommand), Commands.Btdf, modules);
            }
            if (subcommand.StartsWith(Commands.Aludf))
            {
                return ParseFlagCommand(GetSubCommand(Commands.Aludf, subcommand), Commands.Aludf, modules);
            }
            if (subcommand.StartsWith(Commands.Spidf))
            {
                return ParseFlagCommand(GetSubCommand(Commands.Spidf, subcommand), Commands.Spidf, modules);
            }
            if (subcommand.StartsWith(Commands.Spf))
            {
                return ParseFlagCommand(GetSubCommand(Commands.Spf, subcommand), Commands.Spf, modules);
            }
            if (subcommand.StartsWith(Commands.Cfcdf))
            {
                return ParseFlagCommand(GetSubCommand(Commands.Cfcdf, subcommand), Commands.Cfcdf, modules);
            }
            if (subcommand.StartsWith(Commands.Vslodf))
            {
                return ParseFlagCommand(GetSubCommand(Commands.Vslodf, subcommand), Commands.Vslodf, modules);
            }
            if (subcommand.StartsWith(Commands.Cedmf))
            {
                return ParseFlagCommand(GetSubCommand(Commands.Cedmf, subcommand), Commands.Cedmf, modules);
            }
            if (subcommand.StartsWith(Commands.Ref))
            {
                var s = GetSubCommand(Commands.Ref, subcommand);
                double volt = -1;
                if(!double.TryParse(s, out volt))
                {
                    throw new ConsoleInputException("Unrecognized Command");
                }
                return new ModRefVoltCommand(Context, modules, volt);
            }
            int submod = -1;
            if (subcommand.StartsWith("."))
            {
                if (subcommand[1] == 'a')
                {
                    submod = 0;
                }
                if (subcommand[1] == 'b')
                {
                    submod = 1;
                }
                subcommand = subcommand.Substring(2).Trim();
            }

            if (subcommand.StartsWith(Commands.Volt))
            {
                var s = GetSubCommand(Commands.Volt, subcommand);
                if (s.Length == 0)
                {
                    return new ModVoltCommand(Context, modules, submod);
                }
                double volt = -1;
                if (!double.TryParse(s, out volt))
                {
                    throw new ConsoleInputException("Unrecognized Command");
                }
                return new ModVoltCommand(Context, modules, submod, volt);
            }

            throw new ConsoleInputException("Unrecognized Command");
        }

        private ProgramTask ParseFlagCommand(string cmd, Commands flag, IntegerRange modules)
        {
            if (cmd.StartsWith(Commands.On))
            {
                return new ModuleFlagCommand(Context, modules, flag, true);
            }
            if (cmd.StartsWith(Commands.Off))
            {
                return new ModuleFlagCommand(Context, modules, flag, false);
            }
            throw new ConsoleInputException("Unrecognized Command");
        }

        private ProgramTask ParseScriptCommand(string cmd)
        {
            return new ScriptCommand(Context, cmd);
        }

        private ProgramTask ParseSetCommand(string cmd)
        {
            if (cmd.StartsWith(Commands.Mod))
            {
                var subcommand = GetSubCommand(Commands.Mod, cmd);
                return ParseModDataCommand(subcommand);
            }
            if (cmd.StartsWith(Commands.Log))
            {
                return ParseLogCommand(GetSubCommand(Commands.Log, cmd));
            }
            if (cmd.StartsWith(Commands.Console))
            {
                return ParseConsoleCommand(GetSubCommand(Commands.Console, cmd));
            }
            if (cmd.StartsWith(Commands.Latency))
            {
                return ParseLatencyCommand(GetSubCommand(Commands.Latency, cmd), null);
            }
            throw new ConsoleInputException($"Unrecognized command: type\"help\" for usage.");
        }

        private ProgramTask ParseTempCommand(string cmd, IntegerRange modules)
        {
            double temp;
            if(!double.TryParse(cmd, out temp))
            {
                throw new ConsoleInputException("Unrecognized command");
            }
            return new TempCommand(Context, modules, temp);
        }

        private ProgramTask ParseTestCommand(string cmd)
        {
            return cmd.StartsWith(Commands.Mod) ? ParseTestSelectCommand(GetSubCommand(Commands.Mod, cmd)) : new TestNameCommand(Context, cmd);
        }

        private ProgramTask ParseTestSelectCommand(string cmd)
        {
            var modules = IntegerRange.Parse(cmd);
            var subcommand = cmd.Substring(modules.ToString().Length);
            if (subcommand.Length == 0)
            {
                return new SelectTestCommand(Context, modules, null);
            }
            if (subcommand.StartsWith(Commands.Cell))
            {
                var cells = IntegerRange.Parse(GetSubCommand(Commands.Cell, subcommand));
                return new SelectTestCommand(Context, modules, cells);
            }

            throw new ConsoleInputException("Unrecognized Command");
        }

        private ProgramTask ParseVoltCommand(string cmd, IntegerRange modules)
        {
            var cells = IntegerRange.Parse(cmd);
            var subcommand = cmd.Substring(cells.ToString().Length).Trim();
            subcommand = GetSubCommand(Commands.Volt, subcommand);
            var voltage = double.Parse(subcommand);
            return new VoltageCommand(Context, modules, cells, voltage);
        }

        private ProgramTask ParseWaitCommand(string cmd)
        {
            throw new NotImplementedException();
        }

        public abstract class ProgramTask
        {
            protected ProgramContext Context { get; }
            protected ProgramTask(ProgramContext context)
            {
                Context = context;
            }
            public abstract void Run();
        }
    }
}