﻿using System;
using NECES.ModcanLG;
using NECES.Utils;

namespace NECES.ModuleEmulator
{
    internal class ModVoltCommand : Parser.ProgramTask
    {
        private IntegerRange _modules;
        private double? _volt;
        private int _submod;


        public ModVoltCommand(ProgramContext context, IntegerRange modules,int submod, double? volt = null):base(context)
        {
            _modules = modules;
            _submod = submod;
            _volt = volt;
        }

        public override void Run()
        {
//            if (_volt != null)
//            {
                if (_submod == -1)
                {
                    Context.Rack.Loop(_modules, module => module.ModVoltRaw[0] = _volt/2);
                    Context.Rack.Loop(_modules, module => module.ModVoltRaw[1] = _volt/2);
                }
                else
                {
                    Context.Rack.Loop(_modules, module => module.ModVoltRaw[_submod] = _volt);
                }
                Context.Output.PutLine(
                    $"Updated Module{(_modules.Begin != _modules.End ? "s" : string.Empty)} {_modules}{(_submod != -1 ? "." + (_submod == 0 ? "a" : "b") : string.Empty)} Module Voltage to {(_volt != null ? _volt.Value.ToString("#0.0## Volts") : "Default")}");
//            }
        }
    }
}