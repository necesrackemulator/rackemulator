﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using NECES.ModcanLG;
using NECES.Utils;

namespace NECES.ModuleEmulator
{
    internal class DisplayValuesCommand : Parser.ProgramTask
    {
        private Commands _toDisplay;
        private IntegerRange _modules;
        private IntegerRange _cells;

        public DisplayValuesCommand(Commands toDisplay, IntegerRange modules, IntegerRange cells, ProgramContext context) : base(context)
        {
            _toDisplay = toDisplay;
            _modules = modules;
            _cells = cells;
        }

        public override void Run()
        {
            List<string> outputList = new List<string>();
            int mod = _modules.Begin == -1 ? 1 : _modules.Begin;
            Context.Rack.Loop(_modules, m =>
            {
                if (_toDisplay == Commands.Volt)
                {
                    var cell = _cells.Begin == -1 ? 1 : _cells.Begin;
                    var moduleVoltageBuilder = new StringBuilder();
                    moduleVoltageBuilder.Append($"Module {mod}:\n");
                    m.Loop(_cells, true, c =>
                    {
                        moduleVoltageBuilder.Append($"\tCell {cell}: {c:0.0##} Volts\n");
                        cell++;
                    });
                    if (_cells.Begin == -1)
                    {
                        moduleVoltageBuilder.Append($"\tModule Voltage: {m.ModVolt.Sum():##0.0##} Volts\n");
                    }
                    outputList.Add(moduleVoltageBuilder.ToString());
                }
                else
                {
                    outputList.Add($"Module {mod}: {m.Therms[0]} °C");
                }
                mod++;
            });
            foreach (var str in outputList)
            {
                Context.Output.PutLine(str);
            }
        }
    }
}