﻿using System;
using NECES.ModcanLG;
using NECES.Utils;

namespace NECES.ModuleEmulator
{
    internal class ModRefVoltCommand : Parser.ProgramTask
    {
        private IntegerRange _modules;
        private double _volt;

        public ModRefVoltCommand(ProgramContext context, IntegerRange modules, double volt) : base(context)
        {
            _modules = modules;
            _volt = volt;
        }

        public override void Run()
        {
            Context.Rack.Loop(_modules, mod =>
            {
                mod.Ref2V = _volt;
//                mod.Ref2V[1] = _volt;
            });
            Context.Output.PutLine(string.Format("Module{0} {1} Reference Voltage set to {2:#0.0##} Volts.",
                _modules.Begin != _modules.End ? "s" : string.Empty,
                _modules,
                _volt));
        }
    }
}