﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using NECES.CanDriver;
using NECES.CanProtocol;
using NECES.ModcanLG;
using NECES.Utils;
using SQA_utils;

namespace NECES.ModuleEmulator
{
    internal class Program
    {
        private static bool ShouldReadFromFile { get; set; }

        private static TextReader _inputReader;
        private static Output _output;
        private static ProgramContext Context { get; set; }

        private static void Main(string[] args)
        {
            ParseArgs(args);
            RunInputLoop();
        }

        private static void RunInputLoop()
        {
            // TODO: Open Input stream and initialize globals.
            if (!ShouldReadFromFile)
            {
                _inputReader = Console.In;
            }
            _output = new Output();
            var canDriver = new CanDriver.CanDriver();
            ProtocolFactory.Driver = canDriver;
            var protocol = ProtocolFactory.ReadProtocol(File.OpenText("config/lgCan.json"));

            Context = new ProgramContext(new Rack(), _output, protocol,
                canDriver);
            Console.BufferWidth = Console.WindowWidth = 100;
           ConsoleInputLoop(); // TODO: Consider making parallel.
        }


       private static void ParseArgs(string[] args)
        {
            // TODO parse command arguments to initialize proper data.
            AppUtils.extractAssemblyData(Assembly.GetEntryAssembly());


            // Inform user of the running program and its revision.
            AppUtils.displayRevisionOnConsole();

        }

        private static void ConsoleInputLoop()
        {
            // TODO:  Initialization Logic

            var inputParser = new Parser(_inputReader, Context);

            while (true)
            {
                try
                {
                    if (_output.IsInteractive)
                    {
                        Context.Output.ConsolePrompt.WritePrompt();
                    }
                    var task = inputParser.ParseNext();

                    if (task == null)
                    {
                        continue;
                    }
                    try
                    {
                        task.Run();
                    }

                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        continue;
                    }

                    if (task is ExitCommand)
                    {
                        break;
                    }
                }
                catch (ConsoleInputException e)
                {
                    _output.PutLine(e.Message, Output.Display.Error);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }
    }
}
