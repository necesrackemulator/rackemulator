﻿using System.Threading.Tasks;

namespace NECES.ModuleEmulator
{
    internal class ChannelCommand : Parser.ProgramTask
    {
        private readonly int? _channelNumber;
        private readonly bool _toggle;

        public ChannelCommand(ProgramContext context, int channelNumber, bool toggle): base(context)
        {

            _channelNumber = channelNumber;
            _toggle = toggle;
        }

        public ChannelCommand(ProgramContext context, bool toggle) : base(context)
        {
            this._toggle = toggle;
            _channelNumber = null;
        }

        public override void Run()
        {
            if (_channelNumber != null)
            {
                Context.Driver.CanDevice.CanChannel =  (int) _channelNumber;
            }
            if (_toggle)
            {
                Context.Protocol.StartListening();
                Task.Run(()=>Context.OperateWorkerThread());
            }
            else
            {
                Context.Driver.StopListen();
                Context.Driver.CanClose();
            }
        }


    }
}