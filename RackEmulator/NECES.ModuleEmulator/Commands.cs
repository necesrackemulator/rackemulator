﻿namespace NECES.ModuleEmulator
{
    internal class Commands
    {
        public static Commands Btdf { get; } = new Commands("btdf");
        public static Commands Vslodf { get; } = new Commands("vslodf");
        public static Commands Spidf { get; } = new Commands("spidf");
        public static Commands Spf { get; } = new Commands("spf");
        public static Commands Cedmf { get; } = new Commands("cedmf");
        public static Commands Cfcdf { get; } = new Commands("cfcdf");
        public static Commands Aludf { get; } = new Commands("aludf");
        public static Commands Ref { get; } = new Commands("ref");

        private Commands(string name)
        {
            Name = name;
        }

//        public static Commands Disp { get; } = new Commands("disp");
        public static Commands Baud { get; } = new Commands("baud");
        public static Commands Can { get; } = new Commands("can");
        public static Commands CanAlyzer { get; } = new Commands("canalyzer");
        public static Commands Cell { get; } = new Commands("cell");
        public static Commands Channel { get; } = new Commands("channel");
        public static Commands Clock { get; } = new Commands("clock");
        public static Commands Close { get; } = new Commands("close");
        public static Commands Console { get; } = new Commands("console");
        public static Commands Debug { get; } = new Commands("debug");
        public static Commands Device { get; } = new Commands("dev");
        public static Commands Display { get; } = new Commands("display");
        public static Commands Elapsed { get; } = new Commands("elapsed");
        public static Commands Error { get; } = new Commands("error");
        public static Commands Exit { get; } = new Commands("quit");
        public static Commands Get { get; } = new Commands("get");
        public static Commands Hardware { get; } = new Commands("hardware");
        public static Commands Help { get; } = new Commands("help");
        public static Commands Hex { get; } = new Commands("hex");
        public static Commands Kvaser { get; } = new Commands("kvaser");
        public static Commands Latency { get; } = new Commands("latency");
        public static Commands Log { get; } = new Commands("log");
        public static Commands Mod { get; } = new Commands("mod");
        public static Commands Null { get; } = new Commands("null");
        public static Commands Off { get; } = new Commands("off");
        public static Commands On { get; } = new Commands("on");
        public static Commands Open { get; } = new Commands("open");
        public static Commands Rev { get; } = new Commands("rev");
        public static Commands Script { get; } = new Commands("script");
        public static Commands Set { get; } = new Commands("set");
        public static Commands Software { get; } = new Commands("software");
        public static Commands Start { get; } = new Commands("start");
        public static Commands Stop { get; } = new Commands("stop");
        public static Commands System { get; } = new Commands("system");
        public static Commands Temp { get; } = new Commands("temp");
        public static Commands Test { get; } = new Commands("test");
        public static Commands Volt { get; } = new Commands("volt");
        public static Commands Wait { get; } = new Commands("wait");
        public static Commands Warning { get; } = new Commands("warning");

        public string Name { get; }
        public static Commands Flags { get; private set; } = new Commands("flags");

        public static implicit operator string(Commands c)
        {
            return c.Name.ToLower();
        }

        public static explicit operator int(Commands c)
        {
            return c.Name.Length;
        }
    }
}