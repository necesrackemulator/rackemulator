﻿namespace NECES.ModuleEmulator
{
    internal class CanStopCommand : Parser.ProgramTask
    {
        public CanStopCommand(ProgramContext context) : base(context)
        {
        }

        public override void Run()
        {
            Context.Driver.CanClose();
        }
    }
}