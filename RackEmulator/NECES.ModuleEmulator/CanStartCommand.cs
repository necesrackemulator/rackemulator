﻿using NECES.Utils;

namespace NECES.ModuleEmulator
{
    internal class CanStartCommand : Parser.ProgramTask
    {
        public CanStartCommand(ProgramContext context) : base(context)
        {
        }

        public override void Run()
        {
            Context.Driver.CanOpen();
        }
    }
}