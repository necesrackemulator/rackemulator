﻿using NECES.Utils;

namespace NECES.ModuleEmulator
{
    internal class LatencyCommand : Parser.ProgramTask
    {
        private IntegerRange _modules;
        private int _latency;

        public LatencyCommand(ProgramContext context, IntegerRange modules, int latency) : base(context)
        {
            this._modules = modules;
            this._latency = latency;
        }

        public override void Run()
        {
            if (_modules == null) Context.Driver.Delay = _latency;
            foreach (var module in Context.Rack[_modules])
            {
                module.Latency = _latency;
            }
        }
    }
}