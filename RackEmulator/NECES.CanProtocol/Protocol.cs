﻿// (C) NECES All Rights Reserved.
// Protocol.cs, authored by: Andrew Schade
// Created: 08-04-2016; Last Updated: 08-05-2016 @ 1:55 PM

#region Imports

using System;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using NECES.CanDriver;

#endregion

namespace NECES.CanProtocol
{
    public class Protocol
    {
        private CanDriver.CanDriver _driver;
        public BalanceRequest BalanceRequest { get; set; }
        public CanDevice.BaudRate BaudRate { get; set; }

        public CanDriver.CanDriver Driver
        {
            get { return _driver; }

            set
            {
                _driver = value;
                PollRequest.Driver = value;
                PollResponse.Driver = value;
                BalanceRequest.Driver = value;
            }
        }

        public string Name { get; set; }
        public PollRequest PollRequest { get; set; }
        public PollResponse PollResponse { get; set; }

        public void StartListening()
        {
            Driver.CanOpen();
        }
    }

    public static class ProtocolFactory
    {
        public static CanDriver.CanDriver Driver { get; set; }

        public static Protocol ReadProtocol(StreamReader config)
        {
            var text = config.ReadToEnd();
            return ReadProtocol(text);
        }

        public static Protocol ReadProtocol(string text)
        {
            Protocol protocol = JsonConvert.DeserializeObject<Protocol>(text);
            protocol.Driver = Driver;
            return protocol;
        }
    }
}