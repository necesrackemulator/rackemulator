using System;
using System.Collections.Generic;
using System.Reflection;
using NECES.CanDriver;
using NECES.ModcanLG;
using NECES.Utils;

namespace NECES.CanProtocol
{
    public class LgResponseMessage
    {
        public IntegerRange CellRange { get; set; }
        public CanDriver.CanDriver Driver { get; set; }
        public int RootId {get; set;}
        public bool AddModuleId {get; set;}
        public AddressEncoder AddressEncoder { get; set; }
        public VoltageEncoder[] VoltageEncoders { get; set; }
        public TemperatureEncoder TemperatureEncoder { get; set; }
        public VoltageEncoder ModuleVoltEncoder {get; set; }
        public CountEncoder StmCountEncoder { get; set; }
        public Dictionary<string, FlagEncoder> SystemFlagEncoders { get; set; }
        public FlagEncoder BtdFlagEncoder { get; set; }
        public FlagEncoder VslodFlagEncoder { get; set; }
        public FlagEncoder SpidFlagEncoder { get; set; }
        public FlagEncoder SpFlagEncoder { get; set; }
        public FlagEncoder CedmFlagEncoder { get; set; }
        public FlagEncoder CfcdFlagEncoder { get; set; }
        public FlagEncoder AludFlagEncoder { get; set; }
        public VoltageEncoder RefVoltEncoder { get; set; }
        public FlagEncoder[] BalanceFlags { get; set; }
    }
}