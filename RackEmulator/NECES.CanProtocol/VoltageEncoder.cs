// (C) NECES All Rights Reserved.
// VoltageEncoder.cs, authored by: Andrew Schade
// Created: 08-04-2016; Last Updated: 08-05-2016 @ 1:13 PM

#region Imports

using NECES.Utils;

#endregion

namespace NECES.CanProtocol
{
    public class VoltageEncoder : IEncoder<ModuleDataType>
    {
        public int ErrValEncoded { get; set; }
//        public int Precision { get; set; }
        public int Length { get; set; }
        public int NoValEncoded { get; set; }
        public double Offset { get; set; }
        public double Scale { get; set; }
        public int StartBit { get; set; }

        public ModuleDataType DecodeFromBytes(byte[] data, bool fullDataGram)
        {
            int startByte = StartBit/8;
            int startBit = StartBit%8;

            var bytes = (startBit + Length - 1)/8; // Calculates how many different bytes are needed
            // to store the encoded data.
            int dataVal = 0;
            for (int i = startByte; i > startByte - bytes; i--)
            {
                dataVal |= data[i].Shift(8*(startByte - i));
            }
            dataVal &= (2 ^ (Length + 1)) - 1;
            if (dataVal == ErrValEncoded)
            {
                return new ModuleDataType(ModuleDataType.ValueFlags.ErrVal);
            }
            if (dataVal == NoValEncoded)
            {
                return new ModuleDataType(ModuleDataType.ValueFlags.NoVal);
            }

            double v = (dataVal*Scale + Offset)/1000.0;
            return new ModuleDataType(v);
        }

        public byte[] EncodeBytes(ModuleDataType value, bool fullDataGram)
        {
            int startByte = StartBit/8;
            int startBit = StartBit%8;
            int toStore;

            var dataGram = new byte[8];

            if (!fullDataGram)
            {
                var bytes = (startBit + Length - 1)/8; // Calculates how many extra bytes are needed
                dataGram = new byte[bytes + 1]; // to store data. Then shrinks dataGram down
                // to size.
                startByte = bytes;
            }


            if (value.Value != null)
            {
                toStore = (int) ((value.Value - Offset)/Scale)*1000;
            }
            else if (value.Flag == ModuleDataType.ValueFlags.ErrVal)
            {
                toStore = ErrValEncoded;
            }
            else //if (value.Flag == ModuleDataType.ValueFlags.NoVal)
            {
                toStore = NoValEncoded;
            }
            var bitsLeft = Length;
            int i = 0;
            while (bitsLeft > 0)
            {
                dataGram[startByte - i] = (byte) toStore.Shift(startBit);
                startBit -= 8;
                bitsLeft += startBit;
                i++;
            }
            return dataGram;
        }
    }
}