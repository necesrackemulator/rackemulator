using NECES.Utils;

namespace NECES.CanProtocol
{
    public class FlagEncoder : IEncoder<bool>
    {
        public int Location { get; set; }
        public byte[] EncodeBytes(bool value, bool fullDataGram)
        {
            int startByte = Location / 8;
            int startBit = Location % 8;
            var dataGram = new byte[8];
            if (!fullDataGram)
            {
                var bytes = 0;
                dataGram = new byte[bytes + 1];       // to store data. Then shrinks dataGram down
                                                      // to size.
                startByte = bytes;
            }
            var toStore = (value ? 1 : 0).Shift(startBit);
            dataGram[startByte] = (byte) toStore;
            return dataGram;

        }

        public bool DecodeFromBytes(byte[] data, bool fullDataGram)
        {
            int startByte = Location / 8;
            int startBit = Location % 8;
            return (data[startByte].Shift(-startBit) & 1) == 1;
        }
    }
}