using NECES.CanDriver;

namespace NECES.CanProtocol
{
    public interface IResponseMessage
    {
        CanMessage GenerateCanMessage(ModcanLG.Rack rack, int address);
        void SendCanMessage(ModcanLG.Rack rack, int address);
    }
}