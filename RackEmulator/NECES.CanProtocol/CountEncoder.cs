using NECES.Utils;

namespace NECES.CanProtocol
{
    public class CountEncoder : IEncoder<int>
    {
        public int StartBit;
        public int Length;
        public byte[] EncodeBytes(int value, bool fullDataGram)
        {
            int startByte = StartBit / 8;
            int startBit = StartBit % 8;

            var dataGram = new byte[8];

            if (!fullDataGram)
            {
                var bytes = (startBit + Length - 1) / 8; // Calculates how many extra bytes are needed
                dataGram = new byte[bytes + 1];       // to store data. Then shrinks dataGram down
                                                      // to size.
                startByte = bytes;
            }
            var toStore = value;

            var bitsLeft = Length;
            int i = 0;
            while (bitsLeft > 0)
            {
                dataGram[startByte - i] = (byte)toStore.Shift(startBit);
                startBit -= 8;
                bitsLeft += startBit;
                i++;
            }
            return dataGram;
        }

        public int DecodeFromBytes(byte[] data, bool fullDataGram)
        {
            int startByte = StartBit / 8;
            int startBit = StartBit % 8;

            var bytes = (startBit + Length) / 8;  // Calculates how many different bytes are needed
                                                  // to store the encoded data.
            int dataVal = 0;
            for (int i = startByte; i > startByte - bytes; i--)
            {
                dataVal |= data[i].Shift(8 * (startByte - i));
            }
            dataVal &= 1.Shift(Length) - 1;

            var v = dataVal;
            return v;
        }
    }
}