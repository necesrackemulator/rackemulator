﻿namespace NECES.CanProtocol
{
    public interface IEncoder<T>
    {
        byte[] EncodeBytes(T value, bool fullDataGram);
        T DecodeFromBytes(byte[] bytes, bool fullDataGram);
    }
}