using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using NECES.Utils;

namespace NECES.CanProtocol
{
    public class TemperatureEncoder : IEncoder<ModuleDataType>
    {
        public int StartBit { get; set; }
        public int Length { get; set; }
        public Dictionary<double, double> LookupTable;
        public byte[] EncodeBytes(ModuleDataType value, bool fullDataGram)
        {
            int toStore;
            if (value.Value != null)
            {
                var temp = value.Value.Value;

                var below = LookupTable.Values.Where(d => d <= temp).Max();
                var above = LookupTable.Values.Where(d => d >= temp).Min();

                if (Math.Abs(above - below) < 0.001) // Above and below are the same
                {
                    toStore = (int) LookupTable.First(pair => Math.Abs(pair.Value - below) < 0.001).Key;
                }
                else
                {
                    var front = LookupTable.First(pair => Math.Abs(pair.Value - below) < 0.001).Key;
                    var back = LookupTable.First(pair => Math.Abs(pair.Value - above) < 0.001).Key;
                    var slope = (back - front)/(above - below);
                    toStore = (int) ((temp - below)*slope + front);
                }
            }
            else if (value.Flag == ModuleDataType.ValueFlags.ErrVal)
            {
                toStore = 1;
            }
            else
            {
                toStore = 0;
            }

            int startByte = StartBit / 8;
            int startBit = StartBit % 8;

            var dataGram = new byte[8];

            if (!fullDataGram)
            {
                var bytes = (startBit + Length - 1) / 8; // Calculates how many extra bytes are needed
                dataGram = new byte[bytes + 1];       // to store data. Then shrinks dataGram down
                                                      // to size.
                startByte = bytes;
            }

            var bitsLeft = Length;
            int i = 0;
            while (bitsLeft > 0)
            {
                dataGram[startByte - i] = (byte)toStore.Shift(startBit);
                startBit -= 8;
                bitsLeft += startBit;
                i++;
            }
            return dataGram;

        }

        public ModuleDataType DecodeFromBytes(byte[] data, bool fullDataGram)
        {
            int startByte = StartBit / 8;
            int startBit = StartBit % 8;

            var bytes = (startBit + Length) / 8;  // Calculates how many different bytes are needed
                                                  // to store the encoded data.
            int dataVal = 0;
            for (int i = startByte; i > startByte - bytes; i--)
            {
                dataVal |= data[i].Shift(8 * (startByte - i));
            }
            dataVal &= 1.Shift(Length) - 1;

            var v = dataVal;

            if (v == 0)
            {
                return new ModuleDataType(ModuleDataType.ValueFlags.NoVal);
            }
            else if (v == 1)
            {
                return new ModuleDataType(ModuleDataType.ValueFlags.ErrVal);
            }
            else
            {
                var below = LookupTable.Keys.Where(d => d <= v).Max();
                var above = LookupTable.Keys.Where(d => d >= v).Min();
                if (Math.Abs(above - below) < 0.001) // Above and below are the same
                {
                    return new ModuleDataType((int) LookupTable.First(pair => Math.Abs(pair.Key - below) < 0.001).Value);
                }
                else
                {
                    var front = LookupTable.First(pair => Math.Abs(pair.Key - below) < 0.001).Value;
                    var back = LookupTable.First(pair => Math.Abs(pair.Key - above) < 0.001).Value;
                    var slope = (back - front) / (above - below);
                    return (int)((v - below) * slope + front);
                }
            }

        }
    }
}