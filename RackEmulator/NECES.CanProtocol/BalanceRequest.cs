namespace NECES.CanProtocol
{
    public class BalanceRequest
    {
        public int MsgId {get; set;}
        public AddressEncoder AddressEncoder { get; set; }
        public int Address { get; set; }

        public FlagEncoder[] BalanceFlags {get;set;}
        public CanDriver.CanDriver Driver { get; set; }
    }
}