namespace NECES.CanProtocol
{
    public class PollRequest
    {
        public int MsgId { get; set; }
        public AddressEncoder AddressEncoder { get; set; }

        public CanDriver.CanDriver Driver { get; set; }

//        public PollRequest(byte[] bytes, bool fullDataGram = true)
//        {
//            Address = AddressEncoder.DecodeFromBytes(bytes, fullDataGram);
//        }
    }
}