// (C) NECES All Rights Reserved.
// PollResponse.cs, authored by: Andrew Schade
// Created: 08-04-2016; Last Updated: 08-05-2016 @ 2:04 PM

#region Imports

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NECES.CanDriver;
using NECES.ModcanLG;
using NECES.Utils;

#endregion

namespace NECES.CanProtocol
{
    public class PollResponse
    {
        public CanDriver.CanDriver Driver { set; get; }
        public LgResponseMessage[] ResponseMessages { get; set; }
        public int SubModules { get; set; }

        public async Task SendResponse(Rack rack, int address)
        {
            try
            {
                address--;
                var messages = new CanMessage[ResponseMessages.Length];
                int msgNum;
                int modNum = address / SubModules;
                int subMod = address % SubModules;
                var module = rack[modNum];
                int cellNum = 0;

                for (msgNum = 0; msgNum < ResponseMessages.Length; msgNum++)
                {
                    if (messages[msgNum] == null)
                    {
                        messages[msgNum] =
                            new CanMessage(
                                ResponseMessages[msgNum].RootId + (ResponseMessages[msgNum].AddModuleId ? address : 0),
                                new byte[8]);
                    }
                    if (ResponseMessages[msgNum].CellRange != null)
                    {
                        int j = 0;
                        try
                        {

                            for (cellNum = ResponseMessages[msgNum].CellRange.Begin;
                                cellNum <= ResponseMessages[msgNum].CellRange.End && j < ResponseMessages[msgNum].VoltageEncoders.Length;
                                cellNum++)
                            {
                                messages[msgNum].Bytes.MergeBytes(
                                    ResponseMessages[msgNum].VoltageEncoders[j].EncodeBytes(
                                        module.Cells[cellNum + subMod * rack.NumCells / SubModules], true));
                                j++;
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine($"IOOR: msgNum: {msgNum}, cellNum: {cellNum}, j: {j}");
                        }
                    }
                    if (ResponseMessages[msgNum].ModuleVoltEncoder != null)
                    {
                        try
                        {
                            messages[msgNum].Bytes.MergeBytes(
                                               ResponseMessages[msgNum].ModuleVoltEncoder.EncodeBytes(module.ModVolt[subMod], true));
                        }
                        catch (Exception)
                        {

                            throw;
                        }
                    }
                    if (ResponseMessages[msgNum].TemperatureEncoder != null)
                    {
                        try
                        {
                            messages[msgNum].Bytes = messages[msgNum].Bytes.MergeBytes(
                                            ResponseMessages[msgNum].TemperatureEncoder.EncodeBytes(module.Therms[0].Value ?? 0, true));
                        }
                        catch (Exception e)
                        {

                            Console.WriteLine($"IOOR: msgNum: {msgNum}, cellNum: {subMod}");

                        }
                    }
                    if (ResponseMessages[msgNum].SystemFlagEncoders != null)
                    {
                        foreach (KeyValuePair<string, FlagEncoder> pair in ResponseMessages[msgNum].SystemFlagEncoders)
                        {
                            try
                            {
                                messages[msgNum].Bytes.MergeBytes(pair.Value.EncodeBytes(module.SystemFlags[pair.Key], true));
                            }
                            catch (KeyNotFoundException e)
                            {

                                throw;
                            }
                        }
                    }
                    if (ResponseMessages[msgNum].RefVoltEncoder != null)
                    {
                        try
                        {
                            messages[msgNum].Bytes.MergeBytes(ResponseMessages[msgNum].RefVoltEncoder.EncodeBytes(
                                                module.Ref2V, true));
                        }
                        catch (Exception e)
                        {

                            throw;
                        }
                    }
                    if (ResponseMessages[msgNum].StmCountEncoder != null)
                    {
                        try
                        {
                            messages[msgNum].Bytes.MergeBytes(
                                                ResponseMessages[msgNum].StmCountEncoder.EncodeBytes(module.StmCount, true));
                        }
                        catch (Exception e)
                        {

                            throw;
                        }
                    }
                }
                if (module.Latency == -1)
                {
                    return;
                }
                if (module.Latency > 0)
                {
                    await Task.Delay(module.Latency);
                }
                foreach (var canMessage in messages)
                {
                    Driver.CanDevice.WriteCanMessage(canMessage);
                }
            }
            catch (Exception e)
            {

                throw;
            }
        }
    }
}