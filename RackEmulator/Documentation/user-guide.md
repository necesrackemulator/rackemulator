---
title: Module Emulator User's Guide
...

# Introduction

This document outlines the commands in the `NECES.ModuleEmulator` platform and their usage.

It will outline when to use certain commands, their caveats, the information returned,
their development status, and several other things.

# Commands

Command              Category      Syntax
-------------------- ------------- --------------------------------------------------------
Help                 Help          `help`
Exit                 Exit          `quit`
Set System           Setup         `system (system name)`
Set CAN Device       Setup/CAN     `can[ ]dev (kvaser|canalyzer|null)`
Set Baud Rate        Setup/CAN     `can baud ###`
Open Channel         Setup/CAN     `can channel # open`
Close channel        Setup/CAN     `can channel # close`
Set Cell Voltage     Set/Mod       `set mod #[-#] cell #[-#] volt #[.##]`
Set Temperature      Set/Mod       `set mod #[-#] temp ##[.##]`
Set Module Voltage   Set/Mod       `set mod #[-#][.(a|b)] volt [##.[###]]`
Set Module Latency   Set/Mod       `set mod #[-#] latency ####`
Set Module Flags     Set/Mod       `set mod #[-#] (flag name) (on|off)`
Set Module Ref Volt  Set/Mod       `set mod #[-#] ref #[.###]`
Set General Latency  Set/General   `set latency ####`
Display Mod Volt     Display       `display [mod #[-#] [cell [#[-#]]]] volt`
Display Mod Temp     Display       `display [mod #[-#]] temp`
Display Mod Flags    Display       `display [mod #[-#]] flags`
Logfile Command      Log           `set log (file path)`
Log Enable Command   Log           `set log (on|off)`
Set Display Flags    Log           `set (log|console) display (debug|warning|error) (on|off)`
Script               Automation    `input "path/to/file"`


## Misc Commands

These are commands that don't fit into a specific category, namely the *Help* and *Exit*
commands.

### *Help* Command

The *Help* command is simple. Currently all it does is prints a copy of the above table,
formatted for easy reading in the command line.

**Syntax: `help`**

### *Exit* Command

In order to be comparable to the ModBusMonitor and RackMonitor programs, the *Exit* command
uses the *quit* keyword, which might be a bit confusing. But its behavior is straighforward.
It deinitializes the CAN bus, releases any open resources, and then exits with an error
code of 0.

**Syntax: `quit`**

## Setup Commands

These commands are used to set up the program so that it will communicate with the
SCSM. Here you define which system you are emulating, which can devices are being
used, and then open the channel.

You should follow the following order of operations.

1. Set System
2. Set CAN Device
3. Optionally change CAN parameters.
4. Open Channel
5. Set variables and run tests
6. Close channel
7. Exit.

### *Set System* Command

The *Set System* command reads from `config/config.csv` in order to find the system you've
specified. Once found, it uses the values in the file to define how many modules are in the
specified rack, along with the number of cells per module and the number of temperature
sensors in each module. It inializes the objects where this information is set, and then
returns.

**Syntax:** `system (system name)`
:   The system name here is the name as defined in the `config/config.csv` file.

### *Set CAN Device* Command

The *Set CAN Device* command chooses one of the pre-defined CAN device driver classes to
use for CAN communication in the program. It initializes the driver and ensures that all
CAN related settings are valid.

**Syntax:**  `can[ ]dev (kvaser|canalyzer|null)`
:   Specify the device. This command is essential for future execution of the code. The
    space between `can` and `dev` can be ommitted in order provide the RackMonitor-like
    syntax of `candev`.

### *Set Baud Rate* Command

The *Set Baud Rate* command overrides the default baud rate defined for the system test.
This command can be executed any time before opening the CAN channel.

**Syntax:** `can baud ###`
:   The baud rate you want to set in Kbaud. Allowable options are:

    * 125: 125 Kbaud
    * 250: 250 Kbaud
    * 500: 500 Kbaud
    * 1000: 1 Mbaud

### *Open CAN Channel* Command

The *Open CAN Channel* command opens the channel on the selected CAN device (Kvaser,
CANAlyzer, NULL) and starts a listener thread that will poll the device driver repeatedly, 
looping after the delay specified by the general latency command.

**Syntax: `can channel # open`**
:   Allowable channel #'s are currently 1 and 2. CAN channels reflect how they are displayed
    on the Kvaser or CANAlyzer devices.

### *Close CAN Channel* Command

The *Close CAN Channel* command stops the listener thread and then closes the channel on the
CAN driver.

**Syntax: `can channel # close`**
:   Allowable channel #'s are currently 0 and 1. Channels are currently 0-indexed, so if
    you are used to referring to the can channels as CAN 1 and CAN 2, bear that in mind.

## Set Commands

These commands are used to set values modelled within the program for use in Rack Emulations. Here
you can set the voltages, the temperatures and module latencies. All of these commands should only
be executed once the system is set and those data structures are initialized.

### *Set Cell Voltage* Command

The *Set Cell Voltage* command will set the voltage to a specified value for the chosen cell on the chosen module.
It can also set the sepecified voltage for a set of cells speficied by a range of indexes on both cells and
modules.

**Syntax `set mod #[-#] cell #[-#] volt #[.###]`**
:   Each of the `#[-#]` sections indicate an optional range value, which means you can either specify that you
    want to reference module 4, or modules 3-6. Modules and cells are both currently 0-indexed, and ranges are
    inclusive.

    Voltage is specified in volts and can be any decimal value that can be encoded into the CAN register by
    the device.

    **Example Set Cell Voltage Commands**
    ~   `set mod 0-13 cell 0-6 volt 3.333` This would set every cell in an optimus system to have a voltage of
        3.333 volts.

    ~   `set mod 1 cell 3-6 volt 4.5` This would set cells 3-6 (4-7 in an 1-indexed world) on module 1 (2) of
        the rack to have a voltage of 4.5 volts.

### *Set Temperature* Command

Like with the *Set Cell Voltage* command, the *Set Temperature* command sets the values of the temperature sensors
emulated by the program to be the specified temperature, for a module or on a range of modules. Currently
only one temperature sensor per module is supported. If there are more than one temperature sensors, then
all of them will be set with the specified value.

**Syntax `set mod #[-#] temp [-]##[.##]`**
:   The `#[-#]` section indicates an optional range value, which means you can either specify that you
    want to reference module 4, or modules 3-6. Modules are currently 0-indexed, and ranges are inclusive.

    Temperature is specified in degrees ceslius and can be any decimal value that can be encoded into the CAN register by
    the device. It can be positive or negative.

    **Example Set Temperature Commands**
    ~   `set mod 0-13 temp -12.5` This would set every temperature sensor in an optimus system to have a temperature
        of $-12.5^\circ C$.

    ~   `set mod 1 temp 33.5` This would set the temperature on module 1 (2 in an 1-indexed world) of
        the rack to be $33.5^\circ C$.

### *Set Module Voltage* Command

The *Set Module Voltage* Command is used to override the calculated module voltage.
By default, the module calculates the module's overall voltage as the sum of cell
voltages and this command allows the user to force there to be a difference between
the sum of the cell voltages and the module voltage sent to the SCSM.

Because the system is logically represented with two submodules, rather than having
every MBB be seperate modules in the code, the module voltage must be set on a submodule
basis. Therefore the syntax for this command is a little more complicated.

**Syntax `set mod #[-#][.(a|b)] volt [##.[###]]`**
:   The syntax for this command is more complicated due to the submodule communication
    of module voltage. You can choose a single module or range of modules as in the
    previous commands. However, now you can also specify which submodule you want to
    specify the voltage for. In this case if you specify submodule `a`, for example,
    you will only set the module voltage for the first submodule on each of the specified
    modules.

    You can also omit that, and have the code automatically split the voltage between the
    two submodules. Both submodules will get an equal share of the voltage.

    To reset the module voltages, that is, to stop overriding the voltage, call the command
    without specifying a voltage argument.

    **Examples**
    ~   `set mod 0.a volt 25.677`: This command sets the voltage on the first submodule
        of the first module to 25.677 Volts.

    ~   `set mod 0-1 volt 49.232`: This command sets the voltage for the first and second
        modules to be 49.232 Volts. Each submodule will have a voltage of 24.616 Volts in
        this situation.

    ~   `set mod 0-4b volt`: This command causes the voltage to be calculated normally
        for the first submodule on the first 5 modules.

### *Set General Latency* Command

The *Set General Latency* command will set the polling delay to the specified length.

**Syntax `set latency ###`**
:   Latency is expressed in milliseconds.

### *Set Module Latency* Command

The *Set Module Latency* command will set the delay between when a module's data is requested and when it posts it to
the CAN bus. This, like with the the other module-level set commands, can be set for a single module or a range of
them at the same time.

**Syntax `set mod #[-#] latency ###`**
:   Latency is expressed in milliseconds. The `#[-#]` section indicates an optional range value, which means you can either specify that you
    want to reference module 4, or modules 3-6.

### *Set Module Flags* Command

The *Set Module Flags* command will set the specified flag on the specified module to `true`{.cs} or `false`{.cs} as specified.

**Syntax `set mod #[-#] (flag name) (on|off)`**
:   The `#[-#]` section indicates an optional range value, which means you can either specify that you
    want to reference module 4, or modules 3-6. Modules are currently 0-indexed, and ranges are inclusive.

    Allowable flags are as follows:

    * `btdf`: Balancing transistor diagnosis flag.
    * `vslodf`: Voltage sensing line open diagnosis flag.
    * `spidf`: SPI diagnosis flag.
    * `spf`: Second protection flag.
    * `cedmf`: Calibration EEPROM data checksum mismatch flag.
    * `cfcdf`: Code flash checksum diagnosis flag.
    * `aludf`: ALU diagnosis flag.

    `on` will activate the flag, and `off` will deactivate the flag.

### *Set Module Ref Volt* Command

The *Set Module Ref Volt* command will set the voltage for the specified module or range of modules
Reference 2V voltage field.

**Syntax `set mod #[-#] ref #[.###]`**
:   The `#[-#]` section indicates an optional range value, which means you can either specify that you
    want to reference module 4, or modules 3-6. Modules are currently 0-indexed, and ranges are inclusive.

    Voltage is specified in volts and can have any decimal value that can be encoded onto the CAN register
    using the methods built into the program.

    **Example Set Ref Volt Commands**
    ~   `set mod 0-13 ref 4` This would set every reference voltage on an optimus system to 4 volts.

    ~   `set mod 1 temp 3.33` This would set the reference voltage on module 1 (2 in an 1-indexed world) of
        the rack to be 3.33 volts.

## Display Commands

These commands are used to output the data stored by the rack in order to confirm that it matches
with the inputs that you've put in, and that it matches with the output from the SCSM.

### *Display Mod Temp* Command

The *Display Mod Temp* command allows for the display of the temperature of each
module's thermistor, either on at a time, or through a range of modules, or for all modules.

**Syntax `display [mod #[-#]] temp`**
:   The whole module clause is optional, but otherwise would behave just as if it were in a set command.

### *Display Mod Volt* Command

The *Display Mod Volt* command allows for the diplay of the temperature of each cell's voltage, in each
module. You can choose to display a range of cells over a range of modules, just as before, or all cells
on a single module or range of modules, or all cells on all modules.

If all cells are displayed for a module, the module's overall voltage will also be displayed.

**Syntax `display [mod #[-#] [cell [#[-#]]]] volt`**
:   The whole specification of which modules to display is optional. However, you can only specify which
    cells if there is a specification for which modules are to be displayed.

### *Display Mod Flags* Command

The *Display Mod Flags* commands allows for the display of the reference voltage and system flags for each
module, either as a group or on their own.

**Syntax `display [mod #[-#]] flags`**
:   The whole module clause is optional, but otherwise would behave just as if it were in a set command.

## Output Commands

These commands allow for the user to specify how output is displayed and logged.

### *Logfile* Command

The logfile command allows the user to specify which file to log to. No logfile is specified by default, so one 
needs to specified their own logfile in order to begin logging.

**Syntax `set log (file path)`**
:   The file path for the logfile can either be relative or specified starting from C:.

### *Log Enable* Command

The *Log Enable* command allows a user to enable or disable logging from ModuleEmulator.

**Syntax `set log (on|off)`**

## Automation Commands

These commands are used to provide an interface with which to automate testing using
the ModuleEmulator application.

### *Script* Command

The *Script* command loads a script file. The file specified will be run, line by line.
