﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NECES.CanProtocol;

namespace NECES.CanProtocolTests
{
    [TestClass()]
    public class EncoderTests
    {
        [TestMethod()]
        public void ReadProtocolTest()
        {
            var config = new StreamReader(File.OpenRead("C:/Users/aschade/Documents/NECES/RackEmulator/NECES.CanProtocol/Configs/lgCan.json"));
            var protocol = ProtocolFactory.ReadProtocol(config);
            config.Close();
            Assert.AreNotEqual(0, protocol.MessageTemplates["Rpm"].MsgId);
            ((IntegerData) protocol.MessageTemplates["Rpm"].DataTemplates["UnitID"]).Data = 12;
            var bytes = protocol.MessageTemplates["Rpm"].DataTemplates["UnitID"].EncodeDataToBytes();
            foreach (var messageTemplate in protocol.MessageTemplates.Values)
            {
                Console.WriteLine($"MessageTemplate: {messageTemplate.Name} ID {messageTemplate.MsgId}:");
//                foreach (var sendBytes in messageTemplate.GenerateMessage().Bytes)
//                {
//                    Console.WriteLine($"\t Data Byte: {Convert.ToString(sendBytes, 2)}");
//                }

                foreach (var dataTemplate in messageTemplate.DataTemplates.Values)
                {
                    Console.WriteLine($"\tDataTemplate: {dataTemplate.Name}: {dataTemplate.GetDataType()}");
                    Console.Write($"\t\tStartBit: {dataTemplate.Encoder.StartBit} ");
                    if (!dataTemplate.GetDataType().Equals(typeof(bool).ToString()))
                    {
                        Console.WriteLine($"Length: {dataTemplate.Encoder.Length} Offset: {dataTemplate.Encoder.Offset} Scale: {dataTemplate.Encoder.Scale}");
                    }
                    else { Console.WriteLine();}
                }
            }
        }

        private readonly int _startBit = 20;
        private int _length = 6;
        private int _scale = 1;
        private int _offset = 0;

        [TestMethod()]
        public void EncodeIntDataTestTrimmed()
        {
            Encoder intEncoder = new Encoder(_startBit,_length, _scale,_offset);
            IntegerData data = new IntegerData(45, intEncoder);
            var bytes = data.EncodeDataToBytes();

//            Console.WriteLine(JsonConvert.SerializeObject(data));

            Assert.AreEqual(0xD0, bytes[1]);
            Assert.AreEqual(2, bytes[0]);
        }

        [TestMethod()]
        public void EncodeIntDataTestFull()
        {
            Encoder intEncoder = new Encoder(_startBit,_length,_scale,_offset);
            IntegerData data = new IntegerData(45, intEncoder);
            var bytes = data.EncodeDataToBytes(true);

            Assert.AreEqual(0xD0, bytes[_startBit/8]);
            Assert.AreEqual(2, bytes[_startBit/8 - 1]);
        }

        [TestMethod()]
        public void EncodeBoolFlagTrimmed()
        {
            var boolEncoder = new Encoder(_startBit, _length, _scale, _offset);
            var data = new BooleanData(true, boolEncoder);
            var bytes = data.EncodeDataToBytes();


            Assert.AreEqual(0x10, bytes[1]);
        }

        [TestMethod()]
        public void EncodeBoolFlagFull()
        {
            var boolEncoder = new Encoder(_startBit, _length, _scale, _offset);
            var data = new BooleanData(true, boolEncoder);
            var bytes = data.EncodeDataToBytes(true);

            Assert.AreEqual(0x10, bytes[_startBit / 8]);
        }
    }
}