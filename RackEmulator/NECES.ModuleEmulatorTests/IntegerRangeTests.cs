﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NECES.ModuleEmulator;
using NECES.Utils;

namespace NECES.ModuleEmulatorTests
{
    [TestClass()]
    public class IntegerRangeTests
    {
        [TestMethod()]
        public void IntegerRangeTest()
        {
            var IR = new IntegerRange();
            Assert.AreEqual(0, (int) IR);
        }

        [TestMethod()]
        public void IntegerRangeTest1()
        {
            var IR = new IntegerRange(1,5);
            Assert.AreEqual(1, IR.Begin);
            Assert.AreEqual(5, IR.End);
        }

        [TestMethod()]
        public void ToStringTest()
        {
            var IR = new IntegerRange(1, 5);
            Assert.AreEqual("1-5", IR.ToString());
            IR = new IntegerRange();
            Assert.AreEqual(0.ToString(), IR.ToString());
        }

        [TestMethod()]
        public void ParseTest()
        {
            var IR = IntegerRange.Parse("1-5");
            Assert.AreEqual("1-5", IR.ToString());
            IR = IntegerRange.Parse("2 -    6");
            Assert.AreEqual("2-6",IR.ToString());
            IR = IntegerRange.Parse("5");
            Assert.AreEqual("5", IR.ToString());
        }
    }
}