# Language Usage

## General

 1. Do not omit access modifiers. Explicitly declare all identifiers with the
    appropriate access modifier instead of allowing the default.

    Example
      ~ ````cs
        // Bad!
        void WriteEvent(string message) {...}
        ````
      ~ ````cs
        // Good
        private void WriteEvent(string message) {...}
        ````
 1. Do not use the default ("1.0.*") versioning scheme. Increment the
    `AssemblyVersionAttribute` value manually.
 1. Set the `ComVisibleAttribute` to false for all assemblies.
 1. Only selectively enable the `ComVisibleAttribute` for individual classes
    when needed.

    Example
      ~ ````cs
        [assembly: ComVisible(false)]

        [ComVisible(true)]
        public class MyClass {...}
        ````
 1. Consider factoring classes containing `unsafe`{.cs} code blocks into a
    seperate assembly.
 1. Avoid mutual references between assemblies.

## Variables & Types

 1. Try to initialize variables when you declare them.
 1. Always choose the simplest data type, list, or object required.
 1. Always use the built in C# data type aliases rather than the .NET common
    type system (CTS).

    Example
      ~ `short`{.cs} NOT `System.Int16`{.cs}
      ~ `int`{.cs} NOT `System.Int32`{.cs}
      ~ `long`{.cs} NOT `System.Int64`{.cs}
      ~ `string`{.cs} NOT `System.String`{.cs}
 1. Only declare member variables as `private`{.cs}. Use properties to provide
    access to them with public, protected, or internal access modifiers.
 1. Try to use `int`{.cs} for any non-fractional numeric values that will fit
    the `int`{.cs} data type -- even variables for non-negative numbers.
 1. Use `long`{.cs} only for variables potentially containing values too large
    for an `int`{.cs}
 1. Try to use `double`{.cs} for fractional numbers to ensure decimal precision
    in calculations
 1. Only use `float`{.cs} foor fractional numbers that will not fit
    `double`{.cs} or `decimal`{.cs}.
 1. Avoid using `float`{.cs} unless you fully understand the implications upon
    any calculations (referring to floating point errors).
 1. Try to use `decimal`{.cs} when fractional numbers must be rounded to a
    fixed precision for calculations. Typically this will involve money.
 1. Avoid using `sbyte`{.cs}, `short`{.cs}, `uint`{.cs}, and `ulong`{.cs},
    unless it is for interop with native libraries, or embedded systems.
 1. Avoid specifying the type for an `enum`{.cs} -- use the default of
    `int`{.cs} unless you have an explicit need for `long`{.cs} (very
    uncommon).
 1. Avoid using inline numeric literals (magic numbers). Instead use a
    Constant, or an Enum. But don't use something like `Constants.One`{.cs}, as
    that eliminates all refactoring benefits of having eliminated magic numbers.

    Instead, use constant values that refer to the meaning of the number, not
    the number itself. If you are initializing iterators, however, use `0` as a
    literal, it isn't worth attempting to do anything else.
 1. Avoid delcaring string literals inline. Instead use resources, constants,
    configuration files, the registry, or other data sources.
 1. Declare `readonly`{.cs} or `static readonly`{.cs} variables instead of
    constants for complex types.
 1. Only declare constants for simple types.
 1. Avoid direct casts. Instead use the `as`{.cs} operator and check for
    `null`{.cs}.

    Example
      ~ ````cs
        object dataObject = LoadData();
        DataSet ds = dataObject as DataSet;

        if(ds != null) {...}
        ````
 1. Always prefer C# generic collection types over standard or strongly-typed
    collections.
 1. Always explicitly initialize arrays of reference types using a for loop.
 1. Avoid boxing and unboxing value types.

    Example
      ~ ````cs
        int count = 1;
        object refCount = count; // Implicitly boxed
        int newCount = (int)refCount; // Explicitly unboxed
        // Bad!
        ````
 1. Floating point values should include at least one digit before the decimal
    place and one after.

    Example
      ~ `totalPercent = 0.05;`{.cs}
 1. Try to use the "@" prefix for string literals instead of escaped strings.
 1. Prefer `String.Format()`{.cs} or `StringBuilder`{.cs} over concatenation.
 1. Prefer formatted strings using the `$` prefix over `String.Format`{.cs}
    when embedding values into strings.
 1. Never concatenate strings inside a loop.
 1. Do not compare strings to `String.Empty` or `""`{.cs} to check for empty
    strings. Instead, compare by using `String.Length == 0`{.cs}.
 1. Avoid hidden string allocations within a loop.

## Functions and Methods

 1. Avoid `ref`{.cs} or `out`{.cs} parameters when possible. These indicate
    that the function does not follow the principle of single responsibility.


## Flow Control

 1. Avoid invoking methods within a conditional expression.
 1. Avoid creating recursive methods. Use loops or nested loops instead, when
    possible.
 1. Avoid using `foreach`{.cs} to iterate over immutable value-type
    collections. E.g. `string`{.cs} arrays.
 1. Do not modify enumerated items within a `foreach`{.cs} statement.
 1. Avoid complex or compound ternary operations.
 1. Avoid evaluating boolean conditions against `true`{.cs} or `false`{.cs}.
 1. Avoid assignment within conditional statements.
 1. Avoid compound conditional expressions -- use boolean variables to split
    parts into multiple manageable expressions.
 1. Only use switch/case statements for simple operations with parallel
    conditional logic.
 1. Prefer nested if/else over switch/case for short conditional sequences and
    complex conditions.
 1. Prefer polymorphism over switch/case to encapsulate and delegate complex
    operations.
 1. Prefer to do simple `null`{.cs}-checking with the `?` operator rather than
    with `if`{.cs}/`else`{.cs} statements.


## Exceptions

 1. Do not use try/catch blocks for flow control.
 1. Only catch exceptions that you can handle.
 1. Never declare an empty catch block.
 1. Avoid nesting a try/catch block within a catch block.
 1. Always catch the most derived exception via exception filters.
 1. Order exception filters from most to least derived exception type.
 1. Avoid re-throwing an exception. Allow it to bubble-up instead.
 1. If re-throwing an exception, preserve the original call stack by ommiting
    the exception argument from the `throw`{.cs} statement.

    Example
      ~ ````cs
        // Bad!
        catch(Exception ex)
        {
          Log(ex);
          throw ex;
        }
        ````
      ~ ````cs
        // Good!
        catch(Exception ex)
        {
          Log(ex);
          throw;
        }
        ````
 1. Only use the finally block to release resources from a try statement.
 1. Always use validation to avoid exceptions.
 1. Always set the innerException property on thrown exceptions so the
    exception chain and call stack are maintained.
 1. Avoid defining custom exception classes. Use existing exception classes
    where possible instead.
 1. When a custom exception is required
     a. Always derive from `Exception`, not `ApplicationException`
     b. Always suffix exception names with the word "Exception"
     c. Always add the `SerializableAttribute` to exception classes.
     d. Always implement the standard "Exception Constructor Pattern:

        ````cs
        public MyCustomException();
        public MyCustomException(string message);
        public MyCustomException(string message, Exception innerException);
        ````
     e. Always implement the deserialization constructor:

        `protected MyCustomException(SerializationInfo info, StreamingContext context);`{.cs}
 1. Always set the appropriate `HResult` value on custom exception classes.
 1. When defining custom exception classes that contain additional properties:
     a. Always override the `Message` property, the `ToString()` method and the
        **implicit operator string** to include custom property values.
     b. Always modify the deserialization constructor to retrieve custom
        property values.

## Events, Delegates, And Threading

 1. Always check Event and Delegate instances for `null`{.cs} before invoking.
 1. Use the default `EventHandler` and `EventArgs` for most simple events, when
    possible.
 1. Always derive a custom `EventArgs` class to provide additional data.
 1. Use the existing `CancelEventArgs` class to allow the event subscriber to
    control events.
 1. Always use the `lock`{.cs} keyword instead of the `Monitor` type.
 1. Only lock a private or static object.
 1. Avoid locking on a Type.
 1. Avoid locking on the current object instance (`this`{.cs}).

## Object Composition

 1. Always declare types explicitly within a namespace. Never use the default
    "{global}" namespace.
 1. Avoid overuse of the `public`{.cs} access modifier. Typically fewer than
    10% of your types and members will be a part of the public API, unless you
    are writing a class library.
 1. Consider using `internal`{.cs} or `private`{.cs} access modifiers for types
    and members unless you intend to support them as part of a public API.
 1. Never use the `protected`{.cs} access modifier within `sealed`{.cs} classes
    unless overriding a `protected`{.cs} member of an inherited type.
 1. Avoid declaring methods with more than 5 parameters. Consider refactoring
    this code.
 1. Try to replace large parameter-sets ($>5$ parameters) with one or more
    `class`{.cs} or `struct`{.cs} parameters -- especially when used in
    multiple method signatures.
 1. Do not use the "`new`{.cs}" keyword on method and property declerations to
    hide members of a derived type.
 1. Only use the `base`{.cs} keyword when invoking a base class constructor or
    base implementation within an override.
 1. Consider using method overloading instead of the `params`{.cs} attribute
    (but be careful not to beeak CLS compliance of your APIs).
 1. Always validate parameter values before consuming them.
 1. Consider overriding `Equals()` on a Struct.
 1. Always overrid the Equality Operator (`==`) when overriding the `Equals()`
    method.
 1. Always override the string implicit operator when overrriding the
    `ToString()` method.
 1. Always call `Close()` or `Dispose()` on classes that offer it.
 1. Wrap instatiation of `IDisposable` objects with a `using`{.cs} statement
    to ensure that Dispose is automatically called.

    Example
      ~ `using(SqlCOnnection cn = new SQLConnection(connectionString)) {...}`{.cs}
 1. Always implement the `IDisposable` interface and pattern on classes
    referencing external resources.

    Example (shown with optional Finalizer)
      ~ ````cs
        public void Dispose()
        {
          Dispose(true);
          GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
          if(disposing)
          {
            // Free other state (managed objects).
          }
          // Free your own state (unmanaged objects)
          // Set large fields to null
        }

        // C# finalizer (optional)
        ~Base()
        {
            Dispose(false);
        }
        ````
 1. Avoid implementing a Finalizer. \
    Never define a `Finalize()` method as a finalizer. Instead use the C#
    destructor syntax.

    Example
      ~ `~MyClass() {...}`{.cs}
