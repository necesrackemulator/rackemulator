# Object Model and API Design

In this section, we discuss the design standards for application, library, and
module development.

## Always prefer aggregation over inheritance

While OOP programming often extolls the virtues of class extensibility, it is
often counter-productive to make everything a subclass. Here, we should only
use the inheritance model, by extending an interface or abstract class, when
almost all of the functionality of two different data types should be
reproduced.

Aggregation means that you will always prefer to put as little requirements
into base classes as you can. Because eventually, you will extend said base
class with a sub-type that doesn't need that behavior. But if only the
absolutely necessary behaviors are specified in the base interface or class,
then you won't have a situation where you will want only some of the
functionality.

## Avoid "Premature Generalization"

Create abstractions only when the intent is clearly understood. This means
that your inheritance trees must really make sense. If you cannot say that
"Type B" is a kind of "Type A" then you should not have "Type B" extend
"Type A".

This also means that you shouldn't necessarily have base classes for everything
that happens to be related in the real world. Unless a Car and a Plane have a
significant amount of shared behavior and might need to be interchangeable in
your library, you probably don't have to create a Vehicle class that has
Car and Plane extend it.

## Do the simplest thing that works, then refactor when necessary

The first priority is functionality. If you can create a simple,
straightforward, implementation (that follows good coding practices) it
doesn't matter how extensible it is at first. You can worry about that
when you are developing more code later. If you need extensible functionality,
if you have the time to future-proof, or if you want it to run better, then you
can come back to the code and refactor it to work better.

This also means that you shouldn't write code that uses hacky or esoteric
means to get what you want. You need to write the functionality simply, using
as basic techniques as can fit the design specification.

## Always make object-behavior transparent to API consumers

The API should always be properly written and documented so that someone
developing with just the binary and a help-file would be able to understand how
to achieve their desired functionalities with the library.

You need to always warn the user of any side effects of API calls and specify
how to access that information. Each API class and method needs to be up front
about what behaviors and data it can be used for.

API classes and methods also need to be properly organized so that the location
of all these API calls is easy to work out. Even if a developer is not familiar
with the library that they are using, they should be able to find the correct
method to accomplish a common or simple task without consulting the API
specification or documentation.

## Avoid unexpected side-effects

All functions, properties, constructors, etc, should have exactly one purpose.
That means that if, for example, one calls `GetValue()`, then the only
behavior that should follow is that the value is returned. Unless properly
documented, the state of the program overall should not change here.

Similarly a function with one specified behavior should not cause side-effects
where other parts of the API are changed. In general, if someone with no
understanding of the API were to read the function signature, they should be
able to guess exactly what the function does.

## Always seperate presentation layer from business logic

The user-facing presentation layer---where all the UI, logging, input, and
output methods are---shouldn't have any knowledge of or direct ability to
change the the underlying model. Same for the business logic. Your model
code shouldn't ever call methods like `Console.WriteLine()` or *trigger
output of any kind*. Instead, it should raise events or throw errors that
get caught by the presentation layer, which will then provide the appropriate
behaviors.

## Always prefer interfaces over abstract classes

Because an object can implement multiple interfaces, it usually will create
more flexible code if most base types are intefaces. Remember, an interface
only specifies behavior, while not having any data types attached.

Example
  ~ If you have a `Car` class and a `Plane` class, with the following members
    (for the sake of the example, I'm ignoring accessability, type and 
    implementation):
    
    ```{.cs}
    class Car
    {
        State;
        TurnOnEngine();
        TurnOffEngine();
        Drive();
        Lock();
        Unlock();
        ...
    }
    ```

    ```{.cs}
    class Plane
    {
        State;
        TurnOnEngine();
        TurnOffEngine();
        Drive();
        TakeOff();
        Land();
        Fly();
        ...
    }
    ```

    Now, you could define an abstract class, `Vehicle`, and give it the
    `State`, `TurnOnEngine`, `TurnOfEngine`, and `Drive` members, but that
    would make it difficult to properly implement a `Helicopter` class. The
    Helicopter would want to share some of the functions present in `Plane`,
    but not in `Car`, one might make an extension of `Vehicle` called
    `FlyingVehicle`, with the `TakeOff`, `Land`, and `Fly` methods. That class
    would also be abstract.

    But then you'd have to implement `Drive()` for the `Helicopter`, which
    doesn't make sense.

    However, if you had, instead, a `IEngine` interface with `TurnOnEngine` and
    `TurnOffEngine`, a IDrivable interface with `Drive`, an `ILockable` 
    interface, etc., Then all of these can be implemented pretty efficiently.

## Try to include the design-pattern names as a suffix to class names where appropriate

This helps with the self-documenting aspect of the code. If you are looking for
a specific class, it helps when its named according to some well-defined
structure. If, for example, you are looking for a class that executes a
specific command, it is useful when it called `*name of command*Command`.

## Only make members virual if they are designed and tested for extensibility



 1. Refactor often!