# Coding Style

Coding style causes the most inconsistency and controversy between developers.
Each developer has a preference and rarely are two the same. However,
consistent layout, format, and organization are key to creating maintainable
code. THe following sections describe the preferred way to implement C# source
code in order to create readable, clear, and consistent code that is easy to
understand and maintain.

## Formatting

 1. Never declare more than 1 namespace per file.
 1. Avoid putting multiple classes in a single file.
 1. Always place curly braces (`{` and `}`) on a new line.
 1. Always use curly braces (`{` and `}`) in conditional statements.
 1. Always use a Tab & Indentation size of $4$.
 1. Declare each variable independently -- not in the same statement.
 1. Place namespace "`using`" statements together at the top of file.
    Group .NET namespace above custom namespaces.
 1. Group internal class implementation by type in the following order.
     a. Member variables
     b. Constructors & Finalizers
     c. Nested Enums, Structs, and Classes
     d. Properties
     e. Methods
 1. Sequence Declerations within type groups base on access modifier and
    visibility.
     a. Public
     b. Protected Internal (allows extension classes, as well as classes
        within the same namespace to access member)
     c. Protected
     d. Internal
     e. Private
 1. Segregate interface implementation by using `#region`{.cs} directives.
 1. Append folder-name to namespace for source files within sub-folders.
 1. Recursively indent all code blocks contained within braces.
 1. Use white space liberally to seperate and organize code.
 1. Only declare related `attribute` declerations on a single line, otherwise
    stack each attribute as a seperate decleration.

    Example
      ~ ````cs
        // Bad!
        [Attribute1, Attribute2, Attribute3]
        public class MyClass {...}

        // Good!
        [Attribute1, RelatedAttribute]
        [Attribute2]
        [Attribute3]
        public class MyClass {...}
        ````
 1. Place Assembly scope `attribute` declerations on a seperate line.
 1. Place Type scope `attribute` declerations on a seperate line.
 1. Place Method scope `attribute` declerations on a seperate line.
 1. Place Member scope `attribute` declerations on a seperate line.
 1. Place Parameter scope `attribute` declerations on a seperate line.
 1. If in doubt, always err on the side of clarity and consistency.

## Code Commenting

 1. All comments should be written in English, be grammatically correct, and
    contain appropriate punctuation.
 1. Use `//`{.cs} or `///`{.cs} but never `/* ... */`{.cs}.
 1. Always begin a comment block with a space.

    Example
     ~ `//BAD!`{.cs}
     ~ `// Good!`{.cs}
 1. Do not "flowerbox" comment blocks.

    Example
      ~ ````cs
        //****************
        // Comment Block
        //****************
        ````
 1. Use inline-comments to explain assumptions, known issues, and algorithm
    insights.
 1. Do not use inline-comments to explain obvious code. Well written code is
    self documenting.
 1. Only use comments for bad code to say "fix this code" -- otherwise remove,
    remove or rewrite the code!
 1. Include comments using Task-List keyword flags to allow comment filtering.

    Example
      ~ ````cs
        // TODO: Place Database Code Here.
        // UNDONE: Removed P\Invoke call due to errors.
        // HACK: Temporary fix until able to refactor.
        ````
 1. Always apply C# XML Comment-blocks (`///`{.cs}) to `public`, `protected`
    and `internal` declarations.
 1. Only use C# XML comment-blocks for documenting the API.
 1. Always include `<summary>` comments. Include `<param>`, `<return>`, and
    `<exception>` comment sections where applicable.
 1. Include `<see cref=""/>` and `<seeAlso cref=""/>` where possible.
 1. Always add **CDATA** tags to comments containing code and other embedded
    markup in order to avoid encoding issues.

    Example
      ~ ````cs
        /// <example> 
        /// Add the following key to the “appSettings” section of your config:
        /// <code><![CDATA[
        ///   <configuration>
        ///       <appSettings>
        ///           <add key=”mySetting” value=”myValue”/>
        ///       </appSettings>
        ///   </configuration>
        ///]]></code>
        /// </example>
        ````
