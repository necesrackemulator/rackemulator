# Naming Conventions

Consistency is the key to maintainable code. This statement is most tru for
naming your projects, source filees, and identifiers, including fields,
variables, properties, classes, interfaces, and namespaces.

## General Guidelines

 1. Always use Camel Case or Pascal Case Names.
 1. Avoid ALL CAPS and all lowercase names. Single lowercase words or letters
    are acceptable.
 1. Do not create declerations of the same type (namespace, class, method,
    property, field, or parameter) and access modifier (`protected`, `private`,
    `public`, `internal`) that vary only by capitalization.
 1. Do not use names that begin with a numeric character.
 1. Do add numeric suffixes to identifier names (when neccessary).
 1. Always choose meaningful and specific identifier names.
 1. Always err on the side of verbosity rather than terseness.
 1. Variable and property names should describe an entity not the type or size.
 1. Do not use Hungarian Notation!

    Example:
      ~ `strName` or `iCount`

 1. Avoid using abbreviations unless the full name is excessive.
 1. Avoid abbreviations longer than 5 characters.
 1. Any abbreviations must be widely known and accepted.
 1. Use uppercase for two-letter abbreviations, pascal case for logner
    abbreviations.
 1. Do not use C# reserved words as names.
 1. Avoid naming conflicts with existing .NET framework namespaces or types.
 1. Avoid adding redundant or meaningless prefixes and suffixes to identifiers.

    Example:
      ~ ````cs
        //Bad!
        public enum ColorsEnum {...}
        public class CVehicle {...}
        public struct RectangleStruct {...}
        ````

 1. Do not include the parent class name within a property name.

    Example:
      ~ `Customer.Name` NOT `Customer.CustomerName`

 1. Try to prefix boolean variables and properties with `Can`, `Is`, or `Has`.
 1. Append computation qualfiers to variable names like `Average`, `Count`,
    `Sum`, `Min`, and `Max` where appropriate.
 1. When defining a root namespace, use a product, company, or developer name
    as the root.

    Example:
      ~ `Neces.StringUtilities` or `RoddenCan.Application`

## Name Usage and Syntax

+------------------+----------------------------------------------------------+
|Identifier        | Naming Convention                                        |
+==================+==========================================================+
| **Project File** | | Pascal Case                                            |
|                  | | Always match Assembly Name & Root Namespace.           |
|                  |                                                          |
|                  | Example                                                  |
|                  |   ~ ```cs                                                |
|                  |     Neces.Web.csproj -> Neces.Web.dll -> namespace Neces.Web |
|                  |     ```                                                  |
+------------------+----------------------------------------------------------+
| **Source File**  | | Pascal Case                                            |
|                  | | Always match Class name and file name.                 |
|                  |                                                          |
|                  | Avoid including more than one `Class`, `Enum` (global),  |
|                  | or `Delegate` (global) per file. Use a descriptive file  |
|                  | name when containing multiple `Class`, `Enum`, or        |
|                  | `Delegates`                                              |
|                  |                                                          |
|                  | Example                                                  |
|                  |   ~ ```cs                                                |
|                  |     MyClass.cs => public class MyClass {...}             |
|                  |     ```                                                  |
+------------------+----------------------------------------------------------+
| **Resource** or  | Try to use Pascal Case.                                  |
| **Embedded       |                                                          |
| File**           | Use a name describing the file contents.                 |
+------------------+----------------------------------------------------------+
| **Namespace**    | | Pascal Case                                           |
|                  | | Try to partially match `Project`/`Assembly` name.      |
|                  |                                                          |
|                  | Example                                                  |
|                  |   ~ ```cs                                                |
|                  |     namespace Neces.Web {...}                            |
|                  |     ```                                                  |
+------------------+----------------------------------------------------------+
| **Class** or     | | Pascal Case                                            |
| **Struct**       | | Use a noun or noun phrase for class name.              |
|                  | | Add an appropriate class-suffix when subclassing       |
|                  |   another type when possible.                            |
|                  |                                                          |
|                  | Examples                                                  |
|                  |   ~ ```cs                                                |
|                  |     private class MyClass {...}                          |
|                  |                                                          |
|                  |     internal class SpecializedAttribute : Attribute {...} |
|                  |                                                          |
|                  |     public class CustomerCollection : CollectionBase {...} |
|                  |                                                          |
|                  |     public class CustomEventArgs : EventArgs {...}       |
|                  |                                                          |
|                  |     private struct ApplicationSettings {...}             |
|                  |     ```                                                  |
+------------------+----------------------------------------------------------+
| **Interface**    | | Pascal Case                                            |
|                  | | Alwayse prefix interface names with capital "`I`"      |
|                  |                                                          |
|                  | Example                                                  |
|                  |   ~ ```cs                                                |
|                  |     interface ICustomer {...}                            |
|                  |     ```                                                  |
+------------------+----------------------------------------------------------+
| **Generic Class** | Always use a single capital letter, such as `T` or `K`  |
| and **Generic    |                                                          |
| Parameter Type** | **Example**                                              |
|                  |  ~ ```cs                                                 |
|                  |    public class FifoStack<T>                             |
|                  |    {                                                     |
|                  |      public void Push(<T> obj) {...}                     |
|                  |      public <T> Pop() {...}                              |
|                  |    }                                                     |
|                  |    ```                                                   |
+------------------+----------------------------------------------------------+
| **Method**       | | Pascal Case                                            |
|                  | | Try to use a **Verb** or **Verb-Object** pair.         |
|                  |                                                          |
|                  | Example                                                  |
|                  |   ~ ```cs                                                |
|                  |     public void Execute() {...}                          |
|                  |     private String GetAssemblyVersion(Assembly target) {...} |
|                  |     ```                                                  |
+------------------+----------------------------------------------------------+
| **Property**     | | Pascal Case                                            |
|                  | | Property name should represent the entity it returns.  |
|                  |   Never prefix property names with `Get` or `Set`.       |
|                  |                                                          |
|                  | Example                                                  |
|                  |   ~ `public string Name { get; set; }`{.cs}              |
+------------------+----------------------------------------------------------+
| **Field**        | | Pascal Case                                            |
|                  | | Avoid using non-private Fields. Use Properties instead |
| (Public,         |                                                          |
| Protected, or    | Example                                                  |
| Internal)        |   ~ `public string Name`{.cs}                            |
|                  |   ~ `protected IList InnerList`{.cs}                     |
+------------------+----------------------------------------------------------+
| **Field**        | | Camel Case, prefixed by an underscore                  |
| (Private)        |                                                          |
|                  | Example                                                  |
|                  |   ~ `private string _fieldName;`{.cs}                    |
+------------------+----------------------------------------------------------+
| **Constant** or  | | Treat Like a field.                                    |
| **Static Field** | | Choose appropriate Field access-modifier above.        |
+------------------+----------------------------------------------------------+
| **Enum**         | | Pascal Case (both the Type and the Options)            |
|                  | | Add the `FlagsAttribute` to bit-mask multiple options. |
|                  |                                                          |
|                  | Example                                                  |
|                  |  ~ ```cs                                                   |
|                  |    public enum CustomerTypes                             |
|                  |    {                                                     |
|                  |      Consumer,                                           |
|                  |      Commercial                                          |
|                  |    }                                                     |
|                  |    ```                                                   |
+------------------+----------------------------------------------------------+
| **Delegate** or  | | Pascal Case                                            |
| **Event**        |                                                          |
|                  | Example                                                  |
|                  |   ~ `public event EventHandler LoadPlugin;`{.cs}         |
+------------------+----------------------------------------------------------+
| **Variable**     | | Camel Case                                             |
| (Inline)         | | Avoid using single characters like `x` or `y` except   |
|                  |   in `for`{.cs} loops.                                   |
|                  | | Avoid enumerating variable names like `text1`, `text2`, |
|                  |   `text3`, etc.                                          |
+------------------+----------------------------------------------------------+
| **Parameter**    | Camel Case                                               |
|                  |                                                          |
|                  | Example                                                  |
|                  |   ~ `public void Execute(string commandText, int iterations) {...}`{.cs} |
+------------------+----------------------------------------------------------+
