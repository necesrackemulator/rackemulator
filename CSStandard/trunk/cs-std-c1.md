# Introduction

This document describes rules for developing applications and class libraries
using the C# Language. The goal is to define guildines to enforce consistent
style and formatting and help developers avoid common pitfalls and mistakes,
as well as promote maintanability and clarity in code.

## Scope

This document only applies to the C# language and the .NET framework (or the
cross-platform implementation, Mono). The .NET framwork will be discussed in a
more limited fasion than the style of writing code in C#.

Standards for curly braces  (`{` or `}`) and white space (tabs vs spaces) may
be controversial, but these topics are addressed here to ensure greater
consistency and maintainability of source code.

## Document Conventions

Much like the ensuing coding standards, this document requires standards in
order to ensure clarity when stating the rules and guidelines. Certain
conventions are used throughout this document to add clarity.

Below are some of the common conventions used throughout this document.

### Keywords

Always
  ~ Emphasizes this rule must be enforced.

Never
  ~ Emphasizes this action must not happen.

Do Not
  ~ Emphasizes this action must not happen.

Avoid
  ~ Emphasizes that the action should be prevented, but some exceptions may
    exist.

Try
  ~ Emphasizes that the rule shoulf be attempted whenever possible and
    appropriate

Example
  ~ Precedes text used to illustrate a rule or reccomendation.

Reason
  ~ Explains the thoughts and purpose behind a rule or reccomendation.

## Terminology and Actions

The following terminology is referenced throughout this document.

Access Modifier
  ~ C# keywords `public`, `protected`, `internal`, and `private` declare the
    allowed code-accessability of types and their members. Although default
    access modifiers vary, classes and most other members use the default of
    `private`. Notable exceptions are interfaces and enums, whch both default
    to public.

Camel Case
  ~ A word with the first letter lowercase, and the first letter of each
    subsequent word-part capitalized. **Example:** *customerName*

Common Type System
  ~ The .NET Framework common type system (CTS) defines how types are declared,
    used, and managed. All native C# types are based upon the CTS to ensure
    support for cross-language integration.

Identifier
  ~ A developer defined token used to uniquely name a declared object or object
    instance. **Example:** `public class` *`MyClassNameIdentifier`* `{ ... }`

Magic Number
  ~ Any numeric literal used within an expression (or to initialize a variable)
    that does not have an obvious or well-known meaning. This usually excludes
    the integers $0$ or $1$ and any other numeric equivalent precision that
    evaluates as zero.
Pascal Case
  ~ A word with the first letter capitalized and the first letter of each
    subsequent word-part capitalized. **Example:** *CustomerName*

Premature Generalization
  ~ As it applies to object model design; this is the act of creating
    abstractions within an object model not based on croncrete requirements or
    a known future need for the abstraction. In simplest terms: "Abstraction
    for the sake of abstraction."
